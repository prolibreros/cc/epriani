# Libros de Ernesto Priani

1. _Los libros del placer_
2. _Magia y mimetismo_
3. _De espíritus y fantasmas_

## Archivos

En la carpeta `out` están todos los formatos de cada obra.
