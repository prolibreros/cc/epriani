#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Dir.chdir(__dir__)

Dir.glob('src/*') do |d|
  file_prefix = d.split('/').last

  if File.directory?(d + '/epub-automata')
    Dir.glob(d + '/epub-automata/*') do |f|
      if File.extname(f) == '.epub' || File.extname(f) == '.mobi'
        file_name = File.basename(f)
                        .gsub(/\w{4}-\w+?(\.|_)/, file_prefix + '\1')
        FileUtils.mv(f, 'out/' + file_name)
      end
    end
  end
end
