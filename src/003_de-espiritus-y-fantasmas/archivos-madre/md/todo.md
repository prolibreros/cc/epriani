# Dedicatoria @ignore

Para Adriana {.espacio-arriba3 .epigrafe}

# Presentación

++Hace mucho tiempo++ que la filosofía olvidó ---como si se tratase
de un viejo arte ya superado gracias a las argucias de la modernidad---
el uso de una de las facultades que el viejo Aristóteles había
encontrado en el alma humana. En la parte concupiscible o sensible
de esa alma, el Estagirita halló la fantasía. Siglos más tarde,
otros filósofos como Marsilio Ficino y Giordano Bruno, entre
otros, habrían de reencontrarse con esta facultad única. Mientras
tanto, la fantasía ya había sido traducida al latín, convirtiéndose,
en lo que a vocablos respecta, en la imaginación; había sido
a veces confundida con esa nueva imaginatio; y, finalmente, había
sido desvinculada del nuevo término, engendrado por ella misma.
El periplo de la fantasía de la Antigúedad clásica al Renacimiento
es, a su vez, la carta geográfica de los ensayos que contiene
este volumen.

Pero poco o nada nuevo nos diría un recuento de las etapas evolutivas
de la fantasía. Por ello, los ensayos que siguen no son sólo
una historia de esta facultad humana, sino, más allá de esto,
la recuperación de un pensamiento de frontera, el viaje hacia
una encrucijada entre los senderos abiertos por el Renacimiento
y los caminos vetados por la modernidad, la inmersión en la delgada
línea que, en gran parte del pensamiento occidental, divide el
cuerpo del alma, el valor de la virtud, la belleza y el amor
del bien. En esta encrucijada se encuentran, como narran las
antiguas leyendas, los espíritus y fantasmas de aquellos olvidados
por los vivos. Este libro es, entonces, un recordatorio para
el pensamiento filosófico actual, una invitación a rememorar
aquello que, como filósofos, hemos olvidado.

Cada ensayo de este volumen es autónomo, aborda un solo tema
y lo hace desde una sola perspectiva. Y, sin embargo, cada ensayo
se sitúa, a su manera, en esa línea fronteriza que Ficino redujo
---retomando el pensamiento de Lucrecio--- a una frase elíptica
y magistral: «casi cuerpo, casi alma». Pues, para el filósofo
florentino, como para muchos de sus contemporáneos, la fantasía
y la imaginación no serían posibles sin el intermedio de unos
mensajeros peculiares: los _spiriti_, que son casi alma, casi
cuerpo. El espíritu, ese conjunto de pequeños corpúsculos que
no son del todo materiales ni del todo etéreos, conforman, para
el Renacimiento, la parte sensible del alma que media entre lo
físico y lo anímico, entre el puro pensamiento y la funciones
fisiológicas más básicas. En efecto, el espíritu ordena e interviene
en cada idea, en cada acción, en cada pasión humana porque él
es guardián de las puertas entre lo sensible y lo inteligible,
porque él se encarga de llevar, como por magia, el cuerpo al
pensamiento y el alma a la carne.

¿Cómo dejar, entonces, en el olvido al espíritu y a la fantasía
si en ellos se condensa la ambigiedad y, al propio tiempo, la
mayor virtud de la naturaleza humana? La magia y la teoría de
la sensibilidad del Renacimiento son recordadas aquí, traidas
al presente en este volumen con una intención clara: abrir nuevas
vías para el pensamiento filosófico ---particularmente para la
ética--- libres de las ataduras contemporáneas. Y no hay nada
mejor que buscar en un pasado tan exuberante como el Renacimiento
---ahí donde confluyen Marsilio Ficino y Francois Rabelais, Giordano
Bruno y Hermes Trismegisto, Raimundo Llull y Dionisio Acropagita---
para sacudirse un presente que, en ciertos aspectos, parece corto
de vista.

Así, los ensayos que se ofrecen a la lectura quieren traer al
presente la olvidada facultad de la fantasía y a sus mensajeros,
los espíritus. Con ellos y en ellos, el lector sabrá acerca de
la influencia de los astros en la vida contemplativa de los hombres,
acerca del mal de ojo y el enamoramiento, de la belleza como
virtud, del ejercicio creativo de la sexualidad y hasta de una
afortunada digestión mediada por la adecuada administración del
espíritu. De la idea al proceso orgánico o viceversa, los textos
aquí reunidos conforman una breve y sugerente narración de las
aventuras de la fantasía y, sobre todo, un puntilloso cuestionamiento
de la manera en que concebimos, hoy, la naturaleza y la acción
humanas, tan alejadas ya de la fantasia, del cuerpo, del espíritu
y la magia.

Los editores {.espacio-arriba1 .derecha}

# Preámbulo. Una inquietud y una alternativa

Nada deja al azar aquel que posee todas las
cosas y quiere todo lo que posee. \
++Corpus Hermeticum++ {.epigrafe}

++El Imperio romano++ fue siempre un cruce de caminos, un inagotable
circular de personas de las más distintas razas y religiones
en una Babel de límites inmensos. Es ahí donde quiero comenzar
la historia de una inquietud y una alternativa, pues es en ese
espacio y en ese tiempo donde aparece una forma de interrogarse
que, a la postre, será la base a partir de la cual ciertos hombres
buscarán respuestas dentro de ellos mismos, entendiéndose como
cópula entre dos realidades distintas: la que constituye el mundo
físico y material, y la que conforma ese otro plano de la existencia
humana que es lo inteligible, aquello que sólo el entendimiento
aprehende.

Quiero seguir desde el origen el camino que esos hombres trazaron
entonces, pues es justo con ellos como podremos arribar finalmente
a la idea renacentista de espíritu, una realidad intermedia entre
lo físico y lo inteligible, en la que el Renacimiento colocó
la capacidad de sentir, imaginar, recordar y fantasear. Me interesa
hacer este recorrido porque sobre la noción de espíritu aquellos
hombres construyeron una ética que encuentra sus raíces en la
condición anfibia del hombre y que se aleja, por ello mismo,
tanto de la mera descripción psicológica de las costumbres como
de los afanes sistemáticos que buscan fundarla sólo en el plano
de lo inteligible.

Pero antes de llegar a esa idea, que recorre todo este libro,
brotando aquí y allá, es necesario caminar despacio, remontarnos
varios siglos y partir, si no de la fuente misma, sí del punto
en que los caminos concurren en el Imperio romano para dar nacimiento
a tres cuestiones clave sin las cuales el espíritu sería poca
cosa: la concepción anfibia de la naturaleza humana, entre la
inteligencia y el cuerpo; el hallazgo de la interioridad del
hombre como cópula de dos realidades a las que pertenece simultáneamente;
y la capacidad de autotransformación del hombre, producto de
su inquietud interna y, al mismo tiempo puerta hacia el bien.

Para rastrear estas tres cuestiones, tomaré como punto de partida
lo que sostienen algunos historiadores@note en el sentido de
que al término de la expansión del Imperio romano se produce
un cambio significativo en la concepción que el individuo tiene
de sí mismo. El hombre, antes parte de una empresa común con
otros hombres ---la del Imperio--- se encuentra de pronto solo
ante su destino, pues la paz de Roma no significó quietud para
el alma sino, por el contrario, una fuerte y violenta agitación.
Algunas de las peticiones que se conservan de los oráculos así
lo demuestran. Se trata de interrogantes que todavía hoy se hacen
en la penumbra de un tarot o en la afable conversación con una
amigo: ¿renunciaré?, ¿seré senador?, ¿me beneficiaré esta temporada?,
¿me abandonará mi mujer?

La filosofía y la religión son, en ese momento, los instrumentos
para buscar sosiego en el alma, tanto como lo son hoy el ejercicio,
los calmantes o una sesión semanal con el psicoanalista. A partir
del año 100, como señala el historiador Paul Veyne, los dioses
adquirieron la función «de gobernar, aconsejar y proteger a sus
fieles, a fin de _sustraerlos a la ciega fortuna o a la fatalidad_»:@note
a su vez, «lo que la filosofía se proponía entonces era proponer
a los individuos un _método de dicha_».@note Una y otra forma
de sabiduría atendían a una misma necesidad ineludible para el
hombre: la de ofrecer sosiego a la inquietud del alma.

En el ámbito de la reflexión filosófica, no siempre distinguible
por completo de la religiosidad en aquellos días, son dos los
caminos que se ofrecen para hacer frente a la incertidumbre
que caracteriza la vida de los hombres. Por un lado, la resignación
ante el hecho de que la vida es como se muestra y que el camino
del sosiego no es otro que adaptarse y vivir de acuerdo con aquellas
fuerzas de la naturaleza que rigen el acontecer de las cosas.
Por el otro, la rebeldía, caracterizada por la certeza de que
el hombre es capaz de transformar su propia naturaleza para erradicar,
de una vez y para siempre, aquello que produce la zozobra.

En el siglo +++II+++, estoicos y epicúreos son, paradójicamente,
quienes construyen doctrinas que, aunque diferentes, están encaminadas
a aceptar los hechos del mundo. Por eso sugiere Veyne@note que
estoicismo y epicureísmo se articulan como una terapia en la
que la moral se conforma como un método para el sosiego del alma
y la erradicación del dolor y el miedo a la muerte.

La rebeldía la encontramos, en cambio, entre las posturas filosóficas
neoplatónicas: gnosticismo, hermetismo y cristianismo.@note
Con un mismo punto de partida y una misma intención, aunque con
profundas diferencias en cuanto al significado del mundo y de
la empresa humana, todas ellas se proponen modificar de raíz
la naturaleza del hombre, de tal manera que, puesto el destino
en manos de la voluntad humana, se sustraiga de los azares de
la fortuna que causan desasosiego.

※ {.espacio-arriba1 .centrado}

Serán los gnósticos quienes de manera más radical pero, a la
vez, más transparente, muestren cómo se conforma la nueva imagen
que el individuo tiene de sí mismo, y de qué manera ésta conduce
a interrogarse de otra forma y a encontrar un nuevo camino hacia
ciertas respuestas. {.espacio-arriba1 .sin-sangria}

El problema del hombre comienza, para los gnósticos, con el hecho
de que el mundo le es hostil. H.C. Puech afirma que, «decepcionado
o herido por su actual condición en el seno del mundo, de una
sociedad, de un cuerpo en los que no experimenta sino malestar
[…] el gnóstico comienza por reaccionar frente a él y contra
él [mundo] […] Rechaza su condición y se niega a aceptarla».@note

Sin embargo, esta visión de una naturaleza, un mundo y una sociedad
que amenazan al hombre va aparejada con el descubrimiento de
una realidad interior que se muestra no sólo distinta, sino incluso
literalmente contraria al mundo. Esto que Puech llama «el proceso
de extrañamiento» no es otra cosa que un proceso en el que el
hombre se reconoce como ajeno a todo aquello que le causa desdicha
---lo corpóreo, la temporalidad, incluso el alma que ha sido
engendrada por los astros--- y se rebela contra esa condición
a través de una introspección que lo guía hacia el encuentro
de la realidad interior, contramundana, que el gnóstico identifica
como la única verdadera. Dice Puech:

> Al sentirse extranjero a un mundo que ha acabado por concebir como
> radicalmente extraño, tiende a distinguirse […] De aquí la necesidad
> de evadirse, de salir […] El sentimiento progresivamente elaborado
> que experimenta el gnóstico de ser otro que lo que lo rodea, lo lleva a
> persuadirse de que […] no es de este mundo […] A partir de su
> situación presente, el gnóstico llega a concebirse ya desde aquí como
> más allá, a representarse y a definirse con independencia de las
> contingencias […]@note

De esta manera, el gnóstico no sólo rompe con el pensamiento
griego y estoico, para los cuales el mundo es un cosmos, un orden
que la vida humana debe imitar e interiorizar, sino que enfatiza
el absoluto antagonismo entre ese cosmos y la realidad divina
que se manifiesta en el interior del propio hombre. Como apunta
H. Jonas: «La divinidad del orden cósmico deviene lo opuesto
a lo divino casi por exageración. El orden y la ley son aquí
también cosmos, pero un orden rígido y hostil, una ley tiránica
y maligna, desprovistos de significado y de bondad, extraños
a los propósitos del hombre y a su esencia interna, sin sentido
para su comunicación y afirmación».@note

A partir de esta nueva experiencia, el gnosticismo da lugar a
una concepción de la inquietud humana como producto de la participación
del hombre en dos realidades completamente distintas e irreconciliables
que lo constituyen como un ser anfibio, capaz de sobrevivir tanto
en una como en otra, aunque aquéllas no puedan nunca armonizarse
entre sí. La cosmogonía gnóstica subraya precisamente este carácter
dual del hombre.

Según dicha cosmogonía, el gnóstico aspira, mediante la introspección,
al encuentro con el «hombre originario» que, por algún motivo
(en este caso hay una enorme cantidad de explicaciones), se ve
impelido a engendrar una raza caída, mortal, que participa de
él, pero que _se halla cautiva en el mundo_. Se trata del único
ser humano en sentido pleno, ya que ha sido engendrado por la
inteligencia primera, y es por ello intemporal pero, ante todo,
inocente. Es un hombre concebido fuera del tiempo, concebido
también, _más allá del bien y del mal_, más allá incluso de la
elección.

En efecto, los gnósticos parecen identificar al ser con la ausencia
de elección entre bien y mal. En consecuencia, es en el mundo,
que sólo es ser en segundo grado, donde existe la necesidad de
elegir, la cual condena a los mortales a la incertidumbre.

La diferencia ontológica entre ambas naturalezas ---la terrenal
y la divina--- se hace extensiva a una diferencia de valor: una
es plenamente buena, la otra es mala porque encierra la posibilidad
de elegir. Esto es lo que marca la independencia de ambas naturalezas,
que permanecen sin contacto posible entre sí desde el momento
en que se registra la caída del hombre originario.

Cuando este hombre único y primordial cae, entra en la esfera
del tiempo y se fragmenta. Recibe entonces el _pathos_ de la
elección. Su deseo ya no es idéntico a su ser y de esta diferencia
surgen el dolor y la penuria. Pero la caída representa, al mismo
tiempo, un puente entre la trascendencia y el mundo, un puente
del que la raza humana es manifestación permanente, no sólo por
su presencia en el mundo, sino también por la posibilidad de
volver a su esencia originaria:

> Si aquellos que os guían os dicen: mirad, el reino está en el cielo,
> entonces los pájaros llegarán allí antes que vosotros. Si os dicen: está en
> el mar, entonces […] los peces llegarán antes que vosotros […] Más
> bien, el reino está dentro de vosotros y está fuera de vosotros. _Cuando
> lleguéis a conoceros a vosotros mismos, entonces seréis conocidos_ y os
> daréis cuenta de que sois los hijos del padre que vive.@note

La condición humana es, entonces, condición de ruptura y unión
entre una y otra realidad. El hombre está sumergido en la elección,
pero mira, desde dentro de sí, aquella realidad en que las cosas
tan sólo son. De ahí la inquietud que surge por la existencia
de dos realidades irreconciliables dentro de él mismo, pero de
ahí, también, el sendero que lleva hacia las nuevas respuestas
del gnosticismo, y que no es otro que el indagar dentro de sí.

El camino interior que el gnóstico encuentra es el autoconocimiento.
Producto, en primera instancia, del proceso de extrañamiento,
el conocimiento de sí es una meta que se busca pero también un
don que se recibe. Ante todo porque implica la irrupción de una
naturaleza extraña al mundo, naturaleza que se manifiesta por
sí misma y que de inmediato salva y transforma. Como afirma Puech,
«El conocimiento ---que en primer lugar es conocimiento de sí---
no se limita sólo a ilustrarle [al gnóstico] sobre la ruta a
seguir, junto con él le otorga la salvación; le salva por sí
mismo, por el hecho de manifestársele y de que el gnóstico lo
posee […] la obtención de la salvación se reduce por ello a una
pura operación interior».@note

A través del encuentro con su propia interioridad, que significa
un proceso de negación de la naturaleza terrena del hombre y
el «reconocimiento», por parte de él mismo y de la divinidad,
de que él ya es otro distinto de aquel hombre terreno, el gnóstico
alcanza su salvación, es decir, la respuesta a la incertidumbre
que su condición anfibia le produce.

Pero, si bien la salvación es un proceso enteramente interior
que «aliena» al hombre de su naturaleza terrena, ésta comporta
una capacidad de dominio frente ai mundo. Sí, la gnosis, otorga
capacidad mágica:

> _No sólo vosotros, mas todo hombre_ que cumpla con el primer misterio
> inefable, podrá recorrer todas las regiones y todas sus estaciones. Y
> cuando haya cumplido ese misterio y recorrido todas las regiones, será
> preservado de todas las cosas que le hayan reservado los archones de la
> _Helmarmené_ […] para curarse de la posesión de los demonios y de
> toda aflicción y enfermedad. Y para curar a los cojos, y a los mutilados,
> y a los mudos, y a los paralíticos […] Y vosotros obtendréis la pobreza y
> la opulencia, la salud y la enfermedad, la debilidad o el vigor, si lo
> pedis […] Porque a quien haya ejercido el primer misterio, _todas las
> cosas le serán concedidas_.@note

Quien ha alcanzado la gnosis obtiene, pues, su resurrección y
la absoluta libertad para actuar en el mundo sin atender a sus
determinaciones. La salvación es, así, inmediata: una radical
y definitiva transformación ontológica que es a un tiempo libertad
y poder, que se manifiesta como superior a la naturaleza terrenal
en todos los aspectos, tanto interiores como exteriores al hombre.

Dos son, en principio, los aspectos clave de la reflexión gnóstica
en los que me interesa detenerme. El primero, aquello que provoca
la incertidumbre humana, y el segundo, la concepción de la libertad.
En ambos encontramos algunos de los rasgos que comparte esta
propuesta con el hermetismo y el cristianismo, pero también la
diferencia que los distancia de manera definitiva.

La composición dual del hombre, comprendido como entendimiento
y cuerpo a la vez, no es, por supuesto, original del pensamiento
gnóstico. Sin embargo, la coexistencia armónica que esas dos
instancias tienen, por ejemplo, en Platón y Aristóteles, es negada
por los gnósticos, para los cuales la dualidad es radical: entendimiento
y cuerpo, aún dentro del hombre, son irreconciliables y antagónicos
desde el momento en que son modos de existir completamente excluyentes.
Uno, pues, niega siempre al otro.

La inquietud y la incertidumbre del hombre pertenecen, según
los gnósticos, a uno de los modos en que existe: el corporal
y terreno. Esto es hasta tal punto cierto, que se llega a señalar
directamente a este modo de existencia como causa de la confusión
del alma humana. Este tema es abordado en un magnífico relato
gnóstico conocido como _La perla_, que Jonas comenta de la siguiente
manera:

> Pero el dualismo gnóstico va más allá de esta posición desapasionada, ya
> que considera al «alma» misma […] en la misma medida que su cuerpo,
> emanación de los poderes cósmicos y, por tanto, instrumento del
> dominio de éstos sobre su verdadero yo sumergido. La psicología
> gnóstica, por tanto, está impregnada de profunda desconfianza de la
> propia interioridad, de la sospecha de trucos demonáíacos, del temor de
> ser traicionada y apresada. Las fuerzas alienadoras se encuentran en el
> hombre mismo, y están compuestas de carne, alma y espíritu. El
> desprecio del cosmos, entendido de forma radical, incluye el desprecio
> de la psique.@note

La hostilidad del mundo, que se manifiesta en el hombre como
inquietud e incertidumbre, avanza por las venas y los nervios
hasta intentar hacerse del alma y cerrar ahí la única puerta
para salir de la penuria. De ahí su carácter interior; de ahí
que sea desde la interioridad como el hombre, según los gnósticos,
sostendrá la lucha entre el bien y el mal, no como un esfuerzo
por darle forma al cuerpo disciplinándolo, al modo de Platón
y Aristóteles, sino una transformación de su modo de ser, que
implica la negación de su existencia terrena.

Salvado, el hombre se hace libre. Y aquí la libertad es concebida
como lo propio del otro modo de ser: la trascendencia a través
de la introspección. Se trata, pues, de una libertad de carácter
ontológico, que no puede ser anulada por ninguna determinación
y a la que tampoco se puede renunciar ni se desea. En un primer
sentido, esa libertad es poder sobre todo aquello que se halla
determinado. En un segundo sentido, es tranquilidad, ausencia
total de inquietud e incertidumbre, plenitud de ser.

Como hemos visto, en los gnósticos encontramos lo que podría
ser una de las primeras concepciones de la interioridad humana,
que aparece como el terreno de lucha y enfrentamiento entre los
dos modos de ser que constituyen la naturaleza humana. Ambos
modos de ser buscan dominar ese terreno: el cuerpo invadiéndolo
con la determinación; el _nous_ o entendimiento liberándolo de
toda determinación. El dilema ético del gnóstico se juega así
entre esas dos realidades, o bien padeciendo la invasión de la
determinación del cuerpo, o bien transformándose en una libertad
irrestricta.

※ {.espacio-arriba1 .centrado}

Pero en la historia que me he propuesto contar hay todavía otras
fuentes que acabarán por dar forma a la inquietud y la alternativa
que muchos siglos después representará el espíritu.
{.espacio-arriba1 .sin-sangria}

Tal es el caso de la doctrina contenida en los textos del Corpus
hermeticum. Dicha doctrina que, a partir de su asimilación al
neoplatonismo, da lugar a la tradición mágico-hermética que llegará
hasta el Renacimiento, es resultado del diálogo entre la tradición
religiosa egipcia@note y el gnosticismo.

Formado por el _Poimandro_ ---compuesto de siete diálogos---,
el _Asclepio_ y los _Fragmentos de Estobeo_, el _Corpus hermeticum_
no posee ninguna unidad temática o doctrinaria. En él, al lado
del más puro hermetismo, coexisten posiciones del gnosticismo
más radical. Pero esto, que constituye la mayor dificultad para
su interpretación, es también su mayor virtud, ya que puede leerse
como una larga y compleja discusión sobre la inquietud humana
que aflora con el fin del Imperio romano.@note Esto explica el
carácter dialógico y la riqueza que los textos herméticos brindan
al tema que nos ocupa.

Un factor clave distingue al hermetismo del gnosticismo: el valor
conferido al acto creativo como condición propia de la inteligencia.
En efecto, a diferencia de la cosmología gnóstica ---que supone
una inteligencia que sólo procrea en la inteligencia, y un hombre
mortal producto de la caída de una esfera del ser a otra--- el
hermetismo parte de la unidad del acto de procreación. Dice el
_Poimandro_: «El _Nous_, siendo macho-hembra […] procreó con
el Logos un _Nous_ demiurgo que […] hizo siete gobernadores que
envuelven en sus círculos al mundo sensible y este gobierno suyo
[el del _Nous_ demiurgo] es lo que se llama destino».@note

Me explico: el _nous_, que en el hermetismo es macho-hembra y
cuya dualidad supone la reunión de los dos elementos propios
de la generación ---lo masculino y lo femenino---, produce seres
igualmente generadores. Así, el acto de creación pasa del _nous_
al _nous_ demiurgo, y de éste a los planetas ---los siete gobernadores---,
que constituyen la inexorable fuerza del destino.

Estamos ante una unidad ontológica de corte neoplatónico donde
los seres celestes, las formas organizativas de la materia y
la materia misma, son emanaciones del _nous_. En el siguiente
pasaje del _Corpus hermeticum_ se señala la unidad total del
cosmos por la fuerza del eterno proceso de generación: «Gloria
de todas las cosas son dios (Θεος) y lo divino, y la naturaleza
es divina. Dios es principio de todos los seres y _Nous_ y naturaleza
y materia. Dios es principio y naturaleza y energía, y necesidad
y fin y renovación».@note

Como podemos apreciar, la unidad del ser es absoluta: todos los
elementos de la creación son afluentes del único ser que se renueva
así mismo, que se fecunda, que se re-crea a cada instante. De
ahí su eternidad y, también, su capacidad de gobierno en tanto
providencia, en tanto cuidado de cada una de las partes del todo.
Por ello dirá Giovanni Pico della Mirandola en sus _Conclusiones
según la primitiva doctrina del egipcio Hermes_: «Nada es en
el universo susceptible de muerte o de corrupción. Corolario:
en todas partes vida, en todas providencia, en todas inmortalidad».@note
La eternidad y el orden del cosmos son, así, resultado de la
generación, en la medida en que ésta, como renacer perenne, garantiza
la eternidad del ser y, al mismo tiempo, es la ley que gobierna
cada una de las partes de la totalidad, el movimiento que supone
morir y renacer: «El ser existira siempre, de donde se sigue
que los cuerpos existirán siempre también. Por ello declaro que
la generación de los cuerpos es una operación eterna».@note

Así, aquello que para los gnósticos es condenable por ser causa
del dolor y de la muerte ---el tiempo, la materia, el cuerpo,
el mundo---, para el hermetismo es el recurso del que se vale
el ser para construir su eternidad, al grado que la muerte, como
tal, no existe: «La muerte ---dice _Poimandro_, VIII--- no tiene
nada que ver con ninguna de estas cosas [cuerpo y alma] porque
no es más que un concepto forjado sobre la palabra inmortal,
sea pura ficción, sea la supresión de la primera letra, la palabra
_Thanatos_ en lugar de _Athanatos_».@note

La conclusión es obvia: en términos de género, la humanidad es
inmortal en tanto que está sujeta al proceso de generación. Pero
saber esto no basta para el consuelo personal. La inmortalidad
del género humano no libra al hombre individual de la incertidumbre
personal respecto de su destino. Y, sin embargo, una respuesta
a esa incertidumbre parece esbozarse en los textos herméticos:
el hombre es poseedor del acto creativo.

Al referirse al origen del hombre, el _Poimandro_, I, nos muestra
que, único entre todos los seres, el ser humano tiene acceso
a la creación. Dice el texto que, paralelamente a la generación
de las esferas, «el _Nous_ padre produjo un hombre y […] verdaderamente
dios se enamoró de su propia forma y le entregó todas las formas».@note
Estas formas que le son entregadas al hombre, y con las cuales
le es dada la capacidad de generar, lo hermanan con el demiurgo
que ha construido el cosmos todo. Por ello, al mirar la obra
de su hermano y con el poder que le ha sido concedido, el hombre
tiene un deseo: producir una obra. Pero ¿no es acaso este deseo,
el de hacerse similar a su creador, el más condenable de los
deseos en una criatura: desear ser también creador? Lejos de
ser condenado o expulsado, lejos de aparecer como un ser caído,
dentro de la doctrina hermética, el hombre tiene por intercesión
del _nous_ el derecho a crea.

Con las formas, los poderes y el beneplácito del _nous_, el hombre
---al que llamaremos, como lo hace el hermetismo, Hombre Original---
penetra en la esfera demiúrgica donde los siete gobcrnadores
---los astros--- se enamoran de él a su paso y le otorgan, cada
uno, participación en su propia magistratura:

> Entonces el hombre, que tenía poderes plenos sobre el mundo de los
> seres mortales y de los animales sin razón, se descolgó a través de la
> armoniosa maquinaria compuesta de las esferas cuyas envolturas había
> agujereado y manifestó la hermosa forma de Dios a la naturaleza de
> abajo. Cuando ésta hubo visto que el hombre poseía la forma de Dios
> junto con la belleza inagotable y toda la energía de los Gobernadores,
> _sonrió de amor_, pues había visto reflejarse en el agua el semblante de
> esa forma maravillosamente hermosa del hombre, y a su sombra en la
> tierra. En tanto que él, habiendo visto reverberar en el agua la
> presencia de esa forma parecida a la suya, la amó y quiso morar en ella.
> Desde el mismo momento en que lo quiso lo cumplió. La naturaleza
> entonces, recibiendo en ella a su amado, _lo abrazó entero, y ambos se
> unieron ardiendo de amor_.@note

Está claro. Para el hermetismo no hay caida, sino el deseo humano
de generar, de activar los poderes de la inteligencia, de obrar.
Pero el hombre no puede hacer esto solo; necesita del devenir
y de su perpetuo morir y renovarse, pues sólo ahí es posible
la generación. El _Poimandro_ lo señala de esta manera: la causa
de la muerte es Eros.

Es por lo anterior que el hombre elige abrazarse con la obra
del demiurgo y fecundarla, porque sólo con ella puede generar.
De ahí que el producto de sus amores no sea una criatura común,
sino más perfecta:

> Único entre todos los seres que viven sobre la tierra, el hombre es
> doble: mortal por el cuerpo, inmortal por el hombre esencial. Aunque
> en efecto sea inmortal y tenga imperio sobre toda cosa, padece la
> condición de los mortales, sujeto como está al destino. Por ello,
> aunque esté encima de la armoniosa maquinaria compuesta de las
> esferas, se ha vuelto esclavo de ellas.@note

Esclavo y señor, el hombre es el único mortal que puede vivir
una existencia sujeta al destino, pero también independiente
de aquél.

En el fondo, el problema del hermetismo es el mismo que el del
gnóstico: cómo librarse de la determinación del destino. Sin
embargo, el planteamiento es distinto. Para los herméticos, la
forma de alcanzar la libertad no es librando una batalla entre
dos realidades opuestas. Por el contrario, para el hermetismo,
el camino a la libertad consiste en la afirmación de la existencia
de ambas realidades y de los poderes y capacidades que cada una
de ellas encierra. Así, el hombre debe ser destino y destinado,
esclavo y señor. Si bien con la gnosis se recoge la idea de que
el hombre debe recuperarse a sí mismo, la recuperación no coincide
en la doctrina hermética con la alienación que propone la propia
gnosis. Se trata, ahora, de convertir lo que para los gnósticos
era el _pathos_ de la elección en un _ethos_; trasmutar lo que
era sólo condena en el motor mismo de la existencia.

Lo importante aquí es el reconocimiento de la condición dual
del hombre no como motor de un conflicto, sino como punto de
partida de una transformación. Esta transformación comienza cuando
el hermético se sacude cierta ignorancia acerca de sí mismo que
le impide verse como poseedor de la mayor riqueza del mundo:
el _nous_ y el cambio. Esto lo vemos más claramente a partir
de lo que afirma Lynn Thorndike al referirse al origen de la
magia egipcia, en _A History of Magic and Experimental Science_:
«Their [Egyptian] mythology was affected by it [magic] and they
not only combated demons with magical formulae but believed that
they could terrify and coerce the very gods by the same method,
compelling them to appear, to violate the course of nature by
miracles, or to _admit the human soul to an equality with themselves_».@note

En la forma en que Thorndike presenta la magia egipcia, es claro
que el reconocimiento del alma humana como igual a la divina
debe ser entendido en su sentido literal. Aquí los términos de
la relación están invertidos respecto a la gnosis: no es la pertenencia
a la divinidad ---la realización de la salvación--- la que dota
de poderes mágicos; al contrario, es la ejecución de la magia
lo que hace al hombre similar al _nous_ y lo hace, precisamente,
afirmando su condición intermedia, mortal y divina, animal y
humana. Como se lee en el _Corpus hermeticum_:

> Por ello, Asclepio, _es tan gran maravilla el hombre_ […] Pues pasa a la
> naturaleza de un dios como si él mismo fuera dios; tiene trato con el
> género de los demonios, sabiendo que ha surgido del mismo origen
> […] ¡Oh, de qué mezcla privilegiada está hecha la naturaleza del
> hombre! Está unido a los dioses por lo que tiene de divino, la parte de
> su ser que lo hace terrestre, lo desprecia en sí mismo; todos los otros
> vivientes a los cuales se sabe unidos en virtud del plan celeste, se los
> atrae por el amor […] _Tal es la posición de su privilegiado papel
> intermedio_ que ama a los seres que le son inferiores, y es amado por
> aquellos que le son superiores. Cuida la tierra, se mezcla con los
> elementos por la celeridad del pensamiento, por la agudeza del espíritu
> se hunde en los abismos del mismo. _Todo le está permitido_: el cielo no
> le parece demasiado alto, pues gracias a su ingenio lo considera muy
> cercano. La mirada de su espíritu lo dirige, niebla ninguna del aire lo
> ofusca: la tierra jamás es tan compacta que impida su trabajo; la
> inmensidad de las profundidades marinas no perturba su vista que se
> sumerge. _Es todas las cosas a la vez, a la vez está en todas partes_ (_Omnia
> idem est et ubique idem est_).@note

Es de esta forma como el hermetismo descubre que nada le es ajeno
al alma humana que asume su condición original. De ahií el desprecio
por la naturaleza «puramente humana» que no permite al hombre
afirmar su naturaleza doble y ejercer su papel de ser intermedio
y privilegiado.

El ser del hombre se asume, entonces, como el ser sin más, en
su indeterminación y en sus infinitas posibilidades, en su necesidad
y en su maravillosa arbitrariedad y capricho. La transformación
hermética es un proceso de autoafirmación en el devenir y más
allá de él, la asunción de la autonomía que da al ser aquello
que puede ser. Entonces, todo, todo le está permitido al hombre
que asume su condición original:

> […] habiendo puesto en tu pensamiento que nada hay imposible para
> ti, estímate inmortal y capaz de comprenderlo todo, todo arte, toda
> ciencia, el carácter de todo ser viviente. Asciende más alto que toda
> altura, desciende más abajo de toda profundidad. Reúne en ti las
> sensaciones de todo lo creado, del fuego, del agua; estando en todas
> partes, en la tierra, en el mar, en el cielo, que no has nacido todavía,
> que estás en el vientre materno, que eres adolescente, viejo, que estás
> muerto, que estás más allá de la muerte. Si abrazas con el pensamiento
> todas estas cosas a la vez, tiempo, lugares, sustancias, cualidades y
> cantidades, puedes comprender a Dios.@note

Así, en virtud de su condición intermedia, el hombre puede, según
el hermetismo, comprender «hacia abajo» todo arte y toda ciencia
para actuar en el mundo: de la misma forma que entender «hacia
arriba», por la vía de la contemplación, lo divino, en un movimiento
que es descendente y ascendente.

Por ello, tanto el actuar como el percibir la divinidad no son
principio ---como en la gnosis--- sino final. La aspiración religiosa
del hermetismo, su proceso de salvación, se da en términos inversos
a los del gnosticismo: es a partir del vínculo con el mundo,
y no de su negación, a partir de la contemplación del _orden_
del cosmos, y de la capacidad del hombre de generar en y para
ese orden, como el hermetista puede salvarse. Los actos humanos
cobran así una dimensión máística: el hombre se confunde con
la divinidad misma, hasta hacerse él mismo causa entre las causas,
a saber, una teurgia: ««[…] y produjeron la generación de hombres
para conocer las obras divinas y para ser testigos activos, para
acrecentar el número de los hombres, para señorear sobre todo
lo que existe bajo el cielo y reconocer las cosas buenas […]
para conocer la potencia divina […] y para _descubrir rodo el
arte de fabricar cosas buenas_».@note

El acto o el proceso de creación que se propone el hermetismo
es, simultáneamente, el camino de la transformación misma. En
la base del discurso mágico no encontraremos otra cosa que una
cierta manera de asumir la acción humana, trascendiendo el _pathos_
de la elección para trocarlo en _ethos_. Con esta asunción, el
hombre deja de ser un enfermo de su mortalidad, de su falta de
totalidad, de su necesidad, de su deseo, de su voluntad sedienta;
el hermetista es, al actuar, un hombre afirmado en su dualidad,
en el matrimonio que celebran lo divino y lo mortal.

De esta manera, el hermetista es el que actúa sabiendo lo que
está en juego: la naturaleza ética del hombre; es decir, su condición
de voluntad ama y esclava, divina y mortal, que no se deja al
arbitrio de otro, sino que elige y realiza la propia elección
una y otra y otra vez.

Por ello, la cuestión práctica es esencial. El acto, para ser
realmente acto, no puede ser considerado desde la abstracción,
sino solamente en su concreción operativa y eficaz. De ahí el
énfasis en la necesidad de descubrir el arte de fabricar cosas,
de conocer los secretos del cosmos, de dejar grandes monumentos
de la industria humana. La acción, lejos de ser un fin en sí
misma, es sólo un medio de eficacia:

> Dios es el creador del mundo […] gobernando a la vez todas las cosas,
> en conjunción con el hombre que gobierna, también él. Si el hombre
> asume este trabajo y todo lo que implica, entendiendo el gobierno que
> constituye su tarea propia, obra de tal manera que él para el mundo y el
> mundo para él son un ornamento (en razón de la divina estructura del
> hombre, se le llama mundo, aunque el griego lo denomina, con mayor
> justicia, orden). El hombre se conoce y conoce también el mundo […]@note

El hombre actúa, así, no sólo como creador, sino como amo sujeto
a la necesidad y al orden, cuya ley hace eterno al universo,
y eterno también al hombre. Por eso, toda obra de industria,
al igual que la materia y la vida, debe renovarse. Tal es el
principio que, dentro del tiempo y con el tiempo, en el saber
de los dioses cíclicos, conforma la eternidad del mundo y transforma
la necesidad de elegir ---que para el gnóstico era una condena---
en una liberación.

Gnosticismo y hermetismo hacen patente que el problema no es
elegir entre el bien y el mal, sino la elección misma como manifestación
de la fractura temporal en el ser, como desmembramiento entre
desear, pensar y ser. Pero el hermetismo responde afirmando que
no existe la identidad como realidad inmutable, sinó el dinamismo
del acto creativo, síntesis de libertad y necesidad, capacidad
de elección y obligatoriedad, en un continuo juego de tensiones.
Salvarse ---es decir, superar el temor a la muerte y a la necesidad---
es un problema en y dentro del devenir, una cuestión de transformación
humana que tiene lugar en el obrar.

Este cambio en los conceptos centrales del gnosticismo, con los
que comparte tantas cosas el hermetismo, conduce al énfasis no
en la interioridad como vía de salvación ---si bien la interioridad
es parte del camino que debe retomar el hermetista para alcanzar
el _nous_ y afirmar en él el carácter multivoco y dinámico de
la existencia---, sino en la unidad última del acto creativo.
Este une lo disperso, afirma la multiplicidad siendo uno, da
sentido y estructura a la vida del hombre en el mundo.

※ {.espacio-arriba1 .centrado}

La palabra espíritu, en el sentido en que la utilizarán los renacentistas,
la dirá primero san Agustín. No tiene por qué sorprendernos.
Formado como maniqueo y vuelto cristiano a través de Plotino
---curiosa mixtura que sintetiza las tres fuerzas de esa época:
gnosis, hermetismo y cristianismo--- él mismo es la síntesis
que reúne las visiones de uno y otro. Por supuesto, él mismo
y el cristianismo harán aportaciones decisivas. Pero san Agustín
se merece mucho más que unos cuantos párrafos. Dejemos entonces
que, por el momento, él calle y conservemos el principio de esta
historia, crucial para la vida del espíritu.
{.espacio-arriba1 .sin-sangria}

Pues el espíritu surgirá justamente del camino que recorren en
la historia del pensamiento las ideas de gnósticos y herméticos,
de las transformaciones del sentido de la interioridad, del movimiento
de las fronteras que circundan el ámbito propiamente ético, campo
de lucha por la conquista del propio ser y punto de convergencia
de la totalidad en lo particular. El espíritu será el reconocimiento
tangible, casi material, de la existencia de una realidad intermedia
entre la carne y el alma ---el cuerpo del alma, lo llamará Robert
Klein---@note Tendrá a la vez poderes liberadores y creativos,
será, en suma, la clave de la existencia ética del hombre.

# La efigie bella de un hombre excelente

Por eso afirmamos que la belleza es la flor de la
bondad. Y por los atractivos de esa flor que
actuan como señuelos, la bondad que está
adentro como escondida, atrae a los
circunstantes. \
Marsilio Ficino {.epigrafe}

++Los especialistas++ han discutido mucho sobre el significado
de la astrología en el neoplatonismo florentino. Esto se debe
a la ambiguedad de los discursos sobre el tema, en los que la
astrología es algunas veces atacada y otras defendida con la
misma convicción.@note Marsilio Ficino, por ejemplo, escribió
la _Disputatio contra iudicium astrologorum_ en 1477 para refutar
la concepción de la determinación de la vida humana por los astros.
Años después, escribiría _De vita y el Liber de sole_, en los
que la astrología constituye un elemento positivo y esencial
del trasfondo teórico de todo el texto.@note

Sabemos, sin embargo, que durante el Renacimiento se produjo
una renovación del pensamiento astrológico, resultado del cuestionamiento
de la astrología ptolemaica a partir de influencias árabes como
la de Abenjaldún. Para él, «[…] si las estrellas deciden el destino
del hombre en la generación, el hombre, mediante la técnica de
las interrogaciones, descubre las alternativas todavía abiertas,
los intervalos de indiferencia por los que puede intervenir el
proceso y escoger, a su vez, su propia estrella».@note

En este sentido, la ambigiedad del Renacimiento frente a la astrología
es, en realidad, sólo un espejismo. Los numerosos ataques que
hoy podemos leer contra la astrología ---y en particular, contra
las ideas deterministas dentro ella--- no son, como podría pensarse,
una confrontación entre quienes creían en la astrología y los
escépticos, sino entre distintas corrientes astrológicas y teorías
de la profecía.@note Una discusión, en suma, en el marco de astrólogos
y profetas.

Pero más relevante aún que la disputa sobre la astrología es
el hecho de que ésta renace convertida en un instrumento de pensamiento
que sirve de engarce para comprender de qué manera las rígidas
leyes naturales y los estrictos ciclos a los que se encuentra
atada la existencia humana son experimentados por el hombre y
adquieren, por decirlo así, dimensión humana. En otras palabras,
la astrología se convierte en un instrumento que, en cierto sentido,
traduce la conciencia de la determinación en una experiencia
subjetiva por la cual aquello que determina al hombre cobra significado
dentro de él y establece las condiciones y posibilidades de su
libertad. Esto es lo que describe E. Cassirer al afirmar, a propósito
del pensamiento astrológico del Renacimiento, que:

[…] si no le es dado al hombre elegir su astro ni, por lo tanto,
su naturaleza física y moral, su temperamento en cambio es perfectamente
libre de acertar en la elección dentro de los límites que le
prescriben los astros, pues cada uno de ellos contiene en su
propia esfera una multitud de distintas y aun contrarias configuraciones
de la vida, y a la voluntad del hombre le es lícito el decidirse
definitivamente por una de ellas.@note

No es fácil percibir de qué manera esta posibilidad de interiorizar
los rígidos ciclos astrales para construir la propia dirección
de la vida permea la estructura del pensamiento renacentista
desde la metafísica hasta la cotidianidad de la vida. Sin embargo,
en el _Liber de sole_ de Marsilio Ficino, podemos encontrar una
pauta para desentrañar parte del significado y las implicaciones
de esta función que la astrología adquiere en el Renacimiento,
particularmente en el ámbito de la ética.

※ {.espacio-arriba1 .centrado}

El primer encuentro con un texto astrológico del Renacimiento
resulta siempre desconcertante, pues su lectura deja siempre
la sensación de que el texto dice algo más, de que hay afirmaciones
ocultas que sólo entenderá quien pueda penetrar su sentido. Así
pasa con el _Liber de sole_. En una primera lectura, tal vez
veamos sólo un ejercicio poético, como declara el propio Ficino
en las palabras que dirige al lector.@note En realidad, nos encontramos
ante un texto sobre astrología en el que se describe y discute
la función del sol en la lectura e interpretación de las cartas
natales y, sin embargo, al penetrar su sentido vemos también
un opúsculo sobre la naturaleza del bien, y por lo tanto, un
tratado sobre la virtud. Más aún, se trata de un comentario sobre
el pensamiento platónico y la interpretación que, sobre la base
de la tradición neoplatónica, Ficino hace del ateniense. Y, más
allá todavía, el _Liber de sole_ es también un texto sobre el
entendimiento y la inteligencia y, además, una obra sobre Dios.
{.espacio-arriba1 .sin-sangria}

Como señala Angela Voss, en el _Liber de sole_ los «niveles de
realidad simbólica y literal se avienen en el triunfo de la conjunción
de astronomía y astrología, filosofía y poesía, y lo divino y
lo humano, para producir una verdadera aprehensión anagógica
de la unidad. La ciencia y lo divino son uno».@note

En efecto, la utilización del discurso astrológico por parte
de Ficino produce la sensación de que, a partir de un mismo lcnguaje
y una misma serie de conceptos, se habla de muchas cosas simultáneamente
en distintos planos y con distintos significados. Tomemos y un
ejemplo y mirémoslo más de cerca:

> Ninguna otra cosa, más que la luz, reclama la naturaleza del bien. En
> primer lugar, porque entre las cosas sensibles la luz aparece como la
> más pura y elevada. En segundo lugar, porque se propaga
> instantáneamente y ampliamente, más fácil que cualquier otra cosa. En
> tercer lugar, porque se encuentra con las cosas sin producir daño y a
> todas las penetra con levedad y dulzura. En cuarto lugar, porque porta
> consigo un calor vital que nutre, genera y mueve. En quinto lugar.
> porque mientras está presente junto o dentro de cualquier cosa, no se
> contamina de nada, y con nada se mezcla. De modo similar, el bien en
> sí mismo está más allá de todo el orden de las cosas, se difunde
> ampliamente, todo acaricia y atrae hacia sí. No obliga a nada, tiene
> siempre y en todos lados como compañía al amor, casi un calor del cual
> cada cosa singular se siente atrapada y por el cual acoge
> voluntariamente el bien. Está presente en lo íntimo de cada cosa donde
> se llegue, si bien no se une a ninguna cosa. En fin: el bien en sí mismo
> es imposible de comprender y de entenderse; lo mismo ocurre con la
> luz. Ningún filosofo, de hecho, hasta ahora lo ha definido nunca: si
> bien en ningún lugar se ve algo más claro, nada resulta más oscuro. Del
> mismo modo, el bien es la cosa más conocida, y también, la más
> desconocida de todas.@note

¿Cómo comprender este texto que habla de la luz y del bien? ¿Es,
como puede tal vez preverse, sólo una metáfora que ilustra las
cualidades del bien 0, en realidad, Ficino está asumiendo, de
manera literal, que el bien es la luz?

Si lo que hemos dicho hasta aquí es cierto, debemos descartar
que se trate de una metáfora. Pero también debemos descartar,
al menos en parte, que el bien y la luz sean, para Ficino, literalmente
lo mismo. Digo sólo en parte porque, en realidad, el bien y la
luz son lo mismo sólo en el plano de la astrología. Me explico:
el sol físico que ilumina todas las mañanas el mundo no puede
ser literalmente el bien porque, aunque tenga cualidades benéficas
y ---de manera, ahora sí, metafórica--- vivifique, es finito
y está sujeto a las cualidades del tiempo y del movimiento; de
la misma forma, desde el plano divino, el bien no puede reducirse
al sol porque adquiriría limites y perdería sus rasgos divinos.
Pero el astro solar significa el bien mismo en el horizonte astrológico.
Ahí, la realidad física y la naturaleza divina se unifican e
integran, cobran y se dotan de sentido una a la otra. Pero, ¿qué
significa esto?

Parte de la clave podremos hallarla en este pasaje. Escribe Ficino:

> El sol significa todas estas cosas (sabiduría, fe, religión, gloria eterna)
> y, simplemente, toda verdad y vaticinio y reino. A ello se agrega que
> cuando el Sol sale al Medio Cielo nutre milagrosamente nuestro
> espíritu vital y animal; mas, cuando está en el Descendente, sea uno u
> otro espíritu se debilitan. Por ello, David, trompeta de Dios
> omnipotente, cuando se levanta al alba para hacer sonar la cítara y
> entornar los cantos, y exclama «es inútil alzarse antes que la luz»,
> declara que el sol, cuando surge, trae consigo todo bien para nosotros,
> y llama milagrosamente a cosas altísimas a nuestro espíritu excitado e
> iluminado.@note

Aquí encontramos de qué manera se entreveran los dos planos de
la realidad a partir del discurso astrológico. Si examinamos
el pasaje con detenimiento, lo primero que llama nuestra atención
es la manera en que Ficino logra hacer surgir sentidos diversos,
para planos diversos, del medio día. Destaquemos, en primer lugar,
que el significado astrológico del sol ---como aquél que realza
las cualidades positivas del resto de los planetas si está en
el cielo medio, o lo contrario, si se encuentra en el descendente---
está vinculado con una sentencia bíblica de David. Más allá de
que esto muestra cómo, en el pensamiento de Ficino, la astrología
y la doctrina cristiana conviven en el plano argumental, su sentido
es realmente otro. Ficino toma la afirmación de David en un sentido
literal: no es conveniente emprender actividades antes del medio
día. Lo mismo ocurre con el movimiento astral, que es considerado
en el sentido físico: cielo medio y descendente corresponden
al medio día y al atardecer. Porque resulta que la afirmación:
«cuando el Sol sale al Medio Cielo nutre milagrosamente nuestro
espíritu vital y animal; mas, cuando está en el Descendente,
sea uno u otro espíritu se debilitan», hace alusión al proceso
físico según el cual el calor del sol ayuda al cuerpo humano
a producir espíritus que, según Ficino, son el producto de la
purificación de la sangre y necesarios para llevar a cabo funciones
vitales como la digestión y la intelección.@note Así, la frase
de David: «es inútil alzarse antes que la luz» significa precisamente
eso, que antes del medio día, el cuerpo no ha producido los suficientes
espíritus para atender todas sus necesidades vitales, particularmente
las de la intelección. Pero la frase de David conserva, claro
está, su sentido propio en el contexto bíblico: es inútil esforzarse
si antes no se nos ha mostrado Dios.

Así, en el pasaje que analizamos, Ficino hace equivalente el
medio día físico, al medio día del conocimiento y al medio día
de la fe. En este sentido, el mensaje de Ficino puede comprenderse
de esta forma: en el plano humano, es decir, en la experiencia
humana, la percepción de la vivificación física del medio día
produce la misma sensación que cuando se avanza hacia el conocimiento
y la fe, de manera que los tres son vividos como un sólo movimiento
del alma.

De esta manera, a través de la astrología, el mundo físico y
la idea trascendente del bien se dotan de sentido uno a la otra:
el bien adquiere su significado a través de su encarnación en
las cosas y, a la inversa, éstas lo adquieren a través de su
vínculo con el bien. Uno y otro movimiento, el del bien a las
cosas y el de éstas a aquél, se producen en la forma en que el
hombre experimenta en su interior, y gracias a la sensibilidad,
la existencia de ambos en una simultaneidad de planos y no como
realidades ontológicamente distintas. A fin de cuentas, ambos
son movimientos del alma.

Por supuesto, es esta cualidad del pensamiento ficiniano la que
más me atrae e intriga. Ánte todo porque, si esto es así, vale
la pena preguntar a Ficino cómo están construidas el alma y la
sensibilidad humana, y de qué forma operan para constituirse
en una «sensibilidad ética»; es decir, en una síntesis de planos
ontológicos distintos.

Para dar respuesta a estas interrogantcs es necesario remitirnos
a las fuentes teóricas de las que parte Ficino para formular
su propia concepción de una sensibilidad con carácter ético.

※ {.espacio-arriba1 .centrado}

De la psicología aristotélica, que dominó hasta el final del
Renacimiento, Ficino recoge la idea de que el alma humana está
dividida en tres partes: vegetativa, animal y racional. También
asume, con Aristóteles, que es dentro del alma animal ---más
tarde llamada, durante la Edad Media, alma sensible, concupiscible
o intelecto particular--- donde se encuentran las funciones de
la sensibilidad que cumplen los cinco sentidos externos y los
llamados sentidos internos que, en De anima, son: sentido, memoria
y fantasia.@note {.espacio-arriba1 .sin-sangria}

Además de esta caracterización del alma, son decisivos, para
Ficino, tres postulados aristotélicos respecto a la forma de
operar de la psique: primero, que la sensibilidad es «la facultad
de recibir formas sensibles sin la materia».@note Segundo, que
por esta capacidad, la fantasía es el punto crucial de encuentro
entre razón y sentidos. Y, tercero, que en esa medida, la fantasía
tiene capacidad de juicio.@note

Sin embargo, la concepción del alma que hereda Ficino está mediada
por la crítica y la reflexión de otros pensadores, cuyas aportaciones
dan profundidad y mayor complejidad a las ideas aristotélicas.
Así, por ejemplo, los estoicos distinguirán entre fantasía y
fantasma, entre la función y el productó( de manera que «la primera
era una impresión sensorial que se adecuaba al objeto real, el
segundo una imagen mental, como un sueño […]».@note «El término
latino con el que se traduce fantasía es imaginatio, por lo que
la función de obtener una impresión sensorial que se adecua al
objeto, quedará justamente asociada a la imaginación y convertida
ésta en una función del alma.@note La fantasía, en cambio, quedará
vinculada, en un sentido negativo, con la capacidad del alma
de abstraer el producto de la imaginación, las imágenes, de sus
dimensiones espaciales y temporales, y en consecuencia, de hacer
un uso arbitrario de las mismas.

Adicionalmente, los estoicos harán todavía otra aportación sustantiva
a la idea de la psique humana: la concepción de un «cuerpo» del
alma llamado _pneuma_ como asiento de las facultades sensibles.
El _pneuma_, o aire, es una sustancia intermedia entre la materia
y el alma que sirve como vínculo entre una y otra. Ficino recogerá
esta idea de _pneuma_ y las funciones atribuidas a ella por los
estoicos bajo el término de espíritu, popularizado por la medicina
medieval. Sin embargo, el _spiritu_ ficiniano no es una sola
sustancia, sino una pluralidad de pequeños elementos, que recuerdan
en parte a los átomos de Lucrecio, definidos como casi cuerpo,
casi alma.@note Los _spiriti_, pues, recorren todo el cuerpo
y controlan cada uno de los movimientos interiores y exteriores
del organismo, a partir del trabajo de los sentidos internos,
a los que ya se ha integrado la imaginación, con la función que
a ella le atribuyen los estoicos.

Ficino aporta, sin embargo, dos novedades en relación con la
concepción de la psique que recibe de aristotélicos y estoicos:
la naturaleza bidireccional de la relación entre el cuerpo y
el alma ---por lo tanto, también la bidireccionalidad de las
funciones de los espíritus--- y la independencia de las imágenes
formuladas en la fantasía en relación tanto con el cuerpo como
con el alma.

En un pasaje crucial de la _Teología platónica_, encontramos
cómo Ficino sigue y modifica la herencia estoica y aristotélica.
El pasaje es extenso, pero vale la pena examinarlo a detalle.
Comienza diciendo Ficino:

> A través de la sensación Sócrates ve a Platón y ahí mismo, a través de los
> ojos, obtiene un simulacro incorpóreo de Platón, independiente de la
> materia de la cual éste está formado […] En un segundo momento, a
> través de la imaginación interna, y aun cuando Platón esté ausente,
> Sócrates piensa en el color y la figura recogidos a través de los ojos, en
> la voz dulce de Platón, escuchada antes por los oidos, y así con cada una
> de las particularidades advertidas por los cinco sentidos.

> […] [Por ello] el sentido tiene por objeto los cuerpos y la imaginación,
> por su parte, tiene por objeto las imágenes de los cuerpos recibidas a
> través de los sentidos o por éstos concebidas.@note

Hasta aquí no hay ninguna novedad. Ficino se atiene a la enseñanza
aristotélica y estoica según la cual el sentido reúne las percepciones
de los cinco sentidos exteriores para formar un simulacro ---el
término es tomado de Lucrecio y significa la representación inmediata
del objeto, sin constituir, aún, una imagen mental---. Pero continúa:

> Poco después, Sócrates, a través de la fantasía, comienza a formular en
> torno al simulacro de Platón, formado por la imaginación a través de
> los cinco sentidos, un juicio en estos términos: «¿Quién es este
> hombre de complexión alta y robusta, con esta frente tan espaciosa, de
> espaldas amplias, de piel como de alabastro, con esta nariz aguileña,
> una boca un tanto pequeña y de una voz un tanto suave?» Es Platón, un
> hombre bello y bueno, mi dilectísimo discípulo. De esto resulta claro
> cuánto la fantasía de Sócrates supera a su imaginación: ésta formó una
> efigie semejante a Platón, pero no al grado de conocer a quién y a qué
> tipo de hombre designa tal efigie, mientras la fantasía puede discernir
> que esa efigie es la de un hombre que se llama Platón, es la efigie bella
> de un hombre excelente […]@note

Detengámonos aquí. Como se señaló antes, los estoicos distinguieron
la imaginación de la fantasía, atribuyéndole a la primera la
adecuación de la representación al objeto y a la segunda, en
un sentido negativo, la manipulación de imágenes sin concordancia
con objeto alguno, como sucede, por ejemplo, en el sueño. Sin
embargo, en Ficino encontramos que la fantasía discierne y, en
ese sentido. Ficino rescata una de las características que Aristóteles
le atribuía originalmente.

La fantasía, entonces, aporta elementos de los que carece la
pura representación interior del objeto. Así, mientras la imaginación
es pasiva porque se limita a reproducir el objeto, la fantasía
es activa, atribuye cualidades al objeto de la sensación, que
tienen que ver con la apreciación estética y ética de la sensación
misma. Esto, que nos permite ya intuir diferencias decisivas
entre Ficino y la tradición aristotélica, para la cual la fantasía
era pasiva en todos los órdenes,@note se aclara todavía más a
partir de cómo remata este pasaje. Finaliza Ficino: «Por su parte
el intelecto ---mientras la fantasía se ocupa de la condición
particular de este hombre particular--- concibe los universales,
de manera que fantasía e intelecto operan singularmente y simultáneamente».@note

La _Teología platónica_ tiene como argumento central la inmortalidad
del alma. En ese texto, Ficino se ocupa, sobre todo, de establecer
los argumentos que la prueban. Uno de estos argumentos consiste
en mostrar que ni el alma ni el intelecto que le es propio tienen
contacto alguno con la materia o lo corruptible, y esto incluye
lo producido a través de sentidos, es decir, las imágenes producto
de la imaginación y materia de la fantasía.@note Así, pues, intelecto
y fantasía operan con independencia el uno del otro, esto es,
singularmente. Pero además, lo hacen de manera simultánea, lo
cual quiere decir que si bien no hay contacto, la actividad de
uno se refleja en la otra, precisamente como tal, como actividad.@note
Queda así enunciada una parte de la bidireccionalidad de la relación
entre el cuerpo y el alma, justo en la medida en que al entrar
en actividad la fantasía ---como resultado del sentido y la imaginación---
entra en actividad el intelecto, y a la inversa, al entrar en
actividad el intelecto ---que es independiente de la fantasía---
ésta también lo hace.

Además, aquí también encontramos de qué manera la fantasía constituye
un reino independiente entre el cuerpo y el alma, convirtiéndose
en lo verdaderamente constitutivo de la interioridad del hombre.
Gracias a su actividad, que atribuye cualidades a lo percibido,
la fantasía formula juicios referentes a la sensibilidad, que
son de carácter particular ---en contraste con los juicios de
carácter universal que ejecuta el intelecto--- y que se refieren
no ya al objeto de la percepción como tal, sino a las imágenes
creadas por la imaginación. La fantasía, así, es propiamente
la sensibilidad dentro de la psicología de Ficino, pues sus juicios
son, precisamente, los que conforman la apreciación sensible
de parte del sujeto. Pero esa apreciación sensible es, como señalé
antes, ético-estética: «es la efigie bella de un hombre excelente».
En otras palabras, para Ficino, la fantasía estructura la percepción
de tal forma que nos permite identificar la forma sensible de
la bondad y la belleza, y en esa medida, dar un sentido a lo
que es percibido.

Atendiendo, pues, a lo que se ha dicho hasta aquí, aún queda
por resolver un tema. Y éste consiste en preguntarse de qué manera
la fantasía está estructurada, es decir, qué la conforma, qué
la hace capaz de percibir la bondad y la belleza de las cosas.
De qué, pues, está provista para hacer atribuciones de esa indole.

El mayor privilegio del pensamiento de Marsilio Ficino es haber
sido sensible a una gran pluralidad de influencias. Su psicología
no es la excepción. De hecho, y en el fondo, el problema central
de la psique humana que formula Ficino no deriva del pensamiento
aristotélico, sino del neoplatónico, y en él son claras las influencias
tanto de Plotino, como de Orígenes y Agustín.

Sin duda, la preocupación por el tema seminal de la inmortalidad
del alma que recorre toda la _Teología platónica_ está sustentada
en las preocupaciones de estos tres autores. Pues es de ellos
de donde surge el problema ontológico, epistemológico, estético
y moral al que Ficino intenta dar respuesta mediante la independencia
entre fantasía e intelecto.

No discutiré aquí cuáles son las fuentes de la preocupación neoplatónica
por la inmortalidad del alma y por la necesidad de evitar toda
materialidad en el pensamiento, pues ello requiere de un análisis
extenso que no es posible llevar a cabo en tan reducido espacio.
Sin embargo, quisiera recoger las tesis cruciales que Ficino
asimila del neoplatonismo y que pueden darnos una pista sobre
la estructura de la fantasía, por la cual puede constituirse
en sensibilidad ética.

※ {.espacio-arriba1 .centrado}

Ficino recoge de Plotino el conflicto metafísico que hace imposible
que la inteligencia ---y por ende el alma--- tenga contacto alguno
con la materia. De hecho, para Plotino, lo propiamente humano
es la llamada razón particular, que juzga precisamente sobre
los particulares, y el alma irracional o animal, donde radica
la percepción.@note De acuerdo con él, la primera es la parte
más importante del alma humana, porque «recibe las actividades
del intelecto desde el nivel o alma superior, como aquellas de
la sensación y de la percepción desde la fase inferior, y selecciona
el nivel en el que desea permanecer […] Ella puede unirse con
el alma superior o inferior, o permanecer como un poder separado
y fluctuar entre los dos».@note {.espacio-arriba1 .sin-sangria}

Sin duda, Ficino retoma de Plotino este carácter independiente
de la fantasía como _pneuma_ y, junto con él, una tesis común
del neoplatonismo hermético. De acuerdo con Numenio de Apanema,
en su descenso por las esferas celestes, el alma humana va adquiriendo
sus aptitudes por influencia de cada planeta.@note Esta misma
idea aparece en uno de los Fragmentos de Estobeo:

> Siete astros de curso dilatado giran en ciírculo en el umbral del
> Olimpo, con ello el tiempo infinito prosigue eternamente su marcha:
> la Luna que ilumina la noche, el lúgubre Cronos, el sol _Nous_, Paifé
> que aparta el lecho nupcial, el fogoso Ares, Hermes de alas rápidas y
> Zeus, primer autor de todos los nacimientos. Estos mismos astros ha
> recibido en participación la raza humana, y en nosotros está la Luna,
> Zeus, Ares, Paifé, Cronos, el Nous, Hermes. He aquí por qué nuestro
> sino consiste en hacer nuestro, del aliento etéreo, lágrimas, risas,
> cólera, generación, logos, sueño, deseo. Las lágrimas es Cronos, la
> generación Zeus, logos es Hermes, cólera Ares, el sueño la Luna,
> Citérea el deseo, la risa el sol: pues por él ríen, con razón, toda la
> inteligencia mortal y el mundo sin límites.@note

La tesis de que las principales atribuciones activas del alma
humana son un don celeste es sin duda retomada por Ficino. Sin
embargo, no lo es sólo por obvia importancia astrológica para
establecer la base teórica de la carta natal. Lo es, sobre todo
en el caso del hermetismo, porque establece una relación directa
entre cualidades celestes y la estructura del _pneuma_ individual,
especialmente en lo que se refiere a aquello que moldea la vida
interior del sujeto y que tiene consecuencias para la conducta:
el deseo, el sueño, la risa y la cólera, y el juicio particular.

Ficino asimilará estas características planetarias del _pneuma_
neoplatónico a su concepto de la fantasía, pues «cuatro afectos
acompañan a la fantasía: el apetito, el placer, la repulsión
y el dolor».@note Pero no solamente esto; tal vez lo más importante,
lo más trascendental, es que Ficino aceptará esta idea de la
conformación celestial de la fantasía humana y, a partir de ella,
considerará la astrología como el instrumento idóneo para indagar
sobre la estructura de la fantasía en cada persona, es decir,
por los rasgos de su carácter y, sobre todo, por lo que a nosotros
nos interesa: su propia sensibilidad.

No hay ahora más que un paso para entender que si la estructura
de la fantasía puede ser analizada con base en la astrología,
la psicología de Ficino y, por ende, su teoría de la sensibilidad,
asumen que tal estructura es simbólica y dinámica, en la medida
en que los contenidos de la fantasía carecen de un sentido definido
---no son conceptos ni ideas--- y su relación resulta en una
pluralidad de significados temporales.

Tenemos así que la fantasía tiene una doble función. Por un lado,
convierte en símbolos las imágenes que le llegan por intermedio
de la imaginación y los principios inteligibles que le llegan
a través de la razón. Por otro, interpreta el significado de
esos simbolos tanto para juzgar lo que se percibe, como para
desatar el acto de intelección. De esta forma, la fantasía es
propiamente humana e independiente, y capaz de asimilar, en la
interioridad del sujeto, dos planos ontológicos distintos: el
material y el divino.

Precisamente por eso puede apreciar el bien, no como algo propio
de las cosas, sino como fantasía. Me explico: en la interioridad
del sujeto, la simultaneidad de los dos planos hace posible enlazar
un significado a aquello que se percibe, ya sea respecto de sí
mismo o en relación con el mundo exterior. Ese significado, como
en el caso del sol, implica una valoración moral de carácter
particular, es decir, que vale sólo para ese momento y para esa
circunstancia específica. Por eso Platón puede ser un hombre
bello y honesto, esa tarde y no necesariamente otra cualquiera.

Pero además de esto, no debemos perder de vista que la fantasía
es una facultad sensible, está ligada aún, por intermedio de
la imaginación, a los sentidos y, a través de ellos, al mundo
exterior. De ahí, por supuesto, el carácter particular de la
valoración, pero también su carácter sensible: se siente el bien
en Platón, como se siente la belleza.

No me cabe duda: para Ficino el bien puede ser percibido, sentido.
Pero sólo en la fantasía, como una cualidad de las cosas y de
las conductas que se revela en el momento en que uno mismo es
puente entre la materia y la inteligencia.

※ {.espacio-arriba1 .centrado}

De esta conclusión se desprenden para mí rutas de reflexión que
nos traen al día de hoy. Primero, la fantasía fue descalificada
por el racionalismo y el empirismo desde el punto de vista epistemológico.
Incluso Descartes, en el _Discurso del método_, de manera tangencial,
le atribuye la fuente de todo error, y Locke simplemente se refiere
a las ideas fantásticas como no vinculadas a una sustancia y,
por lo tanto, irreales. De manera más reciente, el psicoanálisis
la recoge como un mecanismo compensatorio y de regulación psíquica,
que actúa como satisfactor de deseos inconscientes, por ejemplo.,
a través del sueño. Sin embargo, en uno y otro caso, parece olvidarse
esta función sensible que le otorga Ficino y por la cual la fantasía
constituye la forma de la sensibilidad humana, entendida ésta
como una organización valorativa del significado de nuestras
percepciones, a partir de la cual nosotros actuamos.
{.espacio-arriba1 .sin-sangria}

Segundo. Si es posible asumir esta participación de la fantasía
en la constitución de la sensibilidad, podemos hablar de una
apreciación ética ---como, de manera paralela se habla de apreciación
estética--- que revela la elaboración de un juicio de carácter
sensible, distinto al racional, y, por lo tanto, de emociones
asociadas a una experiencia del bien.

Tercero, y último: en la ética postkantiana, que todavía hoy
es sustrato de las reflexiones contemporáneas sobre la ética,
el juicio sensible está escindido del juicio práctico. Desde
Schiller, quien diagnosticó con precisión el abismo que se abria
con esta ruptura, la posibilidad de enlazar de nueva cuenta la
ética y la estética aparece como una de las claves que ofrecen
alternativas para superar la disociación entre enunciados prescriptivos
y prácticas éticas, entre formulaciones teóricas y conductas
reales. En otras palabras, para introducir en la ética la dimensión
de la vida y en ésta, su significado ético.

# El quinto elemento

A Esther Cohen y Elia Nathan {.epigrafe}

++El dilema actual++ de la ética, la génesis de su insuficiencia
para brindar un asidero seguro, para enfrentar las controversias
de la vida, no yace en la reflexión misma sobre la ética o en
sus resultados, sino en algo menos elaborado y mucho más simple:
una atrofia de nuestra capacidad de sentir.

Oscar Wilde sentenció:

> En la actualidad la gente ve nieblas, no porque haya tales nieblas sino
> porque los pintores y los poetas le han enseñado la misteriosa belleza
> de sus efectos. Es muy posible que desde hace siglos haya habido nieblas
> en Londres. Sí, seguramente las ha habido, pero nadie las veía. Y de
> ahí que nada sepamos de su existencia en aquellos tiempos. Hasta que
> el hombre y su invención, el arte, las inventaron, puede decirse que
> empezaron a existir.@note

En efecto, nuestra sensibilidad no es producto de una condición
dada de antemano, sino una construcción, un producto dinámico
de la vida y de la historia de los hombres, de su propia humanidad.
Lo que apreciamos no es lo que está ahí, sino lo que vemos que
está ahí: el rayo de sol que atraviesa la ventana e ilumina con
claridad un rincón de la casa puede ser apenas el pretexto para
que yo le otorgue significado y sienta, finalmente, la presencia
de ese rayo de luz, y sepa, tal vez, que hoy mi existencia es
mejor.

No estoy inventando el hilo negro. De hecho, voy a valerme de
un filósofo que murió hace más de quinientos años ---de nuevo,
Marsilio Ficino--- para explicar el dilema ético que está en
juego en el etéreo ámbito de las sensaciones. Comencemos por
decir que Ficino establece una distinción entre los sentidos
que, en este contexto, es crucial. Por un lado, tacto, gusto
y olfato son para el florentino los sentidos más groseros y jerárquicamente
inferiores, ya que requieren de la presencia actual del objeto
para percibir: no podemos sentir la tersura de unos muslos si
no colocamos las manos sobre ellos. Con la vista y el oído, en
cambio, la presencia del objeto no es necesaria. Retenemos la
sensación de la imagen y el ritmo aún después de que la amada
ha abandonado el cuarto o la guitarra ha dejado ya de sonar.@note

Pero el verdadero significado de esta distinción se encuentra
en que la vista y el oído son sentidos mucho más espirituales,
es decir, más cercanos al sentir, propiamente dicho, que el resto
de los sentidos. Al tratar de probar la inmaterialidad del alma
en la _Teología platónica_, Ficino recurre al argumento de lo
que ocurriría si las sensaciones fueran de naturaleza estrictamente
física: si lo fueran, la imagen de Platón no cabría en el ojo
de Sócrates y, si eventualmente cupiera, no habría lugar para
otra imagen. Es decir, o no podríamos sentir en su totalidad
las cosas o careceríamos de memoria.@note Pero, puesto que sentimos
y recordamos, la naturaleza de las sensaciones y del sentir en
general debe ser distinta de la de los cuerpos físicos; esa naturaleza
no es otra que la espiritual.

Radical en cuanto al sonido, Ficino afirma: «Los sonidos de las
voces que son percibidas por nuestros oídos, son ellos mismos
de naturaleza espiritual, no son producto de un cuerpo ni se
fijan al cuerpo, antes bien, se trasmiten de alma a alma, y son
creados y conservados por la potencia del alma».@note Pero ¿qué
significa este carácter espiritual del sonido? ¿Qué nos dice
de la naturaleza propia de la sensación?

※ {.espacio-arriba1 .centrado}

El espíritu tiene en Ficino dos significados básicos. Por un
lado, desde el punto de vista de su naturaleza, el espíritu es,
en _De vita_: «Un cuerpo sólido, que casi no es cuerpo sino alma,
ni casi alma sino cuerpo. Suvirtud es contener poquísima naturaleza
terrena, un poco más de aquella líquida, más de la aérea, pero
sobre todo, mucho de aquella del fuego de las estrellas […] Por
ello vivifica todo y es el responsable próximo de toda generación
y movimiento».@note En la _Teología platónica_, Ficino explica
que este corpúsculo sutil y luminoso es «generado por obra del
calor del corazón a partir de la parte más sutil de la sangre
y que se difunde desde ahí a todo el cuerpo».@note
{.espacio-arriba1 .sin-sangria}

Hay que subrayar el carácter casi físico del espíritu que, como
producto de un proceso fisiológico ---la sublimación de la sangre---,
contiene pero no es ninguno de los cuatro elementos mundanos.
Á este quinto elemento ---y éste es el segundo sigriificado básico
que tiene el espíritu en Ficino--- se atribuyen tres funciones
del alma: el sentido común, la imaginación y la fantasía. Ficino
está siguiendo aquí las tesis de Sinesio de Cirice que aporta
la idea de que las facultades del sentido común, la imaginación
y la fantasía, propias del alma animal de la filosofía aristotélica,
son las funciones de ese cuerpo sutilísimo que recoge él, a su
vez, de la idea del _pneuma_ estoico.@note Lo singular de esta
idea es que no sólo reúne la tradición aristotélica y la estoica,
sino que las funde dentro del pensamiento neoplatónico. Así,
el espíritu: «constituye la tierra de nadie entre lo irracional
y la razón, entre lo corpóreo y lo incorpóreo, una frontera común
entre ambos y, por medio de él, lo divino entra en contacto con
el último grado».@note

En efecto, el espíritu en Ficino, como en Sinesio, se convierte
en el verdadero _cupula mundi_; es el lugar ---tanto el elemento,
como la función--- en que lo divino cobra cuerpo y el cuerpo
se diviniza. Su función no sólo es cognoscitiva, como en la teoría
aristotélica; la aportación propiamente neoplatónica es que,
además del conocimiento, el espíritu posee una función y un valor
ontológico.

La función cognoscitiva de las facultades del espíritu es conocida
desde Aristóteles: los sentidos reciben un estímulo físico y
sus datos van a parar al sentido común, el cual los reúne. Éste,
a su vez, los transmite a la imaginación, que conforma una imagen
de los objetos en la que todavía están presentes sus particularidades
específicas. La imaginación, por su parte, envía esta imagen
a la fantasía, donde se sustrae la materia y se elabora el fantasma,
una suerte de imagen sintética y casi universal de la forma de
aquello que se ha percibido por los sentidos y sobre lo cual
la razón elaborará su juicio. Así, el proceso del espíritu, o
de espiritualización de la imagen, es un cambio que desemboca
en el juicio.@note Repasemos la grata explicación de Ficino:

Á través de la sensación, Sócrates ve a Platón, y ahí mismo,
a través de los ojos, obtiene un simulacro (_simulacrum_)@note
incorpóreo de Platón, independiente de la materia de la cual
éste está formado (para lo cual necesita que Platón esté presente)
[…] En un segundo momento, a través de la imaginación interna,
y aun cuando Platón esté ausente, Sócrates piensa en el color
y la figura recogidos a través de los ojos, como la voz dulce
de Platón escuchada antes por los oídos, y así con cada una de
las particularidades advertidas por los cinco sentidos.

> […] Poco después, Sócrates, a través de la fantasía comienza a
> formular, en torno al simulacro de Platón, formado por la
> imaginación a través de los cinco sentidos, un juicio en estos términos:
> «¿Quién es este hombre de complexión alta y robusta, con esta frente
> tan espaciosa, de espaldas amplias […] con esta nariz aguileña, una
> boca un tanto chica y una voz un tanto suave?» Es Platón, un hombre
> bello y bueno, mi dilectísimo discípulo.@note

A lo que agrega:

> De esto resulta claro cuánto la fantasía de Sócrates supera a su
> imaginación: ésta formó una efigie semejante a Platón, pero no en el
> grado de conocer a quién y a qué tipo de hombre designa tal efigie,
> mientras la fantasía puede discernir que esa efigie es la de un hombre
> que lleva por nombre Platón, es la efigie bella de un hombre excelente
> y que ha sido sujeto de una larga amistad.@note

La fantasía, de este modo, constituye en Ficino el sentir mismo
porque en ella está ya presente el juicio particular sobre lo
sensible.@note Pero es aquí donde se plantea el problema crucial
de las relaciones entre lo particular y lo universal, la relación
entre juicio sensible y razón. La forma en que Ficino interpreta
el famoso pasaje del _Fedro_ platónico, según el cual la visión
terrestre desata el recuerdo de las ideas verdaderas, nos permite
advertir que, para el florentino, en el mismo instante en que
la fantasía intuye la sustancia, la bondad y la belleza de Platón,
se emprende el proceso intelectivo que es propio del alma racional:

> Así como la fantasía espontáneamente, en virtud de su naturaleza,
> formula un juicio singular sobre la cosa particular, así el intelecto por
> virtud natural, apéras la imagen de Platón se define en la fantasía,
> inmediatamente, sin recorrer algún pasaje lógico, trasciende el límite
> de este hombre determinado e instantáneamente concibe la naturaleza
> y la idea de un hombre común a todos los hombres.@note

Esta independencia del proceso intelectivo, desatada sólo por
la visión del fantasma, es crucial para entender las consecuencias
posteriores de la psicología ficiniana. De hecho, a pesar de
esta independencia, el intelecto retoma de nueva cuenta a la
materia propiciando una reflexión de segunda instancia en la
que, una vez vistas las ideas, aprehende la cosa particular y
establece la verdad de esa sensación.@note

La consistencia del proceso cognoscitivo dependerá, entonces,
de la posibilidad de un acuerdo de última instancia entre ambos,
intelección y sensación, a través de su homologación en el espíritu.
Este mismo acuerdo será también necesario entre los movimientos
de la voluntad y los procesos orgánicos en su totalidad, incluyendo
la digestión, el sueño reparador, la respiración, etcétera. De
hecho, la aportación neoplatónica al esquema de las facultades
del alma heredado de Aristóteles, es que el proceso en que operan
éstas no sólo es ascendente, sino también descendente. Una alteración
del espíritu impacta no sólo en la capacidad de comprender, que
de suyo ya es grave, sino que lo hace también en la salud misma
del cuerpo. Es decir, paralelo al proceso cognoscitivo, el espíritu
ejecuta transformaciones ontológicas que modifican al ser mismo,
al hombre mismo.

※ {.espacio-arriba1 .centrado}

Marsilio Ficino era un hombre preocupado. Su horóscopo le deparaba
cualquier clase de infortunios.@note En una carta a su amigo
Cavalcanti, escribe: {.espacio-arriba1 .sin-sangria}

> Parece que Saturno me imprimió el estigma de la melancolía desde el
> principio, colocado como está en el centro de mi ascendente Acuario,
> e influenciado por Marte, también en Acuario, y por la Luna en
> Capricornio. Está en ángulo recto respecto al Sol y Mercurio en
> Escorpión, que ocupan la novena casa. Pero Venus en Libra y Júpiter
> en Cáncer, tal vez, ofrecen alguna resistencia a esta naturaleza
> melancólica.@note

Para un hombre como él, en el amanecer del Renacimiento, esta
convicción no era sino la revelación de una virtud que podría
convertirse en un mal terrible. Era una señal y una advertencia.

De acuerdo con una larga tradición que recoge Ficino, el carácter
melancólico es característico del sabio. Gracias a éste, escribe:
«nuestra mente explora vehementemente y persevera en la investigación.
En aquello que trabaje, le es fácil encontrarlo, lo percibe con
claridad, lo juzga con pureza y lo conserva en la memoria mucho
tiempo».@note Pero, simultáneamente, la melancolía puede tornarse
morbosa y, entonces, se enfría el corazón, «carecemos de esperanza,
tememos todo y nos desagrada mirar el cielo».@note Aparecen con
ello «estados de manía o exaltación, seguidos de depresiones
extremas y letargos, causados por el humo negro, después del
fuego».@note

Tres son las causas de la melancolía, según Ficino. La primera
de ellas, celestial,

> porque Mercurio, que nos invita a investigar las doctrinas, y Saturno,
> que hace que perseveremos en su investigación y las retengamos cuando
> las hemos descubierto, dicen los astrónomos, son fríos y secos, como la
> naturaleza de la melancolía, según los médicos. Mercurio y Saturno
> imponen esta misma naturaleza desde el nacimiento a sus seguidores,
> los estudiosos, que perseveran y discuten en ello cotidianamente.@note

La segunda causa es natural, y consiste en que

> el alma de los estudiosos, especialmente de aquellos que estudian las
> ciencias más difíciles, tiene un movimiento que la lleva de las cosas
> externas a las internas, como de la circunferencia hacia el centro, y
> permanece inmóvil en el centro del hombre. Ahora, llevar su ser de la
> circunferencia al centro y fijarlo en el centro, es la propiedad de la
> tierra misma que €s análoga a la bilis negra.@note

Por último, la causa humana es «la frecuente agitación de la
mente [quel seca mucho el cerebro, por lo que, cuando la humedad
ha sido consumida, el calor se extingue; por esta cadena de eventos
el cerebro se torna seco y frío, que son las cualidades de la
tierra y la melancolía».@note

Una cuestión de temperatura y humedad que, sin embargo, está
implicada en el proceso mismo de producción del espíritu. Como
resultado de un ánimo ---la invitación de Mercurio y Saturno---,
una actitud ---la de investigación---, y un proceso físico ---la
agitación de la mente---, todos ellos análogos a la bilis negra,
que es densa y seca, la melancolía ha de entenderse, en primer
lugar, como la densificación del espíritu mismo, que impide claridad
sensitiva e intelectiva y que es, a la vez, causa del segundo
sentido de la melancolia: el proceso orgánico que produce esa
misma cualidad en la sangre, base fisica para la producción del
espíritu.

Veamos cómo es que estos tres niveles de la realidad humana,
el celeste, el natural y el humano, se van homologando, se hacen
correspondientes, en virtud de la reiteración de una imagen.
Pues ¿bajo qué otra forma, sino como imagen, hemos de comprender
que el ánimo influye para enfriar y secar al hombre, ya que así
influye la presencia de Saturno y Mercurio? ¿De qué otra forma,
también, ha de entenderse que el movimiento circular, que lleva
de la circunferencia hacia el centro, impone a la sangre y al
hombre en su conjunto la misma naturaleza de la tierra?

La clave está en ese territorio de nadie y en su capacidad de
homologar los distintos reinos, de establecer un punto de concordancia
donde lo que el alma aprehende se traduce en cuerpo, y la operación
del cuerpo, por ejemplo, la sangre que se concentra en el cerebro,
se traduce en fantasmas. En otras palabras, la capacidad de hacer
del cuerpo imagen, tanto como de la imagen cuerpo. En este sentido,
la apuesta fuerte de Ficino es que aquello que capta el intelecto,
a saber, la bondad y la belleza, tienen el poder de ser sentidos,
de volverse cuerpo, de ser vistos y escuchados, a la vez que
están en el fantasma de las cosas que miramos. Más radicalmente
platónico no puede ser Ficino: la belleza y, con ella, la bondad,
se ve.

El alma de un hombre que no vale nada se encuentra en armonía
minima consigo misma; esto es, que sus sentidos desentonan de
la razón y sus apetitos son, entre sí, contrastantes. El alma
de un hombre justo es armónica en todas sus partes. O bien, si
el alma es armonía, el alma de un hombre honesto que presenta
un mayor acuerdo entre todas sus partes, es más armonía y, por
ende, más alma.

> El alma, por otra parte, es vida, de donde, si es más alma, es que es más
> vida y por ello el alma de un hombre honesto será más robusta que
> aquella de un ladrón. Ni el alma de un ladrón operará una digestión
> igual a la del hombre justo, ni estará en condición de generar una
> prole como el alma del justo, ni, como aquella, de mantener y mover
> los miembros; no será capaz de pensar con agudeza, de recordar
> sólidamente, de hablar con elegancia, de afrontar con energía las
> acciones ni de respirar largamente.@note

Pobre ladrón. Su condición deshonesta no es sólo una cuestión
de juicio, sino que se siente y es sentida, es una cualidad de
su espíritu, que se transmite lo mismo a su capacidad de entender
que a su digestión.

Pero veámoslo con claridad: para Ficino, la ética se juega no
sólo en el plano de las conductas y los juicios, sino en el sentir
mismo, en la forma en que percibimos, porque sobre esto recae
la guía de nuestros movimientos y nuestros pensamientos, la forma
misma del cuerpo, la pureza o morbosidad de sus procesos orgánicos.
Sií, sentir para Ficino, envuelve una ética.

※ {.espacio-arria1 .centrado}

La posibilidad de toda la magia talismánica está fundada en la
cualidad que Ficino atribuye al espíritu. Los talismanes no son
otra cosa que portadores de imágenes, construidos con materiales,
movimientos y órdenes astrológicos, que también se traducen en
imágenes. Pero lo decisivo aquí no es que los talismanes puedan
tener una influencia, sino que son obras de los hombres, que
constituyen una manipulación de los fantasmas o, dicho de otra
manera, del sentir de los hombres. {.espacio-arriba1 .sin-sangria}

De esta forma, la actividad del mago que crea y construye los
talismanes, que evoca o aleja a los demonios ---compuestos de
la misma materia que el espíritu--- se presenta como la capacidad
de manipular fantasmas, de modificar y restituir, a través de
imágenes, las formas propias del sentir. En suma, se trata de
una actividad simple y llanamente ética.

Bajo esta luz, la magia no aparece sólo como una operación sin
más, a la que, por cierto, Ficino no temería declarar imaginaria
y fantástica, sino como la operación ética radical. Es en el
espíritu, es en la imaginación y es en la fantasía donde se disputan
el terreno de la indiferencia y la no indiferencia del hombre,
la posibilidad de distinguir y valorar, o la fría mirada de aquel
que no establece diferencias. En última instancia, es en el espíritu
donde se mezclan o distinguen los venenos de la Gorgona.

Podemos prescindir de la idea del espíritu ficiniano, podemos
objetar su teoría del conocimiento, podemos ser suspicaces, si
queremos, respecto al poder de los talismanes. Pero al menos
yo no puedo dejar de sentir un enorme desasosiego al preguntarme
si al sentir estamos dando un paso en el camino que lleva hacia
la virtud 0 al vicio; 0, algo mucho peor, si al no sentir estamos
dejando de reconocer que hay un camino que es mejor que el otro.

# Cazadores de almas

Para los B.: Flor, Vanesa, Daniel, lván y Esteban {.epigrafe}

++Escribe Giordano Bruno++ en _De magia_: «Todos los poderes
mágicos, activos y pasivos, y sus especies, dependen de los vínculos
mágicos».@note

Esta afirmación, que precede a la explicación de cómo Plotino
evitó ser dañado por el hechizo de un egipcio, es muy reveladora.
El énfasis no está puesto en el control de los poderes de la
magia, instrumentos que los magos utilizan para hacer sus encantamientos,
según la clasificación inicial hecha por el propio Bruno al comienzo
de _De magia_,@note sino en los vínculos.

De acuerdo con el nolano: «Para que las acciones ocurran en el
mundo, se requieren tres condiciones: 1) un poder activo en el
agente; 2) un poder pasivo o disposición en el sujeto o paciente,
que es una aptitud en él de no resistir o hacer imposible la
acción […] y 3) una aplicación apropiada, que se refiere a las
circunstancias de tiempo, lugar y otras condiciones».@note

Esto es propiamente el vínculo: la confluencia de estas tres
condiciones que, juntas, producen la acción. Puesto que se dan
en el mundo, Bruno entiende que los vínculos no son eternos@note
y que no todo puede vincular a todas las cosas, y que, si lo
hace, no puede hacerlo de la misma manera.@note Así, los vínculos
se presentan como momentos específicos dentro del flujo del tiempo,
que hacen de la magia no un dominio sobre las fuerzas físicas,
espirituales o divinas, sino la capacidad de percibir y reconocer
la naturaleza de esas conjunciones.

Dirá Bruno en _De vinculis in genere_: «Incluso un estúpido y
romo de pensamiento puede ver la belleza de las cosas naturales
y artificiales, aunque no pueda, al mismo tiempo, asir y admirar
el talento que ha generado todas las cosas. Para él, “las estrellas
no hablan de la gloria de dios”».@note Esta singular afirmación
aparece en un apartado que lleva por nombre «Cómo el arte vincula»
y señala llanamente cómo el artista divino interactúa incluso
con quienes no pueden admirar su talento, a través de la belleza.
Pero no podemos pasar por alto que, al afirmar lo anterior, Bruno
está haciendo una distinción entre dos sensibilidades: la que
define al estúpido, capaz sólo de admirar los efectos, y la del
sabio, que puede escuchar las estrellas.

Esta misma idea está enfatizada en la parábola de los nueve ciegos
con que concluye _Los heroicos furores_. Ahí, Bruno hace decir
a Minutolo:

> He advertido un pasaje en el que se dice que necios y dementes son
> todos aquellos cuyo sentido resulta extravagante y desviado del universal
> sentido común a todos los hombres. Mas tal extravagancia puede darse
> de dos maneras, según que se salga de lo común ascendiendo más alto
> de cuanto se alzan o pueden alzarse todos o la mayor parte […] o bien
> descendiendo más abajo, hacia donde se hallan quienes están faltos de
> sentido y razón en mayor medida que la multitud y la medianía.@note

La razón por la que Plotino no fue presa del encantamiento de
su enemigo egipcio fue, precisamente, que su sensibilidad era
excepcional, pues podía sentir ---ver, escuchar, imaginar---
las conjunciones. De ahí que, antes de ser vinculado con un poder
dañino, con un demonio inefable, fue capaz no sólo de evitar
el vínculo, sino de revertirlo. Al final, su antagonista egipcio
sintió los efectos de este vínculo revirado, pero no pudo asir
al artista que lo creó. Y éste es, justamente, el otro lado del
horizonte: si el sabio es capaz de percibir las conjunciones
y actuar gracias a ellas, quienes son objeto de un vínculo mágico
natural o artificial, sólo perciben y padecen los efectos.

> Tres son las puertas a través de las cuales los cazadores de almas se
> atreven a vincular: la visión, el oído y la mente o imaginación […]
> Aquél que entra a través del oído está armado con su voz y el discurso,
> el hijo de la voz. Aquél que entra a través de la puerta de la visión está
> armado con formas, gestos, movimientos y figuras adecuadas. El que
> entra a través de la puerta de la imaginación, mente y razón, está
> armado con costumbres y artes. Después de esto, lo primero que
> ocurre es la entrada, después la atadura, luego el vínculo y, en cuarto
> lugar, la atracción.@note

Sólo subrayo lo que es obvio en el texto: los vínculos se establecen
a través de los sentidos; son, si se me permite decirlo, sentidos,
en la doble acepción de ser percibidos y de formar un sentido
en las cosas. Pero esto que se desprende claramente del texto
tiene implicaciones que van más allá de la pura caracterización
de la fuerza mágica de los vínculos. En realidad, y como han
querido sostener R. Klein y I.P. Couliano,@note la teoría de
los vínculos es el nudo central de la psicología de Giordano
Bruno; pero no por su carácter sintético o emblemático de una
tradición, una era o una práctica que, en un momento dado, puede
asociarse ---no sin problemas--- con el psicoanálisis, sino porque
hace de la sensibilidad un proceso multivoco y dinámico de asociaciones,
enfrentado al proceso de ascenso y descenso de datos perceptuales
y estructuras de análisis, característico del proceso cognoscitivo
desde Aristóteles hasta Marsilio Ficino.

Para explicar esta novedad en detalle, es necesario que nos detengamos
a examinar algunas de las ideas sobre las cuales se construye
la psicología renacentista y, en particular, su teoría de la
sensibilidad, a partir de la cual Giordano Bruno reflexiona y
construye su propia tesis.

Una de las constantes del pensamiento renacentista es la inquietud
por la intermediación. Es decir, por aquello que teje la red
de relaciones entre los dos niveles básicos de la realidad: el
divino y el material. La preocupación no es nueva, por supuesto;
las fuentes herméticas de la filosofía del Renacimiento ya apuntan
en esa dirección. En el famoso pasaje del diálogo hermético _Asclepio_,
que recoge Giovanni Pico, se dice del hombre: «Tal es su posición,
su privilegiado papel intermedio, que ata (diligat) a los seres
que le son inferiores, y es amado por aquellos que le son superiores».@note
Pero si el hombre, como totalidad y en esencia, posee un papel
intermedio, dentro de él mismo hay algo que realiza esa función
de intermediación y atadura entre los dos planos de su propia
existencia: ésa es la labor de los sentidos interiores y de su
vehículo, los espíritus.

Sabemos ya que Aristóteles distingue una serie de funciones dentro
del alma humana que explican el proceso de la percepción: el
sentido, la memoria y la fantasía, que terminan por desembocar
en el juicio.@note Este esquema será retomado más adelante por
Plotino, quien integrará la visión aristotélica con el concepto
de _pneuma_ estoico. Así, explica Summers, «Para Plotino este
cuerpo pneumático era la forma somática que el alma retiene de
los astros en su descenso hasta la tierra. Una vez en la tierra
media entre las partes superiores y las inferiores […]».@note
De esta manera, las funciones del alma aristotélica ---sentido,
memoria, fantasía y juicio--- serán las funciones de un nuevo
cuerpo sutil, colocado entre la pura materialidad de los sentidos
exteriores y la inmaterialidad del _nous_.

Sin embargo, a pesar de esta síntesis, el tema central de la
naturaleza de la sensación y la conciencia, como fenómenos psicológicos
interiores del hombre, no está resuelto aún en Plotino. Para
él, la visión y la percepción en general son procesos de asimilación
de la forma a través de los sentidos ---que por ende toman la
forma de la cosa, por ejemplo, el color--- y que se vinculan
con las ideas para producir el juicio mediante la simpatía.@note

Será San Agustín quien introduzca un concepto crucial para el
pensamiento de Occidente: la idea del sentido interno. Las funciones
de la percepción según Plotino quedarán reagrupadas bajo esta
nueva idea, cuya estructura ontológica corresponderá a la del
_pneuma_ estoico, revestido con el nuevo concepto de espíritu.@note
Lo decisivo de la idea agustiniana de sentido interno, empero,
será su distinción de los sentidos exteriores. Según explica
en _De libero arbitrio_: «¿No ves igualmente que el sentido de
la vista percibe los colores, pero que el sentido no se percibe
a sí mismo? Porque el sentido por el que ves el color no es el
mismo por el que ves que él ve».@note Con ello, San Agustín instituye
la existencia de un tercer reino, distinto del material y del
divino: el espiritual, en el que se lleva a cabo la percepción
y se construye la conciencia, cuya nota distintiva es su carácter
interno, frente a la exterioridad de la materia y la divinidad.
Un reino, el de la sensibilidad como tal, cuya verdad es incuestionable.

Sobre la base de estas premisas, a lo largo de la Edad Media
se lleva a cabo una discusión amplia y variada en torno a la
sensación y su espiritualidad. Las aportaciones de Santo Tomás,
Alberto Magno, Hugo de San Víctor y Raimundo Lulio, entre muchos
otros, son decisivas.

Apuntemos, sin embargo, la forma en que estas ideas básicas son
retomadas por los renacentistas para construir el entramado de
la psicología humana. Así, dirá Marsilio Ficino en _De amore_:

Tres cosas hay sin duda en nosotros: alma, espíritu y cuerpo.
El alma y el cuerpo son de naturaleza muy diferente, y se unen
por medio del espíritu, el cual es un cierto vapor sutilísimo
y lucidísimo, engendrado por el calor del corazón desde la parte
más sutil de la sangre. Y esparciéndose de aquí por todos los
miembros, toma la virtud del alma, y la comunica al cuerpo. Toma
también por los instrumentos de los sentidos las imágenes de
los cuerpos de afuera; imágenes que no se pueden fijar en el
alma porque la sustancia incorpórea, que es más excelente que
los cuerpos, no puede ser formada por ellos mediante la recepción
de las imágenes; pero el alma, por estar presente en el espíritu,
en todas partes sin esfuerzo ve las imágenes de los cuerpos como
reluciendo en un espejo, y a partir de ellas juzga a los cuerpos.
Y tal conjunción es llamada sentido por los platónicos. Y mientras
mira, por su virtud concibe en sí imágenes semejantes a aquéllas,
y aún más puras. Y tal concepción se llama imaginación y fantasia.@note

Subrayo dos ideas fundamentales: una es la que muestra la función
del espíritu como un espejo de las cosas al que puede acceder
el alma ---y que debemos entender literalmente como reflexión---,
resultado de la acción propia de la imaginación. La otra es la
función del espíritu como el lugar en que se incuban las ideas
que genera el alma por sí misma ---lo que llamaríamos intuición---
y que es resultado de la acción propia de la fantasía.

Así, el espíritu es el lugar interior, el territorio en que el
alma accede a las cosas materiales, reflejadas en el espíritu
como imágenes, mientras que el cuerpo accede a la luz de la inteligencia,
a través de su reflejo en el espíritu como fantasía. Se entiende,
entonces, que es en el espíritu donde el ascenso y el descenso
de las percepciones y las ideas forman la sensibilidad del sujeto,
a partir de las funciones de los dos intermediarios entre el
reino de lo material y lo divino: la imaginación y la fantasía.

Estos dos conceptos son poco claros en el Renacimiento, por la
tendencia de los autores a asimilarlos uno al otro y fundirlos,
en ocasiones, con la función del sentido en general. Sin embargo,
es posible subrayar un matiz, generalmente aceptado, para ambos.
En un pasaje de la _Teología platónica_, Ficino explica las funciones
del espíritu y aporta varios elementos importantes para este
análisis. En primer lugar, la autonomía del intelecto con respecto
a la fantasía, y la función central de ésta para traducir los
universales en juicios particulares. Así, incluso dentro del
espíritu, entre el puro sentido y la inteligencia, intermedian
imaginación y fantasía; son los traductores entre el lenguaje
del cuerpo y la luz de la inteligencia. El primero aporta una
imagen completa de algo, en tanto que la segunda aporta el discernimiento
de las cualidades estéticas y éticas de ello.

Pero entendámoslo bien, este juicio que discierne las cualidades
universales en el particular es fantástico. La fantasía, así,
es la función intermediadora última del espíritu y, por ello
mismo, la función que al vincular establece un sentido: porque
la voz suave, las espaldas anchas, la boca pequeña, no son ni
bellas ni buenas, como no son ni feas ni malas. Sólo en el momento
en que su condición particular, específica, es puesta en un sujeto,
por ejemplo, pueden ser puestas en contacto con la universalidad,
adquieren un sentido, una orientación.

¿Por qué digo una orientación, y no un valor? Porque son fantásticas
y, por ello, son entendidas más como una sensibilidad que como
un juicio racional.

※ {.espacio-arriba1 .centrado}

Al tratar el tema de los conceptos de aria y maniera en los críticos
de la pintura del Renacimiento, David Summers apunta:
{.espacio-arriba1 .sin-sangria}

El «aire» es en última instancia un reflejo del propio «aire»
o espíritu del pintor […] [pues] la imagen no sólo se asemeja
a su modelo como el hijo al padre: también se asemeja al pintor,
su padre, y parece tener propia vida gracias a que plasma el
_spiritu_s del pintor […] La _phantasia_ ---y de hecho, el alma
del pintor--- representa a un tiempo la «personalidad» del pintor
y la fuente última de la animación virtual de la imagen.@note

De esta forma, más que conocimiento racional, la fantasía proporciona
luz, para utilizar una metáfora a la vez plástica y filosófica.
Esa luz, esa claridad, es la que es propia de la fantasía, es
la que se plasma en el cuadro, es la que da vida, la que, abusando
ahora abiertamente de la ambigiedad de la palabra, da sentido,
más que razón a las cosas. De esta manera, la fantasía es lo
constitutivo de la sensibilidad; el eje de tramas y urdimbres
que van acomodando toda percepción interior y exterior, moldeando
en el interior del sujeto la forma en que siente que existe.
Al igual que Ficino, Bruno concibe el espíritu no como algo privativo
de los hombres, sino como una realidad, un mundo en sí mismo:

> […] hay tres diferentes mundos a ser distinguidos: el arquetípico, el
> físico y el racional. Amistad y odio están localizados en el mundo
> arquetípico, fuego y agua en el mundo físico, y luz y oscuridad en el
> mundo matemático. Luz y oscuridad descienden del fuego y el agua,
> que a su vez descienden de la amistad y el odio. Asi, el primer mundo
> produce el tercero a través del scgundo, y el tercer mundo es reflejado
> en el primero a través del segundo.@note

Para Bruno hay, pues, una fantasía universal en la que se encuentran
todos los vínculos, a través de los cuales se fundan los dos
movimientos básicos del alma: la generación y la conciencia.
Lugar de cruce donde todo cobra sentido; en él el antagonismo
entre la amistad y el odio se celebra en el mundo, mientras que,
a la inversa, las cosas físicas son comprendidas por la luz de
la amistad y el odio. A ese espacio tiene acceso, por supuesto,
la propia fantasía humana. De manera que no sólo produce ella
misma los vinculos y otorga sentido a las cosas, sino que los
recibe. La percepción de la belleza ---quizás el arquetipo del
vínculo en Bruno--- no sólo ata, sino que establece el sentido
de esa atadura.

Cuando la imagen del ser amado ---el ejemplo preferido del Renacimiento---
se instala en la fantasía, los fantasmas que formamos en torno
a esa persona no sólo dicen que nos encanta, también dicen cómo
nos encanta. La fantasía establece el nudo y el orden de cosas,
la conjunción en que ese nudo se ha producido; señala cuál es
la estructura del cosmos, todo lo que explica que ese ser en
particular haya sido atado de esa forma.

De ahí que, al saber cómo nos encanta, ilumina, da sentido a
nuestro ser, a la otra persona, y al universo todo; una posición
envidiable, para cualquiera que descubra el misterio del hechizo,
porque lo coloca en un franco dominio de uno mismo que es la
condición esencial de acción del hombre. Por ello, Couliano señala
que, para Giordano Bruno ««[…] nadie puede escapar del círculo
mágico: cualquiera puede ser manipulado o un manipulador»,@note
y la pregunta es cómo ser manipulador y no manipulado ---por
las cosas o los hombres---. Cómo ser activo y no pasivo.

# Magos

No enste el pasado ni el futuro: todo lo que ha sido y todo lo
que será, es. {.epigrafe}

++Imaginemos a un hombre++ que se levanta por la mañana sudoroso.
Ha pasado una noche inquieta, llena de sueños. Se levanta, pues,
con la cabeza sumergida en el sopor febril del insomnio preguntándose:
¿Me desterrarán? ¿Me darán una embajada? ¿Seré Senador? ¿Tendré
suerte en los negocios? ¿Me amará? ¿Morirá su marido?@note Este
hombre podría estar despertando hoy, aquí, o bien, el IO de agosto
del año 15I d.C, en Constantinopla. La vida cambia tan poco.
Sin embargo, ¿qué es lo que iguala en su inquietud a un hombre
actual y a uno de la Antigiedad?, ¿qué es lo que ha cambiado
mínimamente en cerca de veinte siglos?

Como claramente lo vieron los movimientos gnósticos maniqueos
florecientes hacia el siglo +++II+++, la incertidumbre de nuestro
hombre no tiene su origen en que se pueda equivocar al elegir
el camino, tomando la senda del mal en lugar de la del bien,
y haciendo fracasar así todos sus deseos. Al contrario, es el
hecho mismo de la necesidad de elegir, que implica una mediación,
una distancia obligada entre lo que desea y aquello que lo satisface,
lo que induce a la zozobra. Esta mediación se manifiesta en la
disparidad de caminos para satisfacer el deseo, pero es, ante
todo, signo de la existencia temporal que vive nuestro hombre:
el deseo no se satisface inmediatamente, hay una sucesión de
instantes entre el momento del deseo y el momento de la satisfacción,
que deja la puerta abierta para que el cumplimiento del deseo
se haga incierto y que dependa de la voluntad de otro, las condiciones
climáticas, los dioses, la aparición de un gato negro.

Pero nuestro hombre no puede dejar de desear y no ve, con las
preguntas que le quitan el sueño, cuándo podrá dormir tranquilo.
Y aunque busca la tranquilidad, no sabe que oscila entre dos
caminos: la resignación ante la indiferencia del mundo para con
sus deseos, que lo orilla a reducirlos al mínimo y a soportar
estoicamente la zozobra; o bien, la voluntad de revertir el orden
temporal haciendo que el desear se satisfaga a sí mismo en el
desear.

※ {.espacio-arriba1 .centrado}

Imaginemos ahora a otro hombre. Él está despertando el 25 de
abril de 343 d.C, en la residencia imperial de Macellum en Capadocia.
Según Amiano Marcelino es «de estatura mediana, […] de aspecto
radiante por el encanto de sus fulgurantes ojos, que [son] señal
de la agudeza de su mente».@note Sin embargo, no es un hombre
común: se llama Flavio Claudio Juliano, es hijo de Julio Constancio
y Basilina, y será, en muy pocos años, un mago.
{.espacio-arriba1 .sin-sangria}

En el momento en que lo vemos levantarse ya ha tomado conciencia
de que su tío, el emperador Constancio, es el asesino de su padre,
de su hermano mayor, y quien lo mantiene apartado del mundo.
Años más tarde, Juliano recordará ese momento de conciencia:
«Cuando estuvo criado y se hizo muchacho en el que _apunta la
primera barba y cuya edad es la más encantadora_ comprendiendo
la cantidad de males que habían sobrevenido a sus familiares
y a sus primos, estuvo a punto de arrojarse al Tártaro, estupefacto
ante la magnitud de esos males».@note

La vida juvenil de Juliano es un ejemplo de incertidumbre y zozobra
permanente. No se explica cómo sobrevivió a la cacería familiar
emprendida por Constancio, ni por qué éste, al que todavía no
ha visto, lo mantiene aún con vida. Son momentos de desesperación,
de anhelo de muerte y de olvido. Pero «Helios, benévolo, junto
con Atenea Providencia, le sumergió en una especie de sueño profundo
quitándole esa idea; después, al despertar, se retira a la soledad.
A continuación, encontró una pequeña piedra donde descansó y
examinó en su interior de qué manera podría escapar a la magnitud
de tantos males».@note

El sueño tiene algo de resignación estoica que le han inculcado
desde temprana edad el obispo Eusebio y Maridonio, y que le acompañará
toda su vida. Pero el sueño no es suficiente. En su búsqueda
interior y liberado de Macellum, pero no de la vigilancia del
emperador, Juliano viaja hasta Nicomedia, donde conoce a Crisanto,
un neoplatónico ligado a la escuela de Jámblico; ahí tomará contacto
con la teurgia. Eunapio cuenta que

Crisanto dice a Juliano que:

> Máximo […] no hace mucho nos convocó a un santuario de Hécate
> […] Cuando llegamos […] nos dijo: «Sentaos queridísimos amigos, y
> mirad lo que va a suceder […]» quemó [entonces] un grano de
> incienso y recitó en voz muy baja cierto himno y obtuvo tal éxito […]
> que la estatua empezó a sonreir y, a continuación, a reír abiertamente
> […] Pero no debes maravillarte de estas cosas, como yo no lo hago,
> sino pensar que lo más importante es la purificación por medio de la
> razón. Sin embargo, al oír esto, el divino Juliano dijo: «Adiós […]a
> mí me has revelado lo que andaba buscando» y, tras decir esto, besó la
> cabeza de Crisanto y marchó a Efeso[…]@note

En busca de Máximo, claro. Se le habían abierto las puertas de
la acción.

※ {.espacio-arriba1 .centrado}

Volvamos a nuestro primer hombre. La diferencia que lo lleva
a elegir entre la resignación y la rebeldía tiene que ver con
la indagación que haga de sí mismo y de la naturaleza del cosmos.
{.espacio-arriba1 .sin-sangria}

Resignarse implica creer en la crueldad, en el mal o en la falsedad
de la vida sujeta a la temporalidad, que escinde en momentos
sucesivos la existencia y que, al final, la interrumpe. Rebelarse
es asumir que la temporalidad es un elemento en el camino de
la belleza y la bondad, que es su única ruta. Para los resignados
hay siempre la esperanza en otro mundo, antitético al que se
vive, y que se espera pasivamente alcanzar. Para el que actúa,
en cambio, éste es el único mundo y su esperanza está puesta
en la renovación infinita de la vida.

Para la magia, que hunde sus raíces en el neoplatonismo y el
hermetismo, el cosmos es un cosmos abierto que es vida 0, a decir
de Platón, _psyché_. Pero como la vida es también generación
eterna, «el mundo encuentra su alimento en su propia destrucción»,
dice Platón. Y Hermes Trismegisto afirma: «la causa de la muerte
es Eros».

El cosmos es vida porque es muerte. Y en la sucesión de vida
y muerte, y precisamente porque se suceden, es eterno. Se trata
de un cosmos construido sobre la permanente conjunción de providencia
y necesidad, de tatisfacción y deseo, y cuyo fruto no es otra
cosa que la generación: la sucesión ordenada del cambio, la providencia,
que contrarresta el frenesí mutante de la necesidad, dando al
devenir un sentido único, la eterna renovación de sí mismo. «Lo
divino ---dice Hermes--- es principio y sustancia, y energía,
y necesidad, y fin y renovación».@note Por ello el cosmos es
amo de la vida y de la muerte de las criaturas. «Siempre quien
opera es quien manda y aquello sobre lo que actúa es quien es
mandado».@note

¿No es ésta una señal para el hombre?

※ {.espacio-arriba1 .centrado}

Juliano nos ha dejado constancia de su concepción de la naturaleza
del cosmos y del devenir en dos discursos excepcionales: _A la
madre de los dioses_ y _Al Rey Helios_. {.espacio-arriba1 .sin-sangria}

En el primero, a partir del mito de Atis y la Madre de los dioses,
Juliano revela la perpetuación de la generación del cosmos. Átis
es el dios fecundo que refleja su rostro en el río Galo, la Vía
Láctea. La Madre de los dioses al verlo se enamora de él, entregándole
todos los poderes. Pero Atis ama en realidad a la Ninfa, es decir,
lo que fluye, y se entrega con su poder a la generación.@note
Enterada y loca de celos, la Madre de los dioses lo castra: «¿Y
qué es la mutilación? ---pregunta Juliano--- La interrupción
del infinito. Pues la generación está retenida por la providencia
creadora de formas limitadas, no sin la llamada locura de Átis
que, al colocarse fuera y superarse a sí misma, es como si se
agotara y no pudiera superarse a sí misma».@note

En el discurso _Al Rey Helios_, Juliano completa esta imagen
del cosmos como la perpetua ocurrencia de la generación y la
castración. Helios, en su calidad de enlace entre la inteligencia
y el cosmos, es la entidad que perpetúa el contacto y la gestación:
«Se dice, en efecto, que aunque no todos estén dispuestos a admitirlo,
el disco solar se mueve mucho más alto, en la región sin astros,
que las estrellas fijas, y así no ocupará el centro de los planetas,
sino el de tres mundos, según las hipótesis mistéricas».@note
Y un poco antes afirma:

> […] desde otro punto de vista, uno solo es el demiurgo del universo,
> pero muchos son los dioses demiúrgicos que recorren circularmente el
> cielo. Por tanto, hay que situar como mediadora de ellos la actividad
> demiúrgica de Helios, que desciende hasta el mundo. Pero además, si
> la fecundidad vital es mucha y superabundante en el mundo inteligible,
> es evidente que también el mundo está lleno de esta fecundidad.@note

Pero al igual que Atis, castrado, Helios cierra el círculo de
la generación: «Es una naturaleza así la [de Helios que] […]
con su medio movimiento endereza y despierta al acercarse, mientras
que al alejarse las disminuye y destruye».@note

Juliano mira un cosmos «divino y bellísimo»@note que es obra
en interminable gestación, activo y eterno. Compuesto de ciclos
rítmicos que dan sentido a la vida y a la muerte, que conforman
en su sucesión el camino perenne al bien… Placer y dolor, deseo
y providencia, vida y muerte, son parte de la superabundancia
de la fecundidad. Es un cosmos, en suma, que invita a la gestación
y al obrar.

※ {.espacio-arriba1 .centrado}

> El amor, Sócrates […] _no es amor de lo bello como tú crees_.
> {.espacio-arriba1 .sin-sangria}

> ---¿Pues qué es entonces?

> ---Amor de la generación y procreación en lo bello.@note

Esto enseña Diótima a Sócrates en el Banquete. El sentido de
sus palabras, sin embargo, tiene un alcance mucho mayor, se refiere
a la misión de los hombres, a la necesidad de generar, «[…] porque
la generación es algo eterno e inmortal en la medida en que puede
existir en algo mortal».@note

El hombre que vimos despertar sudoroso y que ha elegido la acción,
convencido de la bondad del cosmos que deviene, busca ahora la
respuesta que lo libere de los tormentos en que vive.

La magia le propone, al igual que el gnosticismo en el que nace,
un retorno al hombre originario. Pero este retorno no es, como
no lo es en el caso del mito de Atis, un regreso temporal. Se
trata, por un lado, de una conciencia de la verdadera naturaleza
humana que yace en cualquier hombre; pero, por otro, de una actitud
ética que se desprende de asumirse como se es.

El hombre es eros. Un ser intermedio. Un dáimon al decir de Platón,
que es lleno y vacío, loco y astuto. Hijo de Poros y Penia, el
más viejo y el más joven, el hombre es un ser inconcluso, pero
con el ingenio que le permite llenar esa falta. Recordemos:

> Por ello, Asclepio, es tan gran maravilla el hombre; pasa a la naturaleza
> de un dios como si él mismo fuera dios; tiene trato con el género de
> los demonios, sabiendo que ha surgido del mismo origen […] tal essu
> posición, su privilegiado papel intermedio que ama a los seres que le
> son inferiores, y es amado por aquellos que lo dominan […] todo le
> está permitido […] es todas las cosas a la vez, a la vez está en todas
> partes.@note

Así lo define Hermes Trismegisto en un pasaje del Asclepio. Es
la conciencia de la dualidad, de la condición mortal y divina,
erótica engendradora del hombre, esa naturaleza original a la
que debe volverse. Pero la consecuencia que sigue a esta conciencia
es la asunción del hombre como demonio, mitad divino, mitad mortal,
al que todo le está permitido: «Habiendo puesto en tu pensamiento
que nada hay imposible para ti, estímate inmortal y capaz de
comprenderlo todo, todo arte, toda ciencia, el carácter de todo
ser viviente[…]».@note

※ {.espacio-arriba1 .centrado}

Una noche de febrero de 360, los enviados del emperador Constancio
a las Galias ordenaron a las tropas partir para atacar Sapor
en Persia. Pero los hombres bajo el mando de Juliano, César de
las Galias, se rebelan. No lo quieren dejar solo. Desconociendo
la orden y la dignidad de Constancio, esa noche las tropas aclaman
Augusto a Juliano. Lo quieren como nuevo emperador.
{.espacio-arriba1 .sin-sangria}

La situación entonces se torna tensa. Un año después, Juliano
recibe noticias de que Constancio se prepara para atacar e inicia
una marcha contra su emperador que le lleva cuatro meses, de
julio a octubre. De pronto, sin saberlo, Juliano ha pasado a
la acción.

5 de octubre. La tensión ha alcanzado su punto máximo. La guerra
civil es inminente. Pero Constancio muere ese día y, en un acto
paradójico, nombra heredero a su enemigo. Juliano será emperador
por aclamación de su ejército y por sucesión imperial. Ocupa
ahora esa otra posición al margen de la ciudad que es cabeza
del imperio.

Para Juliano, sin embargo, su nueva posición lo consterna. Escribe
a Temisto temeroso de su misión: «De la misma manera, dios, que
ama a los hombres, nos colocó la raza superior a la nuestra de
los démones, que con toda facilidad tanto de su parte como por
la nuestra, se ocupa de nosotros y nos proporciona la paz, respeto
y justicia en abundancia, haciendo aparecer entre las razas de
los hombres la concordia y la felicidad».@note

Juliano está ante el reto de reconocerse como dáimon, eros, compartir
lo divino y lo humano. Pero ello no debe entenderse como el gobierno
de los hombres sino como la condición de ser el soberano de sí
mismo.

> Si […] decimos que sólo encuentran la felicidad en la gestión de los
> actos de los asuntos públicos los que son soberanos y reinan sobre
> muchos hombres ¿qué diremos de Sócrates? […] Sócrates que había
> rechazado la contemplación abrazando la vida activa y que no era dueño
> ni de su propia esposa ni de su hijo ¿tenía al menos poder sobre dos o
> tres ciudadanos?, ¿acaso no llevaba una vida activa porque no era
> dueño de nadie? […] Todos cuantos hoy se salvan gracias a la filosofía,
> se salvan gracias a Sócrates.@note

El mago es el soberano de sí mismo. Por eso actúa en los márgenes
de la _polis_. Volver al origen, constituirse en dáimon, imitar
a los dioses, sólo lo hace quien, como Sócrates, o como Juliano,
actúan más allá del orden de la ciudad, porque gobiernan sobre
sí mismos. «El filósofo […] afirmando sus palabras con la acción
y mostrándose tal cual quiere que los demás sean, se puede hacer
mucho más convincente y, en lo que respecta a la acción, más
eficaz que los que se lanzan a las bellas acciones bajo una orden».@note

Preñado de sus convicciones mágicas y teúrgicas en un imperio
vuelto al cristianismo, Juliano acompaña sus palabras con la
acción. Predica la tolerancia para restituir la dignidad a los
cultos no cristianos, atiende a las ceremonias, promulga leyes,
renueva las estructuras municipales, asiste al Senado, es austero,
lleva una vida activa, y prodiga el don de la magnanimidad.

Amiano lo acusa de ser inconstante y locuaz, pero reconoce su
capacidad de aceptar sus errores y, sobre todo, su prudencia.

Juliano se esfuerza por imitar a los dioses, por obrar de acuerdo
con su propio origen, divino y mortal. No obra las maravillas
que Crisanto atribuye a Máximo en el templo de Hécate, pero obra
maravillas: se ocupa de los hombres; proporciona respeto y justicia;
buscándola, hace aparecer la concordia y la felicidad entre los
hombres. Busca la paz: es magnánimo y prudente, dos valores centrales
de quien toma conciencia de sí a partir de su origen demoniaco.

5 de marzo de 363. Juliano se decide a atacar Sapor. Comete errores
tácticos. 20 de junio. Ante una repentina escaramuza, Juliano
se precipita al ataque sin coraza, sin pensar en la defensa de
su vida. Una lanza surgida de cualquier parte lo hiere. En la
noche, a imitación del viejo Sócrates, diserta sobre la naturaleza
del alma y afirma, con su último aliento, su alegría por haber
vuelto a la naturaleza, a la physis, a lo que está más allá de
la polis…

> Ahora es amigos, el momento más oportuno para abandonar esta vida,
> ahora que estoy contento de volver a la naturaleza […]
> No me arrepiento de lo que he hecho ni me atormenta el recuerdo de
> ninguna ofensa grave […] conservé mi alma sin mancha ---según creo---
> como corresponde a su origen divino […] permanecí firme,
> acostumbrado como estoy a pisar las tormentas del azar […]@note

Murió poco después sin nombrar heredero y sin esperar la apoteosis
que él mismo ya había ganado.

※ {.espacio-arriba1 .centrado}

A nuestro hombre, que somos nosotros mismos, Juliano dejó al
morir una máxima: «En filosofía hay un solo principio y fin,
conocerse a sí mismo y hacerse semejante a los dioses».@note
{.espacio-arriba1 .sin-sangria}

# Un dios amoroso

++Hallábase Ramón++ molesto en una tiniebla que intentaba comprender
sin ningún resultado. La tiniebla era y seguiría siendo inescrutable
y, así lo quisiera, Ramón no podría discernir ni la parte n1
el todo de aquélla.

Grandiosa, indefinible, beatífica y divina, la tiniebla era eso,
sólo luz intensísima que hacía imposible mirar, que cegaba, que
hundía en las tinieblas los ojos. Pero Ramón seguía ---detenido
o en marcha ¿quién lo sabe?---, seguía sumido en aquella tiniebla.

Intuyó en ella una presencia. En medio de su confusión, Ramón
se afanó en mirar a través de aquella luz oscura, cierto de que
en algún lugar de esa oscuridad, algo o alguien, hacía esfuerzo
parecido.

Pensó que podria ser Dios, y tuvo un instante de arrobo, pero
al fin, cobrando valor, preguntó:

---¿Qué es lo que presiento en esta tiniebla?

Y una voz, más humana que divina, respondió:

---Lo que ves sin ver y te ciega iluminándote es a mí, a Dionisio.

---¿Acaso aquél ---preguntó Ramón--- que predicara junto a Pablo
en el Aerópago?

---Mentiría si lo niego, ---dijo el otro--- tanto como si lo
afirmara.

En este punto Ramón no supo ya cómo continuar la charla. Entonces,
Dionisio le preguntó:

---Pero tú, ¿qué haces aquí?

---Busco a mi amado.

---Y, ¿quién es tu amado?

---Mi amado ---respondió Ramón--- es cuerda que ata dulcemente
haciendo coincidir en él todo lo que pienso y todo lo que deseo.

---¿No estarás pensando…?

---Muchos nombres tiene mi amado, infinita bondad, virtud, esperanza,
grandeza, gloria…

---Esta tiniebla ---afirmó Dionisio en alguna parte.

---No… ---dijo Ramón--- un Amado…

※ {.espacio-arriba1 .centrado}

Detrás de esta conversación imaginaria entre Ramón Llull y pseudo-Dionisio
el Aeropagita, se esconde un problema que va más allá de la sola
denominación de la divinidad y de lo que se ve o deja de verse
cuando uno ha ascendido hasta ella. Y también de lo que se hace
o deja de hacerse para alcanzarla. Las propuestas místicas de
Ramón Llull y pseudo-Dionisio son, justamente, la piedra de toque
que nos permite distinguir entre un esfuerzo por mostrar vías
de pensamiento alternativas a la razón ---en el caso de pseudo-Dionisio---
y, sobre todo, un pensamiento que involucra una auténtica experiencia
espiritual, es decir, una ética que convierte el _pathos_ místico
en _ethos_ amoroso ---como es el caso de la mística luliana---.
{.espacio-arriba1 .sin-sangria}

La disyuntiva entre Tiniebla o Amado no se refiere a la naturaleza
de ese objeto absoluto que no agota ningún nombre; se refiere,
en realidad, a una divergencia en cuanto a la experiencia mística
como camino de mediación y ascenso hacia el encuentro con el
ser anhelado.

Nos encontramos así con lo que parecen ser dos experiencias,
una que va en pos de la Tiniebla y otra que lo hace en busca
del Amado; y no podemos menos que preguntarnos, un tanto inocentemente,
si se trata de una diferencia en el trayecto, que es siempre
individual, o si esta diferencia se extiende hasta el extremo
de que solamente una de la vías encierra una auténtica experiencia
mística o si sólo una conduce eficazmente a la divinidad.

Tendremos que desechar el problema de la eficacia porque no estamos
aquí para un proceso de ascenso colectivo hacia Dios. Pero propongámonos,
al menos, tratar de ahondar en la naturaleza de la disyuntiva
entre Tiniebla y Amado.

El texto de la _Teología Mística_, nacido de manos de alguien
que se hacía pasar por Dionisio y que se decía convertido por
san Pablo al predicar en el Aerópago ---y que para enojo de Voltaire
plagió también todos sus milagros---, tiene un lugar primordial
dentro de la tradición mística cristiana y es frecuente, si no
invariable, que se presente como el texto fundamental del pensamiento
místico reconocido por la Iglesia,@note al punto de que el mismo
san Juan de la Cruz asocia su célebre Noche Oscura con la tiniebla
dionisíaca, en una pura búsqueda de oscuridades.

Sin embargo, la Tiniebla, indiscutiblemente presente en toda
la tradición mística como interpretación de pasajes del Éxodo,@note
no es aquí el centro de la divergencia. Ramón Llull, por ejemplo,
se hace eco de esa tradición, aunque invirtiendo el sentido de
lo que es luz y lo que es oscuridad: «Con alta voz decía el amigo
“Mi Amado es la Juz inmensa y bajo su sombra es donde vivimos;
es inaccesible, pero a él se acercan los humildes, y es incomprensible,
y le alcanzan los simples. Comparad, pues, humildad y aprended
simplicidad, para que de la _tiniebla paséis a la luz_”».@note

En esta cita del _Libro del amigo y el amado_ encontramos una
diferencia en cuanto a la denominación de la divinidad como luz
y no como tiniebla, que muy fácilmente puede ser traducida a
los términos usados por pseudo-Dionisio, sin que ello rompa la
coherencia del discurso místico luliano.

Pero esto no ocurre y no puede ocurrir con el Amado. Si seguimos
a Ramón Llull en su _Libro de la filosofía de amor_,@note la
divinidad es considerada Amado, en tanto que amante, amable y
amor; es decir, como agente, paciente y nexo, en su relación
con el hombre, pero con un hombre que es su igual: a la vez amante,
amable y amor. De ahí que Lulio recurra a la imagen del amigo
y el Amado, y no a la de esposa y esposo, para subrayar, justamente,
la ausencia de subordinación de uno al otro.

En este sentido, la divinidad es nombrada a partir de la relación
mística y no como independiente de ella. Es más, la divinidad
luliana es amado, en la medida en que sólo amándolo puede ser
a la vez vivido y conocido.

Pero el amor no aparece en pseudo-Dionisio ni dentro de la noción
de Tiniebla, ni como atributo de la divinidad; ni siquiera es
mencionado en alguno de los cinco pequeños capítulos que conforman
la _Teología mística_.

En realidad, si pasamos por alto que la imagen de la Tiniebla
es utilizada por pseudo-Dionisio para hablar de algo que no puede
ser definido por los elementos habituales del conocimiento, entre
ellos los conceptos de amado o amable, de todas maneras extraña
que pseudo-Dionisio no utilice el amor como atributo divino,
ni en la Teología mística, ni en _Los nombres divinos_, donde
señala los de bondad, pensamiento, existencia, vida, libertad,
razón, poder, omnipotencia, grandeza, pequeñez, paz, rey de reyes
y dios de dioses, sin incluir el de amante, amado o amor. Pero
más alarmante aún es que el amor no aparezca siquiera mencionado
por ningún motivo a todo lo largo de la _Teología_.

¿Habrá una razón para esta ausencia?

※ {.espacio-arriba1 .centrado}

Preguntar por el amor en este caso no es preguntar por un atributo,
sino por la posibilidad misma de la unión con la divinidad, porque
el amor no se predica, se actúa. Pero, ¿CómO se actúa el amor?,
¿cómo se es amado, amante y amable si el amor se siente, se padece?
{.espacio-arriba1 .sin-sangria}

La ausencia del amor en pseudo-Dionisio delata la falta de motivos
y fundamentos para la ascensión mística.@note Véase por donde
se vea, en el caso de la _Teología mística_, ésta es un acto
de la más absoluta gratuidad en la que ni siquiera participa
la gracia, puesto que la Tiniebla, lejos de revelar, encubre
a la divinidad.

A ello debe sumarse el hecho de que no hay una descripción, explícita
o implícita, de una experiencia mística que anteceda a la Tiniebla,
porque ésta responde únicamente a un afán especulativo sobre
la naturaleza de la divinidad.@note La ausencia del amor obedece,
así, al hecho de que la _Teología mística_ no es propiamente
una mística, sino tan sólo una especulación teórica que señala
los límites del conocimiento humano para aprehender la divinidad.

No obstante, el mérito de la _Teología mística_ es dejar sentado
que pueden existir otros caminos posibles más allá de la razón
que conduzcan al conocimiento de la divinidad, legitimando en
el seno de la doctrina cristiana la existencia de experiencias
místicas. De este modo, pseudo-Dionisio no es, contra toda creencia
tradicional, un místico. En él no hay motivos, ni una experiencia
mística real o ficticia y, por supuesto, no hay una unión con
la divinidad.

Pues no hay vínculo con Dios sin amor; no hay mística sin experiencia.
Y amor y experiencia entretejen a lo largo de la vida de Ramón
Llull ese extremo ---¿o será principio?--- que la sola especulación
no alcanza, vacía de deseo y de intención. La experiencia del
amor es el hilo conductor que lleva de la insuficiencia de la
racionalidad para conocer a Dios, a la vivencia explicita, carnal,
fulminante, fantástica y, a la vez, verdadera o innegable, del
saber de Dios.

El amor es, en Llull, la experiencia fundadora no sólo del saber
de la divinidad, sino de toda comprensión del mundo, llámese
arte o árbol.@note Habría que detenerse, sin embargo, en el alcance
de la experiencia amorosa, de la experiencia mística luliana,
porque no se trata de un matrimonio espiritual del alma, ni de
un vínculo tras la muerte. La iluminación divina vivida en el
monte de Randa por Llull da una clara orientación a su mística:
se trata no de recogerse y fundirse en la indiferencia de Dios,
sino de actuar y de llevar a cabo una empresa.

La experiencia mística luliana, experiencia espiritual en que
el sentimiento del amor conduce al acto, convierte el proceso
místico en trabajos de amor, en la conformación de grandes obras,
acto de vencer tentaciones, de alabar y adorar a Dios y, sobre
todo, de «hacer que la gente lo honre y lo sirva».@note

Entrar en contacto con la divinidad, vivir una experiencia amorosa
es, finalmente, actuar de tal manera que no haya diferencia entre
reír y llorar, entre descansar y trabajar. Dicho de otro modo,
donde todo lo que se haga sea traducción, en hechos, del amor
divino. Una mística del amor que es también una mística del acto.
De ahí que la mística luliana, a diferencia de la especulación
del pseudo-Dionisio, desemboque en una ética y en una política.
Porque el vinculo con la divinidad no es sólo una experiencia
personal y subjetiva, sino una misión; un vínculo que debe ser
asumido en una práctica, en la cual lo revelado cobra sentido
y donde los actos tienen, a la vez, un valor: el de la obra divina.

Es en este contexto en el que debe entenderse toda la obra vital
y humana de Llull, la fundación de colegios, sus libros prácticos
y teóricos, la conversión de los moros, su descabellada idea
del _Bellator Rex_@note ---de la que se valdría Felipe el Hermoso
para destruir a los Templarios--- y, por supuesto, su locura.

Pero lo más importante es que, en última instancia, Llull fundamenta
la ética en la experiencia espiritual de la revelación, del contacto
con la divinidad. El saber de la divinidad, así, se convierte
en el sentido de la conducta y de los actos. Una forma de mandato
ineludible, inmediatamente verdadero e incuestionable para el
sujeto. Pero para un sujeto, al fin, que ha alcanzado el grado
de divinidad, que se diviniza y se hace gobernador de sí mismo,
actuando.

※ {.espacio-arriba1 .centrado}

Cabría entonces pensar si en ésta, nuestra tiniebla, no es ya
hora de pasar al acto. {.espacio-arriba1 .sin-sangria}

# Una obra de magia

Para Adnana {.epigrafe}

++He soñado++ con este texto largo tiempo. Lo imaginé, primero,
como fruto del trabajo más árido y erudito sobre las sutiles
diferencias entre voluntad y amor. Después, lo concebí como un
simple ejercicio de retórica, con una defensa de las virtudes
frente a la pesadilla de los valores. Más adelante, mi fantasía
lo presentaba como un conjuro o un sortilegio para hechizar.
Ántes de comenzar a escribirlo, simplemente quería que fuera
una declaración de amor, un deseo de morir por amor.

Pero detrás de todos los sueños, este texto sería siempre, y
es, una reflexión sobre el pensamiento filosófico de Marsilio
Ficino. ¿Cómo escribe uno sobre un pensamiento que hace que se
cuelen los sueños y las fantasías? ¿Cómo se escribe sobre un
filósofo cuyos libros se utilizan como amuletos? ¿Cómo, finalmente,
es posible reflexionar sobre Ficino si, en este tiempo, Marsilio
ha llegado a ser un buen amigo, con el que se discuten cuestiones
que terminan por transformarnos? Lo que quiero decir es que este
texto es más que una reflexión. Es la historia verídica de una
metamorfosis, donde hay fantasmas, un amor imposible y un cadáver,
y que, sin saber cómo termina, no he dejado de creer que, al
final, debe conducir a la hermosura.

※ {.espacio-arriba1 .centrado}

¿Podemos preferir la belleza al bien para guiar nuestros actos?
No tengo muy claro en qué momento se me ocurrió por primera vez
plantearme la posibilidad de esta disyuntiva, pero sí tengo una
idea de cómo fui a encallar en esa inquietud.
{.espacio-arriba1 .sin-sangria}

Ficino no es un autor que hable mucho del bien ni de la voluntad.
Kristeller señala que para Ficino: «La perfección del alma humana
[…] está completamente ligada al ascenso interior y a la visión
contemplativa»,@note y en una carta dirigida a Cósimo de Medici,
el propio Marsilio asienta que:

> De todo lo que es nuestro, sólo la sabiduría es buena por sí misma.
> Sólo la ignorancia es mala en sí misma. Por lo tanto, si todos deseamos
> ser felices, y la felicidad no se puede obtener sin el uso correcto de
> nuestros dones, y puesto que el conocimiento revela el uso que les es
> propio, debemos dejar todo lo demás de lado y esforzarnos en el
> conocimiento completo de la filosofía y la religión, para hacernos lo
> más sabios posible. De esta manera, el alma se hace más parecida a
> Dios, que es la sabiduría misma.@note

En efecto, el bien, como tal, aparece circunscrito al ámbito
de la vida práctica y pública, por lo que Marsilio Ficino habla
de bienes en el sentido aristotélico de lo que es útil o adecuado
a algo,@note más que del bien como parte del camino de perfección
de la naturaleza humana. Como podemos ver en las líneas que escribe
a Cósimo, los bienes son, en realidad, una consecuencia del proceso
teúrgico por el que el alma humana se transforma ella misma en
Dios.

Pero si el bien no es un tema relevante en Ficino, otra de las
puertas de entrada a su pensamiento, la idea de la transformación
amorosa, cuya guía y meta es la belleza, sugiere la posibilidad
de plantear la disyuntiva entre bien y belleza como una distinción
ética entre quienes se inclinan por una moral de valores ---mirada
estrecha que sólo se preocupa por clasificar los bienes--- y
quienes lo hacen por una ética de virtudes, en la que los actos
singulares no constituyen objeto de clasificación, sino que su
suma ---tomando un término de Foucault--- constituye un «estilo».@note

En apariencia, enfocar el pensamiento de Marsilio Ficino bajo
la óptica de quien argumenta a favor de una preferencia de la
belleza sobre el bien para guiar la acciones humanas, no sólo
tiene la virtud de mostrar su controversia y diferencia con los
autores medievales ---señaladamente Santo Tomás---, sino que
subraya su cercanía con autores clásicos como Platón y Aristóteles.
Esta óptica posee, además, la ventaja de explicar el énfasis
puesto por Ficino en la superioridad de la imitación, ya sea
de los hombres,@note o del movimiento de los astros, para alcanzar
la virtud, ventaja que la sola discusión de la conducta moral
no aporta. Pero más importante aún, permite entender el pensamiento
de Marsilio Ficino como una enorme reflexión antropológica y
ética, en contraposición con la visión compartida, entre otros
por Kristeller y Cassirer, de un pensador abocado básicamente
hacia la metafísica y la filosofía de la naturaleza.

Sin embargo, el planteamiento parece no funcionar, pues la disyuntiva
es claramente falsa; está montada sobre un error de método, porque
parte de considerar la belleza como una cualidad constitutiva
de los actos ---entendidos éstos ahora como un conjunto---, lo
que finalmente coloca la defensa de la belleza al mismo nivel
que la defensa de los valores. En otras palabras ---aunque sé
que esto puede sonar aún más críptico por ahora---, la defensa
de la belleza exige principios o razones para actuar que deben
ser valoradas a partir del más endeble de los instrumentos: la
sensibilidad.

※ {.espacio-arriba1 .centrado}

Escribe Marsilio Ficino en la dedicatoria a _De amore_: «Suelen
los mortales, las cosas que hacen generalmente y a menudo, después
de una larga costumbre hacerlas bien; y hacerlas aún mejor cuanto
más las reiteran. Y sin embargo esta regla, por nuestra necedad
y para nuestra miseria falla en el amor».@note
{.espacio-arriba1 .sin-sangria}

Y la pregunta obligada es: ¿por qué falla? La respuesta es abismal
por su simpleza: porque el amor no es una costumbre. No se refiere
a actos cuya repetición nos proporcione cierto arte y, por lo
tanto, no crece ni se hace más excelso por la frecuencia, la
cercanía, la intensidad o cualquier otro medio o mecanismo que
utilicemos para hacer más perfecta una conducta. Porque amar,
debería ser claro, no es una conducta.

Y, entonces, ¿qué es? En palabras de Ficino ---y de Platón---,
el deseo de belleza: «[…] un solo círculo va desde Dios hacia
el mundo, y del mundo hacia Dios; y este círculo se llama de
tres modos. En cuanto comienza en Dios y deleita, nómbrase belleza;
en cuanto pasa al mundo y lo extasía, se llama amor; y en cuanto,
mientras vuelve a su autor, a él enlaza su obra, se llama delectación».@note

Notemos una cosa: en dos de los tres puntos que marcan las estaciones
del círculo ---que es al mismo tiempo el círculo que establece
el orden en la naturaleza y en el alma--- Ficino subraya su cualidad
sensible: la belleza, en cuanto comienza en Dios, y la delectación,
en cuanto regresa a él. Hemos de suponer, entonces, que también
el amor es una cualidad sensible.

Ficino explica así lo que él llama el amor vulgar:

> […] ¿quién se maravillará, entonces, si el ojo abierto, y fijo con
> atención en alguien, arroja a los ojos de quien lo mira las flechas de sus
> rayos, y junto con estas flechas, que son el carro de los espíritus, lanza
> ese sangriento vapor que nombramos espíritu? De allí la flecha
> envenenada traspasa los ojos; y por ser arrojada desde el corazón de
> quien la lanza, se lanza hacia el corazón del hombre herido, casi como
> a una región propia y natural. Aquí hiere el corazón; y en sus duras
> paredes se condensa, y se vuelve sangre. Esa sangre extraña, ajena a la
> sangre del herido, la sangre misma del herido perturba; y la sangre
> propia turbada, y casi corrupta se enferma.

> De aquí nace el hechizo, es decir, el mal de ojo[…]@note

Este pasaje, que retoma la visión del flechazo amoroso de la
tradición cortés, para fundirla con la explicación pneumática
del alma humana, nos permite encaminarnos hacia la comprensión
del amor como una cualidad sensible.

Partamos de una base: Ficino asume que alma y cuerpo son dos
entidades completamente distintas que no pueden entrar en contacto
entre sí. Sin embargo, ambas coexisten tanto en el mundo como
en el hombre. Son las sustancias que, según el célebre pasaje
del _Timeo_ platónico, el demiurgo utilizó para crear el cambio
ordenado, es decir, el tiempo del mundo.

Frente al dilema que supone la necesidad de que alma y cuerpo
estén imbricados en la creación de todas las cosas, y la paradoja
de que ambos no pueden entrar en contacto, Ficino, en particular,
y los renacentistas, en general, se plantean el problema de la
intermediación como el eje básico de su reflexión. Pero no sólo
se trata de metafísica, sino que, a partir de la influencia de
la tradición hermética, se presenta al hombre como el ser «intermedio
por excelencia» de una reflexión antropológica.

De esta forma, hay algo privativo del hombre que lo coloca en
el privilegiado papel de ser intermedio. Por lo que se deduce
de amplios pasajes del diálogo hermético _Asclepio_,@note se
trata del pensamiento, pero no entendido aquí como racionalidad,
sino como el enlace entre el mundo físico y la mente. Y ese vínculo
no es otro, me temo, que la sensibilidad.

※ {.espacio-arriba1 .centrado}

Hay muchas maneras de contar la historia de cómo la sensibilidad
llega a jugar este papel clave de intermediador. Según se preocupe
uno por tratar de explicar la esencia de la sensación, esta historia
comienza con Aristóteles y su distinción de tres facultades del
alma que explican el proceso sensible: el sentido, la memoria
y la fantasía. Si, en cambio, lo que se busca es hallar el origen
de la noción de espíritu, que en el Renacimiento es el «cuerpo
sutil» donde se encuentran estas facultades, la historia comienza
con el _pneuma_ estoico, el «aire», el cuerpo sutilísimo que
envuelve al alma. {.espacio-arriba1 .sin-sangria}

En cualquier caso, para el momento en que escribe Ficino, ambas
ideas han quedado fundidas por la tradición neoplatónica. En
la fuente más usada por Marsilio en torno a este tema, el _In
Somniis_ de Sinesio, se habla de un _to phantastikon pneuma_,
que sintetiza las dos funciones básicas atribuidas a la sensibilidad
y al _pneuma_: el acto reflexivo ---como el del espejo--- que
presenta las imágenes de las cosas materiales al intelecto, y
la inspiración o la capacidad creativa, en un sentido amplio,
que genera las imágenes emanadas del intelecto, en la materia.@note
Sabemos ya que Ficino lo explicaba así:

> Tres cosas hay sin duda en nosotros: alma, espíritu y cuerpo. El alma y
> el cuerpo son de naturaleza muy diferente, y se unen por medio del
> espíritu, el cual es un cierto vapor sutilísimo y lucidísimo, engendrado
> por el calor del corazón desde la parte más sutil de la sangre. Y
> esparciéndose de aquí por todos los miembros, toma la virtud del alma,
> y la comunica al cuerpo. Toma también por los instrumentos de los
> sentidos las imágenes de los cuerpos de afuera; imágenes que no se
> pueden fijar en el alma porque la sustancia incorpórea, que es más
> excelente que los cuerpos, no puede ser formada por ellos mediante la
> recepción de las imágenes; pero el alma, por estar presente en el
> espíritu, en todas partes sin esfuerzo ve las imágenes de los cuerpos
> como reluciendo en un espejo, y a partir de ellas juzga a los cuerpos. Y
> tal conjunción es llamada sentido por los platónicos. Y mientras mira,
> por su virtud concibe en sí imágenes semejantes a aquéllas, y aún más
> puras. Y tal concepción se llama imaginación y fantasia.@note

Antes que nada, destaquemos que el espíritu es un cuerpo entre
el alma y el cuerpo. «Casi alma, casi cuerpo», dirá en otro lugar
el propio Marsilio.@note Es en ese cuerpo donde la imaginación
y la fantasía reflexionan e inspiran. Así, según la _Teología
platónica_, «la imaginación tiene por objeto las imágenes de
los cuerpos recibidas a través de los sentidos o por estos concebidas»,@note
en tanto que la fantasía concibe los universales ---propios del
intelecto--- dentro de lo particular, ya sea de una persona o
una cosa.@note En otras palabras, entre la imaginación y la fantasía,
constituidas en un cuerpo sutilísimo, se produce el vínculo último
entre el alma y la materia, entre el intelecto y los objetos
sensibles.

※ {.espacio-arriba1 .centrado}

El mal de ojo, el hechizo de amor físico, lo explica Ficino precisamente
como producto de la recepción, a través de la vista ---el más
alto de los sentidos exterioresjerárquicamente---, del espíritu
de otra persona, y con ella, de una parte del vapor más sutil
de su sangre. Pero lo más importante de todo es que, con ello,
con el espíritu y la sangre, lo que se recibe es un fantasma,
una imagen autónoma de la sensibilidad, que ejerce «una especie
de vampirismo sobre los fantasmas y pensamientos de la otra persona»,@note
de tal forma que domina los pensamientos de quien ama. A partir
de esto se produce el vínculo que nos hace, literalmente, perder
la cabeza. {.espacio-arriba1 .sin-sangria}

Si hemos de creerle a Couliano, el objeto de eros, entonces,
es el fantasma, no la persona física,@note por lo que el amor
no es, en Ficinio, una cualidad sensible, sino la sensibilidad
misma en su autonomía. Me explico: en realidad, no amamos algo
extraño a nosotros, sino algo que está en nosotros.mismos, esa
sangre que nos ha sido trasmitida y que, según Ficino, hace que
nuestra propia sangre la busque;@note amamos el fantasma que
se ha apoderado de todo el espejo en el que se refleja en nosotros
el mundo.@note Amamos, pues, aquello que nuestra sensibilidad
ha dispuesto que amemos, con absoluta independencia de las cosas
físicas y divinas ---por eso es mal de ojo, por eso enferma,
por eso no se ama a uno 0 a otro, Nia nadie: se ama lo que constituye
el sentido, en la doble acepción de lo que se siente y de lo
que otorga sentido a las cosas---. El amor, entonces, no es más
que la experiencia límite de percibir el instante en que lo que
nos rodea toma contacto con su sentido; el punto de inflexión
de la realidad, el lugar donde el caos es conducido a un orden
por cada uno de nosotros.

Así, pues, el amor es en Ficino la pura experiencia de la autonomía
de la sensibilidad. El espacio y el momento en que cada uno es
vértice de sentido. Por eso es una especie de locura, ya se trate
del puro amor físico y vulgar, que busca y encuentra un sentido
particular y limitado, o del amor más alto, el divino, que aspira
a encontrar el sentido último de las cosas.

※ {.espacio-arriba1 .centrado}

Pero persiste la duda de si, para Ficino, el amor es un asunto
narcisista y, por ende, solitario, como lo cree Couliano. Porque
en apariencia, si el amor gira en torno a ese fantasma único
que se apodera del sentido, entonces no hay posibilidades de
ir más allá de uno mismo, de tocar al otro, porque viviriamos
una especie de encierro en los muros transparentes de nuestra
sensibilidad, en la que nada es verdad ni es mentira; aún más:
nada es, porque sólo existe el fantasma. {.espacio-arriba1 .sin-sangria}

Andreas Capellanus, en _Sobre el amor_, llama a este encierro
la _cogitatio inmoderata_, y lo define como una perturbación
del sentido, una enfermedad que altera la capacidad de sentir
hasta aniquilar a la persona. En otras palabras, cuando es la
fantasía la que gobierna toda la existencia, termina por aislarla.
Esta enfermedad es tan grave, que la única manera de evitarla
es morir:

Platón llama amargo al amor; y no sin razón, porque todo aquel
que ama, muere amando […] Muere, amando, todo aquel que ama;
porque su pensamiento, olvidándose de sí mismo, a la persona
amada se dirige…

> Pero allí donde el amado responde en el amor, el amante, apenas está
> en el amado, vive. Aquí acontece una cosa maravillosa, cuando dos se
> aman mutuamente: él en éste, y éste en aquél vive […] [Así]
> ciertamente mientras que yo te amo a ti, que me amas a mí, yo enti,
> que piensas en mi, me hallo a mí mismo; y yo, por mí mismo
> despreciado, en ti que me cuidas, me recupero.@note

Veámoslo de este modo. Lo que Ficino dice es que la muerte es
la supresión del sentido propio. Como para San Agustín, para
Ficino morir es literalmente perder la percepción; pero, sobre
todo, el sentido interno que organiza desde sí lo particular
de la percepción. Desde esta idea, Ficino construye su imagen
del cielo y del infierno. No podemos entrar con detalle en ella,
pero podemos decir que el infierno no es más que la conservación
del sentido, atado a la materialidad. Se cree que te siente,
pero nada es sentido. Mientras que, para ganar la gloria, como
para ganar el amor, es necesario suprimir el sentido; literalmente,
la voluntad de ordenar las cosas. Entonces éstas se mostrarán
como son. Ya no habrá razones que expliquen, sino la explicación
misma; como no hay razones para amar, sino el amor mismo.

※ {.espacio-arriba1 .centrado}

La belleza es simple. Hay un apartado en _De amore_, en que Ficino
discurre acerca del equilibrio y la proporción como cualidades
de la belleza de los cuerpos, pero los desautoriza como cualidades
de la belleza en sí misma porque, dice, la belleza es cosa espiritual,
una cualidad del sentido, una forma de sentir.@note Es como la
luz que hace que exista la habitación en el cuadro, es lo que
ilumina y permite que veamos las cosas como son, no como deseamos
que sean. Es simple, porque no está compuesta de razones, porque
no ofrece explicación alguna, porque sólo es, se siente y, así,
dirige nuestros actos. {.espacio-arriba1 .sin-sangria}

Al final, la disyuntiva no es entre el bien o la belleza, sino
entre el cuerpo, el espíritu y el alma. Pues entre ellos tres
debemos elegir quién ha de gobernarnos. Yo entiendo que hay una
virtud del espíritu, la única que nos eleva, diría Ficino, para
ser nosotros mismos dios: la belleza.

# Modelar la fantasía

++Una y otra vez++, a lo largo del día, cada uno de nosotros
ejercita su fantasía. Lo hace, sin embargo, mucho más de lo que
usualmente estamos dispuestos a reconocer y de formas mucho más
comprometedoras de lo que comúnmente conocemos como sueños diurnos
o ensoñaciones.

De hecho, la fantasía juega un papel crucial en la elaboración
de nuestras formas de conducta e, incluso, en la ejecución misma
de las acciones. Más aún, la fantasía es un ingrediente esencial
en la vida afectiva y, de manera muy puntual, en la construcción
de la percepción del goce sexual y del íntimo placer erótico.

El propósito de este texto es mostrar, primero, de qué manera
la fantasía constituye un principio modelador de la sensibilidad
del sujeto. Segundo, cómo, a partir de ello, la fantasía se constituye
en un elemento esencial para la comprensión de nuestras formas
de conducta. Tercero, qué papel juega la fantasia en la forma
de modelar nuestra percepción del goce y del significado de la
experiencia erótica. Y, finalmente, de qué forma el estatus ontológico
y epistemológico de la fantasía permite su transformación y manipulación,
con la intención de reconstruir y elaborar el erotismo.

Para lograrlo, sin embargo, es necesario que comience por corregir
una infamia: la que desde más o menos cuatro siglos ha padecido
la fantasía a manos de los hombres.

En efecto, a partir del surgimiento del empirismo y el racionalismo
en el siglo +++XVII+++, la fantasía fue apartada de las categorías
epistemológicas y desechada por considerar que conduciía al error.
Así, Descartes, en el _Discurso del metodo_, atribuía a la voluntad
---e indirectamente a la fantasía, que es el espacio en que la
voluntad carece de restricciones--- la fuente de todo error.@note
Locke, por su parte, en el _Ensayo acerca del entendimiento humano_,
dice: «[…] [las ideas] son fantásticas cuando están formadas
por una colección de ideas simples que nunca se han encontrado
verdaderamente unidas ni juntas en una sustancia: por ejemplo,
una creatura racional, consistente en una cabeza de caballo,
unida a un cuerpo de forma humana […]».@note

No debe extrañarnos. Queriéndolo o no, ambos responden así al
cuestionamiento moral que el protestantismo hizo de la fantasía,
y que se revela de manera inequívoca en esa obra panfletaria
de la nueva fe que es el _Fausto_.@note En ella, la fantasía
de Fausto aparece como detonador de su soberbia: no es sino por
su fantasía que Fausto alcanza a Elena y puede, ubicuo, estar
en una corte de Oriente y en los palacios de Bohemia; considerarse,
en suma, como igual a un dios y obrar como tal.

Pero el ataque a la fantasia que se hace en las obras populares
del Fausto y, particularmente, en el Fausto de Marlowe, no es
espontáneo. En estricto sentido, es un ataque contra el humanismo
renacentista y, en especial, contra las ideas de Giordano Bruno
a propósito del poder de la fantasía y su capacidad mágica.@note

El ataque contra Bruno era, remontándonos aún más, un ataque
contra la idea aristotélica de que la fantasía constituye un
principio de la sensibilidad del sujeto. Como sabemos, en la
psicología aristotélica, las funciones que permiten a los hombres
y a los animales sentir se encuentran en el alma sensible o concupiscible.@note
Ahí están los cinco sentidos externos y los llamados sentidos
internos: el sentido, la memoria y la fantasía.@note El primero
recoge y otorga unidad a las percepciones; la segunda las mantiene
a través del recuerdo y según coordenadas de espacio y tiempo;
y la fantasía abstrae esas percepciones de las dimensiones espaciotemporales
para ponerlas a disposición del juicio. Ya en el Renacimiento,
la fantasía no sólo mantuvo su función de abstraer las imágenes
de sus dimensiones espaciales y temporales, sino que se convirtió,
como señala Summers, en la estructura misma del carácter de una
persona.@note

Nosotros todavía usamos el término «aire de familia» cuando decimos
que alguien guarda un cierto parecido con otra persona de la
que hemos descubierto que es un familiar. Este término nos viene
de muy lejos y hace referencia a lo que antiguamente se llamaba
_spiritu_, el _pneuma_ de la filosofía estoica, el aire. Casi
cuerpo casi alma ---según palabras de Ficino---@note el _spiritu_
guardaba las facultades sensibles de la psique y, entre ellas,
como elemento primordial, la fantasía.

Ahora bien, siguiendo la idea aristotélica, la fantasía es la
función del alma sensible que entra en contacto con la razón
al permitir la abstracción. La diferencia entre el pensamiento
aristotélico y el renacentista es que Aristóteles consideraba
que esto se producía en una sola dirección: de abajo hacia arriba.
Es decir, que existía una jerarquía del conocimiento que comenzaba
en la percepción y finalizaba en el entendimiento, y la fantasía
servía sólo para alimentar al juicio racional. El Renacimiento,
en cambio, consideraba que las funciones de la fantasía eran
bidireccionales, es decir, que no sólo traducía los datos sensibles
en elementos que pudieran ser objeto de un juicio racional, sino
que, a la vez, traducía estos juicios en algo que pudiera ser
sentido.

En otras palabras, para el pensamiento renacentista la fantasía
permite percibir de manera sensible la bondad, la belleza y la
amistad. Es decir, es gracias a la fantasía como se puede dar
sentido a lo que es percibido; un sentido que, en última instancia,
constituye la propia sensibilidad del sujeto: lo que es capaz
de percibir y los significados que puede encontrar en ello.

Y en esto quisiera ser muy claro: estamos ante un concepto de
fantasía que, con independencia de la función epistemológica
de la abstracción, es quizás el elemento principal en la conformación
de la estructura de la sensibilidad y, por tanto, de la persona.
Dicho en palabras llanas, es la facultad de organizar y estructurar
lo que sentimos.

※ {.espacio-arriba1 .centrado}

Adentrémonos ahora en cómo la fantasía se constituye en un elemento
esencial para la comprensión de nuestras formas de conducta.
Para Giordano Bruno, la fantasía funciona sobre la base de imágenes
universales.@note Hago énfasis en estos dos últimos términos,
«imágenes» y «universales» para distinguirlos de los conceptos
y de la ideas que utilizan la mente y la razón para elaborar
sus juicios y razonamientos. En realidad, las imágenes universales
que utiliza y produce la fantasía para organizar la estructura
de nuestras sensaciones son los simbolos. {.espacio-arriba1 .sin-sangria}

En su raíz griega, símbolo quiere decir «que remite a algo más»,
y ése es también el sentido del fantasma que produce la fantasía:
remite a los objetos de la percepción, por un lado, y a las estructuras
de organización racional, por otro. Así, el simbolo es algo que
carece de sentido en sí mismo ---el símbolo del árbol, por ello,
no sería lo mismo que el concepto de árbol---, pero que en la
frontera de los dos horizontes a los que remite, crea, llamémoslo
así, una tercera dimensión dependiente de las otras dos y que
es lo que llamamos sensibilidad: la capacidad de registrar los
datos de los sentidos y otorgarles un significado que no está
coaccionado ni por la realidad ni por el compromiso racional.
Es por ello que cuando decimos «Yo siento que…», lo que hacemos
es afirmar una fantasía, pues no es ni falsa ni verdadera y tampoco
es real: remite a lo que se siente que, en última instancia,
es el marco en que se ejecuta la acción humana.

Pero volvamos a Bruno: si la fantasía está constituida por símbolos,
que son estructuras sensibles que organizan y dan significado
a lo que sentimos, entonces, para él, la voluntad opera a partir
de la fantasía, de ese reino intermedio entre la percepción sensible
y la consideración racional.@note

La consecuencia más importante de lo anterior es que, al formarse
de simbolos, la fantasía es objeto de manipulación: la simple
lectura del tarot es eso; lo mismo la astrología, los talismanes
y las fórmulas mágicas, por citar sólo lo más obvio. También
la propaganda, la enunciación de expectativas, etcétera. Cada
una de estas cosas, para Bruno, son capaces de modificar la aprehensión
de la realidad y, en consecuencia, nuestra conducta.

Y es esto, precisamente, lo que el protestantismo vio con malos
ojos. La capacidad de construir la realidad de acuerdo con nuestros
deseos, de hacer que las personas vean lo que deseamos que vean
y sientan.

No pretendo persuadir sobre la efectividad de la magia bruniana.
Mi intención es rescatar la definición y caracterización de la
fantasía que hace el Renacimiento y que fue abandonada tras el
triunfo del protestantismo y la filosofía moderna. Por eso, quiero
retener aquí cuatro cosas: que la fantasía posee un carácter
sensible; que su función es la de estructurar la realidad; que
está compuesta de símbolos; que es manipulable. ¿Cuáles son las
implicaciones de comprender así la fantasía? Primero, que la
fantasía no es un estado de la mente, y segundo, que tampoco
es un objeto. Está en la frontera de estos dos reinos. Es el
producto de un cuerpo que piensa: por ello se siente y se vive.

En este sentido, no debe ser asimilada a la idea de fantasía
que se utiliza, desde hace un siglo, en el psicoanálisis. La
fantasía no se comprende aquí como un elemento compensatorio
y de regulación psiquica, que actúa como satisfactor de deseos
inconscientes, por ejemplo, a través del sueño, aunque tampoco
excluyo este horizonte interpretativo.

Lo que deseo señalar aquí es que la fantasía puede ser el espejo
en que nos representamos los acontecimientos en términos sensibles
y afectivos; es decir, con cargas de placer, dolor, odio, amor,
amistad, etcétera.

※ {.espacio-arriba1 .centrado}

Entremos ahora a describir qué papel juega la fantasía en la
forma de modelar nuestra percepción del goce y del significado
de la experiencia erótica. {.espacio-arriba1 .sin-sangria}

Siempre me ha llamado la atención cómo ciertas fantasías y, en
particular, ciertas imágenes, despiertan en nosotros una sensación
casi física. Esto es particularmente claro cuando nos referimos
a imágenes eróticas o sexuales. Y no aludo sólo a la pornografía
o al arte erótico, sino incluso a la visión involuntaria de un
gesto de una mujer en una reunión o a la cortesía de un hombre
en la calle.

Pero lo que más me asombra no es que produzca esa sensación,
sino que se produce de una manera absolutamente específica (es
a mí y sólo a mí al que ese gesto provoca), lo que forma parte
de una estructura compleja: la de mi subjetividad construida
sobre mi sensibilidad, que me permite apreciar ese gesto de la
forma en que lo hago y con los significados que le atribuyo.

Así, cuando nos preguntamos por la cualidad del placer sexual
o por las cualidades de una amistad, en realidad nos preguntamos
por nuestra capacidad de percibir, y por qué placeres o qué rasgos
de amistad somos capaces de sentir, de acuerdo con el ordenamiento
simbólico al que referimos tanto unos como la otra.

En otras palabras, nos preguntamos por la cualidad de la estructura
de nuestra fantasía, que nos hace aprehender ahora una cosa y
ahora otra, a través de los fantasmas que la habitan, los símbolos
que le dan cuerpo. Pero se trata ---y esto es lo más fascinante---
de algo que puede ser modificado por la persona; algo de lo que
ella misma es responsable.

Hoy, por ejemplo, la felación es uno de los placeres sexuales
más buscados por los hombres. Se trata, por supuesto, de una
actividad sexual que existe desde tiempos inmemoriales. Sin embargo,
en las obras eróticas no la encontramos como un componente esencial
del goce sexual ---más bien como una excepción y, a veces, incluso
de mal gusto--- sino hasta bien entrado el siglo +++XX+++. Más
precisamente, con la producción comercial de películas pornográficas.
Han cambiado los hábitos, se dirá; somos más liberales, hay una
mejor comprensión del juego erótico. Pero yo quisiera enfatizar
que lo que ha ocurrido es que, en algunas personas, ha cambiado
la fantasía de lo que provoca placer.

¿Por qué señalar aquí la fantasía? Precisamente por su estatuto
ontológico y epistemológico. No es que la felación, por ejemplo,
o la utilización de ciertas telas, o la situación en que se produzca
un encuentro sexual incremente la cualidad del placer, que nos
guste más o menos, que sea mejor o peor. Tampoco significa que
sea más verdadero o más falso el goce. Simplemente representa
un acuerdo con el símbolo con que nosotros representamos de una
manera sensible ese placer. Porque sólo entonces somos capaces
realmente de percibirlo.

Pero que se entienda: es meramente fantástico, no existe en los
términos en que usualmente decimos que existen las cosas, ni
es verdadero en el sentido que le damos comúnmente. Es sólo reflejo
de la capacidad que tenemos para percibir y vivir nuestra propia
existencia.

Existe aquí, entonces, la posibilidad de una didáctica y, por
supuesto, la posibilidad de una ética. Es decir, la capacidad
de aprehender la experiencia erótica puede enseñarse sobre la
base de enseñar cómo se aprehenden las cosas simbólicamente,
mostrar cómo se leen los símbolos y cómo éstos tienen un importe
básico en nuestra existencia. Es, por supuesto, una forma de
aprender a escucharse a sí mismo, reconocer la propia estructura
simbólica, y el entramado de que está compuesta.

Pero escucharse a sí mismo es también la puerta para ser mejor.
No podemos calificar nuestras fantasías como buenas o malas,
porque no lo son. Si nosotros somos mejores, si somos capaces
de ser más humanos de lo que somos, si aprendemos realmente a
sacar toda la capacidad que implica el que tengamos sentidos
y la facultad de sentir, entonces recuperaremos la fantasía.

# El arrebato de las pasiones

++Quien ha amado++ ---y espero que los que lean esto lo hayan
hecho alguna vez--- sabe hasta qué punto el amor hace cruelmente
manifiesta la certeza de que todo para el hombre es causa a la
vez de placer y dolor. Pero el amor es doblemente cruel porque
ata a la tortura y al goce más indigno.

Escribe Bruno en el preámbulo de _Los heroicos furores_, dirigido
a

Philip Sydney:

Es ciertamente propio de un genio vil, bajo e inmundo el haber
fijado singularmente la inquietud de la mente en torno a un objeto
cual la belleza de un cuerpo femenino […] ¿qué espectáculo, ¡Dios
mío! más vil e innoble puede ofrecerse a un ojo de refinada sensibilidad
que un hombre pensativo, afligido, atormentado, triste, melancólico,
que se torna con igual presteza frío y acalorado, ya ardiente,
ya tembloroso, ya pálido, ya arrebolado, tan pronto de aspecto
perplejo como con aire resuelto; un hombre que derrocha la más
valiosa parte de su tiempo y los más selectos frutos de su vida
presente destilando el elixir de su cerebro a fin de concebir
por escrito y estampar en públicos monumentos esas continuas
torturas, esos arduos tormentos, esos racionales discursos, esos
fatigosos pensamientos y esos amarguisimos cuidados destinados
a sufrir la tiranía de una indigna, imbécil, estulta y ruin inmundicia?@note

No nos aflijamos. Bruno no pretende, en un arrebato de estudiada
misoginia, abdicar de las mujeres. Nada más contrario a su espíritu.
Le desespera, en realidad, que los hombres dediquen tantos esfuerzos
a huir de su zozobra inmolándose en un altar que nada resuelve.

En el amor a las mujeres, el Nolano encuentra la síntesis de
la inquietud última de todo hombre: la promesa y el ocasional
disfrute de los placeres de la vida y la cruel constancia de
su fugacidad y término, que sólo deja lugar al dolor. De este
modo, el amor en _Los heroicos furores_ se presenta como «heroico
señor y guía» y el objeto como la cosa amable cuya posesión place
al amante; junto a ellos, Bruno coloca azar y celosía, que «viene
a perturbar y emponzoñar cuanto bello y bueno se encuentra en
el amor».@note

Y, sin embargo, el amor siempre persevera en la búsqueda de su
propia promesa. Aliado del amor, el heroico furioso es, en principio,
quien ha hecho suya la promesa de amor y se afana en deshacer
el nudo que ata a cada cosa placer y dolor. Y lo hace no persiguiendo
placeres ocasionales y terrenos que sólo manifiestan su temor
o dolor, sino buscando un camino que lo lleve más allá de la
zozobra. Por ello se propone la transformación de sí mismo; quiere
ser, en palabras de Bruno, artífice y eficiente, dejar atrás
todo azar y todo temor para hacerse constructor de su destino,
como destino de amor.

¿En qué consiste esa transformación que, al cabo, debe conducir
al goce como estado permanente de la sensación?

En primer lugar, dice Bruno, los furores «no son olvido, sino
memoria, no son negligencia de uno mismo, sino amor y anhelo
de lo bello y lo bueno».@note

Tomemos esto en su justa dimensión: Bruno pone por delante el
cuidado de sí mismo. La primera tarea del furioso ---antes incluso
que conocerse a sí mismo--- es la de cuidar los movimientos de
su alma. Esto lo hará a través de lo que Bruno llama el «contacto
intelectual con el objeto divino», porque éste tiene el efecto
de comunicar la belleza y bondad divinas al alma humana.

Desde Platón,@note uno de los mecanismos para el cuidado del
alma es la contemplación de la divinidad, que aquí debe entenderse
como el ejercicio de la capacidad de pensar; es decir, el cultivo
del conocimiento de la naturaleza, de la armonía y de las revoluciones
del todo. En Bruno, tal ejercicio de contemplación es producto
del amor que ha hecho de la divinidad su objeto, con el fin y
la convicción de transformar al amante en el amado. Se funden
en él dos tradiciones: el gnosticismo de los _Hermética_, que
concibe el conocimiento como virtud activa, capaz de operar una
transformación en el sujeto; y el misticismo cristiano, con origen
muy probablemente en Ramón Llull,@note por el cual la transformación
opera sólo a través de la exaltación del lance amoroso.

El cuidado de sí se presenta, en Bruno, como una obra de amor
---producto de la seducción y el deseo, la promesa y el anhelo
de felicidad y placer--- que se propone la transformación del
alma a través del conocimiento.

El heroico furioso viene a ser para Giordano Bruno como el mítico
Acteón. Ovidio cuenta, en sus _Metamorfosis_, cómo Acteón, habiendo
salido de caza y soltado sus perros, viene a parar al lado de
un río donde la virginal Diana se bañaba desnuda rodeada por
las ninfas. Acteón miraa Diana, y ésta se encoleriza cuando su
belleza desnuda es vista por ojos mortales. En su furia, maldice
a Acteón, a quien transforma en ciervo, al que más tarde sus
propios perros darán caza.@note Dice Bruno:

> Así Acteón con esos pensamientos, esos canes que buscaban fuera de sí
> el bien, la sabiduría, la belleza, la montaraz fiera, por este medio llegó
> a su presencia; fuera de sí por tanta belleza arrebatado, convirtiéndose
> en presa, vióse convertido en aquello que buscaba y advirtió cómo él
> mismo se trocaba en la anhelada presa de sus canes, de sus
> pensamientos, pues habiendo él mismo contraído la divinidad, no era
> necesario buscarla fuera de sí.@note

El cuidado de sí conduce al conocimiento de sí. El ejercicio
de contemplación de la divinidad, en _Los heroicos furores_,
produce que el sujeto descubra lo que en él mismo hay de divinidad.
La primera transformación opera, así, a partir del instante en
que el hombre toma conciencia de su condición también divina.
Entonces los pensamientos de amor se dirigen no ya hacia la divinidad
como hacia una cosa distinta del sujeto, sino hacia el sujeto,
en busca de descubrir lo que es.

Pero esto es sólo el principio. El furioso «[…] advierte siempre
que todo lo que posee es cosa mesurada y por ello no puede ser
suficiente, por tanto, progresa desde lo bello comprendido hacia
lo verdaderamente bello, sin límite ni circunscripción algunos».@note
Al furioso, pues, no le basta con la comprensión; su empresa
le promete ir siempre más alto, hasta hacer suya la fuente de
esa belleza. Y es en ese punto donde se examina a sí mismo.

Lo que el furioso encuentra al volver sus ojos hacia sí es la
rebelión del corazón. Encargado de lo que es propio de la generación
en la materia, el mantenimiento de la vida, el corazón se desentiende.
Bruno narra cómo, ante la preocupación por el cuidado del alma,
el corazón busca ya no la atención de las potencias y afectos
del cuerpo, sino avanzar él mismo en la contemplación, en tanto
que el alma replica que su función es la de conservar la vida,
no la de elevarse a la contemplación de lo divino. Pero mientras
alma y corazón discuten, las potencias inferiores naufragan a
la deriva: «¿De dónde ---pregunta el alma al corazón--- os ha
nacido este melancólico y perverso humor de infringir las ciertas
y naturales leyes de la verdadera vida, que está en vuestras
manos, por una vida incierta y que no existe sino en sombras,
más allá de los límites imaginables?»@note

El estado descrito aquí no es otro que el del melancólico y sabemos
bien cuán importante es el asunto de la melancolía en el Renacimiento.
Pero en _Los heroicos furores_, la melancolía no constituye propiamente
una enfermedad, aunque Bruno juegue con esa idea. En realidad,
se trata del estado que sobreviene al conocimiento de uno mismo:
es la certeza de la imposibilidad de vivir conforme a la divinidad,
al ser el furioso a un tiempo divino, animal y humano.

Pero el amor obligará al furioso, pese a la decepción, a mantenerse
fiel, haciendo de la melancolía una prueba más de amor.

Contrario a los afanes de Ficino por curar la melancolía a través
de una vida armónica plena de goces corporales,@note Bruno ve
en el hundimiento melancólico un paso definitivo del furioso.
En efecto: «escindida [el alma] entonces por el doble amor que
experimenta hacia la materia y hacia las cosas inteligibles,
se siente torturar y desgarrar de manera que deberá al fin ceder
a la atracción más fuerte».@note

Al final, el furioso debe renunciar, por su pasión amorosa, a
la atención que requiere prestar a sus potencias y afectos inferiores
para mantener su vida terrenal. Lo que ocurre es que «aquello
que alimenta su fantasía y conforta su espíritu es que si ocurriese
que el severo y rebelde destino se doblegase un poco haciendo
que, sin cólera ni desdén, se le manifestase el objeto supremo,
no concebiría dicha ni vida de felicidad mayor que la dicha que
entonces hallaría en su pena y la felicidad que en su muerte
encontraría».@note

A estas alturas el furioso es como una mosca que gira alrededor
del fuego. El resplandor la seduce y, sin embargo, en la seducción
hallará la muerte. Lo que distingue al furioso de Bruno es que
sabe lo que ocurrirá: que ante la visión del objeto amado, de
la belleza divina, será capaz de perder la vida y que incluso
deseará perderla.

Bruno roza aquí los terrenos del más puro misticismo: la voluntad
de disolver el objeto divino para perpetuar la transformación
última: poseer y ser poseído por lo divino.

※ {.espacio-arriba1 .centrado}

Pero para el Nolano, la divinidad es objeto de deseo, no de posesión.@note
Se trata de una aspiración infinita y no de una realización efectiva.
Hay que tomar en cuenta esto para comprender que el «misticismo»
de Bruno se revela aquií, en realidad, como una técnica de autodominio:
es justamente la conciencia del furioso y su eventual deseo de
perder la vida, lo que lo lleva a alcanzar, por un lado, el pleno
gobierno de sí y, por otro, la meta anhelada del puro placer.
{.espacio-arriba1 .sin-sangria}

Dijimos que la melancolía es una prueba. A través de ella el
furioso toma conciencia del escaso valor del dolor y, particularmente,
del dolor que produce el temor a la muerte ante la magnífica
belleza de la armonía del cosmos. El deseo de participar de ese
bien, de esa belleza infinita, se impone sobre la mezquindad
del temor a la fugacidad de la vida. Es conciencia y certeza
de la plenitud a la que pertenece el hombre mismo en su condición
a la vez mortal y divina.

Á partir de la contemplación del objeto amado, por el que se
ordenan, según Bruno, las potencias aprehensivas y afectivas
atendiendo a la armonía de la divinidad, «ocurre que cuando el
apetito racional se halla en oposición a la concupiscencia sensual,
si se ofrece a la vista del primero la luz intelectual, viene
a recobrar la audacia perdida, a vigorizar sus nervios, amedrentando
y derrotando a los enemigos».@note

Al abandono de la melancolía, donde se pierde la audacia y el
temple nervioso, se impone ahora el pleno dominio de uno mismo.
Ahora, el furioso es quien puede tomar el mando de lo que le
ocurre. Sin miedo, el ataque de la concupiscencia viene a perder
efectividad e importancia. La búsqueda del placer corporal es
sustituida por el efectivo placer, el que está desprovisto de
temor a la muerte:

> Hay una sentencia de los epicúreos que de ser bien comprendida, no
> será juzgada profana como la estiman los ignorantes […] no considera
> Epicuro verdadera y cumplida virtud de fortaleza y constancia aquella
> que siente las incomodidades y las soporta, sino aquella que sin
> siquiera sentirlas las sobrelleva y no estima cumplido amor divino y
> heroico aquel que siente el aguijón, el freno, el remordimiento, o la
> pena causada por otro amor, sino aquel que no tiene absoluto
> sentimiento de otros afectos […] Y eso es trocar la suma beatitud en
> este estado: gozar de la voluptuosidad sin tener sentido del dolor.@note

La meta cumplida del heroico furioso es un estado en que puede
gozar, legitimamente, de la voluptuosidad. Y sin embargo, ¿podemos
creer que, en efecto, se trate de una meta, un lugar de arribo,
un estado permanente?

Sería ingenuo, e incluso contrario al pensamiento de Giordano
Bruno, creer que el furioso «llega» de una vez y para siempre
a esa beatitud. Aquí se encuentra un nuevo límite al misticismo
de Bruno: el arribo del furioso a este estado no significa término
ni una modificación de la sustancia humana, que lo transforme
en sustancia propiamente divina. Al contrario, «el alma realiza
el doble movimiento de ascenso y descenso por el cuidado que
tiene de sí misma y de la materia, siendo movida por el apetito
del bien e impulsada, por otra parte, por la providencia del
destino».@note

El cuidado de sí y el de la materia constituyen los dos momentos
dinámicos del furioso, pero en ello no se aprecia ya el miedo
a lo temporal y al dolor. Ese doble movimiento de ascenso y descenso
es, en contraste, la expresión de la belleza divina, el equilibrio
y la armonía eterna, por lo que constituyen ya no desgarre y
zozobra, sino principio de acción y certidumbre. La transformación
del furioso es una modificación de sí, en cuanto toma cuidado
de sí y se conoce a sí mismo, y no en cuanto modifica su sustancia.

En otras palabras, el camino del heroico furioso no parece ser
otra cosa que el ejercicio de unas técnicas por las cuales se
propone, finalmente, llegar al cabal cumplimiento del ideal epicúreo:
«en la supresión de todo tipo de dolor está el límite de la magnitud
de los placeres».@note

El epicureísmo de Giordano Bruno es más que la identificación
de felicidad y placer. El Nolano comparte con Epicuro, en principio,
la convicción de superar el temor a la muerte como medio para
alcanzar la tranquilidad del alma. Más aún, en el proceso de
alcanzar esta tranquilidad, Bruno restituye el orden clásico
de las máximas «cuida de ti» y «conócete a ti mismo»; como el
griego, considera el cuidado de sí como el principio que debe
conducir al conocimiento de uno mismo y, en ello, Bruno es más
cercano al pensamiento clásico que al mundo moderno. Pero, sobre
todo, aspira, por medio de las prácticas complejas del camino
del furioso, a fortalecer la razón en su capacidad de discernir,
de modo que, como escribe Epicuro a Meneceo, sea «el juicio certero
que examina las causas de cada acto de elección o aversión [el
que guiíe) nuestras opiniones lejos de aquellas que llenan el
alma de inquietud».@note

Una diferencia distingue, sin embargo, a Epicuro de Bruno. Para
el primero la clave está en la economía de las pasiones, en la
administración cabal de las deudas y los haberes. Para el segundo
ésta estriba en el arrebato de las pasiones por el furor del
amor. En el primero, es el afán último de tranquilidad; en Giordano
Bruno, es el ansia de vivir, el deseo heroico de la armonía y
el orden cósmico.

※ {.espacio-arriba1 .centrado}

Pregunta Cicada, uno de los dialogantes de _Los heroicos furores_:
«Pero, de entre los hombres, no todos pueden llegar allí donde
sólo uno o dos pueden acceder». Ésta es la respuesta: «Basta
con que todos corran; bastante es que haga cada uno cuanto esté
en su mano, pues la naturaleza heroica antes prefiere caer o
fracasar dignamente en altas empresas en las que muestre la nobleza
de su ingenio que triunfar a la perfección en cosas menos nobles
o bajas».@note {.espacio-arriba1 .sin-sangria}

# La rosa y los monstruos

Para Bruno y Paolo {.epigrafe}

Por la carne también se llega al cielo. \
++Gilberto Owen++ {.epigrafe}

Pues debéis saber que me vi obligado \
a hacer una cosa y pensar en otra \
practicando así la doble intención, \
cosa que hasta entonces nunca había hecho. \
_Roman de la Rose_ {.epigrafe}

++La rosa++ está cautiva. Es prisionera en la torre del castillo
levantado por los Celos. La custodian, en primer lugar, Peligro,
al mando de treinta soldados; después, Vergienza, Pavor y Malaboca,
que guardan otras tantas puertas. Afuera, Amador espera y desespera
de amor:

> Grande es mi dolor y mi desconsuelo, \
> Pues nadie jamás podría aliviarme \
> Si un día perdiera vuestro antiguo amor; \
> Solamente en vos puse mi esperanza[…]@note

Así finaliza el texto inconcluso del _Roman de la Rose_, de Guillaume
de Loris. La falta de conclusión, sin embargo, no impidió la
difusión de la obra, ni el interés y el respeto por ella a partir
de su aparición en 1240. La razón ha de buscarse en que el final
abrupto no necesariamente obliga a echar de menos una continuación.
Al contrario, es probablemente la conclusión idónea de una obra
que se ha propuesto cantar el amor desgraciado.

El _Roman_ de Guillaume de Loris es una ingeniosa alegoría construida
mediante la personificación de las pasiones humanas, masculinas
y femeninas, que describe cómo el hombre (Amador) no sólo voluntariamente,
sino impelido por una fuerza mayor ---Amor, que lo hiere para
hacerlo su vasallo---@note busca alcanzar a la Rosa que es la
imagen virginal del sexo femenino.@note Sin embargo, la intención
del hombre es vana. La dama, en tanto que Rosa, le está vedada:
si la alcanza, la degrada y la destruye al corromper su virginidad:
si no la alcanza, el amor es sólo una perpetua insatisfacción.

Loris es heredero cultural de una tradición, la del amor cortés,
que en el siglo +++XIII+++ es fruto de una movilización social
amplia y vigorosa, orientada a restituir y fortalecer las costumbres
morales y religiosas. El movimiento cátaro, al que se ha ligado
siempre el amor cortés.@note y poderoso entonces en el sur de
Francia, impulsa una metafísica y, en consecuencia, una ética
dualista, en la que se condena todo acto sexual, incluyendo el
destinado a la procreación.@note Paralelamente, la Iglesia católica,
que ha emprendido una campaña para moralizar el matrimonio, discute
en qué condiciones puede permitirse el acto sexual y no faltan,
por supuesto, aquellos que se inclinan por censurar como pecado
mortal aun el realizado bajo el sacramento del matrimonio.@note

En uno y otro caso, la disputa acerca del carácter pecaminoso
del acto sexual se centra no tanto en el acto mismo, sino en
su naturaleza placentera. Cátaros y católicos consideran que
el placer es connatural al coito en la medida en que se trata
de un acto de la carne y, por ello, bajo el dominio del mal.
La Iglesia católica, sin embargo, llegará a la conclusión de
que la condena al coito no debe ser tan radical, puesto que sería
equivalente a condenar en definitiva el único medio conocido
de reproducción del hombre. Decidirá, en consecuencia, que el
placer experimentado en la cópula puede no ser pecado mortal,
sino venial, y por tanto exculpable, cuando se realiza siempre
con una única mujer, con la que no se tenga parentesco, dentro
del matrimonio y siempre y cuando esté orientado hacia la procreación
y no hacia el placer.@note

Guillaume de Loris asume este código moral, que prohíbe en general
el placer y establece restricciones a la realización del acto
sexual, de una manera que va más allá de los limites de lo permitido
y lo prohibido. La alegoría del _Roman_, la lucha que se establece
entre Celos, que mantiene cautiva a la Rosa, y el vasallaje de
amor del hombre, ilustran una forma de asumir e interiorizar
el código, por el que el deseo sexual de posesión y ansias de
placer se legitima por vía de la comunión espiritual con la mujer
y la búsqueda última del encuentro con la divinidad. No otra
cosa son los consuelos que Amor regala a Amador, y que se explican
por sí mismos: Esperanza, Dulce Pensar, Dulce Palabra, Dulce
Mirar.@note Se trata de sentimientos y acciones que sustituyen
la posesión por la contemplación espiritual y que aceptan el
doble sentido de poder dirigirse por igual a la dama que a la
divinidad.

A final de cuentas, el _Roman de la Rose_ de Loris asimila el
amor humano a la realización espiritual del hombre, y ha de entenderse
dentro del mismo modelo por el cual los padres de Blanquerna,
personaje de una de las novelas de Ramón Llull, la separan de
su amado, quien, al igual que ella, ingresará a una orden religiosa.@note

※ {.espacio-arriba1 .centrado}

> Pero si he perdido ya toda esperanza, \
> Yo me desespero por poquita cosa. \
> ¿Yo desesperarme? ¡Cómo! ¡No lo haré \
> Y nunca jamás desesperaré!@note

Así inicia Jean de Meun, 36 años después, el segundo texto del
_Roman de la Rose_. El pertenece a una generación distinta, en
todos los sentidos, de la de Guillaume de Loris. El suyo es un
espíritu universitario que ha podido aproximarse a la cultura
clásica desde una posición más libre, menos prejuiciada por las
ideas religiosas dominantes en la época. Esto no quiere decir
que haya desaparecido en él todo atisbo de religiosidad, sino
que es capaz de asimilar la cultura clásica a un proceso que
se propone como una renovación de la religiosidad y de la vida
personal y comunitaria del hombre, en el que juega un papel crucial
la reivindicación del placer.

En el _Roman de la Rose_ escrito por Jean de Meun, el autor reacciona
al código y a la forma de interiorización de éste, implícitos
en el texto de Loris. Su intención es argumentar, utilizando
el mismo modelo alegórico, contra la prohibicion del placer y
a favor de una conducta fundada en la «naturaleza». No vamos
a encontrar en él una problematización del placer similar a la
que existe en Epicuro, Lucrecio u Ovidio ---de quienes heredará
ideas, métodos, estrategias--- porque no parte de una ética de
virtudes que aspira al autodominio, sino de la imposicióni de
un código inflexible que exigirá ser exhibido como absurdo antes
de avanzar en cualquier sentido. Y ello exige definiciones.

En el primer discurso que Razón hace a Amador en el texto de
Loris, aquélla le advierte de la locura que significa el vasallaje
de amor. «Sus penas ---dirá Razón--- serán desmesuradas, / mientras
que el placer dura muy poco. / Pues aunque lo tenga será muy
pequeño, dado que sentirlo es aventurado, / que yo veo a muchos
que se esforzaron, / pero nunca lograron tener lo que querían».@note
Frente a esta Razón que sentencia el riesgo y la fugacidad del
placer y la perpetua insatisfacción, el _Roman_ de Meun opone
una Razón que afirma:

Que aquel que se acopla con mujer \
En su fuero interno debiera querer \
El continuar la naturaleza \
Y perpetuarse en su semejante… \
Por esta razón, Natura dispuso \
Que en tales uniones hubiera placer, \
Para Qque a ese acto nadie se negase \
Y esta obligación nadie aborreciera.@note

Razón presenta el placer como un mecanismo propio de la naturaleza
para forzar a los animales a la procreación y garantizar la continuidad
de las especies. Lejos ya de ser una estrategia del demonio para
seducir y condenar a los hombres, ei placer aparece aquí como
un instrumento para la realización de uno de los fines de todos
los seres: su perpetuación.@note Sin embargo, para Razón, el
placer, a pesar de ser natural, es problemático. En la misma
línea que la Antigiedad clásica, Meun no distingue entre amor
y deseo sexual, entre amor y satisfacción del deseo sensual.
Búsqueda de placer y búsqueda de amor, en consecuencia, se asimilan
e identifican. Por ello a la denuncia de que el amor es contradictorio,
se sigue que quien hace uso de los placeres sin entender su función
natural, consume en vano las fuerzas de la juventud que deberían
dirigirse al cultivo de la razón en el orden de alcanzar una
vida feliz y tranquila, que permita una buena vejez.@note

A Razón le preocupa la inmoderación, el gasto, el derroche de
energías sin un fin determinado. Responde a un esquema estoico.
Pero además, introduce una separación que no es posible encontrar
en el pensamiento clásico: el amor, el deseo sexual, no forma
parte de la conducta racional, porque no responde a ella. Lo
que Meun entiende por amor y placer natural atenderá a otra lógica,
a lineamientos distintos de los de la razón.

Hubo un tiempo, dirá Amigo, en que «los amores eran bellos y
leales, / sin codicia alguna y sin interés, / la vida así era
placentera».@note Un tiempo en que todo era común, «y sobre un
manto… / sin otro _interés que el puro placer_, / venian a unirse
y entrelazarse / aquellos a quienes urgía el amor».@note Un tiempo
en que nadie pretendía ---incluidos hómbre y mujer--- algo que
le fuera propio.

Así, evocando una fabulosa Edad de Oro, Jean de Meun introduce,
a través de Amigo, el personaje más radical del _Roman_ y uno
de los que no es un elemento de la psicología del hombre, la
idea del amor natural.

La Edad de Oro es ahistórica. No está ubicada en el tiempo ni
en el espacio. Recuerda, en abstracto, los amores de Dafnis y
Cloe, pero también se confunde con el Paraíso. Nada impide que
Meun haya proyectado en ambos sentidos su imagen. La función
de esta Edad de Oro es referencial, sirve en el _Roman_ para
asir y comprender la experiencia amorosa sin otro fin que el
placer.

No es difícil, pues, concluir que, para Meun, aquel que se acuesta
por placer con una amiga experimente una aproximación a esa Edad
de Oro. La violencia del acto sexual, el hecho de la carne, condenado
y asociado al demonio, aparece aquí comprendido y vivido como
un acto revestido de la experiencia paradisíaca. ¿Pero qué conjunto
de ideas encierra esta Edad de Oro? ¿Qué significa en la forma
de entender, experimentar y dirigir el placer sexual?

Natura es otro de los grandes protagonistas del _Roman_. A ella
se recurre como última instancia para que la Rosa se convenza
de entregarse a Amador. Natura es quien se ocupa de producir
las piezas individuales de las especies para que Muerte no acabe
con ellas. Su función es esencialmente procreadora y proveedora.
Por ello Dios la hizo «cual si fuera manantial continuo / que
siempre está lleno y siempre fluyente, / cuyo fondo y bordes
son inabordables, / de donde derivan todas las bellezas».@note

Natura no otorga el ser por sí misma. Ello es prerrogativa de
Dios. La individuación de la que se hace cargo tiene un significado
cercano a la acción del demiurgo en algunas metafísicas neoplatónicas.
Es el último ser dentro de la escala divina y su función es la
de reunir en un individuo las características que requiere para
existir. Sin embargo, en el _Roman_ aparece también como _voluntad
de vivir_, porque su función no es sólo dotar de atributos al
individuo, sino impulsarlo, como un sino interior, al deleite
de la existencia y al deseo de perpetuarla.

En el sermón de Genio, enviado por Natura para explicar su posición
frente a la controversia entre Amador y Rosa, Natura queda asociada
con los órganos genitales.@note Éstos son las herramientas de
las que la naturaleza se vale para llevar a cabo la obra divina
y su propia misión.

Tendrian que ser enterrados vivos \
Quienes no hacen uso de las herramientas \
Que Dios conformó con sus propias manos \
Y proporcionó a mi ama Natura \
Para que pudiera proseguir su obra. \
Puesto que empleados convenientemente \
Tales utensilios, los cverpos mortales \
Podrán gozar de perpetuidad.@note

El uso de los genitales, en el hombre como en la mujer, se revela
como lo propio natural, como el ejercicio de la naturaleza y,
a la vez, como la ejecución de la obra divina. Empresas, ambas,
que por su trascendencia no sólo material sino también divina,
reciben el beneficio del placer, que es lo que delata la obra
de Dios y que define, en el mismo sermón de Genio, la estancia
en el Paraiíso.@note

Más que útiles, racionales, bondadosas, las obras de Dios y de
Natura aparecen en el _Roman_ como placenteras para los hombres.
Parecería, entonces, que basta con actuar conforme a la naturaleza,
hacer caso de los designios divinos, para vivir en el placer.

※ {.espacio-arriba1 .centrado}

Meun reconoce en el _Roman_ las dificultades que impiden al hombre
actuar conforme a Natura. Resulta difícil, sin embargo, seguirlo
en la argumentación que intenta explicar las tendencias contranaturales
de los hombres, si es que se trata efectivamente de tendencias.
En realidad, recurre a tres hechos, que ocupan el lugar de los
argumentos, con el fin de mostrar las consecuencias perversas
que de ellos se derivan. {.espacio-arriba1 .sin-sangria}

Así, el reconocimiento del origen supranatural y divino del entendimiento
humano sirve a Meun para explicar que la inmaculada concepción
de Jesús es un hecho extraordinario y que, por ello, no constituye
modelo alguno a seguir para los hombres.@note

De la misma forma, la alegata recogida de Lucrecio contra la
creencia en el significado divino de los fenómenos extraordinarios,
como terremotos y tormentas, y en la determinación de las acciones
de los hombres, es dirigida para mostrar «que sólo el hombre
es el que consiente en su daño» al atacar su propia naturaleza.@note

Finalmente, la castración de Saturno por Júpiter, que establece
el paso de la Edad de Oro a la de Hierro, ejemplifica la sustituación
del orden natural y divino, paradisíaco, por uno humano que privatiza
la propiedad de la tierra y sus frutos, y se revela, finalmente,
como contrario a Natura.@note

Meun no se interesa tanto en explicar por qué el hombre actúa
contra Natura, como en denunciar aquellas prácticas que son contrarias
a la naturaleza humana. No hay que perder de vista que se enfrenta
a una moral que a partir de ciertos principios establece un conjunto
de normas y prohibiciones. Cuestionados los principios y, de
hecho, invertidos al trasponer el placer ---estrategia del demonio---
a lo definitorio de la obra divina, Meun no renunciará al cristianismo
ni al establecimiento de una moral de código que respondan a
este nuevo principio.

Son cuatro las prácticas que en el _Roman_ aparecen como contrarias
a la naturaleza y a cuyo través pueden describirse las ideas
que complementan el contenido de la experiencia natural del placer
como experiencia paradisíaca: la castración, la sodomía, la abstinencia
y el matrimonio.@note Cada uno de ellos es un ataque contra uno
de los principios que opera Natura.

La castración, una práctica muy común y extendida a fines de
la Edad Media,@note es presentada en el _Roman_ como el peor
ataque a la naturaleza. La interrupción misma de su curso, el
simbolo de la caída ---Meun asimila la castración de Saturno
con la expulsión del Paraíso. En estricto sentido, la castración
es un atentado contra la fecundidad desbordante de la naturaleza;
uno de los principios por los que es capaz de trascender la muerte.

Condena similar recibe la sodomía. No se trata aquí sólo de la
homosexualidad, sino, sobre todo, de la sodomía como una extendida
y regular práctica anticonceptiva medieval. La razón es la misma
que la anterior, significa una limitación a la fecundidad, aunque
por supuesto, de ninguna manera tan grave y drástica.

Trato equivalente tiene la abstinencia, tanto como práctica laica
como religiosa. En cualquier caso, es una forma de ir en contra
de uno mismo y de la fecundidad natural.

Finalmente, el matrimonio es la práctica que más ocupa a Meun.
En él se manifiesta ya no el ataque contra la fecundidad natural,
sino contra los otros dos principios que conforman a la naturaleza:
la libertad y la gratuidad o generosidad.

Inicialmente, el matrimonio limita el natural y ardiente deseo
de las mujeres, fomentando la aparición de una horda de maridos
celosos que temen y son explotados por el temor al irrefrenable
ardor femenino. Temor que se traduce en violencia, desesperación
y angustia.

El argumento se remonta ya a Ovidio. El deseo femenino es prácticamente
insaciable por un solo hombre.@note De ahí la contradicción que
encierra el matrimonio como monopolio del amor de una mujer.
Del otro lado, Meun recurre a Eloísa para mostrar el sinsentido
de la práctica matrimonial.@note

Para universitarios como Jean de Meun, el matrimonio no es otra
cosa que una transacción comercial donde la mujer se prostituye
y el hombre «compra» la voluntad de la mujer. De hecho, en el
matrimonio no existe el amor en la medida en que está sometido
a un monopolio que hace menguar la voluntad libre ---violentada
a aceptar como satisfacción del deseo a una única persona---
y en el que la voluntad es objeto de comercio, oponiéndose a
la gratuidad de la naturaleza.

El problema aquí es doble, pues si bien no se atenta contra la
fecundidad, se atenta contra el orden de la naturaleza en lo
que tiene de libre y generoso, condiciones básicas de la abundancia
natural.

Estamos ahora en posición de entender cuándo el placer constituye
una experiencia paradisíaca: en el momento en que se trata de
la realización efectiva de un acto fecundo, producido libremente
y sin un fin determinado.

Aunque Amigo diga a Amador en el _Roman_, al describir el amor
en la Edad de Oro, que «sin otro interés que el puro placer /
venían a unirse y entrelazarse / aquellos a quienes urgía el
amor», el puro placer no implica, como podría entenderlo un lector
del siglo +++XX+++, la realización del acto serual privándolo
de toda intención engendradora. Al contrario, se invita al placer
a pesar de sus eventuales consecuencias procreadoras, porque
ha de tratarse de una entrega libre, generosa, a tal punto desinteresada,
que su fecundidad no es obstáculo, sino fruto consecuente, se
dé o no, de esa generosidad y de esa libertad.@note

Las preguntas que, sin embargo, ha de responder el _Roman_ son:
¿cómo llevar a cabo estos actos paradisíacos en un mundo cuyas
prácticas sociales y privadas atentan contra Natura? ¿Cómo hacerlos
posibles? ¿Cómo llevar a cabo la obra divina? ¿Cómo enfrentar
la corrupción en el mundo? Por paradójico que parezca, Meun encontrará
la respuesta en Ovidio.

※ {.espacio-arriba1 .centrado}

Celos, Malaboca, Verguenza, Peligro y Pavor son quienes mantienen
cautiva a la Rosa. Para Loris, como para Meun, son la personificación
de los temores interiores a la dama. Son elementos de la psicología
femenina. Celos representa el cuidado de uno mismo, y a cuyas
órdenes está Peligro, que advierte sobre todo de la cercanía
del deseo;@note Pavor «no tiene otra aspiración más que hacer
lo que le manden»;@note Verguenza, hija de Razón y Malos Hechos,
que ha venido para ayudar a Castidad,@note es el recato. Malaboca,
por último, es un personaje singular que personifica la traición
a estas potestades de la castidad. En Meun, sin embargo, además
de la personificación propia de los temores femeninos que hereda
de Loris, Celos, Malaboca, Vergiienza, Peligro y Pavor personifican
también la cultura y la educación medieval. Vergúenza es recato,
pero también temor a la fama; Malaboca es el rumor, el qué dirán;
Celos no es ya sólo el cuidado de uno mismo, sino, sobre todo,
los celos de quienes sienten suya a la Rosa y simbolizan lo mismo
la tacañería que la propiedad. {.espacio-arriba1 .sin-sangria}

Para Amador, es decir, para cualquiera que se haya propuesto
la meta del placer, la realización de la obra divina, estos enemigos
enquistados en la psicología femenina y en las convenciones sociales
son la dificultad extrema que ha de enfrentarse. Si bien en general
estos enemigos remiten al pudor y al recato que Ovidio atribuye
a las mujeres como producto del cuidado de sí, en el _Roman_
no implican el reconocimiento del otro como sujeto de deseo y
carecen, por tanto, del sentido positivo que poseen en el _Arte
de amar_.

Meun las presenta como frutos perversos de la caída, de la privatización
de la obra divina; resultados de una moral hipócrita que se ofrece
como salvadora pero que en realidad va en contra del proyecto
divino. Lo que está en juego aquí, el problema de Amador, es
cómo realizar el placer, entendido como recuperación del Paraíso
aquí en la tierra, a pesar de un conjunto de normas externas
---convertidas ya en componentes psicológicos--- que ha pervertido
el sentido de lo placentero y lo divino.

Se trata de llevar a cabo un mandato ---el del amor--- aun a
pesar de las dificultades que se encuentren. Ésta es la diferencia
crucial con el mundo grecorromano, con las estrategias del epicureísmo
clásico. En el _Roman de la Rose_, el yo debe constituirse a
través de la obediencia. Lo curioso es que esa obediencia a la
naturaleza se logra por la vía de la honestidad y la verdad frente
a uno mismo, pero por la vía del engaño y la falsedad ante los
otros.

Falso Semblante es el personaje que representa la vía para cumplir
con el mandato. He aquí su origen:

> Engaño engendró a Falso Semblante, \
> Que se apodera de los corazones, \
> Al cual concibió con Hipocresía \
> Que es dama pérfida y muy traicionera. \
> Toda la crianza de Falso Semblante \
> Vino de su madre, esa puerca hipócrita, \
> Que en todo lugar vive del engaño \
> Valiéndose siempre de la religión.@note

Falso Semblante es ciertamente el engaño, la hipocresía, un personaje
nefasto que el Ejército de Amor rechaza, conformado como está
por valores positivos tomados del _Roman_ de Loris. Y, sin embargo,
ha de aceptarlo. Falso Semblante es el compañero de Abstinencia
Forzosa y, por tanto, el extremo al que ha de recurrir Amador
para evitar ésta, que es un estado contra la naturaleza. La hipocresía
y el engaño se vuelven aquí estrategia para cumplir con el amor,
para restituir el orden divino, verdadero y natural de las cosas.

Falso Semblante tiene su correspondiente femenino en la vieja
encargada de vigilar a Buen Recibimiento (el rasgo femenino que
se abre a los hombres). Uno y otro harán una exposición, extensa,
de las recomendaciones que Ovidio indica en el _Arte de amar_
para la conquista de hombres y mujeres. No hay lugar aquí para
detallar cómo recoge Meun la influencia de Ovidio, en muchos
casos una reiteración enteramente fiel de las estrategias del
_Arte de amar_. Sin embargo, es importante subrayar de qué manera
estas prácticas, heredadas de la Antiguedad clásica, tienen un
sentido diferente, una dirección distinta de la que tienen en
Ovidio.

En los _Remedios de amor_, como en el _Arte de amar_, Ovidio
reitera cómo las conductas están en función del cuidado de uno
mismo y sujetas, por tanto, al talante y la naturaleza particular
de cada individuo. En el _Roman_, en cambio, aparecen como medios
para el cumplimiento de un mandato, como vehículos para la obediencia
de la naturaleza, en un mundo y una organización social que se
muestra contraria a esos fines. Lo que prevalece, entonces, no
es la conformación del sujeto al deseo del otro, como en Ovidio,
sino la falsedad y el engaño, cuyo paradigma es la promesa incumplida
de matrimonio, como estrategia para hacer efectiva una encomienda.
De ahí el acento en Falso Semblante. De ahí, también, que el
amor, el acto sexual y el placer se experimenten como una liberación
del reino de lo humano y la experiencia de lo paradisíaco, en
contraposición con la compraventa de voluntades y la esclavitud
del matrimonio.

El saber, que en el caso del _Roman_ producen las estrategias
de Falso Semblante, es nuevamente conocimiento de uno mismo,
pero un conocimiento entendido como asunción de la libertad.
Dice Natura:

> A los animales, y en esto no hay duda, \
> Que están desprovistos de razón, \
> Les está negado conocerse. \
> Si les fuera dado poder hablar \
> Y usar la razón convenientemente \
> Para así poder tener buen sentido, \
> Saldrían los hombres muy perjudicados. \
> Jamás el caballo, el de buena estampa, \
> Permitir podría que lo domase \
> Ni lo cabalgase algún caballero.@note

La práctica del amor natural, la experiencia paradisíaca del
placer, trae consigo el reconocimiento del libre arbitrio. Es
la conciencia de la propia libertad que conduce al abandono de
los yugos sociales, económicos y morales a los que el hombre
está sujeto, y que lo vuelven temeroso y capaz de actuar en contra
de sí mismo.

El mandato de Natura, como sus exigencias de fecundidad, gratuidad
y libertad, y cuya promesa es el Paraíso, se revela como un acto
de liberación de alcances inmensos. Libera de la condición humana,
al hacer al hombre en parte artífice de la obra divina; lo libera
del temor a la muerte, al prometer la continuidad de la especie;
lo libera, al fin, de la falsa moral y de los falsos sacerdotes.
Pero no sólo. Al liberarlo, al llevar a cabo el mandato, lo hace
desafiar el destino, acrecentar su ingenio, su valor, su honestidad.

※ {.espacio-arriba1 .centrado}

Gian de Prócida ---cuenta Boccaccio---@note es un joven apuesto
que ama secretamente y sin derecho alguno a Restituta, la hija
de un noble de la isla de Ischa. Una mañana, mientras la joven
se pasea por la costa, es raptada por un grupo de sicilianos
que la regalan al rey Federico de Sicilia.
{.espacio-arriba1 .sin-sangria}

Sin pensar en las consecuencias, Gian se hace llevar a Sicilia
con el objeto de rescatarla. Su audacia tendrá resultados imprevisibles.
Primero, encontrará y logrará entrar a la habitación en la que
Restituta está cautiva. Y lo hará ayudado por ésta que, al creerse
deshonrada, prefiere entregarse a Gian en lugar de al rey. Dentro
de la habitación, ambos jóvenes se entregarán al amor con un
frenesí insospechado, acostándose muchas veces hasta que el cansancio
los vence y caen dormidos.@note Entonces, Gian es sorprendido
por el rey en el lecho de su amada. Ambos son atados desnudos
para ser expuestos en público y más tarde quemados.

Sin embargo, el rumor de la condena lleva a un viejo amigo del
padre de Gian y almirante del rey Federico, a interceder por
él ante el monarca, del cual obtiene la libertad de ambos. Además
de los argumentos políticos ---que son contundentes---, el almirante
señala al rey: «Son, además de esto, jóvenes que largamente se
han amado y empujados por el amor y no por el deseo de desafiar
tu señoría, aun cuando esto sea pecado, _si se puede llamar pecado
al que por amor los jóvenes han cometido_. Por lo que ¿cómo quieres
hacerlos morir cuando con grandísimos placeres y presentes debías
honrarlos?».@note

El argumento del almirante hace hincapié en que la audacia de
enfrentar al rey, en su tierra y tomando a la doncella que le
ha sido regalada, es fruto del mandato del amor. Es éste el que
lleva a cometer actos en los que, antes que otra cosa, lo que
está en juego es la vida. Para el que ama, no realizar el amor
o morir es lo mismo, de la misma forma en que lo es vivir y amar.
Boccaccio comparte con Meun la idea de que vida y amor constituyen
una fórmula indisociable. La falta de una equivale a la pérdida
de la otra. Por ello, cuando el amor no logra su meta, la vida
perece.

Boccaccio también cuenta la historia de Micer Guiglielmo de Rosellón
y Micer Guiglielmo Guardastaño, vecinos y amigos. Pero Guardastaño,
no obstante la amistad que había entre ellos, «fuera de toda
medida», se enamora de la mujer de Rosellón. Ella corresponde,
pero la falta de discreción los delata. El de Rosellón cita a
Guardastaño, que aún lo cree su amigo, y lo mata. Con sus propias
manos le arranca el corazón que más tarde habrá de preparar y
servir a su mujer durante la cena. Cuando, ignorante de lo que
come, ella lo termina, él le pregunta si fue de su gusto. Sí,
responde ella. Y el marido: «[…] y no me maravillo si muerto
os ha gustado lo que vivo os gustó más que cosa alguna».@note
Oyendo esto y entendiendo que se ha comido el corazón de su amado,
ella reflexiona y, en un gesto, se precipita a través de una
ventana del castillo y muere.

Tras la muerte de uno de los amantes, que ocurre siempre que
logra vencer la ley humana o los celos de posesión del padre
o marido, el otro ha de morir también. Hasta tal punto vida y
amor se vinculan en Boccaccio. Es por ello que frente al desafío
de amor, la ley humana, la pretensión patriarcal, los celos del
marido, aparecen como absurdos. Son ellos, siempre, en su arrogancia
e inflexibilidad, los que precipitan a la muerte.

El _Decamerón_ destila la convicción de la podredumbre, la hipocresía,
la corrupción de la era. El argumento parece ser el mismo que
encontramos en el _Roman_: las leyes humanas se oponen al mandato
de Natura, ponen obstáculos a la realización de la vida y tienen
por resultado extinguirla. A partir de esta premisa todos los
amores se vuelven legítimos, incluso aquellos que se realizan
con el engaño, la hipocresía, el ingenio, pues llevan siempre
el sino de la realización del mandato.

Masetto de Lamporecchio se hace pasar por mudo para entrar a
trabajar como jardinero en un convento. Las monjas y las abadesas,
que saben que será discreto por necesidad, porfiarán por acostarse
con él, y lo harán. Pero el trabajo de satisfacer a nueve insaciables
mujeres convence a Masetto de «recuperar» la voz y confiar a
la abadesa sus fatigas. Muerto el viejo mayordomo del convento,
se difunde la noticia de que se obró un milagro: el jardinero
mudo recuperó la voz. Vuelto mayordomo, «de tal modo [las monjas]
se repartieron sus trabajos que pudo soportarlos. Y en ellos
bastantes monaguillos engendró […] Así pues, Masetto, viejo,
padre y rico, sin tener el trabajo de alimentar a sus hijos ni
pagar sus gastos, por su audacia habiendo sabido bien proveer
a su juventud, al lugar de donde había salido con una segur al
hombro, volvió, afirmando que así trataba Cristo a quien le ponía
los cuernos sobre la guirnalda».@note

La historia de Masetto bien puede ser tomada como un retrato
del paraíso que imagina Meun. Están presentes, señaladamente,
la fecundidad, la gratuidad y la libertad que caracterizan la
experiencia paradisíaca del amor natural. Los numerosos monaguillos,
la prodigalidad de las monjas y la abadesa con su jardinero,
y la falta de miedo de Masetto, son imágenes que describen estas
tres grandes categorías.

Lo interesante, sin embargo, no es que Boceaccio comparta e ilustre
ideas de Meun, sino que las convierta en un estilo en que el
riesgo a perder la vida se resuelve, gracias al ingenio y a la
audacia, en alegría y risa.

A la estrategia del cazador y el artista, que definen el estilo
del epicureísmo romano, se suma aquí la de la vida festiva, por
la que se celebra el triunfo de la vida sobre la muerte; el éxito
de la sensibilidad y los placeres, frente a la aspiración a la
insensibilidad y el vacío.

Los amantes de Boccaccio son personajes frágiles, expuestos a
la muerte, porque con el amor se juegan la vida. Cuando la consumación
del amor es evitada por la ambición humana, que siempre es deseo
de propiedad y exclusividad, la vida perece. Por eso cuando triunfa
lo hace sobre la muerte y lo hace siempre burlándose, elevando
el ingenio y la audacia frente al riesgo.

Antes de extremar precauciones, quien busca el placer ha de crecer
en imaginación y astucia. Para consumar el Paraíso ha de perseguir
no sólo el engaño, como mecanismo para desvelar, estimular y
conservar el deseo; ha de perseguirse la burla, propiciar acciones
encaminadas no sólo a engañar, sino a poner en ridículo los afanes
del contrario.

Al disfrazarse de franciscano para asaltar la torre de Celos,
Falso Semblante no sólo engaña; se burla. Lo mismo hacen los
personajes de Boccaccio. No les basta con engañar, han de ridiculizar
lo que es contrario a la vida y mostrarlo como un absurdo. Ésa
es la razón de la alegría y de la risa. Alegría de triunfo y
risa de burla.

Pero este estilo no es privativo de los afanes amorosos. Exagerada,
festiva, la obra de otro francés, Francois Rabelais, completa
el estilo propio de este goce que abandona la Edad Media y surge
en el Renacimiento. La burla es también aquí la celebración del
cuerpo en su sentido más amplio, como celebración de la naturaleza
toda.

No hay diferencia. Quien alcanza la meta de hacerse con la Rosa,
burla y se burla de todo aquello que la ha hecho cautiva. Y lo
hace, precisamente, celebrando el cuerpo, generoso, natural y
libre.

# El ejercicio del recto pensamiento

Para Luis Ernesto Nava {.epigrafe}

Todos los prejuicios proceden de los intestinos \
++Friedrich Nietzasche++ {.epigrafe}

++En una breve réplica++ al consejero Hufeland, y tal vez pensando
en su maestro Leibniz, Kant escribía:

> Para un sabio el pensar es un alimento sin el que no puede vivir cuando
> está despierto y solo; puede consistir en la instrucción (lectura de
> libros) o en la meditación (reflexionar y descubrir). Pero ocuparse con
> un pensamiento determinado al comer o al andar, cargar la cabeza y el
> estómago, o la cabeza y los pies, con dos trabajos al mismo tiempo
> produce hipocondria lo primero y mareo lo segundo […]

> Tienen lugar sensaciones patológicas cuando, en una comida sin
> compañía, se ocupa uno al mismo tiempo con la lectura de libros o la
> reflexión, porque la fuerza vital es desviada por el trabajo de cabeza del
> estómago, al que se molesta.@note

No es casual que Kant pida una dietética del pensamiento para
lograr una afortunada digestión y evitar malestares ficticios.
Acerca de los problemas y meditaciones más mundanos de los filósofos,
como un feliz encuentro con el idolo de cerámica y la fortuna
de una calma, más bien llamada alegría, en el vientre, hay innumerables
documentos. La alusión a Leibniz no es gratuita. Según se cuenta,
este filósofo alemán comía sentado sobre un asiento en cuyo centro
había un hoyo y bajo él una bacinica, y quiero imaginarme que
sin vestido alguno que impidiera la salida franca de sus despojos
cuando el alimento llegaba a su destino. Habría que preguntarnos
seriamente si esta generosa actividad de sus intestinos es una
razón de la brevedad de su obra. Pero, escenas escandalosas aparte,
los filósofos han abordado el tema desde más de una arista.

Otro alemán. Nietzsche ---y aquí quiero hacer notar que no es
fortuito que los problemas gástricos relacionados con el pensamiento
conciernan sobre todo a los filósofos de extracción germánica---,
confirma esta opinión y recomienda:

> Una comida fuerte es más fácil de digerir que una demasiado pequeña.
> Que el estómago entre todo él en actividad es el primer presupuesto de
> una buena digestión. Es preciso conocer la capacidad del propio
> estómago. [Por ello] No tomar nada entre comida y comida, no beber
> café: el café ofusca. El té es beneficioso tan sólo por la mañana. Poco,
> pero muy cargado; el té es muy perjudicial y estropea el día entero
> cuando es demasiado flojo, aunque sea en un solo grado.

Y agrega, siempre incisivo: «estar sentado el menor tiempo posible;
no prestar fe a ningún pensamiento que no haya nacido al aire
libre y pudiendo nosotros movernos con libertad, ---a ningún
pensamiento en el cual no celebren una fiesta también los músculos.
Todos los prejuicios proceden de los intestinos».@note

Hay muchas otras ideas, recomendaciones y problemas para tratar
el delicado estómago de los filósofos, pero sería muy prolijo
entrar en más detalles. En realidad, basten estos pocos ejemplos
para dejar en claro que los filósofos recomiendan no hacer coincidir
el acto de pensar con el de comer, por la sencilla razón de que
la ejecución simultánea de ambos procesos termina lo mismo por
volvernos hipocondríacos o prejuiciosos, cosas ambas que, como
todos hemos podido comprobar alguna vez, caracterizan a los filósofos.
Pero lo que nos preocupa aquí es saber cuál es la fuente de estas
ideas, cuál es ese extraño vínculo que une al pensamiento con
la digestión. Si la tradición popular ha querido que el Pensador
de Rodin sea el emblema del hombre que está cagando, ello se
debe no sólo a que la figura del pensador se asemeja en postura
y gestos al sujeto que ha ido a aliviar las tripas, sino a que
existe, desde siempre, ur vínculo que une el acto de pensar ---con
su esfuerzo implícito--- al de expulsar el excremento de nuestro
cuerpo.

Como todo en la historia, este vínculo entre el ejercicio del
recto pensamiento y el ejercicio, simple, del recto, posee una
genealogía. Aquí me referiré tan sólo a un momento de esta historia
en la que se encuentran un filósofo, Marsilio Ficino, y un escritor,
Frangois Rabelais, con la intención de aproximarme a entender
cuál es la razón por la que los filósofos padecen con frecuencia
trastornos gástrico-intestinales y, paralelamente, por qué las
obras del ingenio son el resultado de una digestión afortunada
y evacuaciones, en consecuencia, igualmente felices.

※ {.espacio-arriba1 .centrado}

En un par de largos tratados, Marisilio Ficino ha querido dar
cuenta de la razón de este vínculo, que tal vez no le fuera extraño.
Hablo de la _Teología platónica_ y de los _Libros de la vida_.
Como la cuestión de que a uno se le «afloje el mastique» es algo
poco docto en el hablar, aunque común en el obrar, Ficino no
nos dice, franca y directamente, cómo es que en la persona de
los filósofos, sobre todo, pero en general, entre los estudiosos,
los problemas de gastro son más que el pan de cada día. Pero
podemos inferirlo a través de sus cavilaciones en torno a la
salud del cuerpo de los sabios. {.espacio-arriba1 .sin-sangria}

La primera razón por la que el filósofo suele padecer del estómago,
según se desprende de los textos ficinianos, es porque la digestión,
desde el momento en que el alimento entra a la boca, hasta que
sale, es un proceso espiritual. Y aquí convendría que nos detuviéramos
a examinar en qué sentido el proceso que ocupa a los intestinos
puede ser obra del espíritu, y cuál es el sentido que tiene el
espíritu para que pueda ocuparse de una función tan heroica como
la de llevar el alimento a su destino final.

Ya sabemos que el espíritu es concebido por Ficino como un intermediario
entre el alma, donde reside el ejercicio del intelecto, y el
cuerpo, donde reside el proceso de la digestión. Para uno y otro
acontecimiento el espíritu está compuesto, a su vez, de una gran
cantidad de pequeños espíritus que son el alimento de la reflexión
y los portadores del orden que debe seguir la transformación
de los alimentos en desperdicios. Los espíritus ficinianos son
«un cuerpo sutilísimo, que casi no es cuerpo sino alma, ni casi
alma sino cuerpo. Su virtud es contener poquísima naturaleza
terrena, un poco de más de aquella líquida, más de la aérea,
pero sobre todo, mucho de aquella del fuego de las estrellas
[…]. Por ello vivifica todo y es responsable próximo de toda
generación y movimiento».@note

En efecto, materia tan sutil que no es cuerpo, alma tan densa
que no es alma, los espíritus tienen la cualidad de conectar
entre sí las funciones que son propias de uno y otra: la claridad
del intelecto y el esfuerzo de tirar el tronco al río. Ficino
señala que «después de generados por el calor del corazón de
la parte más pura, sutil, caliente y clara de la sangre, los
espíritus se dirigen al cerebro; ahí, el alma los utiliza continuamente
para el ejercicio de los sentidos interiores y exteriores. Ésta
es la razón por la cual la sangre sostiene los espíritus; éstos,
los sentidos y, finalmente, éstos, la razón».@note Más adelante,
Ficino explica: «el estómago, por la acción diurna del aire,
abre sus poros y se dilata; este movimiento debilita los espíritus,
entonces, al aproximarse la noche, demanda un nuevo suplemento
de espíritus para mantenerse».@note Por ello, dirá a continuación,

> La persona que inicia una importante reflexión en ese momento (al
> aproximarse la noche], fuerza a los espíritus a retornar a la cabeza.
> Cuando se han dividido, no son suficientes para ambos […] además, el
> hecho de que un movimiento de esta clase hace que la cabeza se cubra
> de los vapores densos que provienen de los alimentos, y la comida en el
> estómago, sin el calor de los espíritus, permanezca indigesta y se pudra,
> por lo que bloquea y lastima la cabeza. Finalmente, por la mañana, es
> necesario purgar todo el excremento acumulado por el cuerpo durante
> el sueño. Por ello, lo peor de todo, es que la persona que interrumpe
> su digestión estudiando en la noche y durmiendo después del
> amanecer, está impedida de expulsar el excremento durante mucho
> tiempo.@note

Así, pues, el limitado número de espíritus, que no alcanza a
atender al intelecto y al estómago de manera simultánea, genera
trastornos tanto en la mente como en los bajos fondos, cuando
se les ocupa en ambas tareas. Esto nos permitiría pensar que
Kant tendría razón al invitar a una dieta de la reflexión durante
la comida, y nos dejaría entender que Leibniz padecía de una
especie de incontinencia espiritual durante la digestión, a cambio
de un estreñimiento en el pensar.

Pero las consecuencias gástricas del espíritu no sólo tienen
que ver con su cantidad, sino también con las funciones que le
ocupan y que Ficino hace protagonistas de este acontecimiento:
el sentido, la imaginación, la fantasía y la memoria. Estas cuatro
funciones del espíritu que, desde el punto de vista gnoseológico,
son las más bajas, mientras que desde la perspectiva del cuerpo
son las más altas, constituyen el núcleo de la vida sensible:
la sensibilidad. Se trata de una cadena jerárquica, heredada
por Ficino de la tradición aristotélica, que comienza en el sentido
y finaliza en la fantasía, y que constituye un proceso en que
la percepción va despojándose de su materialidad, primero, después
del orden de la sucesión en el espacio y el tiempo, hasta convertirse
en un fantasma, desvinculado por completo de cualquier relación
con el objeto sensible del cual proviene. Sin embargo, para el
florentino, el fantasma puede también irse revistiendo de orden,
temporalidad y materia, hasta constituirse en una verdadera percepción
sensible.

De esta forma, debemos entender que la otra manera en que el
espíritu colabora con la digestión, lo mismo que con el pensamiento,
es a través de las imágenes. De hecho, lo que se consume en la
reflexión, y lo que el movimiento de gastro agota en los espíritus,
son las imágenes que éstos transportan, volviéndolas borrosas.
Una buena o mala digestión, como una buena o mala intelección,
dependen de la calidad de las imágenes que les ofrezcan los espíritus.
Son ellas las que, de una parte, despiertan las inquietudes reflexivas
del alma y, por otra, establecen el orden en que se procesan
los alimentos. En efecto, los espíritus no participan en la intelección,
que es un proceso autónomo, ni en la descomposición de alimentos.
Su papel es crucial desde el punto de vista de que establecen
el ritmo en que ambos se efectúan. Simplemente, el hecho de que
no sean suficientes para pensar y comer señala la necesidad de
una sucesión en estas tareas. A nivel de imagen, lo son porque
impactan y modifican ese ritmo. De ahí que haya necesidad de
cuidar lo que nuestros sentidos reciben y la forma en que lo
hacen.

Al explicar qué ocurre cuando la imaginación y la fantasía eventualmente
se ven ocupadas por imágenes horribles, Ficino señala:

> El acto de estas virtudes (ver, oír, sentir) concuerda con la vibración
> del espíritu y, a la inversa, del mismo modo que la sensibilidad y el
> movimiento de la araña que se encuentra en el centro de su telaraña,
> donde concurren cada uno de sus hilos, en el momento en que ésta
> viene a ser golpeada por una mosca que volando tropieza con ella, sigue
> inmediatamente la vibración por todos los hilos de la tela […]

> Del hecho de que un objeto que estimula nuestros sentidos en una
> medida superior a las demás, no sólo viene a provocar una confusión
> en el acto de discernimiento, sino que también determina una lesión
> dañina en nosotros mismos, de modo que, una vez lesionado el
> espíritu corporal, por esta fuerza, en congruencia con una corriente
> recíproca entre ésta y el espíritu corpóreo, deriva en el ánimo una
> lesión parecida.@note

Pero si, como se ha dicho, las facultades del espíritu no son
unidireccionales en Ficino, convendría preguntar qué sucede cuando
el espíritu corporal, lejos de sufrir una lesión como la de una
imagen horrible, experimenta placer. Y aquí me veo obligado a
recordar varias anécdotas de las vidas de Pantagruel y Gargantúa
que me son especialmente gratas.

※ {.espacio-arriba1 .centrado}

En uno de los pasajes más célebres de la obra de Rabelais, Pantagruel
conoce en uno de sus viajes a Gaster, un dios ventripotente;
es decir, un estómago, a quien los gastrólatas sacrifican un
día sí y otro también innumerables alimentos.@note A este dios
se le atribuye la invención de todas las artes, de todas las
máquinas, de todos los oficios, de los instrumentos y las utilidades
y, todo, por la panza. {.espacio-arriba1 .sin-sangria}

En este pasaje se exalta, pues, el vientre y el ingenio que a
partir del primero se despliega para alcanzar su satisfacción.
Pero cuidado, no se celebra aquí sólo el comer. No se trata ni
por el volumen ---Bajtin afirma que la lista de alimentos que
se ofrecen a Gaster es la más extensa del Medioevo---@note ni
por los alimentos, casi todos ellos pesados y flatulentos,@note
de gozar la delicadeza del paladar. No, la abundancia y la especie
de alimentos van dirigidos, en realidad, a celebrar todo el proceso
de digestión. De ahí que cobren importancia no sólo el comer
sino su contraparte: el cagar.

Se goza lo que entra y por dónde entra, como lo que sale y por
dónde sale. No hay vergienza ni pena, sólo exaltación del goce
de esas partes del cuerpo humano que transforman lo más necesario
y vital en excrecencias. Se disfruta, en suma, el cuerpo como
totalidad, en su magnífica formación de equilibrios. Hay imágenes
en que lo puro bajo también engendra. Así, Pantagruel, al querer
imitar a Panurgo, quien ha soltado un pedo para exaltar a su
señor, suelta él también uno, pero con consecuencias impredecibles,
pues «la tierra tembló en nueve leguas a la redonda y el aire
corrompido engendró más de cincuenta y tres mil hombrecillos
enanos y contrahechos […]J alos que llamó pigmeos».@note Por
todas partes se exalta, pues, al mismo tiempo que el equilibrio
sutil del cuerpo, su perfección ---aún en lo más execrable---,
su capacidad procreadora. La pregunta de Panurgo ante los inesperados
resultados de la acción de Pantagruel ---«¿Cómo es que vuestros
pedos son tan fructiferos?»--- es esencial. Robert Klein ha mostrado
en un ensayo la relación que guardan los suspiros, eso que dejamos
escapar como un soplo de aire, con la teoría general de los espíritus.
En particular, ha hecho notar cómo en Cavalcanti, un amigo personal
de Ficino, las imágenes poéticas que utiliza de los espíritus,
de los sentidos o de la mirada hiriente o consoladora, el espíritu
mensajero, soporte de una imagen mental, pero también de los
_spiriti_ expulsados que escapan del cuerpo, están sostenidas
en la concepción médica y mágica que la tradición le atribuye
a los espíritus.@note No debe extrañarnos, pues, que esos espíritus
que escapan al suspirar, cuando el corazón está ya muy cargado
de ellos, también lo hagan por lo bajo. No olvidemos que los
espíritus son cuerpos sutiles que se agolpan lo mismo en el corazón
que en la cabeza o en el vientre. Son, según la más vieja tradición
estoica, una suerte de _pneuma_, de aire que, aunque hediondo,
todavía conserva parte de su función principal: transportar una
imagen. Así como al suspirar dejamos escapar la imagen de la
amada que nos apresa el corazón y nos liberamos de la tristeza,
del mismo modo, cada flatulencia deja escapar la imagen, ya maltrecha,
que nos tortura el vientre. Y valdría la pena pensar si éstas
no serán esas ideas que los filósofos, después de un arduo trabajo
de crítica, dejan que se filtren hasta los intestinos.

En otro pasaje, Grangaznate, padre de Gargantúa, cae en cuenta
de la intcligencia de éste cuando, tras plantcarse el que a mi
juicio sigue siendo uno de los problemas centrales de la raza
humana: el medio más señorial, más excelente, más expedito que
jamás se viera para limpiarse el culo, Gargantúa concluye:

> Mas en conclusión digo y mantengo que no hay limpiaculos mejor que
> el de un gansito de muy fino plumón, con tal que se le sujete la cabeza
> entre las piernas. Creedme por mi palabra de honor. Pues sentís en el
> agujero del culo una voluptuosidad mirífica, tanto por la suavidad del
> plumón como por el calor templado del propio ganso; voluptuosidad
> que se comunica fácilmente a la tripa cular y demás intestinos, hasta
> llegar a las regiones del corazón y el cerebro. Y no penséis que la
> bienaventuranza de los héroes y semidioses que están en los Campos
> Eliseos consista en su asfódelo, ambrosía o néctar, como dicen por
> aquí las viejas. Según mi opinión, es debida a que se limpian el culo
> con un gansito, y tal es también la opinión de Juan de Escocia.@note

Éste no es el único pasaje de la obra de Rabelais que destaca
la importancia de los instrumentos destinados a propiciar mejores
sensaciones a los hombres. Sin embargo, bien puede ser un ejemplo
de aquello que Ficino señalaba respecto a la necesidad de cuidar
nuestras propias percepciones. La clave del gansito de fino plumón
no está sólo en la suavidad y la frescura del mismo, sino en
que produce una sensación que se comunica hasta el cerebro y
que tal vez termine por arrojar algunas buenas ideas y, en un
camino de ida y vuelta, cagar bombones.

En cualquier caso, es más que probable que la tesis de los espíritus
esté presente en Rabelais, y la razón es simple: la función del
espíritu es ligar al alma con el cuerpo. Tal es el principio
vital y la condición misma del engendramiento. Ya hablamos del
pasaje de Gaster. Al estómago se le atribuye toda la inventiva
humana, toda la capacidad técnica del hombre, en fin, el don
de inventar todo lo necesario para vivir 0, más exactamente,
para comer, así como, por supuesto, los medios de obtener y conservar
el grano.

Esto no es gratuito. La capacidad de engendrar no se refiere
sólo al proceso fisiológico de gestación y parto. Más sutilmente,
engendrar es también la creación de cosas humanas, la perfecta
poesía, las técnicas arquitectónicas, las fantásticas estrategias
militares, en fin y, para decirlo en una palabra: todas las cosas
del espiíritu. De ahí que, en una larga consecuencia Rabelais
acepte que «en la composición [de Gargantúa] no perdí ni empleé
nunca más otro tiempo sino el que estaba establecido para tomar
mi refrección corporal, a saber, para beber y comer».@note

El goce del cuerpo es, así, el engendrador del espíritu en un
sentido amplio, de la imaginación, del ingenio de toda construcción
humana. Para entender qué clase de goce espiritual del cuerpo
es éste que nos ofrece Rabelais, es necesario pensar, más allá
de los críticos que enlazan su pensamiento con la escuela paduana
de Pomponazzi,@note en el neoplatonismo renacentista. En _Timeo_,@note
Platón define el alma como cuerpo y espíritu. En el célebre pasaje
del Demiurgo, éste toma a la materia (la sustancia de lo otro)
y la mezcla profusamente con el espíritu (la sustancia de lo
mismo) para conformar el alma del mundo, pues sólo de esta forma
el espíritu y la materia pueden engendrar uno en el otro, el
orden y la concreción de las cosas. Esta idea está en Ficino
y en los miembros del circulo de la Nueva Academia, aunque todavía
con su carácter metafísico, con algunas derivaciones en el ámbito
de la medicina. Será, sin embargo, Rabelais quien la condense
finalmente en un estilo de vida, en un arte de la existencia
preñada de valores éticos, pero también estéticos, y que celebran
a cada página Gargantúa y Pantagruel.

Las imágenes producen en el cuerpo y en el alma, por su cualidad
de medio y mensaje, una alteración del ritmo mismo del organismo.
Pero es posible apreciar en ello la unidad del cuerpo humano
que se pone de manifiesto, y la necesidad de asirla cabalmente,
pues no dejo de pensar, y me encanta, que la belleza no radica
en esas malas costumbres que atribuimos a héroes y semidioses:
la fatiga, el desánimo, la muerte y el horror. La belleza radica
en percibir en la sensación el acuerdo simple de todo nuestro
cuerpo. Algo en que los filósofos parecen buenos aficionados,
mas no peritos, especialmente los alemanes.

Es curioso ver que lo que sobrevivió de estas ideas compartidas
por Ficino y Rabelais fue sobre todo la idea de que la insuficiencia
del espíritu para estar, a la vez, en el estómago y la cabeza,
fuera la causa de tantos malestares digestivos; y a la vez, el
descubrimiento de que los héroes son aquellos que han sabido
comer hasta el hartazgo y disfrutar de las consecuencias gástricas
de sus actos. La filosofía alemana, por ejemplo, se olvidó del
espíritu y de su capacidad de transportar imágenes. Por lo tanto,
olvidó a los sentidos con su peculiar forma de decirnos lo bueno
y lo malo de la existencia y lo fundamental que esto resulta
para una vida dichosa. Veámoslo claramente: lo más cerca que
Leibniz estuvo del espíritu y de una buena digestión, fue pensando
en unas mónadas carentes de sensibilidad. Kant, por su parte,
nos dotó de principios puros que resultaron ser rectores inflexibles
de la sensibilidad, y es probable que él mismo obrara con la
misma rigidez. Y Nietzsche… Nietzsche alguna vez visitó Italia.
En fin, hipocondríacos o prejuiciosos, los filósofos de vez en
cuando deberían retornar a la vida sensible.

# Epílogo. Breve defensa de la fantasía

Para Aureho y Maria Pia {.epigrafe}

++Prometí contar++ la historia de una inquietud y una alternativa,
para seguir su huella hasta la idea de espíritu que brota aquí
y allá en el pensamiento renacentista. Pero, y ¿después?

Después, los espíritus y fantasmas han desaparecido. O al menos,
han sido abandonados a su suerte y olvidados. Un pasaje de los
Principia ethica de G.E. Moore escrito en los albores del siglo
+++XX+++, es casi un réquiem:

Pensamos que la contemplación emotiva de una escena natural,
suponiendo que sus cualidades sean igualmente bellas, de algún
mo es un estado de cosas mejor que la contemplación de un paisaje
pintado; pensamos que el mundo mejoraría si sustituyéramos las
mejores obras de arte representativo por objetos reales igualmente
bellos.@note

Aun concediendo, con sentido común, que sería magnífico que las
escenas naturales tuvieran la belleza del arte, ¿tiene sentido
pensar que lo real, lo «natural», el mundo físico, es un atributo
que hace más bella a la belleza? Esto es tanto como suponer que
merece de manera absoluta el calificativo de belleza aquello
que, siendo bello, es natural.

La superioridad de lo natural frente al arte es una de las formas
de señalar la superioridad de la realidad frente a la fantasía;
de lo pesado frente a lo ligero, de lo tangible frente a lo intangible.
Una manera, en suma, de privar al hombre del valor de su propia
interioridad, de sus sueños, de sus visiones, de sus voces, de
sus fantasmas. A final de cuentas, ¿dónde están hoy las fantasías?
¿Dónde yacen esos fantasmas que habitarán las alcobas del futuro
y que hoy deben recorrer los corredores de nuestro espíritu?
¿Dónde la posibilidad de ser otra cosa distinta de lo que se
es?

En sus _Seis propuestas para el próximo milenio_, ltalo Calvino
propone la ligereza como atributo de la literatura del nuevo
milenio. Una ligereza que es a la vez una defensa de la fantasía
y de la imaginación, más allá incluso del arte: «Antes de ser
codificadas por los inquisidores, estas visiones [el vuelo de
las brujas en una escoba o en una brizna de paja] formaba parte
de lo imaginario popular o, digamos, de lo vivido».@note

Y tal vez no fueron sólo los inquisidores. Iloan Couliano ha
reparado en que la historia del Fausto, contada en las obras
populares de titeres y el inmortal texto de Marlowe, es un panfleto
en contra del humanismo italiano. De manera mucho más específica,
habría que decir que es un panfleto en contra de la utilización
de las imágenes fantásticas, de los fantasmas y, por ello, de
la fantasía misma, que el pensamiento de la Reforma encuentra
peligrosamente ligadas al erotismo y, por lo tanto, al demonio.

Escribe Couliano: «La más perfecta expresión de la Reforma es
la leyenda del Fausto, que contiene todas las características
ideológicas [de esta] […] censura de lo imaginario; la culpabilidad
intrínseca de la naturaleza y su principal instrumento, la mujer…».@note
De hecho, para él, esto «produjo todas nuestras neurosis crónicas,
que son el resultado de la completa unilateralidad de la orientación
cultural de la Reforma y de su negación de los cimientos principales
de lo imaginario».@note

Siguiendo el orden del pensamiento de Couliano, el verdadero
centro del ataque del pensamiento reformista es el spirito peregrino
por el que el alma asciende al paraíso en el soneto _Oltre la
sphera que piú largo gira_ de la _Vita Nuova_ de Dante. Porque
ese spirito no fue una invención sólo de poetas, sino también
de médicos y filósofos, y en sí mismo comprende una manera de
concebir la percepción y, por ende, una posibilidad de percibir
la existencia y de vivirla. Es, si se puede, el costado ético
de la levedad literaria que Calvino pide conservar para el siglo
+++XXI+++.

Comencemos por entenderlo en su consistencia física: los espíritus,
una pluralidad como los átomos de Demócrtito y Epicuro, son,
según los médicos y filósofos del medioevo y el renacimiento,
producto del vapor de la sangre.@note De ahí que Marsilio Ficino
diga que los espíritus son casi cuerpo, casi alma.@note A estos
cuerpos sutilísimos que recorren todo el organismo, se les atribuía
en el Renacimiento las funciones que Aristóteles asignaba al
alma animal: el sentido, la imaginación, la fantasía y la memoria.
Es decir, los elementos necesarios para la percepción del mundo
externo y su enlace con la capacidad intelectiva del hombre.
Así, la imaginación era la facultad en donde se produce la sustracción
del cuerpo y, por ende, del peso de las cosas, y la fantasía
donde estas imágenes sin peso son organizadas a voluntad para
darles un valor y un sentido.

Para Ficino, por ejemplo, el sentido únicamente percibe las cosas
cuando están presentes. Es decir, cuando su peso es palpable
a los sentidos exteriores. Pero, una vez que han desaparecido
de la percepción, la imaginación transforma ese peso en imágenes,
imago es el término que utiliza Ficino para referirse al producto
de la imaginación. Estas imago, ligerísimas de peso y dinámicas,
que recorren todo el cuerpo a través de los espíritus, son la
materia de la memoria y de la fantasía. La primera las conserva
y archiva en perfecto orden, tal y como fueron adquiridas por
la imaginación. La fantasía, en cambio, las utiliza libremente
para dar cuerpo a los fantasmas, aún más sutiles porque están
conformados por fragmentos de imágenes ordenadas a partir del
capricho de la libre voluntad del hombre.

Para que sirvan a la memoria y a la fantasía por igual, no obstante
haber eliminado el peso del cuerpo, la imaginación forma las
imágenes respetando la dimensión del espacio y el orden temporal
en que se formaron. Eso es lo que, con fidelidad, trata de conservar
la memoria, pero que la fantasía sustrae para ser libre, generosa,
creativa. Si la imaginación es como el espejo a través del cual
Perseo puede mirar, sin petrificarse, a la Medusa, la fantasía
es el lugar donde aparece todo aquello de lo que se vale Perseo
para ser un héroe: las sandalias aladas, Pegaso, la virtud para
petrificar a los enemigos, los corales.

Para el Renacimiento en general, era crucial que la fantasía
estuviera ligada a la voluntad libre del hombre. De todas las
facultades de los espíritus, la fantasía era la única que operaba
con absoluta libertad.@note Capaz de combinar entre sí las imágenes
más dispares, la fantasía operaba fuera de las restricciones
del resto de las funciones sensibles de los espíritus. Esta características
la convertían en el reflejo privilegiado del carácter de cada
persona, porque mostraba la forma en que cada individuo se representaba
a sí mismo el mundo. De hecho, el concepto de «aire», que hoy
todavía utilizamos para decir que alguien posee un aire de familia,
remitía en parte, durante el Renacimiento, a la carga fantástica
de los espíritus que formaban parte de la herencia que los padres,
a través del semen y la gestación, dan a los hijos. Pero el hecho
de que la fantasía se vinculara con la voluntad libre la hermanaba
también con la prudencia, una derivación del concepto de la fronesis
aristotélica,@note que más que una entidad racional, apuntaba
hacia la práctica de la evaluación necesaria, ante las circunstancias
exteriores y la condición de uno mismo, a fin de establecer el
contenido correcto del término medio.

Así entendida, la prudencia es una suerte de disciplina y ascesis
de la fantasía. Era el juicio de la sensibilidad ---cultivado
con la finalidad de sustraer los fantasmas de los espíritus al
libre capricho de la voluntad y de los deseos, y dirigirlos hacia
lo más alto: la belleza--- la más leve de todas las cosas, que
resplandece en los cuerpos, según Ficino, como orden, modo y
hermosura, pero que no es sino «una cierta gracia, vivaz y espiritual»,@note
que sólo se percibe y se manifiesta a través de la fantasía.

Era de tal importancia la fantasía para los hombres del Renacimiento,
como Ficino y Bruno, que se ocupaban de cómo alimentarla en el
plano físico ---pues no olvidemos que la fantasía es ejercida
por los espíritus que son corpúsculos emanados de la sangre---
y también en el plano espiritual. En _De vita_, Ficino se ocupa
de los colores, los olores, los sonidos de la fantasía y las
imágenes que ayudan a esa ascesis de la fantasía para embellecer
el alma. Rescato de este texto de Ficino un ejemplo de cómo construir
un talismán:

Para obtener una vida larga y feliz, ellos [los antiguos] hacían
una imagen de Júpiter en una piedra clara o blanca. Éste era
un hombre coronado, sentado sobre un águila o un dragón, vestido
con un hábito amarillo, hecho en la hora de Júpiter cuando él
está, afortunamdamente, ascendiendo en su exaltación.@note

Tratemos ahora de entender cómo es que este talismán podía colaborar
en la obtención de una vida efectivamente larga y feliz. ¿Cuál
era la influencia positiva en quien lo construye y lo porta?
¿De qué manera podía cobrar cuerpo y, digamos, consistencia y
peso en la existencia de la persona?

No nos sorprendamos si descubrimos que su función no era la de
concentrar las fuerzas positivas, o la de conjurar los poderes
negativos. Porque no se perseguía a través del talismán canalizar
los rayos cósmicos o modificar desde fuera los poderes que supuestamente
rigen la vida de los hombres. El poder real del talismán, como
el del olor del vino o la cadencia de un ritmo, es que ordenaba,
según el propio Ficino, la fantasía. Más simple de lo que se
cree, menos misterioso y supraterreno, el talismán era una pieza
de evocación, al igual que lo son alguna tonada o el suave olor
de los nísperos. Una evocación destinada a moderar, en un caso,
el súbito desorden de nuestras fantasías, o a fortalecer, en
otro, la luz de la belleza con que construimos la experiencia
de nuestra vida.

Busco otra forma de expresar lo mismo. Los fantasmas que construimos
con nuestra voluntad, libremente, nos sirven para asir lo que
sentimos y lo que pensamos. Para traducir eso que se percibe
en una reflexión y el resultado de ésta, en algo que seamos capaces
de sentir. Si ello es así, el punto donde lo sentido se cruza
con lo pensado, y a partir del cual podemos enunciar si el acto
de amor fue placentero, o si tal decisión que hemos tomado ha
sido justa, o bien que la tarde ha sido cubierta de belleza,
es la fantasía ---y su estatuto ontológico, precisamente es ese:
fantástico, es decir, subjetivo, vinculado, pero a la vez autónomo
respecto de la percepción y de la razón---. Si ello es así, toda
imagen colabora a cribar y orientar nuestra propia percepción
del mundo y de nuestra propia existencia.

Por ejemplo, Júpiter. Júpiter es, en la astrología ficiniana:
«el dios de la vida común, es la deidad que representa, sobre
todo, aquellas cosas que mantienen la vida moderada, especialmente
las leyes y el gobierno, el mantenimiento de la vida social y
civil».@note

Nuestro talismán en cuestión muestra, entre otras cosas, para
alcanzar una vida larga y feliz, necesitamos ejercitar la moderación.
Ésa es la razón de la presencia de Júpiter coronado, pues su
sola imagen, que como tal puede llegar hasta la fantasía, no
sólo nos invita a considerar la moderación de la vida social,
en oposición, por ejemplo, a la imagen de Saturno, que elogia
el aislamiento y la senilidad, sino que induce a la fantasía,
con su sola visión, a ceñirse a la moderación. Es decir, sirve
a la vez de ejemplo y de estructura para nuestras fantasías.

Dado que los fantasmas son los elementos a partir de los cuales,
según Ficino, se desata el proceso de reflexión, pero que, simultáneamente,
conforman la estructura del cuerpo ---no olvidemos que son cuerpo,
también--- y el modo de evaluar el entorno, el que se induzca
a la fantasía a la moderación significa también que ello repercutirá
en fantasmas más claros para la mente que reflexiona, y en moderación,
no sólo del organismo todo ---como sosiego---, sino de la forma
de comportarse de los hombres en la vida civil.

El mismo Ficino, en un pasaje de la _Teología platónica_, señala
cómo la fantasía de Sócrates es capaz de apreciar la bondad y
la belleza de Platón.

Esta apreciación, todavía atada firmemente a la sensibilidad
y la percepción, no corresponde a una aserción que acepte las
categorías de falso o verdadero, porque se trata sólo de la expresión
de algo que ha sido sentido por Sócrates. Aquí no se ha establecido
aún, para Ficino, el vínculo con la universalidad de lo bueno
y de lo humano; simplemente, Sócrates fantasea que Platón es
bueno y bello, y ello es suficiente para hacer grata la tarde
que ambos pasen juntos.

La fantasía de Sócrates es suficiente para sustraer el peso de
la realidad, y rodearnos con la capa mágica de la apreciación
de lo bello-bueno. Esto no significa, ni por mucho, evasión ---como
hoy escuchamos en los gritos de alarma de aquellos que, siguiendo
a Moore, nos urgen a ser realistas, auténticos, coherentes, iguales
siempre a nosotros mismos---.

No, no es evasión. En un sentido fuerte, tomar en cuenta la fantasía
---la que es propia, la de cada uno de nosotros, aquella con
que hoy nos decimos a nosotros: éste es un buen día, este rostro
me ilumina, éste es el perfume del amor--- significa simplemente
no ajustarse, no conformarse.

A diferencia del pensamiento adaptativo que hoy compartimos con
los estoicos, el pensamiento de magos, filósofos, poetas y médicos
del Renacimiento se orientaba a la transformación; una que parta
de uno mismo, del modo en que se contempla el mundo, la forma
en que se estructura las fantasías; de alguna manera, lo que
se siente, lo que se anhela, lo que se piensa.

Cito de una carta que Marsilio Ficino envía a Piero Vanni, porque
a un mismo tiempo nos retrata y nos enfrenta:

> Los hombres piden todos los días a Dios cosas buenas, pero nunca
> piden que ellos hagan buen uso de ellas. Desean que la fortuna
> responda a sus deseos, pero no les importa que sus deseos respondan a
> la razón. Les gustaría que todos los muebles de su casa, hasta el más
> mínimo adorno, sea lo más bello posible, pero casi nunca se interesan
> porque su alma sea bella. Diligentemente buscan remedios para todas
> las enfermedades del cuerpo, pero ignoran las enfermedades del alma.
> Ellos piensan que pueden estar en paz con los demás, cuando
> continuamente están en guerra con ellos mismos; porque siempre hay
> una batalla entre cuerpo y alma, entre sentidos y razón. Creen que
> pueden encontrar amigos sinceros, pero nadie se mantiene fiel así
> mismo […] Ellos hacen piedras a semejanza de hombres y convierten a
> los hombres en piedra.

> Qué estado más penoso. Nosotros buscamos lo mejor en lo peor, lo
> alto en lo bajo, lo bueno en lo malo, el descanso en la actividad, la paz
> en la disensión, la plenitud en la penuria; en suma, la vida en la
> muerte.@note

Aunque hoy ya no creamos en esos corpúsculos que se llamaron
espíritus, aunque no pensemos ya, nunca más, que salían de nuestro
cuerpo en un suspiro, todavía tenemos la fantasía. Sobre ella
es que, en adelante. necesitamos construir la ética del nuevo
mileno.

# Los autores

Abenjaldún (citado desde E. Garin) (Túnez, 27 de mayo de 1332-17
de marzo de 1406), es un renombrado filósofo árabe, cuyos trabajos
en historia, ciencia y astrología tuvieron gran influencia en
Occidente. {.frances}

Agustín, san, obispo de Hipona (13 de noviembre de 354-28 de
agosto de 430), es considerado el más grande de los padres de
la lIglesia católica y un filósofo a la altura de Platón y Aristóteles.
Su doctrina sienta las bases del pensamiento medieval y es fundamental
para entender el desarrollo posterior de la filosofía. {.frances}

Alberto Magno, san (1206-Colonia, 15 de noviembre de 1280), maestro
y guía de santo Tomás de Áquino, es considerado uno de los fundadores
del pensamiento escolástico. Comparte con su discípulo la influencia
de Aristóteles, aunque su pensamiento sobresale por su interés
en la física. {.frances}

Andreas Capellanus (siglo +++XII+++) es el autor del libro sobre
el amor más influyente en la Edad Media: De amore. Su texto,
basado en las obras de Ovidio El arte de amar y los _Remedios
de amor_, es una de las fuentes más apreciadas de lo que hoy
conocemos como amor cortés. {.frances}

Dionisio Aeropagita (pseudo) es el nombre con el que se identifica
a un grupo de obras escritas entre los siglos +++IV+++ y +++V+++,
y que son fundamentales dentro del pensamiento cristiano. Entre
estas obras se encuentran la Teología mística y los Nombres divinos,
en los que se fundamentan las vías negativa y positiva del conocimiento
de dios. {.espacio-arriba1 .frances}

Enrique Cornelio Aggripa (1486-1535), filósofo, alquimista y
mago es, junto con Ficino y Bruno, uno de los pensadores claves
del hermetismo renacentista. Su obra principal, La filosofía
oculta o la magia, es de extraordinaria importancia por la síntesis
de los conceptos mágicos que hace en ella.
{.espacio-arriba1 .frances}

Flavio Claudio Juliano (331-363), llamado también el apóstata,
fue emperador del Imperio romano y accedió al trono tras la muerte
de Constancio. Intentó restituir los cultos paganos prohibidos
por sus antecesores. Se conservan de él numerosos escritos y
discursos, los cuales conforman una interesante síntesis de pensamiento
mágico y míistico. {.espacio-arriba1 .frances}

Francois Rabelais (1483?-1562?) es el autor de Garagantúa y Pantagruel,
dos de las obras más asombrosas de la literatura francesa por
su irreverencia, gracia y comicidad. {.frances}

Giordano Bruno (Nola, Napoles, 1548-Roma, 1600) es la figura
filosófica más conocida del Renacimiento y la más destacada de
la segunda mitad de ese periodo. Considerado un pensador original,
a menudo es presentado como un visionario por el carácter de
sus tesis, algunas de las cuales han sido retomadas hoy en día.
{.espacio-arriba1 .frances}

Giovanni Cavalcanti (1444?-1509), poeta italiano, íntimo amigo
de Marsilio Ficino. Uno de los personajes del texto De amore,
del mismo Ficino. {.frances}

Giovanni Pico della Mirandola (24 de febrero de 1463-17 de noviembre
de 1494) es una de las personalidades más destacadas de la filosofía
florentina del Renacimiento. Contemporáneo de Ficino y Savonarola,
con los que compartió afinidades e intereses filosóficos en distintos
momentos de su vida. Es autor del Discurso acerca de la dignidad
del hombre, en el que sintetiza la visión renacentista del ser
humano. {.frances}

Guillaume de Loris y Jean de Meun son los dos autores de la obra
que hoy conocemos como _Roman de la Rose_. De Meun (c. 1260-1305)
es el continuador de la obra escrita por de Loris alrededor de
cincuenta años antes del nacimiento de Jean de Meun. En su conjunto,
los dos escritos representan una amplia visión del cambio de
las ideas sobre el amor en la Edad Media . {.frances}

Hermes Trismegisto. A esta figura se le atribuye la redacción
de lo que conocemos como el _Corpus hermeticum_. No hay pruebas
de su existencia real, pero durante siglos representó al sabio
egipcio autor de las tesis mágicas que hoy caracterizan al hermetismo.
{.espacio-arriba1 .frances}

Hugo de San Victor (1096-1141), filósofo, teólogo y místico,
es citado con frecuencia como una de las referencias fundamentales
en la inflexión de la doctrina Aristotélica de las facultades
del alma hacia el uso que el Renacimiento hará de ellas. {.frances}

Lucrecio (96 a.C.-55 a.C.) es autor de De rerum natura, un poema
filosófico de inspiración epicúrea, valorado tanto por la belleza
de su lengua, como por su influencia en autores posteriores,
como el propio Marsilio Ficino. {.espacio-arriba1 .frances}

Marsilio Ficino (Florencia, 19 de octubre de 1433 -Correggio,
1 de octubre de 1499) es tal vez el pensador más importante de
la primera mitad del Renacimiento. De él se destaca, tradicionalmente,
su labor como traductor de Platón. Sin embargo, poco a poco se
le ha reconocido originalidad e importancia filosófica. Su pensamiento
funde la tradición aristotélica y neoplatónica.
{.espacio-arriba1 .frances}

Numenio de Apanema (c. 120-160 d.C.) es un filósofo de probable
origen sirio y de gran influencia entre pensadores cristianos
y neoplatónicos ---llegó incluso a rumorarse que Plotino no hacía
sino repetir sus enseñanzas---. Se considera una fuente crucial
para comprender los puntos de contacto entre el gnosticismo,
el neoplatonismo y la cristiandad. {.espacio-arriba1 .frances}

Origenes (185-254 d.C.) es uno de los padres de la iglesia católica
más brillantes, pues dotó al pensamiento cristiano de firmes
bases filosóficas retomadas de la antigúedad clásica, particularmente
de Platón a través de Plotino. Tuvo una influencia decisiva en
San Agustín. {.espacio-arriba1 .frances}

Plotino (204-270 d.C:.) es el fundador del neoplatonismo y uno
de los filósofos más influyentes de los primeros siglos de nuestra
era. Destaca sobre todo su influencia en Origenes y San Agustín,
a los que su compleja teoría de la unidad última de todas las
cosas servirá de sustento. {.espacio-arriba1 .frances}

Ramón Llull (Raimundo Lulio) (Palma de Mallorca entre 1232 y
1236-Túnez, 29 de junio de 1315) es uno de los filósofos y místicos
más originales de la Edad Media y uno de los más influyentes
en el pensamiento renacentista. Sus trabajos alrededor del arte
combinatoria constituyen una de sus mayores aportaciones.
{.espacio-arriba1 .frances}

Salustio (86-35 a.C.) es un historiador romano autor de la Conjuración
de Catilina. {.espacio-arriba1 .frances}

Sinesio de Cirice (c. 414) es un autor sumamente relevante para
la historia de la psicología occidental. Su tratado Sobre los
sueños, constituye uno de los textos más influyentes en la filosofía
de Marsilio Ficino. {.frances}

Tomás de Aquino, santo (Nápoles, 1225?-Fossa Nuova, 7 de marzo
de 1274) es el más importante filósofo de la escolástica medieval
y una de las principales figuras en la historia de la filosofía.
Su pensamiento, que tiene un amplio soporte en Aristóteles, constituye
una influencia determinante en la filosofía de Ficino.
{.espacio-arriba1 .frances}

# Bibliografía

Agustin, San, _De libero arbitrio_, en _Obras de San Agustín_, vol. 2., +++BAC+++, Madrid,
1964. Éste es, quizás, uno de los tratados más importantes de San Agustín porque
concentra los temas centrales de su pensamiento de una manera más reflexiva que
doctrinaria. {.frances}

Aristóteles, _Acerca del alma_, traducción de Tomás Calvo Martinez, Gredos, Madrid,
1988. Se trata de una de las obras que mayor influencia han ejercido sobre la
concepción del alma humana a todo lo largo de la historia. Es clave para poder
comprender cualquier concepción medieval o renacentista del alma. {.frances}

Bruno. Giordano. Cause, _Principle and Unity_, Cambridge University
Press, Cambridge, 1998. Esta edición de la _Causa, principio
y uno_ de Bruno contiene, además, los dos tratados breves sobre
magia: _De magia_ y _De vinculis_. Las tres obras son excelentes
para entender la relación entre el pensamiento metafísico y mágico
en el nolano. {.espacio-arriba1 .frances}

Bruno. Giordano, _Los heroicos furores_, traducción de Maria
Rosario González Parada, Tecnos, Madrid, 1987. Libro crucial
porque contiene el pensamiento ético de Bruno y su consistencia
con sus ideas mágicas. {.frances}

_Evangelios apócrifos_, traducción de Edmundo González Blanco,
Hyspánica, Buenos Aires, 1985. Los llamados evangelios apócrifos
son una de las fuentes fundamentales para comprender el pensamiento
gnóstico y la complejidad de su doctrina, aunque este volumen
carece de un buer aparato crítico. {.espacio-arriba1 .frances}

Ficino, Marsilio. _Scritti sull'astrologia_, bajo el cuidado
de Ornela Pompeo Faracovi, Bur, Milán, 1999. Este volumen reúne
un par de tratados breves y cartas de Ficino sobre la astrología.
En particular, el tratado _De sole_, uno de los textos más útiles
para comprender los diversos sentidos que la simbología astrológica
tiene para él. {.espacio-arriba1 .frances}

---, _Sobre el amor. Comentarios al Banquete de Platón_, traducción
de Maria Pia Lamberti y José Luis Bernal, Universidad Nacional
Autónoma de México, México, 1994 . Quizás la obra más popular
de Ficino, sintetiza una parte sustancial de su pensamiento,
con énfasis en su concepción del amor como principio universal.
{.frances}

---, _Teologia platonica_, traducción de Michele Schiavone, Centro
di studi filosofici di Gallarte, Bologna, 1965. Considerado como
el más completo tratado filosófico del pensamiento ficiniano,
es un texto sbre la inmortalidad del alma en que, sin embargo,
se discurre extensamente sobre metafísica y sobre la percepción
y el conocimiento. {.frances}

---, _The Letters_, traducido por miembros del Language Deparment
of the School of Economic Science, Londres, Shepheard-Walwyn,
Dallas, 1979. Ficino publicó sus cartas en vida y son consideradas
de un alto valor filosófico. Muchas de las ideas cruciales de
su pensamiento están expuestas aquí. {.frances}

---, _Three Books on Life_, traducción de Carol V. Kaske y John
R. Clark, Medieval 6 Renaissance Texts & Studies, Nueva York.
1989. Segundo y último de los tratados de Ficino, puede ser considerado
su obra definitiva sobre magia. Extraño dentro de la tradición
filosófica, es un libro que se plantea consejos para prolongar
la vida, mejorar la vida de los estudiosos y vivir conforme al
cielo. {.frances}

Hermes Trismegisto, _Obras completas_, 3 t., traducción de Miguel
Ángcl Muñoz Moya, Biblioteca Esotérica, Madrid, 1988. Esta edición
reúne los diálogos llamados hermética en una traducción descuidada,
pero en texto bilingüe. {.espacio-arriba1 .frances}

_Hermetica_, Solos Press, editado y traducido por Walter Scott,
Gran Bretaña, s/f. La edición de Scott de los Hermetica es una
de las más reconocidas sobre estos documentos que sirven de fuente
para comprender tanto lo que llamamos hermetismo en el siglo
+++II+++ como su influencia posterior en pensadores como Ficino.
{.frances}

## _Bibliografía coplementaria_

Aries, Philippe (director), _Historia de las religiones. Las
religiones en el mundo mediterráneo y en el oriente próximo_,
I y II, traducción de Lorea Barrutti y Alberto Cardini, Siglo
XXI. México, 1979. Este volumen es una de las vías más accesibles
para comprender el contexto y el sentido de los movimientos gnósticos.
{.frances}

--- y Georges Duby (directores), _Historia de la vida privada_,
vol. I (dirigido por Paul Veyne), traducción de Francisco Pérez
Gutiérrez, Taurus, Madrid, 1990. Obra fundadora de una nueva
visión del quehacer del historiador. El volumen coordinado por
Paul Veyne se concentra en la vida imperial de Roma. {.frances}

Cassirer, Ernest, _El individuo y el cosmos en la filosofía del
Renacimiento_, traducción de Alberto Bixio, Emece, Buenos Aires,
1951. Texto clásico sobre el pensamiento renacentista que, aunque
discutible en la actualidad, aporta todavía elementos clave para
comprender la cosmovisión del Renacimiento.
{.espacio-arriba1 .frances}

Couliano, loan P., Eros and Magic, traducción de Margaret Cook,
The University of Chicago Press, Chicago, 1987. Polémica desde
su publicación, la obra de Couliano se ha convertido en un clásico
en cuanto al tema de la magia renacentista, particularmente la
magia fantástica de Bruno. {.frances}

Eyjólfur Kajalar Emilsson, _Plotinus on Sens-Perception: A Philosophical
Study_, Cambridge University Press, Cambridge, 1988. Se trata
de un estudio sumamente especializado en el tema de la percepción
en Plotino. {.espacio-arriba1 .frances}

Foucault, Michel, _Historia de la sexualidad_, 2. _El uso de
los placeres_, traducción de Martí Soler, Siglo XXI, México,
1988. Esta obra de Foucault, que puede considerarse como un parteaguas
en su propio pensamiento, constituye a mi juicio una de las aproximaciones
más originales al problema de la moral. {.espacio-arriba1 .frances}

Garin, Eugenio, _El zodiaco de la vida_, traducción de Antonio
Prometeo Moya, Peninsula, Barcelona, 1981. Garin es uno de los
autores fundamentales sobre el pensamiento renacentista. En esta
obra aborda los orígenes del pensamiento astrológico en el Renacimiento.
{.espacio-arriba1 .frances}

Granada, Miguel Ángel, _Cosmología, religión y política en el
Renacimiento_, Anthropos, Madrid, 1988. Obra obligada para quienes
se interesan en el pensamiento político renacentista, contiene
elementos muy útiles para comprender la articulación del pensamiento
de Ficino, Pico, Savonarola y Maquiavelo. {.frances}

Jonas, Hans, _La religión gnóstica. El mensaje del Dios Extraño
y los comienzos del cristianismo_, traducción de Menchú Gutiérrez,
Siruela, Madrid, 2000. Otro de los estudios clásicos sobre el
pensamiento gnóstico. La obra de Hans destaca por el estudio
de las diferencias entre el pensamiento clásico y el gnóstico.
{.espacio-arriba1 .frances}

Klein, Robert, _La forma y lo intelegible_, traducción de Inés
Ortega Klein, Taurus, Madrid, 1990. Los ensayos de Klein constituyeron
en su momento una de las miradas más intensas sobre el Renacimiento
y constituyen, todavía hoy, una fuente de debate continuo.
{.espacio-arriba1 .frances}

Kristeller, P.O., _Il pensiero filosofico di Marsilio Ficino_,
Sansoni, Firenze, 1953. Esta obra es un estudio de referencia
sobre la filosofía de Marsilio Ficino, por lo que su lectura
es obligada. {.frances}

Pagels, Elein, _Los evangelios gnósticos_, traducción de Jordi Beltrán, Crítica, México,
1982. Estudio sobre ciertos aspectos del pensamiento gnóstico, en el cual se discute,
principalmente, la imagen de dios en los evangelios apócrifos.
{.espacio-arriba1 .frances}

Puech, H.C., _En torno a la gnosis_, traducción de Francisco
Pérez Gutiérrez, Taurus, Madrid. 1982. Ensayos del que es, sin
duda, uno de los investigadores más influyentes sobre el tema
del gnosticismo. {.frances}

Shumaker, Wayne, _The Occult Sciences in the Renaissance_, University
of California Press, Los Angeles, 1972. El contenido de este
libro puede ser considerado de difusión sobre las cuatro prácticas
básicas del ocultismo renacentista: alquimia, magia, astrología
y producción de talismanes. {.espacio-arriba1 .frances}

Summers, David, _El juicio de la sensibilidad_, traducción de
José Miguel Esteban, Tecnos, Madrid, 1993. Un largo estudio sobre
la evolución del llamado juicio de la sensibilidad y sus implicaciones
en la construcción del gusto renacentista, que resulta especialmente
sugerente. {.frances}

Thomas Moore, _Planets Within: the Astrological Psychology of
Marsilio Ficino_, Marquette University Press, s/1, 1990. Aunque
escrito por un terapeuta reconocido, no deja de ser una exposición
bastante clara de algunas de las implicaciones que la concepción
del alma en Ficino pueden tener en la actualidad.
{.espacio-arriba1 .frances}

Thorndike, Lynn, _A History of Magic and Experimental Science_,
vol. IV, _Fourtheenth and Fifteenth Centuries_, Columbia University
Press, Nueva York, 1934. La más clásica de las obras contemporáneas
sobre magia. Aunque en muchos sentidos ha sido cuestionada, continúa
siendo un esfuerzo meritorio por su erudición. {.frances}

Tripolitis. Antonia, _The Doctrine of the Soul in the Thought
of Plotinus and Origen_, Libra Publishers, Nueva York. 1978.
Un ensayo marginal sobre los vinculos en el pensamiento de estos
dos autores. Pese a su esquematismo, el libro traza diferencias
y coincidencias que resultan clave para entender la evolución
del pensamiento cristiano desde una fuente pagana. {.frances}

Voss, Angela. «The Astrology of Marsilio Ficino: Divination or
Science?», en _Culture and Cosmos_, vol. 4, núm. 2, Autumn/Winter
2000. Éste esun ensayo especializado en uno de los puntos más
oscuros del pensamiento de Ficino: el carácter explicativo o
mágico de la astrología en su filosofia. {.espacio-arriba1 .frances}

Walker, D.P., _Spiritual and Demonic Magic_, Warburg Institute,
Londres, 1958. Uno de los libros fundamentales sobre el pensamiento
mágico en el Renacimiento. Este es, junto con las obras de Yates
sobre Bruno, el punto de partida de la reivindicación de la magia
como problema filosófico en el Renacimiento.
{.espacio-arriba1 .frances}
