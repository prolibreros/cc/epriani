Por ejemplo, Paul Veyne, Philippe Ariés, Hans Jonas, Wayne A.
Meeks, entre otros.

P. Veyne, «El Imperio romano», en _Historia de la vida privada_,
p. 213.

P. Veyne, _op. cit._, p. 218.

_Ibidem._

Aunque el neoplatonismo jugará un papel crucial en la transformación
del concepto que el individuo tiene de sí mismo, no lo considero
aquí como corriente independiente porque su máximo representante,
Plotino, aún conserva una posición ética más ligada a la antigiedad
clásica que a la revolución gnóstica.

H.C. Puech, _En torno a la gnosis_, p. 15.

H.C. Puech, _op. cit._, p. 15.

H. Jonas, _La religión gnóstica_, p. 269.

Evangelio de Tomás, 32, 19-33. citado por E. Pagels, _Los evangelios
gnósticos_, p. 180.

H.C. Puech, _op. cit._, p. 20.

_Evangelio de San Valentín_, cap. XVI-621. en _Evangelios apócrifos_,
p.754.

H. Jonas, _op. cit._, p. 288.

_Cf._ J. Dorresse, «El hermetismo egipciante», en _Las religiones
en el mundo mediterráneo y en el Oriente próximo_, II, pp. 96
y ss.

J. Dorrese, _op. cit._, pp. 118 y ss.

_Corpus hermeticum. Poimandres_, I, 1, p. 3.

_Corpus hermeticum. Pormandres_, III, 1. p. 25.

G, Pico della Mirandola, _Conclusiones mágicas y cabalísticas_,
X, 6, pp. 46-47.

_Corpus hermeticum, Fragmentos de Estobeo_. IV, 6, p. 34.

_Corpus hermeticum. Poimandres_, VIII, 1, p. 43.

_Corpus hermeticum. Poimandres_, I, 12, p. 6.

_Corpus hermeticum. Poimandres_, I, 14, p. 7.

_Idem._, I, 15, p. 7.

Thorndike, _A History of Magic and Experimental Science_, vol.
I, p. 8.

_Corpus hermeticum. Asclepio_, 6. p. 37.

_Corpus hermeticum. Poimandres_, III, 3-4, p. 26.

_Ibidem._

_Corpus hermeticum. Asclepio_, 10. pp. 42-43.

R. Klein, «Spiritu peregrino», en _La forma y lo inteligible_,
p. 41.

Como ejemplo de esta discusión pueden verse, entre otros, E.
Garin, _El zodiaco de la vida_; W. Shumaker, _The Occult Sciences
in the Renaissance_; D. P. Walker, _Spiritual and Demonic Magic_;
T. Moore, _Planets Within: the Astrological Psycholcgy of Marsilio
Ficino_; y L. Thorndike, _A History of Magic and Experimental
Science_.

Respecto de la ambiguedad en Ficino puede verse A. Voss, _The
Astrology of Marsilio Ficino: Divination or Science?_ y E. Garín,
_El zodiaco de la vida_.

E. Garín, _El zodiaco de la vida_, p. 62.

Respecto a los ataques de Giovanni Pico della Mirandola, Giambatista
Vico y Savonarola a la astrología, _cf._ M.A. Cranada, _Cosmología,
religión y política en el Renacimiento_.

E. Cassirer, _El individuo y el cosmos en la filosofía del Renacimiento_,
pp. 147-148.

_Cf._ M. Ficino, _Scritti sull'astrologia_, p. 183.

A. Voss, _op. cit._, p. 37.

M. Ficino, _op. cit._, p. 187

_Idem._, p. 195.

«[…] el espíritu, que los doctores definen como un vapor de sangre ---puro, sutil, bueno y
claro--- luego de que el calor del corazón lo genera a partir de la sangre más sutil, se encamina
 hacia el cerebro; y allí el alma lo utiliza continuamente para el ejercicio de los sentidos
tanto interiores como exteriores». M. Ficino. _Three Books on Life_. p 111.

_Cf._ Artistóteles, _De anima_, Libro III.

Aristóteles, _op. cit._, 424a.

D. Summers, _El juicio de la sensibilidad_, p. 46. Imaginación,
en el pasaje de Summers, corresponde a la fantasía, que es el
término utilizado por Aristóteles.

D. Summers, _op. cit._, p. 76.

_Idem._, p. 33.

M. Ficino, _Three Books on Life_, III, III. p. 257.

M. Ficino, _Teología platónica_, VIII, I, p. 395.

_Loc. cit._

«[…] la imaginación es el punto crucial de encuentro entre razón
y sentidos en la psicología de Aristóteles. La imaginación es
pasiva, padece desde abajo la acción de la sensación, desde dentro
la de los sueños y alucinaciones, desde arriba la de formas y
diagramas mentales de artes y ciencias, o de las nociones de
recta conducta. La sensación arroja las imágenes internas de
las que parte el pensamiento que ha de volver a ellas para actuar».
D. Summers, _op. cit._, p. 46.

M. Ficino, _Teología platónica_, VIII-I.

_Cf._ M. Ficino, _op. cit._, VII-V.

_Ibidem._

A. Tripolitis, _The Doctrine of the Soul_, p. 56.

A. Tripolitis, _op. cit._, p. 57.

_Idem._, p. 28.

_Corpus hermeticum. Fragmentos de Estobeo_, XXIX.

M. Ficino, _Teología platónica_, XIII-I.

O. Wilde, «La decadencia de la mentira», _Obras_, vol. III, p.
52.

M. Ficino. _Teología platónica_, VII, I, p. 259 ss. En _De amore_,
Ficino presenta la misma distinción entre sentidos, con la salvedad
de que, además, los relaciona con la escala de la matería; así.,
el tacto corresponde a la tierra, el gusto al agua, el olfato
a los vapores, el oído al aire y la vista al fuego. _Cf._ M.
Ficino, _De amore_, V, II.

M. Ficino, _Teología platónica_, VII, , p. 359.

M. Ficino, _op. cit._, VIII, I, p. 303.

M. Ficino, _Three Books on Life_, III, III, p. 257.

M. Ficino, _Teología platónica_, VII, 6, pp. 376-378.

R. Klein, _La forma y lo inteligible_, p. 62.

Sinesio, _Tratados_, p. 265.

Skinner, _The Cambridge History of Renaissance_, p. 467.

Ficino utiliza el término de Lucrecio para caracterizar las particulas
que contienen la imagen del objeto. _Cf._ Luerecio, _De la naturaleza
de las cosas_, IV.

M. Ficino, _Teología platónica_, VIII, I, p. 395.

_Ibidem._

La capacidad de juzgar, propia de la estimatio o sentido común
medieval, queda asociada a la fantasía en Ficino.

M. Ficino, _Teología platónica_, VIII, I, p. 395; el subrayado
es mío.

_Idem._, VIII, I, p. 397.

C. Kaske, en su introducción al texto _Three Books on Life_ de
Marisilio Ficino, p. 22.

Citado por C. Kaske, _loc. cit._

M. Ficino, _Three Books on Life_, p. 119.

_Ibidem._

D.P. Walker, _Spiritual and Demonic Magic_, p. 4.

M. Ficino, _Three Books on Life_, p. 113.

_Ibidem._

_Idem._, p. 114.

M. Ficino, _Teología platónica_, VII, XV, pp. 385-387.

G. Bruno, «De magia», en _Cause, Principle and Unity_, p. 142.

_Cf._ C. Bruno, _De magia_, p. 105.

_Idem._, p. 132.

_Cf._ G. Bruno, «De vinculis in genere» en _Cause, Principle
and Unity_, p. 158.

_Cf._ G. Bruno, _op. cit._, p. 149.

_Idem._, p 146.

G. Bruno, _Los heroicos furores_, p. 207.

G. Bruno, «De vinculis in genere», en _Cause, Principle and Unity_,
p. 155.

_Cf._ I.P. Couliano, _Eros and Magic_, p. 58; y R. Klein, _La
forma y lo inteligible_, p. 69.

_Corpus hermeticum. Asclepio_, 6, p. 37.

_Cf._ Aristóteles, _De anima_, libro III.

D. Summers, _El juicio de la sensibilidad_, p. 157.

_Cf._ E. Kajalar Emilsson, _Plotinus on Sens-Perception: A Philosophical
Study_.

_Cf._ D. Summers. _op. cit._, pp. 157 y ss.

San Agustin, _Del libre albedrio_, p. 258.

M. Ficino, _De amore_, p. 112.

D. Summers, _op. cit._, pp. 170-171.

G. Bruno, «De magia», en _Cause, Principle and Unity_, p. 142.

Ioan P. Couliano, _Eros and Magic in the Renaissance_, p. 95

_Textos de magia en papiros griegos_, p. 335.

J. García Blanco, _Introducción a Juliano. Discursos_, p. 16.

Juliano, _Contra el cínico Heraclio_, 230a.

Juliano, _op. cit._, 230b.

Eunapio, _Vidas de filósofos y sofistas_, p. 87.

_Corpus hermeticum. Poimandres_, III, I.

_Corpus hermeticum. Fragmentos de Estobeo_, IV, II.

J. García Blanco. _op. cit._, p. 84.

Juliano, _A la madre de los dioses_, 167c-d. Me gustaría apuntar
aquí sólo un detalle que agrega Salustio refiriéndose al mito
de Atis: «Esto no ocurrió nunca, pero existe siempre, la inteligencia
lo ve todo a un tiempo, mientras la palabra expresa una cosa
primero y otra después», citado en J. Garcia Blanco, _op. cit._,
p. 84.

Juliano, _Al Rey Helios_, 148a.

Juliano, _op. cit._, 140a.

_Idem._, 139c.

_Idem._, 132c.

Platón, _Banquete_, 260c.

Platón, _op. cit._, 207a.

_Corpus hermeticum. Asclepio_, 6.

_Corpus hermeticum. Poimandres_, XI, 19-20.

Juliano, _Carta a Temisto_, 258b-c.

Juliano, _op. cit._, 264a-d.

_Idem._, 266b.

Citado en J. García Blanco, _op. cit._, p. 10.

Juliano, _Contra el cínico Heraclio_, 225d.

A propósito de la importancia del pseudo-Dionisio el Aeropagita,
véase Sparrow-Simpson, _The Influence of Dionysius_.

Respecto a la tiniebla o rayo de tiniebla como interpretación
del _Éxodo_ en la tradición, _cf._ H.C. Puech, _En torno a la
gnosis_, pp. 180 y ss.

3 R. Llull, _Libro del amigo y el amado_, §288, 103.

R. Llull, _Libro de la filosofía de amor_, §240.

H.C. Puech, _op. cit._, pp. 176 y ss.

H.C. Puech, _op. cit._, p. 180; y R. Xirau, _Vida_, p. 213.

R. Xirau, _op. cit._, p. 213.

R. Llull, _Libro de la filosofía del amor_, p. 213.

N. Cohn, _Los demonios_, p. 117.

P.O. Kristeller. _Il pensiero filosofico di Marsilio Ficino_,
p. 311.

M. Ficino, _The Letters_, p. 34

P.O. Kristeller, _op. cit._, pp. 313 y ss.

M. Foucault, _Historia de la sexualidad_, vol. 2, p. 36.

Por ejemplo, en M. Ficino, _op. cit._, p. 34.

M Ficino, _Sobre el amor_, p. 11.

M. Ficino, _op. cit._, p. 30.

_Idem._, p. 160.

_Cf._ _Corpus hermeticum. Asclepio_, p. 14.

D. Summers, _El juicio de la sensibilidad_, p. 159.

M. Ficino, _Sobre el amor_, p. 112.

M, Ficino, _op. cit._, p. 114.

M. Ficino, _Teología platónica_, p. 393.

M. Ficino, _op. cit._, pp. 394 y ss.

I.P. Couliano, _Eros and Magic_, p. 31.

I.P. Couliano, _op. cit._, p. 31.

M. Ficino, _Sobre el amor_, p. 166.

I.P. Couliano, _op. cit._, p. 31.

M. Ficino, _Sobre el amor_, p. 45.

M. Ficino, _op. cit._, pp. 87 y ss.

R. Descartes, _Discurso del método_, p. 39.

J. Locke, _Ensayo acerca del entendimiento humano_, vol. I, p.
276.

Me refiero aquí esencialmente a las obras populares para titeres
del Fausto que :.compañaban el nacimiento del protestantismo,
pero también al Fausto de Christopher Varlowe. A Propósito de
esto puede verse I.P. Couliano, _Eros and Magic_.

_Cf._ I.P. Couliano, _Eros and Magic_, pp. 270 y ss.: y G. Bruno,
_Cause, Principle and Unity_.

_Cf._ Aristóteles, _De anima_.

_Cf._ Aristóteles, _op. cit._

D. Summers. _El juicio de la sensibilidad_, pp. 170 y ss.

M. Ficino, _Three Books on Life_, III, III, p. 257.

G. Bruno, _op. cit._, p. 254.

_Idem._, p. 267.

G. Bruno. _Los heroicos furores_, pp. 3-4.

G. Bruno, _op. cit._, pp. 38-39.

_Idem._, p. 57.

Platón, _Timeo_, 89d-91a.

_Cf._ R. Llull, _Libro del amigo y del amado_.

Ovidio, _Las metamorfosis_, III, vv. 230-721.

G. Bruno, _op. cit._, p. 74.

G. Bruno, _idem._, p. 78.

_Idem._, p. 86.

_Cf._ M. Ficino, _Three Books on Life_, III.

G. Bruno, _op. cit._, p. 90.

G. Bruno, _idem._, p. 100.

_Idem._, p. 79.

_Idem._, p. 103.

_Idem._, p. 116.

_Idem._, p. 90.

Epicuro, _Máximas para una vida feliz_, III.

Epicuro, «Carta», _op. cit._, p. 132.

G. Bruno, _op. cit._, p. 67.

G. de Loris, _Roman de la Rose_, vv. 4055-4058.

G. de Loris, _op. cit._, vv. 1681 y ss.

J. Huizinga, _El otoño de la Edad Media_, pp. 161 y ss.

D. de Rougemont, _Amor y Occidente_, pp. 81-82.

D. de Rougemont, _op. cit._, pp. 81-82.

G. Duby, _El caballero_, pp. 20-27,

G. Duby, _op. cit._, pp. 29-30.

G. de Loris, _op. cit._, vv. 2618 y ss.

R- Llull, _Blanguerna_, pp. 29 ss.

J. de Meun, _Roman de la Rose_, vv. 402-403.

G. de Loris, _op. cit._, vv. 3051-3056.

J. de Meun, _op. cit._, vv. 4549 y ss.

El argumento es recogido de la misma disputa sobre la conveniencia
de entender el acto sexual como pecado venial y no mortal en
el matrimonio.

J. de Meun, _idem._, vv. 4837 y ss.

_Idem._, vv. 8355 y ss.

_Idem._, v. 8431; el subrayado es mío.

_Idem._, vv. 16233-16376.

_Idem._, vv. 19505 y 55.

_Idem._, vv. 19575-19582.

_Idem._, vv. 19345-19356.

_Idem._, v. 17716.

_Idem._, vv. 19153-19162.

_Idem._, vv. 20031-20017.

Natura da un listado mayor de prácticas que la atacan ---el suicidio,
la embriaguez contumaz, el ayuno, etcétera--- pero que no merecen
la misma atención que estas cuatro.

J. Hoeppner Moran. The «_Roman de la Rose_», pp: 7-8.

En Bocaccio, por ejemplo, leemos de voz de uno de los personajes
que «harían falta diez hombres para satisfacer a una mujer».
_Cf._ _Decamerón_, pp. 158-159.

«Y una Mujer debería saber que de casarse con un hombre rico
antes que con uno pobre, de desear a su esposo más por sus posesiones
que por su persona, está ofreciéndose en venta». Abelardo y Eloisa,
_Letters_, p. +++III+++.

Un ejemplo de esto mismo puede verse en los cuentos de la jornada
tercera del _Decamerón_.

J. Huizinga, _op. cit._, p 103.

G. de Loris, _op. cit._, vv. 2861-2862.

_Idem._, vv. 2840-2845.

_Idem._, vv. 2850 y ss.

_Ibidem._

Boccaccio, _Decamerón_, p. 315.

Boceaccio, _op. cit._, p- 321.

_Idem._, p. 323; el subrayado es mio.

_Idem._, p. 276.

_Idem._, pp. 158-159.

Kant, _El poder de las facultades afectivas_, p. 37

F. Nietzsche, _Ecce Homo._, p. 43

M. Ficino, _Three Books on Life_, III, p. 257.

M. Ficino, _op. cit._, I, III, p. 111.

_Idem._, I, III, p. 127.

_Ibid._

Ficino, _Teología platónica_, II, IX, pp. 5, 14-15.

F. Rabelais, _Gargantúa y Pantagruel_, pp. 499 y ss.

M. Bajtin, _La cultura_, p. 270.

C. Gaignebet, _El carnaval_, 102.

F. Rabelais, _op. cit._, 212.

R. Klein, _La forma y lo inteligible_, 45.

F, Rabelais, _op. cit._, p. 88.

_Idem._, p. 34.

L. Febvre, _El problema_, 159.

Platón, _Timeo_, 34b-36d.

G.E. Moore, _Principia ethica_, p. 278.

Italo Calvino, _Seis propuestas para el próximo milenio_, p.
39.

Ioan P. Couliano, _Eros and Magic in the Renaissance_, p. 214.

Ioan P. Couliano, _op. cit._, p. 222.

_Cf._ R. Klein, «Spiritu peregrino», _La forma y lo inteligible_,
p. 41.

_Cf._ M. Ficino, _Sobre el amor_, p. 89.

_Cf._ David Summers, _El juicio de la sensibilidad_, p. 165.

_Cf._ _Ibidem._

M. Ficino, _Sobre el amor_, p. 89.

M. Ficino, _Three Books on Life_, p. 337.

T. Moore, _The Planet Within_, p. 175.

M. Ficino. _Meditation on the Soul_, p. 38.
