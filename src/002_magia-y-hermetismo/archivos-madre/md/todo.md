# Dedicatorias @ignore

A Pepita Sa1só Sempere \
y Adriana Aceves Martinek. {.espacio-arriba3 .epigrafe}

Qué prueba de la existencia \
habrá mayor que la suerte \
de estar viviendo sin verte \
y muriendo en tu presencia \
++Xavier Villaurrutia++ {.epigrafe}

# Epígrafe @ignore

Filosofar es invertir la dirección \
rutinaria del trabajo intelectual. \
No pretende establecer leyes ---invita sólo a la \
responsabilidad de pensar libremente, \
e invita a ello pensando. \
++Antonio Escohotado++ {.epigrafe}

# I. Los límites del horizonte

++Este es un libro++ sobre la magia. Sin embargo, si una palabra
ha quedado prácticamente excluida es ésa. No se trata de un capricho,
sino de una tentación vencida. Magia es uno de esos térmmos para
el que cada uno tiene una 1dea determinada y que ademas tiene
la particularidad de desatar las pasiones. De otra manera, es
una palabra útil como coartada y que facilita la retórica, porque
suele cubrir con un velo los argumentos. En última instancia,
es una palabra que mueve más a los sentimientos que a la reflexión.

Y ocurre que éste, además de ser un libro sobre la magia, es
también una reflexión que la toma como punto de partida para
tratar de entender algo más sobre nosotros mismos. Su horizonte,
así, se traza sobre ambos costados de la historia, en el pasado
remoto, cuando los hombres practicaban cotidianamente la magia,
y el día de hoy, en el que ya no lo hacemos, pero en el que tampoco
hemos suprimido las inquietudes que dominaban también a nuestros
antepasados. Preocupaciones tan simples como el ser amado, permanecer
sano, saber cómo vivir una vida sujeta al tiempo, con un límite
determinado, y para las que las prácticas mágicas jugaban el
papel de proporcionar respuestas que muchos creyeron eficaces.

Para no nublar ese horizonte hay que darle la vuelta a la magia,
evitar los encantos del misterio y el susurro, y tal vez escarbar
en el pasado para encontrar el modo en que los hombres planteaban
el problema de su propia vida. Cualquier aproximación que se
haga a la magia occidental, tomando como punto de referencia
sus momentos de esplendor, llevará siempre hacia el hermetismo
y al conjunto de los ricos aunque fragmentarios textos que hoy
conocemos como _Hermética_, que durante siglos fueron leídos
y estudiados como una de las fuentes primordiales de la magia
por quienes se veían a sí mismo como magos.

Las prácticas mágicas, sin embargo, no aparecen con el hermetismo.
Existen, por supuesto, desde mucho antes. Por eso, al trazar
la relación entre éstas y hermetismo no puede evitarse hacer
referencia a la doctrina que sirve de puente entre unas y otro.
Aquella que, en estricto sentido, las vinculará entre sí a partir
del hermetismo, para conformar lo que los hombres de la Edad
Media y el Renacimiento conocerían como una doctrina y una práctica
a la que llamarían magia. Esa doctrina, a la que llamaré neoplatonismo
hermético, es la de aquellos pensadores como Apuleyo, lámblico,
Juliano el Apóstata, y siglos más tarde, Ramón Llull, Marsilio
Ficino, Giovanni Pico, Cornelio Agrippa, y tantos otros que,
bebiendo en Platón y el hermetismo, formularon de tal forma el
problema de la existencia humana que un grupo de prácticas populares
pasaron de ser simples técnicas operativas con fines muy específicos
---conseguir el amor, mantener la sombra junto a uno, evitar
la llegada de las cucarachas, hacerse invisible--- a ser prácticas
encaminadas a construir al hombre como sujeto de su propia vida.

En un sentido, neoplatonismo hermético y magia son lo mismo,
pero en otro no. Lo son en tanto que se trata de la misma doctrina
y la misma práctica que los hombres de la Edad Media y el Renacimiento
llamaron magia. No lo son en la medida en que el neoplatonismo
hermético no agota la riqueza y los sentidos múltiples que se
agolpan bajo el nombre de magia. Utilizar neoplatonismo hermético
en lugar de magia, además de evitar el naufragio y la tentación
de la retórica, tiene la intención de hacer énfasis en el problema
ético que encontrará en las prácticas mágicas una forma de conducta
para dar forma a la propia existencia, en el orden de dar respuesta
a ese problema.

Y es aquí donde alcanzamos el horizonte de nuestros días. Más
que una disquisición erudita sobre los orígenes de la magia medieval,
este libro busca cuatro ideas fundamentales que pueden alimentar
la reflexión de nuestros días: la primera es el modo en que el
hermetismo, retomando algunas de las ideas del gnosticismo dualista,
formula el problema de la existencia en la temporalidad. La segunda
es el modo en que esta forma de plantear el problema conduce
el hermetismo a introducir una noción de temporalidad fuerte
dentro del pensamiento cosmológico y ético; es decir, una estructura
en la que el momento, el día y el mes del año definen una parte
de las relaciones entre las personas, las cosas y las sustancias.
En tercer lugar, la idea del hombre como un ser que elige en
la medida en que es temporal y, derivada de ésta, la cuarta,
la propuesta de la construcción del sujeto sobre la base del
cambio y la inquietud, que puede sintetizarse en la fórmula:
sé cuanto quieras ser.

La pauta de exposición que he seguido aquí intenta responder
a los dos extremos que tiene esta búsqueda. Por una parte he
querido seguir la sucesión histórica y por otra, una perspectiva
sistemática de los problemas, que al final nos lleve a rescatar
aquello que puede ser útil para la reflexión de nuestros días.

De este modo, en la primera parte se aborda el hermetismo desde
sus raíces gnósticas para establecer las bases de la originalidad
de su pensamiento y de la forma en que plantea el problema de
la existencia humana, partiendo de la concepción del cosmos hasta
llegar a la del hombre. En la segunda, que es la más extensa,
nos concentramos en el neoplatonismo hermético y en cómo, a partir
de Platón, las ideas herméticas toman cuerpo y consistencia,
pierden su carácter religioso y adquieren uno ético, en el que
prácticas como la astrología, la medicina, la música y la adivinación
conforman modos de conducta propias de un sujeto.

Ésta es, después de todo, una reflexión ética sobre la magia.

# II. Gnosis y hermetismo

Siete astros de curso dilatado giran en círculo en el umbral del
Olimpo, con ello el tiempo infinito prosigue eternamente su marcha:
la Luna que ¡lumina la noche, el lúgubre Cronos, el sol _Nous_, Paifé
que aparta el lecho nupcial, el fogoso Ares, Hermes de alas rápidas y
Zeus, primer autor de todos los nacimientos. Estos mismos astros ha
recibido en participación la raza humana, y en nosctros está la Luna,
Zeus, Ares, Paifé, Cronos, el _Nous_, Hermes. He aquí por qué nuestro
sino consiste en hacer nuestro, del aliento etéreo, lágrimas, risas,
colera, generación, logos, sueño, deseo. Las lágrimas es Cronos, la
generación Zeus, Logos es Hermes, Cólera Ares, el sueño la Luna,
Citérea el deseo, la risa el sol: pues por él ríen, con razón, toda la
inteligencia mortal y el mundo sin límites. \
++Fragmentos de Stobeo++ +++XXIX+++ {.epigrafe}

## _La gnosis_

++Al término de la expansión++ del Imperio Romano, la sociedad
imperial sufre una transformación radical. Se vive una época
de paz pero, al mismo tiempo, de una intensa zozobra individual.
El ciudadano, antes actor y receptor de las glorias de Roma,
se vuelve el único protagonista de su vida; los oráculos, que
revelaban el destino del Imperio, ahora reciben las dudas mezquinas
de los ciudadanos: ¿Renunciaré? ¿Seré Senador? ¿Me beneficiaré
esta temporada?

Disuelta la esperanza colectiva, la vida propia se vuelve el
eje de las preocupaciones de los individuos, y el temor supersticioso
por la fortuna y la fatalidad se acentúa. La filosofía y la religión
dejarán de ser cultivadas como puro pensamiento o pura piedad,
para serlo en vista de un objetivo específico: ofrecer un método
para disipar los temores humanos; una vía que provea tranquilidad
y esperanza en una vida feliz.

A partir del año 100, señala Paul Veyne, los dioses tenían como
función «[…] la de gobernar, aconsejar y proteger a sus fieles,
a fin de sustraerles a la ciega fortuna o a la fatalidad; a su
vez, lo que la filosofía se proponía entonces era proponer a
los individuos un método de dicha […].»@note Una y otra forma
de sabiduría atendían a una misma necesidad definitiva para el
hombre: responder a la pregunta de cómo vivir.

Dos fueron los caminos que se siguieron para ello: el de la adaptación
y el de la transformación. Por el primero, se concibieron un
conjunto de prácticas dirigidas hacia la adecuación de la vida
humana a las condiciones generales de existencia, de tal forma
que todo aquello que en ella produce dolor e incertidumbre, fuera
aceptado por el hombre para que dejaran de serlo. En el segundo,
la intención es que las prácticas modificaran la propia naturaleza
humana, hasta erradicar de ella, para siempre, todo lo que produce
zozobra.

La doctrina estoica y, en menor medida, el epicureísmo, son ejemplos
de propuestas adaptativas donde, a partir de una terapia, (en
la que la moral es sólo un método para el sosiego) se buscaba
eliminar el dolor y el miedo a la muerte.

La vía de la transformación, en cambio, la encontramos en las
posturas de origen neoplatónico, gnóstico y hermético, donde
la apuesta es colocar la propia existencia en manos de la voluntad
humana para que se sustraiga de los azares y determinaciones
que le causan desasosiego.

Así, mientras en Roma el estoicismo y el epicureísmo ofrecían
sus fórmulas para el sosiego, en los márgenes del imperio nacían
grupos que perseguían la alternativa radical de la transformación.
Son movimientos de rasgos dispares y permeables a todo tipo de
influencia, surgidos unas veces de interpretaciones esotéricas
de textos bíblicos (_Ophitismo_, _Sethianismo_), o de personalidades
definidas como Basíilides, Carpócrates o Valentín.

Conocidos como gnósticos, por articular sus convicciones a partir
de la certeza en la salvación a través del conocimiento (gnosis),
fluctúan desde las posiciones más cercanas al Judeocristianismo
hasta el mazdeísmo y los cultos zoroástricos. Sin embargo, comparten
la misma preocupación por la fortuna, el dolor y la muerte, que
subyacen a las doctrinas tranquilizantes que dominan en la urbe.
Pero no buscan sosiego, su intención es la de encontrar una solución
definitiva y permanente a la incertidumbre humana; es decir,
cómo modificar la naturaleza del hombre para adquirir otra ---que
consideran similar a la condición divina--- que le diera derecho
a la felicidad.

Entre los movimientos gnósticos hay un camino común para operar
la transformación de la naturaleza humana que comienza con el
desengaño. No debe sorprendernos, los gnósticos conciben al hombre
como un ser engañado que, sin pertenecerse a sí mismo, va a la
deriva de las circunstancias, del giro de la fortuna, de la malevolencia
de los astros. Su existencia es un camino sin orientación n contenido;
como si otros ---la carne, el clima, la posición del cielo---
fueran quienes realmente viven y deciden por el hombre, ocultos
tras el velo de una precaria conciencia.

Por ello conciben la necesidad de un desengaño: pese a las apariencias,
el hombre no está predeterminado como otros seres; no es sujeto
ni de la fatalidad ni del destino.

A partir de esta premisa, se entiende que no puedan conformarse
con aceptar y plegarse a lo que determina al hombre para vivir
tranquilo, sino que pretenden 1r más allá.

> […] Decepcionado o herido por su actual condición en el seno
> del mundo, de una sociedad, de un cuerpo en los que no experimenta
> sino malestar […]; el gnóstico comienza por reaccionar frente
> a él (mundo) y contra él […)]. Rechaza su condición y se niega
> a aceptarla. Al sentirse extranjero en un mundo que ha acabado
> por concebir como radicalmente extraño, tiende a distinguirse
> […]. De aquí la necesidad de evadirse, de salir […]. El sentimiento
> progresivamente elaborado que experimenta el gróstico de ser
> otro que lo que le rodea lo lleva a persuadirse de que […] no
> es de este mundo […]. A partir de su situación presente, el gnóstico
> llega a concebirse ya desde aquí como más allá, a representarse
> y a definirse con independencia de las contingencias […].@note

El descubrirse como otro, ajeno a lo que le rodea, lleva al gnóstico
a descorrer los velos del engaño y descubrirse como radicalmente
distinto de todo aquello que es objeto de determinación en el
mundo. Porque para él:

> […] el conocimiento que se ha propuesto alcanzar de sí mismo
> encuentra un sentido contrario al que le atribuyen por lo común
> los antiguos intérpretes de la máxima délfica: tiene por fin
> y por resultado, no determinar y discernir exactamente el puesto
> que ocupa en el mundo, reconocer sus propios límites, subordinar
> su suerte y sus deberes a un orden universal, sino a la inversa,
> conducirle al descubrimiento y a la afirmación de sí como extraño
> y superior al mundo, hacer que se niegue a comprometerse con
> él, a fin de que establezca por encima y más allá de él su soberana
> independencia […].@note

El proceso de extrañamiento es el principio para que el hombre
descubra qué lo distingue del resto de los seres de este mundo.
Pero al mismo tiempo, este descubrimiento es la culminación del
proceso ---al que confirma--- y el pasadizo hacia una nueva existencia.

Para entender esto con mayor claridad es necesario darse cuenta,
primero, que el gnóstico se reconoce como un extraño en el mundo,
porque es ajeno a todo aquello que causa desdicha: lo corpóreo,
la temporalidad, etcétera. En segundo lugar, que al dejar atrás
los velos que le hacían ver al hombre como irremediablemente
ligado a la carne y al tiempo, alcanzó un estado superior de
conciencia de sí, y descubre que él existe de «otra manera»,
ésta es espiritual y atemporal. Pero, en tercer lugar, que no
sólo es consciente de este hecho, sino que la sola conciencia
ha operado un cambio ontológico: él es, ya, otro, aJeno a todo
lo que es causa de desdicha. Es decir, se concibe a sí mismo
como trascendencia.

Para los gnósticos, el conocimiento de sí como un «otro» es suficiente
para que el hombre lo sea, puesto que sólo puede conocerse aquello
que es. Así, el proceso de desengaño es, en realidad, un proceso
de autoconocimiento y alienación, por el que comienza por descubrir
cuál es la verdadera naturaleza del hombre y acaba por darse
cuenta de que, gracias a ella, él mismo es un ser distinto del
que partió.

> El conocimiento ---que en primer lugar es conocimiento de sí---
> no se limita sólo a ilustrarle (al gnóstico) sobre la ruta a
> seguir, junto con él le otorga la salvación; le salva por sí
> mismo, por el hecho de manifestársele y de que el gnóstico lo
> posee […] la obtención de la salvación se reduce por ello a una
> pura operación interior.@note

En los textos gnósticos este nuevo ser es ¡identificado como
el hombre primordial que, por alguna razón (en este caso hay
una enorme cantidad de explicaciones), se ve impelido a engendrar
una raza caída y mortal, que participa de él, pero que se encuentra
cautiva en el mundo.

Se trata del único hombre engendrado directamente por la inteligencia
primera, y por ello intemporal, pero ante todo, inocente. Es
el hombre concebido _fuera del tiempo_ pero también más allá
del bien y del mal, más allá incluso de la elección.

Afirmar aquí, como lo hace Doresse,@note que el gnosticismo postula
una dualidad ontológica de corte maniqueo es insuficiente. Porque,
en realidad, los gnósticos identifican el ser con la supresión
total de la elección entre bien y mal. Solamente en el mundo,
que es ser en segundo grado, es posible elegir. Y esto, en la
medida en que por estar contenido en la temporalidad, hay una
sucesión entre el momento del deseo y el de la satisfacción,
en que es posible la dicotomía moral. Así, la diferencia ontológica
entre las esferas del ser, no implica una diferencia axiológica.
El mundo terrenal no se identifica con el mal sino con la elección,
y el ser no lo hace con el bien, sino con la supresión de toda
elección.

Al caer el hombre primordial y entrar en el tiempo, lo que recibe
es el _pathos_ de la elección, y con ello la necesidad de elegir
entre múltiples opciones, unas buenas, otras malas, sin saber
cuál es la correcta. De ahí que el mundo se convierta en un lugar
de penuria y dolor; de ahí, también, que el gnóstico aspire a
que no tenga que esperar para que al fin todo le sea permitido.

> No sólo vosotros, mas todo hombre que cumpla con el primer misterio
> del inefable, podrá recorrer todas las regiones y todas sus estaciones.

> Y cuando haya cumplido ese misterio y recorrido todas las regiones
> será preservado de todas las cosas que le hayan reservado los
> archontes de la Heimarmené (jerarquía celeste). Escucha María,
> te digo en verdad que no sólo vosotros, sino todos los hombres
> podrán cumplir el misterio de resurrección de entre los muertos.

> Para curarse de la posesión de los demonios y de toda aflicción
> y enfermedad.

> Y para curar a los cojos, y a los mutilados, y a los mudos, y
> a los paralíticos.@note

Pero si esta es la condición a la que aspira el gnóstico, sólo
puede concebirse como posible si existe una fractura en el orden
temporal por la que se mantiene un vínculo con el ser atemporal
del hombre primordial. Y esa fractura, de carácter ontológico,
es el hombre. Por ello:

> Si aquellos que os guían os dicen: mirad, el reino está en el
> cielo, entonces los pájaros llegarán allí antes que vosotros.
> Sí os dicen: está en el mar, entonces, dice, los peces llegarán
> antes que vosotros […] Más bien, el reino está dentro de vosotros
> y está fuera de vosotros. Cuando lleguéis a conoceros a vosotros
> mismos, entonces seréis conocidos y us daréis cuenta de que sois
> los hijos del padre que vive.@note

El hombre es un ser terreno mientras no sea capaz de percibir
que él mismo es parte del hombre primordial. Por ello, el conocimiento
de sí como «otro», el proceso de extrahamiento, es el que le
otorga la salvación. Dejará de ser el que se conozca para ser
conocido.

Aquí hay que enfatizar que no se trata sólo de una operación
voluntaria. El neumático, porque posee el _Nous_, el gnóstico,
porque posee la gnosis, saben que lo son porque les ha sido revelado
por quien verdaderamente es. Se trata de una transformación dramática
desde cualgquier punto de vista porque, pese a ser ellos mismos,
en realidad, sólo lo descubren cuando son otros.

En efecto, el proceso de extrañamiento que sigue el gnóstico
invierte el orden de la caída, revela al hombre la existencia
de la fractura ontológica que lo constituye a él mismo, y lo
hace partícipe de un nuevo ser que, sin necesidad de elegir,
sólo es:

> El gnóstico reposa en sí mismo en su propia plenitud, en la de
> su _pleroma_. Habiendo recobrado su autonomía, esa situación
> de disponer libremente de sí mismo y de obrar a su guisa. Amoralismo
> de principio, por lo demás, ya que entre los gnósticos, y a veces
> en el seno de una misma escuela, pueden encontrarse ascetas y
> libertinos, los unos que señalaban su desprecio y, tal vez, su
> temor con respecto a las cosas del mundo mediante su abstención
> y los otros que usaban y abusaban de ellas en nombre de su absoluta
> libertad de poder (_exousia_) ilimitada que le confiere su soberanía
> nativa.@note

A la inquietud humana la gnosis responde con una alternativa
de transformación, concebida como una operación interior y de
extrañamiento, que abre la puerta a una dimensión «ajena al mundo»:
la del ser que lo redime del _pathos_ de la elección.

En este sentido el gnosticismo es un proyecto amoral, en tanto
que las normas tradicionales de conducta carecen de significado
dentro de él. Pero también es un proyecto «metaético», porque
se propone la supresión de la eticidad, como el medio de alcanzar
la felicidad. La suya es una aventura ontológica que no se preocupa
por dar respuesta a cómo vivir en el ámbito de la temporalidad,
sino en el terreno del ser como existencia atemporal.

A nuestros ojos, la oferta gnóstica es una salida en falso. Lo
radical de su propuesta elude o busca eludir, mediante lo que
concibe como una transformación ontológica que elimina finitud
y elección, el problema de encontrar una respuesta a cómo vivir
eligiendo y con conciencia de la propia finitud. Porque el problema
no es, y no podemos verlo así con nuestros ojos de fin de milenio,
buscar una «salida» de la temporalidad. La cuestión es cómo vivir
esta que, indefectiblemente, es nuestra única vida.

En el seno de los numerosos movimientos que se dan en el siglo
+++II+++ y aún dentro del gnosticismo, no todos aspiraban a modificar
la naturaleza del hombre de esa forma. Arraigados en tradiciones
populares, los llamados gnosticismos «paganos» ofrecen, dentro
de la alternativa de la transformación, otras rutas.

## _El hermetismo_

La doctrina contenida en los llamados textos herméticos (_Hermética_)
son un producto del diálogo entre la tradición religiosa egipcia@note
y el gnosticismo.

Los siete diálogos que componen el _Corpus Hermeticum_, el diálogo
_Asclepio_ y los _Fragmentos de Stobeo_, no constituyen ninguna
unidad temática ni doctrinaria, pues al lado del más puro hermetismo
coexisten posiciones del dualismo más radical. Pero esto que
constituye la mayor dificultad para su interpretación, es también
su mayor virtud, en la medida en que intenta reunir, en un discurso,
lo que en aquel entonces conformaba la sabiduría de la época.@note

Los _Hermeética_ así, no es sino un tratado de sabiduría práctica,
en el que se confrontan diversas propuestas de lo que hemos llamado
alternativa de la transformación, y donde puede verse, esa otra
vía, que se denomina hermetismo.

> Todo es producido por el destino y no hay lugar alguno donde
> no se ejerza la providencia. La providencia es el designio perfecto
> del logos de dios. Este designio tiene dos poderes, el destino
> y la fatalidad. El destino está al servicio de la providencia
> y la necesidad, y es servido a su vez por los astros. Nada escapa
> al destino ni se hurta a la fuerza 11mensa de los astros.@note

Todos los pasajes 1dentificables como gnósticos, en sentido dualista,
dentro del los _Hermética_ (como el citado) muestran que la vida
humana está determinada por el influjo de los astros ---símbolo
del devenir, maquinaria infalible del tiempo---. El fragmento
+++XI+++-24 de Stobeo es determinante: «Nada en la tierra es
libre».@note Por ello: «Nada es bueno en la tierra […] el hombre
es malo […]; el animal humano es, en su totalidad, desmedidamente
inclinado al vicio».@note

El mundo, en suma, es un lugar lleno de fuerzas y demonios que
no dejan de ejercer su influencia maléfica en cada instante.

> Pues una vez que cada uno de nosotros ha venido a nacer y ha
> sido animado, es tomado a su cargo por los demonios que están
> de servicio en el instante mismo del nacimiento; es decir, por
> los espíritus que han sido puestos bajo las órdenes de los astros.
> Pues los espíritus se sustituyen mutuamente de instante en instante
> […] sirviendo cuando les toca. Entonces estos espíritus, tras
> haber penetrado a través del cuerpo, atormentan el alma […].@note

¿Cómo puede tolerarse un mundo donde la propia existencia se
revela como un puro instrumento del destino? Ni siguiera la conciencia
de la fatalidad es capaz de detener esa voraz determinación de
todo lo humano:

> Este animal [el hombre], si llega a saber que todo acaece de
> acuerdo a la providencia y al destino, ya que la fatalidad señorea
> todas las cosas ¿no será mucho peor? En efecto responsabilizando
> a la fatalidad por el mal, no habrá en adelante acción maligna
> de la que no se abstenga […].@note

Ni saberse determinado, n1 el tomar una decisión a partir de
este saber, logra esquivar el designio de los astros y lo demonios,
porque una y otra, conciencia y camino, sólo logran hacer más
efectiva la fatalidad. Pero, ¿hay alguna salida? Claro, cambiar
de ser:

> […] Como hay dos clases de seres y estas dos categorías se reparten
> lo mortal y lo divino, no falta sino elegir uno u otro; allí
> donde no queda sino elegir. La derrota de uno manifiesta la potencia
> activa del otro […].@note

¿Pero es realmente una elección? ¿No contradice esto el hecho
de que toda elección es inducida por los astros? ¿No es acaso
un doble engaño, una salida fácil, que elude el hecho mismo de
la determinación?

Los _Hermética_ ponen al descubierto la inconsistencia del gnosticismo
dualista: si no hay elección posible, porque el hecho mismo de
elegir es ya condenable, ¿quién elige? ¿quién puede ascender
hacia lo divino? Para el hermetismo, el gnosticismo sólo ofrece
un camino de pasividad y de esperanza en la gracia de un «otro»
que anula todo el esfuerzo puesto en la transformación, porque
deja la decisión no al sujeto, sino a una entidad trascendente.
¿De qué manera, entonces, se puede superar la determinación que
hace al hombre esclavo malvado y sujeto de la zozobra?

En los diálogos cosmológicos del _Corpus Hermeticum_ el cosmos
es creación del _Nous_ y objeto perenne de su dominio. Dice el
_Poirmandres_:

> El _Nous_, siendo macho-hembra […] procreó con el logos un _Nous
demiurgo_ […] que hizo siete gobernadores que envuelven en sus
círculos al mundo sensible y este gobierno suyo [el del _Nous
demiurgo_] es lo que se llama destino.@note

El énfasis está puesto en la capacidad del _Nous_ para procrear
una versión de sí mismo como _demiurgo_, a través de logos y
ser, por tanto, autor del mundo material. Esta concepción, con
claros ecos neoplatónicos, diviniza incluso la materia:

> Gloria de todas las cosas son dios y lo divino, y la naturaleza
> es divina. Dios es principio de todos los seres y _Nous_ y naturaleza
> y materia, puesto que es sabiduría para la revelación de todas
> las cosas. Dios es principio y naturaleza y energía, y necesidad
> y fin y renovación.@note

Para el hermetismo la unidad del ser es absoluta: todos los elementos
de la creación son afluentes de un único ser. La eternidad del
ser y del cosmos todo obedece a ese principio unitario de creación
que es reproducido en todos los niveles del ser.

> El ser existirá siempre, de donde se sigue que los cuerpos existirán
> siempre también. Por ello declaro que la generación de cuerpos
> es una operación eterna.@note

En el momento en que el ser se identifica con este principio
eterno de creación la muerte no existe: «La muerte ---dice _Poimandres
+++VIII+++_--- no tiene nada que ver con ninguna de estas cosas
[cuerpo y alma], sino que es más que un concepto forjado sobre
la palabra inmortal, sea pura ficción, sea la supresión de la
primera letra, la palabra _Thanatos_ en lugar de _Athanatos_
[…].»@note El mismo principio que destruye, fecunda, de manera
que la muerte está en función de la vida, es vida. Así ha de
entenderse el gobierno que el _Nous_ ejerce a través de los astros,
el principio de creación de todas las cosas y, en última instancia,
la propia naturaleza del ser: un proceso infinito de ocaso y
renacimiento.

Al referirse al origen del hombre, el _Poimandres +++I+++_ destaca
que, único entre todos los seres, el ser humano no sólo contiene
en sí mismo esta cualidad de ser sino que es capaz de imprimirla
a otros seres. Es decir, no sólo la reproduce, sino que es agente
de creación.

Dice el texto que, paralelo a la creación de las esferas, «el
_Nous_ padre produjo un hombre y […] verdaderamente Dios se enamoró
de su propia forma y le entregó todas las formas […]».@note

El hombre se hermana con el _demiurgo_ al tener como origen al
_Nous_. Pero al ver la obra de su hermano y con el poder que
le ha sido concedido, el hombre ---dice el _Poimandres_--- sólo
tuvo un deseo: «producir una obra».

Lejos de ser condenado por su atrevimiento, en la doctrina hermética
al hombre se le concede el dereciio a obrar. Con las formas,
los poderes y el beneplácito del _Nous_, el hombre ---al que
llamaremos, como lo hace el hermetismo, Hombre original--- penetra
en la esfera demiúrgica donde los siete gobernadores (los astros)
se enamoran de él y le otorgan, cada uno, participación en su
propia magistratura.

> Entonces el hombre, que tenía poderes plenos sobre el mundo de
> los seres mortales y de los animales sin razón, se descolgó a
> través de la armoniosa maquinaria compuesta de las esferas cuyas
> envolturas había agujereado y manifestó la hermosa forma de Dios
> a la naturaleza de abajo. Cuando ésta hubo visto que el hombre
> poseía la forma de Dios junto con la belleza inagotable y toda
> la energía de los gobernadores, sonrió de amor, pues había visto
> reflejarse en el agua el semblante de esa forma maravillosamente
> hermosa del hombre y a su sombra en la tierra. En tanto que él,
> habiendo visto reverberar en el agua la presencia de esa forma
> parecida a la suya, la amó y quiso morar en ella. Desde el mismo
> momento en que lo quiso lo cumplió. La naturaleza entonces, recibiendo
> en ella a su amado, lo abrazó entera, y ambos se unieron ardiendo
> de amor.@note

No hay caída, sólo el deseo del hombre por engendrar, activar
sus poderes, obrar.@note Pero no puede hacerlo solo. El hombre
original necesita del devenir, de la ley que rige todas las cosas
para poder, él también, ser capaz de generación. En una frase
lo dice el _Poimandres I_: la causa de la muerte es eros.

Por ello el hombre elige abrazar la obra del _demiurgo_ y fecundar
en ella, porque sólo así puede generar. El producto de estos
amores no será sólo una criatura común, sino más perfecta:

> Único entre todos los seres que viven sobre la tierra, el hombre
> es doble: mortal por el cuerpo, inmortel por el hombre esencial.
> Aunque en efecto sea inmortal y tenga imperio sobre toda cosa,
> padece la condición de los mortales, sujeto como está al destino.
> Por ello, aunque esté encima de la armoniosa maquinaria compuesta
> de las esferas, se ha vuelto esclavo de ellas.@note

Y más adelante:

> Lo que voy a decirte es un misterio que ha sido mantenido oculto
> hasta este día. Habiéndose unido en efecto la naturaleza al hombre
> con amor, ocasionó un prodigio completamente sorprendente. El
> hombre tenía en sí la naturaleza de la conjunción de los siete
> compuestos, como te die, de fuego y aliento; la naturaleza, incapaz
> de esperar, parió al punto siete hombres correspondientes a la
> naturaleza de los siete gobernadores, a la vez machos-hembras,
> irguiéndose hacia el cielo.@note

Esclavo y señor, el hombre es el único mortal que puede vivir
su existencia sujeto al destino pero, también, con independencia
del destino. Aunque al hombre macho-hembra no parece faltarle
nada, pues reúne en sí todos los elementos de la generación,
su autosuficiencia es chocante, no pertenece propiamente al ciclo
del devenir. Por ello, más tarde, lo macho será separado de lo
hembra, y habrá hombres y mujeres y la virtud de la fecundidad.

A diferencia del mito del andrógino en el _Banquete_ platónico,
en el que sin duda está inspirado el pasaje hermético, dividir
al macho de la hembra no es el resultado de un castigo por desafiar
a los dioses, sino del deseo de producir. El auténtico optimismo
de los _Hermética_ consiste, justamente, en que el hombre individual
no es concebido como fracción, sino como suma; así, el hombre
primordial y los siete hombres machos-hembra fecundados con el
poder de los astros, están contenidos a su vez en cada varón
y en cada mujer individual, que suman en sí mismos los poderes:
destino y libertad, esclavitud y autonomía, cuya tensión ---no
hay que olvidarlo--- es la única que posibilita la perpetuidad
del ser.

La alternativa de transformación que ofrece el hermetismo se
dibuja, entonces, no como un proceso de alienación, al modo del
gnóstico, sino como _un recuperarse a sí mismo_. Se trata de
convertir lo que para los gnósticos era el _pathos_ de la elección
en el fundamento de la eticidad. Trasmutar la condena, en el
motor de la existencia y de la conducta.

Pero, ¿en qué consiste este regreso, este encuentro consigo mismo?
Lyn Throndike en _A History of Magic and experimental Science_
nos dice, al referirse al origen de la magia egipcia:

> There [Egipcian] Mythology was affected by it [magic] and they
> not only combated demons with magical formulae but believed that
> they could terrify and coerce the very gods by the same method,
> compelling them to appear, to violate the course of nature by
> miracles, to admit the human soul to an equality with themselves.@note

Hay algo en la forma con que Thondike presenta la magia egipcia
que dehe llamarnos la atención: el reconocimiento del alma humana
como igual a la divina es el más alto de los dones que ofrece
la magia y no, como pudiera pensarse, el punto de partida. La
empresa hermética recoge esta 1dea porque entiende que el hombre
ha llegado a conocerse a sí mismo y se sabe capaz de engendrar,
de ser él mismo artífice y formar parte del gobierno de las cosas.
En suma, se sabe igual a un dios:

Por ello, Asclepio, es tan gran maravilla el hombre, animal digno
de reverencia y honor [_animal adorandum atque honorandum_].
Pues pasa a la naturaleza de un dios como si él mismo fuera dios;
tiene trato con el género de los demonios, sabiendo que ha surgido
del mismo origen. Desprecia la parte de su naturaleza solamente
humana, pues ha depositado su esperanza en la divinidad de la
otra parte. ¡Oh, de qué mezcla privilegiada está hecha la naturaleza
del hombre! Está unido a los dioses por lo que tiene de divino,
la parte de su ser que lo hace terrestre, lo desprecia en sí
mismo; todos los otros vivientes a los cuales se sabe unido
en virtud del plan celeste, se los atrae por el nudo del amor;
el hombre eleva su mirada al cielo. _Tal es su posición, de su
privilegrado papel intermedio_ que ama [_diligat_] a los seres
que le son inferiores, y es amado por aquellos que le son superiores.
Cuida la tierra, se mezcla con los elementos por la celeridad
del pensamiento, por la agudeza del espíritu se hunde en los
abismos del mismo. _Todo le está permitido_ [_omnia ille licent_]:
el cielo no le parece demasiado alto, pues gracias a su ingenio
lo considera muy cercano. La mirada de su espíritu dirige, niebla
ninguna del are lo ofusca; la tierra jamás es tan compacta que
impida su trabajo [_operam_]; la inmensidad de las profundidades
marinas no perturba su vista que se sumerge. _Es todas las cosas
a la vez, a la vez está en todas partes_ [_Omnia idem est et
ubique idem est_].@note

El reencuentro consigo mismo es asumir que nada le es ajeno al
alma humana porque _omnia idem est_, el hombre es todo, terrenal,
divino, demoniaco. De ahí el «desprecio» por la «naturaleza puramente
humana» que no es capaz de percibir su doble naturaleza y ejercer
su papel intermedio y privilegiado.

El ser del hombre hay que asumirlo entonces como el ser sin más,
en su indeterminación y en sus infinitas posibilidades, en su
necesidad y en su maravillosa arbitrariedad y capricho. La transformación
hermética es un proceso de afirmarse en el ser como creador porque
todo le está permitido.

> Júzgalo también de la manera siguiente, desde ti mismo. Ordena
> a tu psique irse a la India, y he ahí que, más veloz que tu orden,
> allá estará. Ordénale pasar en seguida al océano, y he ahí que,
> de nuevo, allí estará al punto, no por haber viajado de un lugar
> a otro, sino como si se encontrara ya allí. Ordénale incluso
> que se remonte hacia el cielo, no tendrá necesidad de alas; nada
> puede obstaculizarla, ni el fuego del sol, ni el éter, ni los
> torbellinos del cielo, ni los cuerpos de los demás astros, sino
> que, cortando a través todos los espacios, ascenderá en su vuelo
> hasta el último cuerpo. Y si todavía quisieras reventar la bóveda
> del universo mismo y contemplar lo que hay más allá (si es que
> existe algo más allá del cosmos), puedes.

> […] Habiendo puesto en tu pensamiento que nada hay imposible
> para ti, estímate inmortal y capaz de comprenderlo todo, todo
> arte, toda ciencia, el carácter de todo ser viviente. Asciende
> más alto que toda altura, desciende más abajo de toda profundidad.
> Reúne en ti las sensaciones de todo lo creado, del fuego, del
> agua; estando en todas partes, en la tierra, en
> el mar, en el cielo, que no has nacido todavía, que estás en
> el vientre materno, que eres adolescente, viejo, que estás muerto,
> que estás más allá de la muerte. Si abrazas con el pensamiento
> todas estas cosas a la vez, tiempo, lugares, sustancias, cualidades
> y cantidades, puedes comprender a Dios.@note

El hermetismo reivindica el acto humano como teurgia. Obrar es,
al mismo tiempo, una prueba de la divinidad del alma humana y
una condición para serlo. Porque el vínculo que mantiene el alma
del hombre con el _Nous_ es, también, un vínculo con el mundo.
Por ello no sólo se concibe al hombre emparentado con el _Nous_,
sino también capaz de engendrar cuando construye nuevos seres
que siguen el principio unitario de la creación, el gobierno
de la providencia; la inmortalidad que propicia la muerte y la
regeneración.

La segunda génesis del _Poimandres_ llamada _Discurso Sagrado_,
apunta a este doble sentido del actuar:

> […] y produjeron la generación de hombres para conocer las obras
> divinas y para ser testigos activos, para acrecentar el número
> de los hombres, para señorear sobre todo lo que existe bajo el
> cielo y reconocer las cosas buenas […] para conocer la potencia
> divina […] y para descubrir todo el arte de fabricar cosas buenas.

> Desde entonces comenzó para ellos llevar vida humana y adquirir
> la sabiduría de los dioses cíclicos, el disolverse en lo que
> quedará de ellos después de haber dejado sobre la tierra los
> grandes monumentos de sus industrias. Y de todo nacimiento de
> una carne animada o de la semilla de los frutos, y de toda obra
> de industria, lo que haya disminuido será renovado [….].@note

Actuar es, para el hermetismo, el camino de la transformación
ontológica del hombre por el que trasciende el _pathos_ de la
elección para trocarlo en _ethos_. En otros términos, el obrar
humano es el camino por el cual el hombre deja de ser sujeto
del destino, para ser sujeto de sus propios actos. Abandona,
así, la esfera de lo que padece, para entrar en otra donde el
deseo no es fuente de dolor, sino principio de satisfacción.

De esta manera, el hermetista es el que actúa sabiendo lo que
está en juego. El que toma decisiones, intenta, inventa, crea.
Sus actos y la emoción que producen, placer y dolor, no están
mediados, no buscan ninguna atenuación: es la sed animal y la
parsimonia divina, el dolor brutal y la alegría estruendosa,
porque lo que busca, en última instancia, es la afirmación de
su naturaleza ética en la elección y decisión que implica el
acto humano.

La pregunta, sin embargo, es qué elementos caracterizan estos
actos como instrumentos de transformación ontológica del hombre
o, en otras palabras, en qué consiste la eticidad del acto humano,
en virtud de la cual el individuo se convierte propilamente en
artífice de su obra. Dice el _Corpus Hermeticum_:

Dios [_deus_] es el creador [_effector mundi_] del mundo […]
gobernando a la vez todas las cosas, en conjunción con el hombre
que gobierna, también él [_simut cuncta gobernando cum homine
ipso, gobernatore compositi_]. Si el hombre asume este trabajo
y todo lo que implica, entendiendo el gobierno que constituye
su tarea propia, obra de tal manera que él para el mundo y el
mundo para él son un ornamento [_ornamento_] en razón de la divina
estructura del hombre, se le llama mundo, aunque el griego lo
denomina con mayor justicia orden. El hombre se conoce y conoce
también el mundo […].@note

El principio a partir del cual el obrar humano tiene un sentido
ético es la noción de _orden_, que aquí ha de entenderse como
el ritmo de la sucesión en el devenir. En efecto, en la medida
en que el individuo opere en conjunción con ese orden, sus actos
serán fruto de la conciencia de su propia naturaleza; enfrentarlo,
en cambio, buscando escapar de la maguinaria de la providencia
y el destino, no sólo no lo libra sino que lo somete.

Si la gnosis evidenció que el problema no era elegir entre el
mal o el bien, sino la elección misma como manifestación de fractura
temporal en el ser, un desmembramiento de la identidad entre
desear y ser, el hermetismo parte de que no existe la identidad
como realidad inmutable sino dinámica en el acto creativo, porque
éste sintetiza libertad y necesidad, capacidad de elección y
obligatoriedad, en un juego de tensiones. Rebelarse al destino
entonces es un problema en y dentro del devenir: es una cuestión
de transformación humana que tiene lugar en el obrar: es una
cuestión ética.

Pero se trata de una cuestión que rebasa al propio hermetismo,
porque si bien es capaz de formular las dimensiones del problema,
no cuenta con los elementos para traducir su visión en una estructura
práctica que convierta la meta que se ha propuesto en un modo
de vida.

# III. Neoplatonismo hermético

Pues lo que hay de inverosímil en los mitos, con eso mismo se
abre camino hacia la verdad. \
++Juliano++ {.epigrafe}

## _El orden del mundo_

++Sabemos hoy que++ los _Hermética_ son apócrifos, elaborados
alrededor del siglo +++II+++ d.C. Su atribución al mítico Hermes
Trismegisto los hizo aparecer como fruto de una sabiduría remota.
Lactancio,@note por citar un ejemplo, encontró en ellos ---y
particularmente en _Asclepio_---, un anuncio del advenimiento
del cristianismo, anterior incluso a los hechos de Moisés. Pero
cualquiera que sea su origen, el hecho es conforme fueron construyéndose,
puesto que no se trata de la obra de un autor, sino de una sabiduría
elaborada entre muchos, que van dejando constancia de los cambios
y los contrastes del saber de la época.

En realidad, la doctrina de los _Hermética_ está constituida
por una síntesis enciclopédica del saber vigente entonces. Sin
embargo, al colocarse como una de las fuentes originarias de
la sabiduría, tuvo, necesariamente, el efecto de proporcionar
elementos para suponer una unidad última del saber en un mundo
de doctrinas fragmentanias. La afirmación de Lactancio posee
ese sentido al pretender enlazar el advenimiento del cristianismo
con la «predicción» hermética. Platónicos como Porfirio, lámblico,
pero señaladamente Apuleyo, Juliano y en el Renacimiento Ficino,
Pico y Bruno, no dejarían de señalar la continuidad entre Hermes
y Platón, y terminarán por dar, a través de su interpretación,
un cuerpo a una ética y una conducta hermética, sobre la base
del propio Platón. Tracemos aquí los ejes de esa supuesta continuidad
en el terreno de la conformación del cosmos.

Un cosmos dinámico y ordenado no puede serlo sin _psyché_. Para
Platón, el cosmos tiene dos causas, la inteligente y la necesaria.
La primera pertenece a la sustancia de lo que es idéntico a sí
mismo, privada de movimiento y de cambio. La segunda, a la sustancia
de lo otro, que no soporta «las expresiones de «esto» o «aquéllo»,
ni cualquier otra fórmula que las designara como «realidades
permanentes».@note Ninguna de las dos, sin embargo, es propiamente
hablando, causa primera, porque ninguna es capaz de producir
el movimiento armónico que caracteriza el cosmos. S1 una es todo
cambio caótico, la otra es orden rígido y estático. Es necesario,
pues, un nexo que haga posible que lo que se transforma refleje
la inteligencia de las formas inmóviles. Ese vínculo no es otro
que _psyché_, la obra que construye el _demiurgo_.

Según el mito expuesto en _Timeo_,@note el _demiurgo_ tomó la
sustancia de lo que es idéntico a sí mismo y la de lo otro, y
las mezcló. De ellas extrajo una sustancia que sirve de puente
entre las otras dos ---porque está compuesta de ellas. Después,
unirá las tres y, siguiendo un riguroso orden matemático, divide
y une, de nueva cuenta, esta nueva mezcla. A partir de ahí las
«cortará en sentido longitudinal para unirlas por su centro formando
una X a la que, tomándo:a de cada una de las puntas, juntará
entre sí, formando dos círculos: uno externo y otro interno,
a los que imprimirá movimiento. Al círculo exterior le otorgará
el movimiento de lo mismo que no es sino el movimiento de rotación
sobre el propio eje. Al segundo círculo lo dividió seis veces
e hizo siete círculos iguales a los que dio el movimiento de
traslación, y con ello rodeó el cuerpo del mundo».

Este pasaje, que combina elementos míticos y proposiciones matemáticas,
no deja de constituir un enigma en cuanto a su significado literal,
ni en cuanto a su sentido.

El alma del mundo tiene una triple constitución, de lo uno, de
lo otro y ese tercer elemento que intercede entre los dos. Y
es por ello que en ella se da el cambio ordenado que constituye
el ser.

Visto desde el hermetismo, las ideas platónicas de _Timeo_ vienen
a validar su propia tesis de una condición dinámica del ser que
produce una conjugación entre intelecto y materia. Además, no
se contradicé con la unidad ontológica postulada por el hermetismo
a partir de las emanaciones del _Nous_. Pero, sobre todo, es
fundamental porque traduce el contenido míitico-religioso del
_Poimandres_, a un lenguaje filosófico, lo que no sólo implica
una nueva forma discursiva sino, ante todo, un orden explicativo
orientado hacia la ética y, por ende, a un conjunto de prácticas
que le serán propias.

La clave está en que, a partir de estas correspondencias, el
neoplatonismo hermético encontrará no sólo una doble vertiente
para fundamentar su pensamiento y postulario, incluso como producto
de un único saber revelado, sino que sacará las consecuencias
prácticas que se derivan de la noción del _Nous_ _demiurgo_ del
hermetismo y del _demiurgo_ de Platón.

Al modelar el alma del mundo, cuya característica esencial es
fijar el orden del devenir, el _demiurgo_ ha de construir el
cuerpo del mundo, es decir, aquello que deviene. Se trata de
un cuerpo único, autónomo y eterno que agota toda la materia.

Pero lo que vale la pena resaltar es cómo lo forma:

> He aquí cómo y con qué cuatro elementos fue formado el cuerpo
> del mundo; lleno de armonía y de proporción, tiene de su naturaleza
> la amistad con la que se une tan íntimamente a sí mismo, que
> ningún poder podría desasociarlo como no fuera el mismo que encadenó
> sus partes.@note

Se trata de un vehículo dinámico que define las relaciones entre
el fuego, la tierra, el agua y el aire, pero no sólo en términos
de sus relaciones físicas, sino en tanto que constitutivas de
_psyché_. Los elementos mantienen entre sí vínculos no sólo a
nivel de la experiencia sensible, como algo que se produce por
sí mismo, sino también en uno, al que llamaré emotivo, en tanto
que constituyen los cimientos de un organismo vivo e inteligente
como lo es el alma del mundo.

Así, no sólo los elementos pueden unirse físicamente, sino que
esas posibilidades son entendidas como amistad o rechazo, amor
u odio; responde, pues, a la vida, a lo orgánico ---entendido
como inteligente--- y no únicamente a lo físico. La amistad que
los une no es, en realidad, otra cosa que la manifestación del
deseo.

Si atendemos a lo expresado por Platón en _Lisis_, la amistad
corresponde a la necesidad de satisfacción de un deseo. En efecto,
una vez que Sócrates ha expresado que «movido por una especie
de inspiración adivinadora, que lo que es amigo de lo bello y
del bien es lo que no es ni bueno ni malo»@note y que por ello «lo
que no es bueno ni malo es llevado a la amistad del bien por
la presencia del mal»,@note concluye que es el mal la causa de
la amistad lo que, evidentemente, es imposible. Así, propone
Sócrates que s1 desapareciera el mal del mundo, no por ello desaparecería
la amistad, pues «[…] si el mal desapareciera, ¿qué vendrían
a ser el hambre y la sed y las demás necesidades del mismo género?;
¿quedarían suprimidas? ¿O bien subsistiría el hambre mientras
hubiera hombres y animales, si bien dejando de ser nociva?».@note
La respuesta está dada en que hambre y sed unas veces producen
el bien y otras el mal; de ahí que, si se suprime el mal no por
ello desaparecería la necesidad positiva, es decir que incluso
luego de la supresión del mal subsistirían los deseos que no
son buenos ni1 malos en sí mismos sino necesarios para la vida.
Así, la causa de la amistad es el deseo ---ni malo ni bueno---
de lo que falta y que según _Lisis_ «[…] se refiere a una cosa
que está vinculada a nosotros por una cierta conveniencia […]».@note

En _Timeo_ Platón parece recoger esta idea del _Lisis_ en la
medida en que la amistad expresa la necesidad de algo en relación
con otro, por encima de consideraciones de bondad o maldad, simplemente,
para que el cosmos exista y siga su curso. En otros términos
la necesidad, la sustancia necesaria, en su sentido más pleno,
no es otra cosa que la expresión desordenada y caótica del deseo,
que al entramarse con la inteligencia y constituir a _psyché_,
encuentra en el orden de la sucesion de las cosas, la literal
providencia.

Alimentando el cambio, el deseo es el motor de _psyché_; el cuerpo,
la llama que quiere y anhela, que lo busca todo, que se inflama
de vida. Pero así como el deseo opera como el destino en su más
pleno sentido, también opera la fuerza voraz a la que se obedece
ciegamente y que terminará por transformarlos. La providencia
es la operación de la inteligencia capaz de traducir la locura
del deseo en el principio sutil de la vida: el amor que en el
nivel de la experiencia emotiva es la imagen del orden.

El hermetismo neoplatónico entenderá la providencia, a partir
de Platón, al mismo tiempo como límite al deseo, y como aquello
que posibilita su satisfacción. En otros términos, gracias a
la providencia el mundo no aparece a los ojos herméticos como
un corte entre deseo y satisfacción (al modo gnóstico) sino como
el principio que los une. Brutal, Juliano el Apóstata representa
a la providencia como la madre de los dioses que castra a Atis.

> Esta diosa [la madre de los dioses], que es también la providencia,
> experimentó un desapasionado amor por Atis […]. Al conservar
> la providencia lo que nace y se destruye, el mito dice que ama
> su causa creadora y fecunda y que le ordena dar a luz sobre todo
> en lo inteligible y desear volverse hacia ella misma y cohabitar
> con ella, pero prescribiéndole que con ningún otro, persiguiendo
> a un tiempo la salvación uniformal y rehuyendo a la vez su inclinación
> a la materia. Le ordenó [a Atis] que se contemplara a ella misma,
> ya que es la fuente de los dioses creadores y no es arrastrada
> hacia la generación ni se dejaba engañar. _De esta manera el
> gran Atis iba a ser un creador más poderoso_.

> Y él [Atis] avanzó en su descenso hasta las extremidades de la
> materia […] dicen que el león ayudó a la providencia creadora
> de los seres, es decir, la madre de los dioses, y que después,
> al descubrir el hecho y denunciarilo [el enamoramiento de Atis
> de la Ninfa], fue el causante de la mutilación del joven ¿Y qué
> es la castración? La interrupción del infinito.

> Pues la generación está retenida por la _providencia_ creadora
> en unas formas limitadas, no sin la llemada locura de Atis.@note

La castración de Atis viene a significar aquí no la interrupción
del deseo, sino su acotamiento. De la delirante insatisfacción
que se disuelve en una búsqueda frenética por la materia, el
deseo encuentra su objeto no en el desear sin más, sino en la
generación, en la creación de formas limitadas.

No hay frase que sintetice tan bien esta 1dea que la ya célebre
del _Poimandres_: «La causa de la muerte es Eros», y que en Platón
cobra la forma de: «Es el mundo el que se da su propio alimento
de su propia destrucción».@note

Para el hermetismo neoplatónico, la providencia traduce la muerte
en principio de vida, y con ello hace del deseo voluntad de generación.
El cosmos todo es, entonces, una entidad viviente, todo vive,
todo es _psyché_ ---como lo viera Ficino---@note y todo, hasta
la muerte, se traduce en vida.

Visto en perspectiva, para Platón, como después para el hermetismo,
destino y pruvidencia son las operaciones básicas de _psyché_.
Su manifestación más clara es el devenir ordenado y armónico
en el que la destrucción sucede, siempre, un principio de creación.
Son, en una palabra, los instrumentos de _psyché_ para repetir
en su propio interior el modelo que ella misma representa. Pero
qué mejor que decirlo con Giordano Bruno:

> […] tal alma (el alma del mundo) no asciende ni desciende […]
> estando compuesta de potencias superiores e inferiores, tiende
> con las superiores hacia la divinidad y con las inferiores hacia
> la mole material, que por ella es vivificada y mantenida entre
> los trópicos de la generación y corrupción de las cosas vivientes
> conservando la propia vida eternamente, pues la acción de la
> divina providencia les conserva siempre con la misma medida y
> orden.@note

La función de _psyché_ es mantener la continuidad de un modelo
(una misma medida y un mismo orden) en cada una de aquellas cosas
que, a su vez, ella misma engendra. Lo hace construyendo las
relaciones entre destino y providencia que comparten cada uno
de los seres:

> […] según ella [el Alma del Mundo] entra en contacto con un objeto
> que posea una sustancia divisible, o con un objeto cuya sustancia
> sea indivisible, ella dice, moviéndose, a través de todo su propio
> ser, a qué sustancia es idéntico el objeto y de qué sustancia
> se diferencia. Pero sobre todo pone de manifiesto de qué, cuándo
> y cómo las cosas que devienen llegan a ser y a padecer unas por
> relación a las otras o en relación con las cosas que son inmutables.@note

Y serán esas misma relaciones, que definen el ser de cada cosa,
las que sean objeto de conocimiento.

> Hay una absoluta necesidad de tal espíritu [el espíritu del mundo)]
> con el fin de que las almas celestes alcancen a penetrar en un
> cuerpo grosero y comunicarle sus cualidades maravillosas y así
> en la materia del mundo como en el cuerpo humano […]. Así la
> virtud del Alma del Mundo se d:sparrama sobre todas las cosas
> del mundo merced a la quintaesencia y no existe nada en el universo
> que no sea iniluenciado por cualquier parte de su virtud y que
> sea sujeto a su poder. En virtud de tal espíritu todas la cualidades
> ocultas de las cosas se difunden […].@note

Sin embargo, cómo es que opera _psyché_ y cómo reconocer los
vínculos que conforman a cada ser.

Platón señala en _Timeo_ que una vez que el mundo ha sido formado
y las semillas de todas las cosas sembradas, a cada planeta le
corresponde tomarlas y completar con ellas la acción demiúrgica.

Cada planeta tiene, así, un número igual de semillas o almas
de acuerdo con las cuales formarán los cuerpos que les corresponden,
confiriéndoles una dignidad y tomando la tarea de «gobernar a
ese viviente mortal con el mayor grado de belleza y bondad».@note

Esta afirmación no es una narración mítica. En el contexto de
_Timeo_ cumple la función de fijar un principio para ordenar
las características de los seres sublunares y establecer el número
y el tipo de sus relaciones, que más adelante adquirirá un carácter
operativo.

Platón, en realidad, no hace sino reflejar aquí la creencia de
que cada planeta posee una serie de atributos. En principio,
éstos se definen por la relación de subordinación entre dos de
los cuatro elementos, uno de los cuales es dominante. A ella
quedan asociadas varias cosas: uno de los cuatro humores (melancólico,
venial, violento y flemático) y ciertas cualidades: sabores,
colores, climas, hasta agotar todas las características de los
seres que habitan el mundo.

Organizados así, resulta claro que todas las cosas del mundo,
lo mismo si son hombres, animales, plantas o piedras, se identifican
por sus características con un planeta en particular, El régimen
de sus relaciones recíprocas y sus efectos se construye a partir
de las correspondencias o diferencias que existan entre los atributos
de los astros a los que pertenece. Para determinar cómo, cuándo
y por qué los elementos que conforman la naturaleza de un planeta
específico predominan sobre las otras, se remite a la ubicación
de los signos zodiacales en el cielo. Aquel que ocupa el cenit,
y en función de que posee atributos de forma similar a los planetas,
será la referencia para conocer cuál es la naturaleza que gobierna,
que a su vez, será el punto de partida para establecer qué relaciones
son convenientes en un tiempo y lugar determinados.@note Por
supuesto, estos no son más que los fundamentos de cualquier astrología.
Sin embargo, a Platón no le interesan los horóscopos. Al contrario,
en _Timeo_ todo ello cumple la función de convertir las múltiples
características de las cosas y sus relaciones caóticas en el
inundo, en un conjunto ordenado que revela la continuidad de
_psyché_ en cada una de ellas.

Bajo esta perspectiva, las tesis de que los planetas forman las
cosas se convierte en el retrato mitológico de la correspondencia
entre micro y macrocosmos. Cada ser es una manifestación de _psyché_
---en cuanto es retrato de la unión de inteligencia y materia---
porque sigue un mismo modelo en lo que se refiere a su estructura
última. Al mismo tiempo, con ello establece la estructura de
las correspondencias, es decir, las relaciones horizontales y
verticales entre los seres, que determinan los distintos fenómenos
que pueden apreciarse lo mismo en la tierra que en el cielo.

A partir de la correspondencia entre los _Hermética_ y Platón,
en cuanto a la condición dinámica del ser y su unidad ontológica,
el neoplatonismo hermético construirá una noción de providencia
y destino como las operaciones básicas del _psyché_, productoras
de una creación constante, definida a su vez como orden y amor.
Sobre esta base, las creencias astrológicas del hermetismo encontrarán
eco en el propio Platón, como definitorias de las relaciones
entre las cosas, y entre éstas y el alma del mundo. Un armazón
sobre el cual se puede construir una estructura práctica que
dé forma a un modo de ser hermético.

## _La naturaleza del hombre_

El _Timeo_ describe un mundo de orden y necesidades, deseos y
satisfacciones fugaces, al que también pertenece el hombre.

Y en ese mundo, como cualquier ser viviente, el hombre es _psyché_,
producto de lo necesario y lo divino, hijo de la providencia
y el destino.

Pero no se trata de una forma viviente como cualquier otra: es
la forma viviente ejemplar, equiparable únicamente a la totalidad
del cosmos. Tan sólo para Platón, en principio, la degradación
del hombre hace concebir la naturaleza de los demás vivientes:

> La raza de las aves […] no es más que una ligera metamorfosis
> de esos hombres sin malicia, frívolos, grandes habladores de
> las cosas celestes […]. Los animales que caminan y las fieras
> proceden de hombres extraños a la filosofía […]. El cuarto género
> que vive en el agua proviene de los hombres desprovistos de inteligencia
> y conocimientos.@note

De tal manera que, continúa:

> Por estas misma razones se transforman hoy en día los unos en
> los otros, según que desciendan de la inteligencia a la estupidez
> o que se eleven desde la estupidez hasta la inteligencia.@note

En suma, en el hombre se juega la naturaleza de cada ser vivo
particular, su composición, su equilibrio y su ordenamiento en
una jerarquía. En su calidad de «ejemplar», es decir, como modelo
de la perfección armónica de _psyché_, el hombre resume «ordenamientos
vivientes». Cuando la relación entre la inteligencia, la conducta
---entendida como un modo de ser--- y el cuerpo se ve alterada
por la fortaleza o debilidad de alguna de las partes, en realidad
estamos ante una _psyché_ distinta, no humana. AÁsí, los animales
que caminan, según _Timeo_, son hombres extraños a la filosofía
que «para nada se preocupan de la naturaleza del cielo, porque
son incapaces de hacer uso de las revoluciones que se verifican
en la cabeza, se dejan conducir ciegamente por el alma que reside
en el pecho. Estos hábitos hacen que tengan los miembros y la
cabeza inclinados hacia la tierra con la que tienen una especie
de parentesco; […] si tienen cuatro patas o más es porque dios
ha querido que los más estúpidos estuviesen más estrechamente
ligados a la tierra».@note Esos hombres son, en más de un sentido,
gusanos enfundados en cuerpos humanos y si hay lugar para una
metamorfosis, ésta sería para ellos una salida, una compensación
a la vida desdichada y, en más de un sentido, enferma y delirante
que han llevado. Y hablo de compensación y no de castigo porque
cuando no hay correspondencia entre la inteligencia, el modo
de ser y el cuerpo, cuando en uno predomina la flaqueza y en
otros la potencia; cuando, en fin, se descuida una de las partes,
la naturaleza misma, orientada hacia el bien y la belleza, los
dirige a despreocuparse de la forma humana y adquirir la forma
que les conviene, donde están en equilibrio y pueden ser pródigos.

Pero si la potencia de _psyché_ es tal que puede recogerse en
sí misma y recuperar su equilibrio hasta en la más ínfima de
sus partes, ¿no puede el hombre, como _psyché_ ejemplar, hacer
lo mismo respecto de sí, como artista en un universo que es una
obra de arte?

Al igual que el cosmos, para Platón el hombre tiene una conformación
trinitaria: las almas inmortal y mortal, y el cuerpo. La primera
reposa en el encéfalo, la segunda en el pecho y el cuerpo está
conformado por la mole material. En él, como en el cosmos, _psyché_
es el «artista interior» que va construyéndose desde adentro.
Va ordenando el deseo en cada órgano, uniendo entre sí necesidad
y providencia, para que el hombre todo, tenga la impronta de
su enlace. Por ello, si bien el diafragma es útil y nos permite
alimentarnos y reponer fuerzas, es también el lugar donde yace
la gula, regulada a si vez por el bajo vientre.@note El cuerpo
del hombre, como el cuerpo del mundo, se encuentra dominado por
la voracidad de la necesidad que no se satisface nunca en su
afán absurdo por satisfacerse siempre. Pero la providencia ---que
no es sólo la capacidad de raciocinio---, compensa la voracidad
con un ritmo de satisfacción constante y se percibe en el sutil
equilibrio entre cada órgano del cuerpo, para que mantengan la
mesura y la armonía que es base para el equilibrio último entre
inteligencia, conducta y cuerpo.

Precisamente porque el equilibrio orgánico es base de la armonía
total del hombre, cuando se rompe aparece la enfermedad que,
además de alterar los procesos orgánicos, altera el modo de ser
y la capacidad intelectiva. La degeneración de un órgano puede
conducir, en última instancia, a la degeneración total del hombre
y, por supuesto, a la muerte.

La enfermedad es, para Platón, un deseguilibrio que al atacar
un órgano específico, merma el orden completo del hombre y puede
denigrarlo. Y eso es decisivo, porque supone que la enfermedad
es precisamente una ruptura en el orden y por tanto irracional,
pero originada dentro del cuerpo humano; es decir, en _psyché_.

Con ello se distancia en definitiva de la idea de un ultradeterminismo
donde agentes exteriores (demonios, divinidades, etc.) son los
causantes de toda desgracia. Al contrario, mantiene el carácter
irracional e involuntario de la enfermedad. EÉsta ya no tiene
el sentido absoluto de «destino». Ambigua, la enfermedad tiene
su origen en lo que se es.

La larga explicación dada en el _Timeo_ para las enfermedades
del cuerpo, a las que distingue en tres clases según corresponden
a los elementos (aire, fuego, agua, tierra), a los humores, a
la pituitaria o a la bilis, atiende a cómo se produce la enfermedad,
pero no por qué se produce. Esto puede sorprendernos porque s1
encontramos una descripción del padecimiento, no hayamos la explicación
de su ocurrencia. Pero Platón es consecuente en este punto. No
hay una razón de la enfermedad porque ésta es irracional.

Uno de los temas fundamentales de _Timeo_, desde la perspectiva
del hermetismo neoplatónico, no es otro que el de la cura. Antes
que nada porque el peligro de la enfermedad no radica en la molestia
y el dolor, o en la posibilidad de muerte. Más apremiante para
el que practica el arte de la medicina, es evitar la locura,
la imbecilidad y la degradación humana, que hacen más turbia
una vida de por sí expuesta a la inclemencia de la temporalidad.

Las enfermedades del alma ---dirá Platón--- sobrevienen como
consecuencia del cuerpo. Y todo malestar del alma, obvio es decirlo,
confunde la naturaleza del hombre. Por ello se debe mantener
siempre un equilibrio no sólo en el terreno de la correspondencia
de constitución ---alma fuerte en un cuerpo fuerte---, sino en
las afecciones que pueden disminuir la capacidad del cuerpo y
embotar el alma.

La máxima de la sanidad platónica parangona el cuidado del hombre
por sí mismo, con el cuidado que el cosmos tiene de sí: «Tener
igualmente cuidado con las distintas partes del cuerpo y del
alma imitando al universo entero».@note

Pero el que toda enfermedad del alma tenga un origen corporal
implica que es involuntario y que se manifiesta tanto en la naturaleza
de la inteligencia (como locura o ignorancia) como en la conducta
(intemperancia, exceso de alegría, etc.). Pero si la enfermedad
es producto de la propia naturaleza puede revertirse imitando
al universo, el movimiento de _psyché_ que renace de la degeneración.

De todos los movimientos ---dice Platón--- es mejor el que uno
produce en sí mismo porque no hay otro que se asemeje al movimiento
del pensamiento y del universo.@note Sin proponer un proceso
de transformación de la naturaleza humana, sino una forma de
conservación del carácter humano en el hombre, la máxima del
«automovimiento» ---del que se deducen terapias concretas---,
además de la correspondencia con el funcionamiento general de
la _psyché_, es una recomendación que atiende, por igual, al
cuerpo, a la inteligencia y a la conducta. Platón recomienda
la gimnasia en primer lugar, el paseo en embarcación y, sólo
por último, el uso de drogas, anteponiendo aquello que es autoquinético
sobre la embriaguez, cuya función regenerativa supone la pasividad
del hombre.

El principio «medicinal» del automovimiento en el _Timeo_ no
es sólo una terapia, sino una disposición del hombre frente a
su propia naturaleza. La relación entre fisiología y conducta
inducen a Platón a plantear más una ética de la salud, en el
sentido de una actitud permanente de vida, que una terapia regenerativa
propiamente dicha.

Separado de la tradición hipocrática cuyo eje es una terapia
dietética centrada en evitar las dolencias y el malestar, Platón
centra su atención en _psyché_ como totalidad humana, y en la
que no se debe desprotejer un aspecto en el orden de atacar otro.
Ási, el problema de la enfermedad no es el dolor en sí mismo,
sino las relaciones de proporción entre las distintas partes
de la psique humana.

Desde la perspectiva del médico Erxímaco expuesta en el _Banquete_,
se trata de la preocupación por las operaciones amorosas del
cuerpo:

> […] La medicina es, para decirlo en una palabra, el conocimiento
> de las operaciones amorosas que hay en el cuerpo en cuanto a
> repleción y vacuidad y el que distinga entre ellas el amor bello
> y el vergonzoso será el más experto.@note

Se trata de un arte, que no es conocimiento o técnica, sino un
ánimo que busca, a través del saber y la pericia técnica, la
regeneración de un orden perdido. Y es en eso en lo que consiste
la operación del médico. Es decir, en someter el cambio dinámico
a la disciplina de la inteligencia.

> […] el que logre que se opere un cambio, de suerte que el paciente
> adquiera en lugar de un amor [malo] el otro y en aquellos que
> no hay amor, pero es preciso que lo haya, sepa infundirlo y eliminar
> el otro cuando está dentro, será también un profesional […].@note

Pero lo será también de la música y la adivinación.

> La armonía ---dice Erxímaco a sus oyentes---, ciertamente es
> una consonancia, y la consonancia es un acuerdo; pero un acuerdo
> a partir de cosas discordantes es imposible que exista
> mientras sean discordantes y, a su vez, lo que es discordante
> y no concuerda es imposible que armonice […] y el acuerdo de
> todos estos elementos lo pone aquí la música de la misma manera
> que antes lo oponía la medicina.@note

La música y la medicina aparecen aquí como actos similares en
tanto que generadores de orden. Siglos más tarde Marsilio Ficino
dirá que «cualquiera que haya aprendido de los pitagóricos, de
los platonistas, Mercurio y Aristodenos que el alma y el cuerpo
universal, en tanto que son un viviente, están conformados por
una proporción musical […]; no debe sorprendernos que todo lo
vivo haya sido cautivo por la armonía».@note Y traduce este postulado
en términos prácticos:

> Platón y Aristóteles piensan, como hemos podido comprobar por
> nuestra propia experiencia, que la música seria mantiene y devuelve
> la armonía a las partes del alma, como la medicina devuelve la
> armonía a las partes del cuerpo.@note

El supuesto es que la música terrena imita la música celeste,
asociada desde Platón a la sucesión de las esferas. Así, escuchar,
como crear música, e incluso bailar, son formas restituyentes
y creadoras de orden, porque se dirigen no al cuerpo o la inteligencia,
sino a _psyché_.

Un papel distinto juega la adivinación. He aquí la razón del
hígado:

> […] organizaron [los astros] excelentemente la parte interior
> de nuestra naturaleza y para que a lo menos tuviera una
> matriz de verdad, le dieron la adivinación. Es bastante evidente
> que la adivinación no es más que un suplemento a la imperfección
> natural del hombre. Nadie, en efecto, en el pleno ejercicio de
> la razón, alcanza una adivinación inspirada y verdadera: es preciso
> que el pensamiento esté dificultado por el sueño o extraviado
> por la enfermedad o por el entusiasmo.@note

Platón coloca en uno de los órganos del cuerpo el principio de
comunicación con los dioses que gobiernan el cosmos, cuando el
delirio febril o el entusiasmo perturban al pensamiento. Pero
más que atribuirle funciones visionarias, lo convierte en una
práctica proveedora de ordenr.

> Más aún: también todos los sacrificios y actos que regula la
> adivinación, esto es, la comunicación entre sí de los dioses
> y los hombres no tiene ninguna otra finalidad que la vigilancia
> y cura de Eros.@note

Para la cura, cuando el pensamiento está extraviado por la fiebre,
permite conocer la fuente de desequilibrio como el camino para
la salud. Cuando es resultado del éxtasis, señala rutas que debe
seguir el hombre.

Cuando el pensamiento está turbado por el entusiasmo, la verdad
se abre paso a partir del contacto con los dioses. Pero hay muchas
clases de furores. Giordano Bruno, en _Los heroicos furores_,
distingue entre aquellos que son resultado de la manifestación
misma de un dios, de aquella en que es el hombre mismo el que
se diviniza y a quién se adora.@note

Platón, a su vez, distingue cuatro tipos de éxtasis. La mántica
propiamente dicha, o locura profética, que es la inspirada en
Apolo. La locura teléstica o iniciática, relacionada con Dionisios;
la locura poética, inspirada en las musas y, finalmente, la locura
erótica. Estos cuatro tipos de manías, las tres primeras corresponden
a la manifestación del dios a través de un hombre y sólo la última
es aquella en que Bruno reconoce a los hombres como «más dignos,
más potentes y eficaces […] se considera y se ve [en ellos] la
excelencia de la propia humanidad».@note

Agrippa, por ejemplo, en _La filosofía oculta o la magia_ se
extiende sobre la naturaleza ritual de los furores apolíneo,
dionisiaco y poético.@note Así, el furor poético es el que revela
las relaciones de las cosas del mundo con las esferas celestes.
Los rituales apolíneos, es decir, los misterios, votos, sacrificios,
oraciones, etcétera, hacen «ascender el alma para conjuntarla
con la divinidad».@note Finalmente mediante expiaciones, exorcismos
y sacramentos, el rito dionisiaco pone al alma en contacto con
los dioses intermedios, como el daimon de Sócrates.

El propio Agrippa, sin embargo, distingue al furor erótico:

> La cuarta especie de furor proviene de Venus y convierte y trasmuta
> el espíritu humano en el divino con el ardor de amor, haciéndolo
> similar a dios. Es esto lo que hace decir a Hermes Llohi Asclepio,
> qué gran milagro es el hombre; pasa a la naturaleza de un dios
> como si él mismo fuera dios; tiene trato con el género de los
> demonios, sabiendo que ha surgido del mismo origen; desprecia
> la parte de su naturaleza solamente humana, pues ha depositado
> su esperanza en la otra parte».@note

El punto de convergencia entre hermetismo y platonismo es aquí
crucial. El argumento que Platón desarrolla en _Fedro_ intenta
demostrar que la manía es un bien, porque permite intuir la verdad
sobre la naturaleza divina y humana. Parte de que el alma es
inmortal, gracias a que se mueve por sí misma, pues, sólo aquello
que se mueve por sí mismo tiene una existencia eterna.

Al igual que en _Timeo_, el principio divino de la naturaleza
del alma, su eternidad, está fundada en su capacidad de generar
su propio movimiento. El hermetismo, a su vez, encontrará en
este principio platónico el fundamento ontológico de la libertad
del alma humana y, por ende, del hombre, pues «todo lo que es
movido es esclavo, sólo lo que mueve es libre».

Pero volvamos a Platón. En _Fedro_, el alma humana es representada
como un auriga que conduce un carro alado jalado por dos caballos.
En el caso de los dioses, los caballos son ambos buenos y dóciles
al jinete. En los hombres los caballos no son iguales: uno es
dócil, el otro indómito. Lo que distingue al alma humana de la
divina, así, es la existencia de un conflicto.

El alma inmortal del hombre, inmortal porque ella es el origen
de su propio movimiento, es un alma intrínsecamente conflictiva;
eso es lo que la hace ser su propio motor.

El caballo bueno, dócil, representa en Platón el valor, el coraje,
corazón, ánimo, en una palabra, templanza. El indómito, en cambio,
es el deseo de «concupiscencia» y «apetito». La nota distintiva
es que aquello que ciertas tradiciones atribuyen al cuerpo, haciendo
de la encarnación de las almas una tragedia, en Platón forma
parte del alma misma, forma parte de su naturaleza y eternidad.

Al hermetismo no le fue difícil reconocer en estas 1deas en Platón
la tesis de que el cuerpo es propio también de los dioses, y
que es su cualidad la que establece la jerarquía. Dice Platón
«nos figuramos a la divinidad como viviente inmortal, que tiene
alma, que tiene cuerpo, unidos ambos de forma natural».@note
La diferencia con los hombres consiste en el hecho de que «el
cuerpo» al que anima el alma humana es mortal.

Por eso el alma humana puede caer o elevarse. La fuerza del auriga
es la que determinará cuál es el rumbo que toma el alma, si se
dirige al cielo y se eleva o si pierde las alas, precipitándose
hasta la materia.@note

En este sentido, el movimiento del alma está diferenciado «ontológica
y axiológicamente»,@note de donde se concluye, finalmente, no
sólo la libertad sino también la eticidad del alma.

Pero éste, que es aquello que distingue a los dioses de los hombres,
será para el hermetismo la condición de superioridad del hombre
respecto a los dioses. En efecto, la condición intermedia del
hombre, divina y mortal, es superior a la de los dioses precisamente
por la cualificación de su movimiento, por la posibilidad de
opción no sólo axiológica sino ontológica, de la que carecen
los habitantes de la región supralunar.

El movimiento del alma humana en el cielo, el lugar que alcanza
en el cortejo divino y lo mucho o poco que logre apreciar de
la verdad del ser condicionan su caída y ello genera una jerarquía
que pone al alma encarnada en lugar de elegir, de nueva cuenta,
si asciende o desciende.

Las almas que caen, indica Platón, entran al cuerpo humano y,
según el grado de verdad que hayan alcanzado en su viaje celeste,
serán:

* Amigos del saber, la belleza y el amor Reyes o guerreros
* Políticos o administradores Gimnastas o médicos
* Adivinos o iniciados
* Poetas
* Artesanos
* Sofistas o demagogos
* Tiranos

A diferencia de _Timeo_, donde es la desproporción entre los
elementos que constituyen, _psyché_ es la que genera a partir
del hombre el resto de los seres materiales; en _Fedro_ es la
actividad humana la que establece una jerarquía entre los hombres,
a partir del principio de cómo se vinculan con el orden.

En el _Banquete_ se hace explícito que todo trabajo humano tiene
una naturaleza amorosa en la que se refleja, por supuesto, el
vínculo del hombre con lo divino. Esta apreciación de la actividad
como ámbito de lo divino humano está también en Hermes:

> Y así formó al hombre con naturaleza corporal y espiritual, a
> fin de que […] pudiese comportarse de acuerdo a su doble origen
> […] teniendo cuidado de las cosas mortales y gobernarlas. 

> Por cosas mortales no entiendo la tierra y el agua […] sino
> todo lo que produce el hombre, sea en esos elementos no sacándolos
> de ellos, por ejemplo, el cultivo de la tierra, los pastos, las
> relaciones sociales, los intercambios mutuos, obras todas que
> constituyen el lazo más sólido de hombre a hombre, y entre el
> hombre y la parte del mundo constituida por tierra y agua.@note

En Hermes, como en Platón, el hombre podrá construirse a sí mismo
a través de sus actos. En última instancia, ser hombre es una
mera posibilidad, un tránsito, un puente o un precipicio, una
indefinición originaria que sólo la acción determina.

Giovani Pico della Mirandola lo resume así en su _Discurso sobre
la dignidad del hombre_:

> Al hombre, en su nacimiento, le infundió el padre toda suerte
> de semillas, gérmenes de todo género de vida. Lo que cada cual
> cultivare, aquello florecerá y dará fruto dentro de él. Si lo
> vegetal, será planta; si lo sensual, se embrutecerá; si lo racional,
> se convertirá en un viviente celestial; si lo intelectual, en
> un ángel y en un hijo de dios.

> Y si no está satisfecho con ninguna clase de criaturas, se recogerá
> en el centro de su unidad, hecho un espíritu con Dios, introducido
> en la misteriosa soledad del padre, el que fue colocado sobre
> todas las cosas, las aventajará a todas.@note

Aunque es posible detectar la correspondencia entre la idea platónica
del hombre ejemplar y su capacidad para restituir el orden amoroso
a través de sus actividad y el hombre como ser intermedio del
hermetismo, aquí el neoplatonismo hermético no toma su base de
Platón ---como en el caso del cosmos--- sino que interpreta a
partir del hermetismo.

La ejemplaridad del ser humano en Platón no implica indefinición
de su ser, como sí ocurre con el hermetismo; pues se es hombre
en tanto que no se degenere o enferme. Por contra, en los _Hermética_,
se es hombre mientras no se cancele ninguna otra posibilidad
de ser. De ello se desprende su capacidad operativa, su capacidad
de generar orden y no sólo restituirlo.

Esta diferencia crucial, porque el hermetismo neoplatónico no
se reduce a un Platón condimentado, su fuerza y originalidad
consiste precisamente en esto: en considerar al hombre fuera
de toda jerarquía, en perpetuo tránsito, en construcción perenne.

Así puede describirse la matriz de la sensibilidad del neoplatonismo
hermético: como la percepción de la condición intermedia del
hombre y la construcción de su tránsito. Esto nos coloca, de
nueva cuenta, en el punto de partida: ¿cómo darle forma a la
vida de este ser mortal e inmortal, armónico y conflictivo?

## _El furioso_

A través de Platón el neoplatonismo hermético desarrolla una
sensibilidad, una forma específica de apreciar la condición humana
y formular los problemas que le atañen, y cuyos rasgos generales
han sido ya examinados. Sin embargo, ella corresponde una actitud,
es decir, una forma de comportarse dirigida a construirse a sí
mismo de acuerdo con metas definidas.

El neoplatonismo hermético recurre aquí, de nueva cuenta, a Platón
y retoma lo dicho en _Fedro_ sobre la forma en que el hombre
ha de conducir su vida hacia la belleza, entendida ésta no sólo
en su relación con el bien, sino de manera mucho más significativa,
como la experiencia tangible y visible de la existencia.

Según el mito expuesto por Sócrates en _Fedro_, los hombres pueden
ser sorprendidos en el mundo por una visión de la belleza. Un
cuerpo, un rostro hermoso es suficiente para cambiar el comportamiento
de un hombre, enloquecerlo y entusiasmarlo. La belleza sirve
para encaminarlo hacia una vida que la pretenda. En principio,
el hombre deseará como «cuadrúpedo, cubrir y tener hijos»,@note
y llegará al extremo de ofrecer a su amado, «sacrificios como
si fuera la imagen de un dios».@note En tanto, la propia belleza
está haciendo nacer de nueva cuenta las alas dentro de la «sustancia
misma del alma», aquellas que ésta perdió al caer en la tierra.

Y sí, el amor se padece. Nacido de un rapto, el amor es una locura
montada en el desenfreno del deseo que anhela avasallar su objeto.
Prisioneros de la pasión, los hombres se comportan «según sea
el dios a cuyo ségquito se pertenece». Así, dignamente llevan
su amor los que siguieron a Zeus; pero los que están al servicio
de Áres, s1 se sienten agraviados, «se vuelven homicidas y son
capaces de inmolarse a sí mismos y a quien aman».@note

En el primer trance de amor hay siempre dependencia y búsqueda
en el otro, pero esa búsqueda se revela para Platón al final
como búsqueda de sí mismo: «en efecto, los de Zeus buscan que
aquel al que aman sea, en su alma, un poco también Zeus»,@note
y siguen las huellas y rastrean hasta que se les abre el camino
para encontrar, por sí mismos, la naturaleza de su dios, al verse
obligados a mirar fijamente hacia él.@note

Pero este encuentro con uno mismo plantea ya una disyuntiva:
ante la presencia de la belleza, los dos caballos que conforman
el alma reaccionan según su naturaleza. El caballo indócil, al
sentir la belleza, intenta lanzarse sobre aquello que lo ha exaltado
al obligar al caballo dócil y al auriga a 1r hacia el amado y
probar los goces de Afrodita.

Pero si el auriga es fuerte debe saber refrenar la excitación
del caballo indómito, pero no es fácil. Inclinada una vez hacia
la voluptuosidad, otra hacia la prudencia, la lucha sirve al
alma para entrar en conflicto consigo misma y vencerse, pues
una vez que tras la dura lucha el corcel indócil se fatiga, con
ello ha sido derrotada esa parte del alma, y el auriga logra
para sí la templanza.

La respuesta diferenciada de los caballos ante la presencia de
la belleza sensible se trasciende cuando el auriga agota el ímpetu
del corcel negro. Pero lejos de sacrificar y expulsar del carro
al caballo indómito, el auriga lo libera. Una vez identificado
el auténtico objeto del deseo y no su imagen, el corcel puede
desbocarse hacia el verdadero amor.

La locura erótica, tal como leemos en _Fedro_, es magnanimidad
y pasión, prudencia y templanza, porque afirma no sólo la virtud
del caballo dócil, sino también lo indómito del otro:

> […] se reconoce, en efecto, que la templanza es el domino de
> los placeres y deseos, y que ningún placer es superior a Eros.
> Y si son inferiores, serán vencidos por Eros que los dominará,
> de suerte que Eros, al dominar los placeres y deseos, será
> extraordinariamente templado.@note

Eros es un demonio y sirve, como tal, de intermediario entre
hombres y dioses. Es el más viejo y el más joven; mortal e inmortal,
bello y feo, pero es, en última instancia, el ser del hombre.

> ---¿Pues qué entonces?

> ---Como en los ejemplos anteriores ---dijo---, algo intermedio
> entre lo mortal y lo inmortal.

> ---¿Y qué es ello, Diótima?

> ---Un gran demon, Sócrates. Pues también todo lo demoniaco está
> entre la divinidad y lo mortal.

> ---¿Y qué poder tiene? ---dije yo--- Interpreta y comunica a
> los dioses las cosas de los hombres y a los hombres las cosas
> de los dioses […]. Al estar en medio de unos y otros llena el
> espacio entre ambos, de tal suerte que el todo queda unido
> consigo mismo, como un continuo […].@note

Amar es, así, el ser propio del hombre.@note La perpetuación
de la elección, pues, en la naturaleza diferenciada de Eros,
celeste o pandemo, está también el ascenso y descenso del auriga
y sus corceles, la elección permanente de la propia conducta.
Eros es libertad «puesto que todo el mundo sirve de buena gana
a Eros»,@note dirá Agatón en el _Banquete_; pero libertad de
elección entre un arriba y un abajo, entre la vulgaridad y la
virtud.

Según el mito que narra Diótima a Sócrates, Eros es hijo de Poros
y Penia, de la abundancia y la carencia. Es «siempre pobre» pero,
por otra parte, «está al acecho de lo bello y lo bueno».

> El hecho revelador ---dirá Juliana González a propósito del
> _Banquete_--- es que el hombre es y no es al mismo tiempo: es
> «lleno» y «vacío», plenitud y carencia, y en esto consiste su
> condición erótica: penetrado de deseo de llenar la oquedad de
> un ser que le falta.@note

Pobre y rico de medios; carente y pleno de recursos, el amor
es una búsqueda, como lo declara Aristófanes a través del mito
del andrógino en el _Banquete_: la búsqueda de sí a través de
otro; acecho de la plenitud a través de la carencia. Finalmente,
Eros, como ser del hombre, es contradictorio, una ruptura, una
sed que no se sacia, un ser bello y feo, alto y bajo, que halla
su razón de ser, su sentido, no en la disolución de la contradición,
en abjurar de su condición, sino en su afirmación que es, en
última instancia, una afirmación generosa.

> ---Entonces ---dijo---, el amor es, en resumen, el deseo de poseer
> siempre el bien.

> Es exacto ---dije yo--- lo que dices.

> ---Pues bien ---dijo ella---, puesto que el amor es siempre esto ¿de
> qué manera y en qué actividad se podría llamar amor al ardor
> y esfuerzo de lo que lo persiguen? ¿cuál es, justamente esta
> acción especial? ¿puedes decirla?

> ---Pues yo te lo diré ---dijo ella---. Esta acción especial
> es, efectivamente, una procreación en la belleza, tanto según
> el cuerpo como según el alma.

> ---Te lo diré más claramente ---dijo ella---. Impulso creador, Sócrates,
> tienen, en efecto, todos los hombres, no sólo según el cuerpo,
> sino también según el alma, y cuando se encuentran en cierta
> edad, nuestra naturaleza desea procrear. Pero no puede procrear
> en lo feo, sino sólo en lo bello. De ahí, precisamente, que al
> que está fecundado y ya abultado porque le sobrevenga el fuerte
> arrebato de lo bello, porque libera al que lo posee de los grandes
> dolores del parto. Pues el amor, Sócrates ---dijo---, no es
> amor de lo bello como tú crees.

> ---¿Pues qué es entonces?

> ---Amor de la creación en lo bello. Así sea.@note

Eros es quien, para Platón, conduce al hombre a la condición
privilegiada de demon, ser intermedio, que mantiene el contacto
entre los dioses y los hombres. Pero sólo lo es cuando no ama
lo bello sino cuando lo crea.

Sobre la base del hermetismo, el neoplatonismo hermético traducirá
las ideas platónicas en la actitud que corresponde a su sensibilidad:
el hombre en su condición intermedia es erótico y lo es en la
medida en que crea belleza en lo bello. Penetra la naturaleza
para fecundarla y lo que fecunda es su propia vida, la condición
en que vive.

Esto tiene dos significados: por una parte comprende lo erótico
como la condición ---equivalente a la castración de Atis--- por
la cual al refrenar el deseo, se logra la capacidad de creación
y recreación en la belleza. El sólo deseo busca fecundar a partir
de lo bello, por amor a la belleza, como lo hace el animal. Frenarlo,
en cambio, es lo que posibilita encontrar una sucesión ordenada
entre deseo y satisfacción, en relación con la propia vida humana
y equivalente a aquella con que providencia y destino gobiernan
el mundo. En otras palabras, aquello que hace del hombre un sujeto
de sus propios deseos, providente, v no un sujeto del destino.

Por la otra, que es sobre esta base que el hombre tiene realmente
un carácter intermedio, que liga ese orden con el de los hombres
y, por lo cual, las obras humanas tiene la virtud tanto de restituir
el orden perdido, como lo hace el médico, o engendrarlo, como
lo hacen, a su manera y entre vtros, el arquitecto y el agricultor.

# IV. La magia

++Todo comienza con una++ pregunta que nos hacemos todos los
días: ¿cómo vivir nuestra existencia, sujeta como está al tiempo
y que posee un término ineludible? Respuestas hay muchas. Cada
día, probablemente, tenemos una distinta. Pero no puede decirse
lo mismo en cuanto al sentido de la pregunta. El hermetismo,
gracias a su herencia gnóstica, la fórmula con un sesgo que precisa
uno de sus sentidos: ¿cómo vivir una existencia sujeta a la elección?
Pues ésta y no otra cosa es lo que significa la temporalidad
para el gnosticismo y el hermetismo.

No está de más detenerse en el valor mismo de la pregunta. Antes
de perderse en el mar de las respuestas, es aquí donde está la
clave: ¿qué es lo que preocupa, propiamente, de la existencia
temporal? La elección. Pero, ¿qué implica ésta? La pluralidad
de las alternativas. Lo significativo, sin embargo, es que esta
pluralidad no es positiva por sí misma. Se trata de algo que
se padece, porque la elección es posible sólo a través de una
ruptura entre el momento del deseo y el momento de la satisfación.
El hecho mismo de tener que optar por uno u otro camino, de los
muchos posibles para conseguir una misma cosa, quiere decir que
la ruta nunca es directa y que los senderos sin salida también
están ahí.

Uno de los rasgos más interesantes de la cercanía del hermetismo
con el gnosticismo es precisamente ésta. La elección no vale
como fundamento de la libertad, sino como condición de _pathos_.
La originalidad del hermetismo consistirá en afirmar que hay
un modo de conducta que transforma el _pathos_ de la elección
en un _ethos_ en la medida en que el reverso de la elección como
algo que se padece es que también permite determinar cómo ha
de construirse uno a sí mismo a través de la acción.

La diferencia que lleva del _pathos_ al _ethos_ es, así, una
diferencia en la conducta, en el modo de hacer y en el destino
mismo de la acción.

Mientras el gnosticismo busca la forma de encontrar en la trascendencia
la vía para escapar de la necesidad de elegir, el hermetismo
encuentra en el actuar humano la forma de revertir lo negativo
de la elección. De ahí que su preocupación se centre en cuáles
son las características que debe tener la acción humana para
que se funde en un principio de eticidad.

El hermetismo parte de un cosmos cerrado al que concibe organizado
sobre dos principios enlazados entre sí, sucesión y orden, por
lo que éste no es otra sino una sucesión ordenada. Sobre esta
base, el hombre es concebido como un ser intermedio, compuesto
de la misma doble naturaleza que constituye el cosmos y, al igual
que éste, sin determinación alguna, pues es todo pero, al mismo
tiempo, no es nada, solamente un principio de existencia.

Así, el hombre es hombre en la medida en que simplemente sea
un ritmo armónico abierto a innumerables consonancias, y no una
entidad cerrada y con límites.

Pero el hermetismo no puede ir más allá. No puede, estrictamente,
resolver su inquietud en una conducta que se corresponda a la
forma misma del hombre, a esta naturaleza intermedia. La razón
es simple, no formula su preocupación en términos éticos sino
teúrgicos. Su intención es llevar al hombre hacia la condición
divina, pero no cuenta con los instrumentos para ver que esa
intención implica construirse a sí mismo conforme a un fin preciso
y que existen conductas que llevan hacia él.

La lectura que hacen de Platón autores como Apuleyo, lámblico,
Juliano, y más tarde Ficino, Pico y Bruno, tomando como punto
de partida el hermetismo, dará al hermetismo los elementos conceptuales
que harán de sus concepciones teúrgicas términos de problemas
éticos.

La correspondencia entre las ideas herméticas y la cosmología
del _Timeo_ platónico no sólo sirven para dar profundidad teórica
a las creencias del hermetismo, también lo son para formular
cabalmente los fundamentos de la ética del neoplatismo hermético.

Sobre la base de que el hombre es un ser ejemplar ---en tanto
que puede transformarse en todos los seres--- cifra la esencia
humana precisamente en su carácter ejemplar que fácilmente puede
asociarse con la noción de ser intermedio del hermetismo. Pero
lo decisivo es que define en qué consiste esa condición de ejemplaridad,
la reproducción, a nivel de la conducta, del orden de sucesión
armónico del universo.

Pero la posibilidad misma de llevar a cabo esto consiste en que
el hombre debe, como antes el _Atis_ de Juliano el Apóstata,
poner un límite al movimiento de la materia. Dicho de otro modo,
establecer los límites dentro de los cuales el deseo alcanza
la satisfacción.

Las prácticas destinadas a crear y restituir el orden armónico
de la sucesión entre deseo y satisfacción en la vida humana,
son las prácticas que llamamos mágicas cuando hablamos en el
contexto de la magia culta del medievo y el renancimiento. Es
a través de ellas que el neoplatonismo hermético creará la sensibilidad
y la estética que definen la sustancia ética de la conducta del
mago hermético.

Los elementos de esta sensibilidad se fundan, tanto en el modo
de plantear el problema de la vida humana como en los hechos
sobre los que llama la atención, sobre los cuales las respuestas
a las preguntas conforman un modo de ser que es distintivo.

El neoplatismo hermético será especialmente temporalidad, pero
una temporalidad que tenemos que llamar fuerte, porque no se
preocupa de periodos temporales prolongados como los que definen
las edades del hombre, o los periodos históricos. A partir de
los ciclos anuales lo que hace es establecer un sentido del tiempo
que define la oportunidad de cada cosa según el mes, el día y
la hora. La astrología no es sino producto de esta sensibilidad
extrema al paso del tiempo. Tomo algunos ejemplos al azar. En
los textos de magia en papiros griegos hay una fórmula para sujetar
la sombra en la que, además de los productos necesarios para
el caso, hay dos referencias temporales clave: la estación y
la hora del día.@note En el _Libro de astrología_ de Ramón Llull,
dingido a las prácticas médicas, hora, día y mes son claves para
establecer la convenencia de un medicamento a un paciente dado.
Algo similar ocurre con los _Libros de la vida_ de Ficino, _La
filosofía oculta o la magia_ de Agrippa.

Hoy no tenemos ningún equivalente, ya no al pensamiento astrológico
---aunque la práctica subsista--- sino a esa forma de incorporar
al juicio ético una dimensión temporal fuerte. Al contrario,
la pugna ha sido en sentido inverso. Y sin embargo, una de las
tareas que aún quedan pendientes para la construcción de nuevos
modelos de pensamiento ético es precisamente esa: concebir y
elaborar un eje de relaciones temporales sino comprensivos de
la vida singular de una persona en el mundo que está viviendo,
e incorporarlos a nuestras consideraciones sobre la conducta.

Pero esta noción de temporalidad fuerte, que mide las oportunidades
en los instantes de la vida de un hombre, no supone una camisa
de fuerza. Al contrario, ella misma abre el horizonte de la tesis
de la transformación humana.

En el mundo donde a cada instante se establecen nuevas condiciones
de existencia, la vida humana pede también transformarse a cada
momento. S1 temporalidad significa elección entre un conjunto
de factores cambiantes, también significa metamorfosis.

Más allá de que el hombre erótico sea capaz de transformarse
a sí mismo para reproducir el orden cósmico en su propia vida
---que es ese en última instancia el fundamento ético de la conducta
hermética--- la idea misma de vincular elección y transformación
ofrece una perspectiva muy rica para la reflexión.

La idea puede resultar simple: no es necesario permanecer siendo
uno mismo todo el tiempo ---algo ciertamente 1mpensable para
el neopiatismo hermético--- ni es necesario mantener una coherencia
entre actos pasados, presentes y futuros. En tanto que ser intermedio,
cuya condición ontológica consiste, paradójicamente, en su indefinición,
el hombre es siempre una multitud polívoca de posibilidades,
y la virtud que consiste en no arraigarse a ninguna.

Aunque basada en un universo y un mundo cerrados y limitados
---el alma del mundo con sus siete planetas y su última esfera
encerraban la totalidad del cosmos--- el planteamiento que hace
el neoplatonismo hermético es abierto y en ello radica su interés.

Sin estar atado a convicciones y a modos ---pues qué clase de
modelo pvede ser aquel que no se define--- propone una construcción
del sujeto sobre la base del cambio y la inquietud. S1 hubiera
una fórmula, ésta no sería otra que: «Se cuanto puedas ser»,
y a esto no traza límite ninguno, ni de forma ni1 de contenido.

Al contrario, si algo desprecia el neoplatismo hermético es el
circunscribir la vida humana a una sola condición. Así, el proceso
de construcción de uno mismo, el camino que lleva al hombre a
constituirse en sujeto, no define una ruta. sino múltiples; no
implica un hábito, sino muchos y variados. Y esto no es muy distinto
a la imagen que todavía conservamos del mago: un ser que no está
atado a nada en particular, que lo mismo combina profesiones
que ciudades y rostros. Que no se resigna a ser de acuerdo a
un modelo, sino a todos los que sea capaz de inventar.

# Bibliografía

++Agrippa++, Cornelio, _La filosofía occulta o la magia_, Edizioni
Mediteranee, Roma, 1993, traducción al italiano de Alberto Fidi.
{.frances}

++Apuleyo++, Lucio, _Apología_, Gredos, Madrid, 1980, traducción
al español de Santiago Segura Munguía. {.frances}

---, _El asno de oro_, Gredos, Madrid, 1989, traducción al español
de Santiago Segura Munguía. {.frances}

---, _Tratados Filosóficos_, +++UNAM+++, México, 1968, traducción
al español de Antonio Camarero (Bibliotheca Scriptorum Graecorum
et Romanorum Mexicana). {.frances}

++Cardini++, Franco, _Magia, brujería y superstición en el occidente
medieval_, Península, Barcelona, 1982, traducción al español
de Antonio-Prometeo Moya. {.frances}

++Christeller++, Paul Oscar, _El pensamiento renacentista y sus
fuentes_, +++FCE+++, México, 1982, traducción al español de Michael
Mooney. {.frances}

++Dodds++, E. R., _Los griegos y lo irracional_, Alianza, 4a.
ed., Madrid, 1985, traducción al español de María Araujo (Col.
Alianza Universidad No. 286). {.frances}

++Eunapio++, _Vida de filósofos y sofistas_, Aguilar, Buenos
Aires, 1985, traducción al español de Francisco de P. Samaranch.
{.frances}

_Evangelios Apócrifos_, Hyspánica Ediciones Argentina, 1985,
traducción al español de Edmundo González-Blanco (Col. José Luis
Broges, Biblioteca Personal No. 2 y 3). {.frances}

++Ficino++, Marsilio, _Teología Platónica_, Zanichelli, Bologna, 1965,
edición de Michele Schiavone.
{.frances}
 
---, _Three books on life_, Center for Medieval and Early Renaissance
Studies, New York, 1989, edición crítica y traducción al inglés
de Carol V. Kaske y John K. Clark.
{.frances}

_The letters of Marsilio_, Columbian University, New York, 1979.
Prefacio de P. O. Christler. {.frances}

++Garin++, Eugenio, _El zodíaco de la vida_, Península, Barcelona,
1981, traducción al español de Antonio-Prometeo Moya. {.frances}

---, _Ermetismo del Rinascimento_, Riuniti, Roma, 1988. {.frances}

++Bruno++, Giordano, _Los heroicos furores_, Tecnos, Madrid,
1987, traducción al español de M. Rosario González Prada. {.frances}

---, _Mundo, magia y memoria_, Taurus, Madrid, 1973, edición
de Ignacio Gómez de Lliaño. {.frances}

++González++, Juliana, _Ética y libertad_, +++UNAM+++, México,
1989. {.frances}

Hermética en dos ediciones: {.espacio-arriba1 .frances}

Hermética Hermes Trismegisto, _Obras completas_, Biblioteca Esotérica,
Madrid, 1988, traducción al español de Miguel Ángel Muñoz Moya.
{.frances}

_Hermética_, Solos Press, Gran Bretaña, s/n, edición y traducción
al inglés de Walter Scott. {.frances}

++Iámblico++, _On the mysteries_, Chthonios Books, Gran Bretaña,
1989, edición de Stephen Ronan y traducción de Thomas Taylor
y Alexander Wilder. {.frances}

++Juliano el apóstata++, _Discursos_, Gredos, Madrid, 1979, traducción
al español de José García Blanco. {.frances}

++Lactancio++, _Instituciones divinas_, Gredos, Madrid, 1986,
traducción al español de José García Blanco. {.frances}

++Lucrecio++, Tito, _Sobre la naturaleza de las cosas_, +++UNAM+++,
México, 1984, versión rítmica de Rubén Bonifaz Nuño (Bibliotheca
Scriptorum Graecorum et Romanorum Mexicana). {.frances}

++Mirandola della++, Giovanni Pico, _Conclusiones mágicas y cabalísticas_,
Obelisco, Barcelona, 1982, traducción al español de Eduardo Sierra
(Col. Biblioteca Hermética). {.frances}

---, _Discurso sobre la Dignidad del hombre_, Ed. Nacional, Madrid,
1984, edición de Luis Martínez Gómez. {.frances}

++Momigliano++, Árnaldo _et al._, _El conflicto entre el paganismo
y el cristianismo en el siglo +++IV+++_, Alianza, Madrid, 1989,
traducción de Marta Hernández Íñiguez. {.frances}

_Oráculos Caldeos_, Gredos, Madrid, 1991, traducción de Francisco
García Bazán. {.frances}

++Pagels++, Elein, _Los Evangelios Gnósticos_, Crítica, México,
1982, traducción de Jordi Beltrán. {.frances}

++Platón++, _Timeo_, en _Obras completas_, Aguilar 2L, Madrid,
1986, traducción de Francisco de P. Samaranch. {.frances}

_Platón_, _Fedón_, _Banquete_, _Fedro_ en _Dialogos III_, Gredos,
Madrid, 1988, traducciones de Carlos García Gual, M. Martínez
Hernández y Emilio Lledó. {.frances}

++Puech++, H. C., _En torno a la gnosis_, Taurus, Madrid, 1982,
traducción de Francisco Pérez Gutiérrez. {.frances}

---, _Historia de las religiones_, «Las Religiones en el Mundo
Mediterráneo y en el Oriente Próximo +++I+++ y +++II+++», Siglo
XXI, México, 1979, traducciones de Lorea Barruti y Alberto Cardini.
{.frances}

_Textos mágicos en papiros griegos_, Gredos, Madrid, 1987, traducciones
de José Luis Calvo Martínez y Ma. Dolores Sánchez Romero. {.frances}

++Thorndike++, Lyn, _A history of magic and experimental science_,
Columbia University Press, 5a. ed., New York, 1958. {.frances}

++Veyne++, Paul _et al._, _Historia de la vida privada_, Tomo
I «Imperio romano y antugiedad tardía», Taurus, Madrid, 1990,
traducción de Francisco Pérez Gutiérrez. {.frances}
