Paul Veyne, «El Imperio romano», en _Historia de la vida privada_,
p.13.

H.C. Puech, _En torno a la gnosis_, p. 15.

_Ibid._, p. 19.

_Ibid._, p. 20.

_Cf._ Jean Doresse, «La gnosis», en _Las religiones en el mundo
mediterráneo y en el Oriente próximo II_, pp. 49-54 y, Elein
Pagels, _Los evangelios gnósticos_, pp. 91-116.

«Evangelio de San Valentín», Cap. +++XIV+++-6-21, en _Evangelios
apócrifos_, p. 754.

«Evangelio de Tomás», 32, 19-33, citado por Elein Pagels, _Los
evangelios gnósticos_, p. 180.

H. C. Puech, _op. cit._, p. 20.

_Cf._ Jean Doresie, «El hermetismo egipciante», en _La Religiones
en el Mundo Mediterráneo y en el Oriente Próximo II_, p. 9688.

_Ibid._, pp. 118 ss.

_Fragmentos de Stobeo_, +++XIII+++-2, p. 90-91 (Las páginas corresponden
aquí, como en todas las referencias a los _Hermética_, a la edición
castellana de Miguel Angel Muñoz, _Hermes Trismegisto_, _Obras
completas_, publicada por la Biblioteca Esotérica).

_Ibid._, +++XI+++-2, p. 82.

_Idem._

_Corpus Hermeticum_, «Pormandres», +++XVI+++-7, p. 20.

_Fragmentos de Stobeo_, +++XI+++-5, p. 88.

_Corpus Hermeticum_, «Poimandres», +++IV+++-6, p. 29.

_Ibid._, +++I+++-1, p. 3.

_Ibid._, +++III+++-1, p. 25.

_Fragmentos de Stobeo_, +++IV+++-6, p. 34.

_Corpus Hermeticum_, «Poimandres», +++VIII+++-1, p. 43.

_Ibid._, +++I+++-12, p. 6.

_Ibid._, +++I+++-14, p. 7.

Festugiéres ha considerado que no obstante la apariencia de «no
caída» en el génesis hermético, la conscecuente mortalidad del
hombre, tras el enamoramiento con la naturaleza, es un castigo
a su audacia; aunque en este caso, la audacia es del propio Festugiéree
que, a toda costa, busca rastros cristianos en el hermetismo.

_Corpus Hermeticum_, «Poimandres», +++I+++-15, p. 7.

_Ibid._, +++I+++-16, p. 8.

Lyn Thorndike, _A History of Magic and Experimental Science_,
tomo I, p. 8.

_Asclepio_, 6, p. 37.

_Corpus Hermeticum_, «Poimandres» +++XI+++, 19-20, pp. 70-71.

_Ibid._, +++III+++, 3-4, p. 26.

_Asclepio_ 10, pp. 42-43.

Lactanoio, _Instituciones divinas_, +++IV+++, +++VI+++.

_Cf._ Platón, _Timeo_, 49d.

_Cf._ Platón, _Timeo_, 36c-37e.

Platón, _op. cit._, 32a.

Platón, _Lisis_, 216e.

_Ibid._, 218a.

_Ibid._, 220d.

_Ibid._, 222a.

Juliano, _La madre de los dioses_, 166b-167d.

Platón, _Timeo_, 34a.

_Cf._ Marsilio Ficino, _Teología plátonica_, pp. 240 ss.

Giordano Bruno, _Los heroicos furores_, pp. 91-92.

Platón, _Timeo_, 37e.

Cornelio E. Agrippa, _La filosofía oculta o la magia_, p. 27.
La idea de espíritu del mundo es en Agrippa equivalente al Alma
del Mundo, asimismo la quintaesencia ---de origen aristotélico
y que ya encontramos en Juliano--- se refiere al quinto cuerpo
que sin ser ninguno de los cuatro elementos da lugar al mundo
y a las correspondencias internas del mismo.

Platón, _Timeo_, 42e.

Para una descripción más detalla de cómo opera la astrología,
puede consultarse el magnífico manual de Ramón Llull, _Tratado
de astrología_, así como _La filosofía oculta o la magia_ de
Enrique Cornelio Agrippa.

Platón, _Timeo_, 92c.

_Idem._

_Idem._

Platón, _Timeo_, 73c.

Platón, _Timeo_, 85d.

_Ibid._, 88b.

Platón, _Banquete_.

_Ibid._

Platón, _Banquete_, 186c.

Marsilio Ficino, _Letters_, p. 143.

_Ibid._, p. 142.

Platón, _Timeo_, 91a.

Platón, _Banquete_, 188b.

Giordano Bruno, _Los heroicos furores_, pp. 56-57.

_Ibid._, p. 57.

Enrique Cornelio Agrippa, _La filosofía oculta y la magia_, pp.
301ss.

_Ibid._, p. 303.

Enrique Cornelio Agrippa, _op. cit._, p. 308.

Platón, _Fedro_, 246d-248c.

_Ibid._, 249c-d.

Juliana González, _Ética y libertad_, p. 87.

_Asclepio_ 8, p. 40.

Giovanni Pico della Mirandola, _Discurso sobre la dignidad del
hombre_, p. 106.

Platón, _Fedro_, 250e.

_Ibid._, 251a.

_Ibid._, 252c.

_Idem._

_Ibid._, 253a.

Platón, _Banquete_, 196c.

Platón, _Banquete_, 202e.

Platón, _op. cit._

_Ibid._, 196b-c.

Juliana González, _Ética y libertad_, p. 73.

Platón, _Banquete_, 206c.

Textos mágicos en papiros griegos.
