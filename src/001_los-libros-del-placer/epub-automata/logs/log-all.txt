Pecas: 1969.12.31-7e451d5
 Ruby: 2.6.3-p62
 Host: x86_64-pc-linux-gnu

Software bajo licencia GPLv3+: <https://gnu.org/licenses/gpl.html>.
Documentación: <http://pecas.cliteratu.re/>.

------------------------------------------------------------------

# pc-pandog
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../../base-files/pandog/pandog.rb -i /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/archivos-madre/md/todo.md -o /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/.todo.xhtml

# pc-creator
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../creator/creator.rb -o epub -c /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/archivos-madre/img/portada.jpg -i /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/archivos-madre/img    -s /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/archivos-madre/css/styles.css 

# pc-images
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../../base-files/images/images.rb -i '/home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/epub/OPS/img' --compress   

# pc-divider
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../divider/divider.rb -f /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/.todo.xhtml -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3 

# pc-notes
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../notes/notes.rb -n /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/archivos-madre/md/notas.md -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3  --reset

# pc-recreator
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../recreator/recreator.rb -d epub -y /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/meta-data.yaml 

# pc-changer
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../changer/changer.rb -e /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/epub-5e06e891.epub --version 3.0.0

# pc-analytics
$ ruby /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../../base-files/analytics/analytics.rb -f /home/nika-zhenya/Repositorios/perro-tuerto/epriani/001_los-libros-del-placer/epub-automata/epub-5e06e891.epub --json 

# epubcheck 4.0.2
$ java -jar /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../../src/alien/epubcheck/4-0-2/epubcheck.jar epub-5e06e891.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove"
         date="2016-11-29"
         name="epubcheck"
         release="4.0.2">
     <date>2019-07-08T13:35:16-05:00</date>
     <repInfo uri="epub-5e06e891.epub">
        <created>2019-07-08T13:34:48Z</created>
        <lastModified>2019-07-08T13:34:59Z</lastModified>
        <format>application/epub+zip</format>
        <version>3.0.1</version>
        <status>Well-formed</status>
        <mimeType>application/epub+zip</mimeType>
        <properties>
           <property>
              <name>CharacterCount</name>
              <values arity="Scalar" type="Long">
                 <value>244690</value>
              </values>
           </property>
           <property>
              <name>Language</name>
              <values arity="Scalar" type="String">
                 <value>es</value>
              </values>
           </property>
           <property>
              <name>Info</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Identifier</name>
                    <values arity="Scalar" type="String">
                       <value>1_0_0-5e06e8911ff54d24b86586c4b07f2a2e</value>
                    </values>
                 </property>
                 <property>
                    <name>CreationDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-07-08T13:34:48Z</value>
                    </values>
                 </property>
                 <property>
                    <name>ModDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-07-08T13:34:59Z</value>
                    </values>
                 </property>
                 <property>
                    <name>Title</name>
                    <values arity="Scalar" type="String">
                       <value>Los libros del placer</value>
                    </values>
                 </property>
                 <property>
                    <name>Creator</name>
                    <values arity="Scalar" type="String">
                       <value>Priani Saisó, Ernesto</value>
                    </values>
                 </property>
                 <property>
                    <name>Publisher</name>
                    <values arity="Scalar" type="String">
                       <value>Azul Editorial</value>
                    </values>
                 </property>
                 <property>
                    <name>Subject</name>
                    <values arity="Array" type="String">
                       <value>Ensayo</value>
                       <value>Erótico</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>Fonts</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Regular</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Italic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Bold</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya BoldItalic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>References</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Reference</name>
                    <values arity="Scalar" type="String">
                       <value>http://www.georgetown.edu</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>MediaTypes</name>
              <values arity="Array" type="String">
                 <value>application/x-dtbncx+xml</value>
                 <value>application/xhtml+xml</value>
                 <value>image/jpeg</value>
                 <value>text/css</value>
                 <value>application/vnd.ms-opentype</value>
              </values>
           </property>
        </properties>
     </repInfo>
  </jhove>

# epubcheck 3.0.1
$ java -jar /home/nika-zhenya/Repositorios/perro-tuerto/pecas/epub/automata/../../src/alien/epubcheck/3-0-1/epubcheck.jar epub-5e06e891_3-0-0.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="epubcheck" release="3.0.1" date="2013-05-10">
   <date>2019-07-08T13:35:18-05:00</date>
   <repInfo uri="epub-5e06e891_3-0-0.epub">
    <created>2019-07-08T13:34:48Z</created>
    <lastModified>2019-07-08T13:34:59Z</lastModified>
    <format>application/epub+zip</format>
    <version>3.0</version>
    <status>Well-formed</status>
    <mimeType>application/epub+zip</mimeType>
    <properties>
     <property><name>CharacterCount</name><values arity="Scalar" type="Long"><value>244690</value></values></property>
     <property><name>Language</name><values arity="Scalar" type="String"><value>es</value></values></property>
     <property><name>Info</name><values arity="List" type="Property">
      <property><name>Identifier</name><values arity="Scalar" type="String"><value>1_0_0-5e06e8911ff54d24b86586c4b07f2a2e</value></values></property>
      <property><name>CreationDate</name><values arity="Scalar" type="Date"><value>2019-07-08T13:34:48Z</value></values></property>
      <property><name>ModDate</name><values arity="Scalar" type="Date"><value>2019-07-08T13:34:59Z</value></values></property>
      <property><name>Title</name><values arity="Scalar" type="String"><value>Los libros del placer</value></values></property>
      <property><name>Creator</name><values arity="Scalar" type="String"><value>Priani Saisó, Ernesto</value></values></property>
      <property><name>Publisher</name><values arity="Scalar" type="String"><value>Azul Editorial</value></values></property>
     </values></property>
     <property><name>Fonts</name><values arity="List" type="Property">
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Regular</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Italic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Bold</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya BoldItalic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
     </values></property>
     <property><name>References</name><values arity="List" type="Property">
      <property><name>Reference</name><values arity="Scalar" type="String"><value>http://www.georgetown.edu</value></values></property>
     </values></property>
    </properties>
   </repInfo>
  </jhove>

# kindlegen
$ kindlegen epub-5e06e891.epub

  *************************************************************
   Amazon kindlegen(Linux) V2.9 build 1028-0897292 
   A command line e-book compiler 
   Copyright Amazon.com and its Affiliates 2014 
  *************************************************************
  
  Info(prcgen):I1047: Added metadata dc:Title        "Los libros del placer"
  Info(prcgen):I1047: Added metadata dc:Creator      "Priani Saisó, Ernesto"
  Info(prcgen):I1047: Added metadata dc:Publisher    "Azul Editorial"
  Info(prcgen):I1047: Added metadata fixed-layout    "false"
  Info(prcgen):I1047: Added metadata dc:Subject      "Ensayo"
  Info(prcgen):I1047: Added metadata dc:Subject      "Erótico"
  Info(prcgen):I1052: Kindle support cover images but does not support cover HTML. Hence using the cover image specified and suppressing cover HTML in content.     URL: /tmp/mobi-Woq5iS/OPS/xhtml/000-portada.xhtml
  Info(prcgen):I1002: Parsing files  0000014
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-count' in file: /tmp/mobi-Woq5iS/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-gap' in file: /tmp/mobi-Woq5iS/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-rule' in file: /tmp/mobi-Woq5iS/OPS/css/styles.css
  Info(prcgen):I1015: Building PRC file
  Info(prcgen):I1006: Resolving hyperlinks
  Info(pagemap):I8000: No Page map found in the book
  Info(prcgen):I1045: Computing UNICODE ranges used in the book
  Info(prcgen):I1046: Found UNICODE range: Basic Latin [20..7E]
  Info(prcgen):I1046: Found UNICODE range: Latin-1 Supplement [A0..FF]
  Info(prcgen):I1046: Found UNICODE range: General Punctuation - Windows 1252 [2013..2014]
  Info(prcgen):I1017: Building PRC file, record count:   0000079
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  51.40%
  Info(prcgen):I1040: The document identifier is: "Los_libros_del_placer"
  Info(prcgen):I1041: The file format version is V6
  Info(prcgen):I1031: Saving PRC file
  Info(prcgen):I1032: PRC built successfully
  Info(prcgen):I1016: Building enhanced PRC file
  Info(prcgen):I1007: Resolving mediaidlinks
  Info(prcgen):I1011: Writing mediaidlinks
  Info(prcgen):I1009: Resolving guide items
  Info(prcgen):I1017: Building PRC file, record count:   0000076
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  53.55%
  Info(prcgen):I1041: The file format version is V8
  Info(prcgen):I15000:  Approximate Standard Mobi Deliverable file size :   0001381KB
  Info(prcgen):I15001:  Approximate KF8 Deliverable file size :   0002018KB
  Info(prcgen):I1036: Mobi file built successfully
  
