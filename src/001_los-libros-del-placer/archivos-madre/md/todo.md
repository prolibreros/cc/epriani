# Epígrafe @ignore

A Pepita Saisó Sempere. \
Mi madre {.espacio-arriba3 .epigrafe}

# Introducción

## 1. _Dioses tutelares_

++El libro++ _La historia de las drogas_ de Antonio Escohotado
ofrece al lector algo más que la simple narración histórica de
aquellos hechos culturales y políticos que han dado cuerpo a
esa particular relación del hombre con las drogas. Regala, además,
el esbozo de una historia de las costumbres y de las prácticas
humanas, aquí vinculadas al consumo de fármacos, pero que permite
intuir consecuencias fuera de ese ámbitro.

Para el lector desapercibido, como lo fui yo mismo, el libro
es capaz de demoler hasta los cimientos más sólidos de las propias
creencias. El mérito radica más en mostrar que en argumentar.
En efecto, lo que con más contundencia ataca la comodidad de
nuestras convicciones no es otra cosa que el enseñar, simplemente,
que otros han hecho aquello a lo que hoy tememos. El libro tiene
el mismo efecto que el de mirar a alguien hacer algo a lo que
no nos atrevemos, y además salir intacto de la experiencia. Por
encima de la sensación de ridículo, lo que prevalece es en realidad
una certidumbre: las cosas bien pueden ser de otra manera; a
lo mejor, el temor es sólo un fantasma sin fundamento.

Lo que hay en el texto de Escohotado es otra forma de argumentar
en filosofía y, particularmente, en ética, en la que la lógica
de la argumentación se ve consolidada en la sustancia de los
hechos. No se trata, por supuesto, de que corrobore sus afirmaciones
teóricas en hechos empíricos, al modo de la ciencia, sino que
exhibe las costumbres históricas como la otra cara de nuestras
propias costumbres.

La historia y la narración histórica pueden otorgar hoy a la
reflexión filosófica y ética la visión del uso práctico de las
ideas en la percepción de la experiencia de la vida. Su encarnación,
por decirlo así, en la conducta humana.

Esto es particularmente importante en estos días aciagos de la
suspicacia y la sospecha. Juliana González ha trazado claramente
en _Ethos, destino del hombre_ los alcances de la crisis que
caracteriza a este fin de siglo.

> No se trata sólo de una crisis teórica que se produzca en
> el ámbito de la filosofía o de las ciencias humanas. Es crisis
> vital que irradia en todas direcciones e invade los diversos
> campos de la existencia.@note

Esta situación, que ha hecho entrar en crisis «la razón misma
y el sujeto en su propia interioridad», ha tenido el efecto de
hacer vana la sola argumentación teórica. De hecho, ha llegado
a poner en jaque la validez del puro ejercicio reflexivo, como
proveedor de certezas y certidumbres en todos los órdenes, pero
de manera crucial, para el propio individuo en sus convicciones
y costumbres.

En consecuencia, la incorporación de la narración histórica en
la forma en que lo ha venido haciendo la llamada historia de
las mentalidades, ofrece un nuevo cuerpo a la exposición de razones,
gracias a un principio de certidumbre distinto al del ejercicio
del puré pensar: la constancia de la experiencia del pensamiento,
su condición de uso en la práctica cotidiana.

No se trata entonces de apelar a la universalidad de la razón,
sino a su historia; a la «vida» de las ideas, como una forma
de mostrar que no constituyen ficciones o creencias acomodaticias
a conveniencia, sino expresiones de un modo de vida cuyos rasgos
generales, inevitablemente compartimos. Esto es especialmente
importante cuando hablamos en concreto de las ideas éticas.

«La historia ---ha escrito Juliana González--- tiene importe
ontológico»@note y, precisamente por ello, el que en apariencia
es un argumento más a favor de la relatividad de los valores
y de nuestras concepciones en general, se torna en una prueba
de su contrario: la universalidad de la condición humana.

Así, la historia de la ética, que devendría ella misma en ética,
sería como ha señalado Ernst Júnger, una historia _in nuce_.

> Esta última [la concepción cíclica de la filosofía de la
> historia] ha de ser completada por el conocimiento de la
> historia _in nuce_, el tema, que sufre variaciones en la
> infinita diversidad del espacio y el tiempo, es siempre
> el mismo; en ese sentido hay no sólo una historia de la
> cultura, sino también una historia de la humanidad, y esa
> historia es precisamente historia en la sustancia, historia
> _in nuce_, historia del ser humano.@note

## 2. _Los términos de la inquietud_

++El placer++ es uno de los temas de esta historia; uno, además,
que ha pasado a ser en nuestros días el centro de la discusión
ética. Gilles Lipovetsky ha definido este fin de siglo como la
época del placer.@note Proposición audaz y cierramente polémica,
que sin embargo, atina en su diagnóstico, a pesar de ser complaciente
con el modo como los hombres abordan hoy su relación con el placer.
Es decir, que no percibe que si éstos son los días del goce,
lo son no porque el placer sea solución satisfactoria para los
interrogantes de la vida, sino porque, al contrario, se ha tornado
problemático, incluso me atrevería a decir que se ha convertido
en la fuente principal de incertidumbre. Y es precisamente esto
lo que me propongo abordar aquí.

No se trata sin embargo, de discurrir acerca de la naturaleza
del placer, ni de indagar sobre el hedonismo o el epicureísmo
como doctrinas sobre el goce, ni de elaborar una historia de
las ideas filosóficas sobre el tema. En realidad, esta investigación
se propone hacer historia, necesariamente fragmentaria e incompleta,
_de los modos positivos_ a través de los cuales el hombre ha
planteado, dentro del mundo occidental, sus relaciones con el
placer.

La intención es sumar a la reflexión filosófica el espíritu de
la historia _in nuce_. Esto es, mostrar cómo los hombres han
hecho uso práctico de sus ideas sobre el placer, a través de
la elaboración del significado de la sensación placentera en
un momento dado de la historia, y que son el trasfondo de nuestra
inquietud contemporánea. Ésta pretende ser, en sentido estricto,
_una historia de la práctica ética del placer_. Una reflexión
alrededor de las variaciones que tiene el tema del placer a lo
largo de la historia. Sus implicaciones en la construcción de
la subjetividad y en la creación de horizontes éticos.

## 3. _Metodología_

¿++Cómo construir++ una historia de la práctica ética?

En primer lugar, hay que advertir que no se trata sólo de rastrear
y registrar las prácticas como tales. Los datos menudos como
las 500 libras diarias que Oscar Wilde invertía en ir al Music
Hall, almorzar en el Café Royal o en Berkeley, cenar en el Savoy
o en Title Street y tomar una copa en Willys, en compañía de
su amante Arthur Douglas pueden ser valiosos para el historiador,
pero son insuficientes para el filósofo. Es necesario, entonces,
poner ese dato en relación con la conducta del sujeto pero no
sólo, también hace falta indagar de qué forma es vista ésta por
el individuo mismo, y la forma en que es valorada como conducta
deseable a partir de los problemas, los valores y las metas que
él mismo ha adoptado para regir su vida, y que es donde de hecho,
se juega la experiencia ética.

Esto implica un problema de orden metodológico: ¿cómo definir
esta relación entre concepciones morales y problemas éticos del
sujeto en relación con su conducta? La cuestión se complica si
tenemos en cuenta que no se trata de explicar los mecanismos
que obligan a actuar de tal o cual manera; es decir, las fuerzas
represivas a las que atribuimos cierto grado de determinación
sobre la conducta, sino las razones por las que los hombres consideran
deseables ciertas conductas con la intención de transformarse.

Cuando uno trata de encontrar una explicación, por ejemplo, a
la conversión al cristianismo de paganos cultos, formados en
la tradición filosófica clásica, uno no puede apelar sólo a mecanismos
de represión externos o internos. Ello no sería suficiente para
entender una adopción libre, razonada y consciente, como la de
Antenágoras, que apela a razones de índole ética para solicitar
la aceptación de su conversión al cristianismo, aun cuando la
primera vez le fue negada. Dicho de otro modo, la ética no puede
reducirse a una exposición de las determinaciones de la acción
humana, no sólo porque ello anularía los afanes de una ética,
sino porque simplemente dejaríamos de comprender por qué los
hombres actúan como lo hacen. Seríamos, en suma, incapaces de
ver las razones que le impulsan voluntariamente hacia la conversión.

La solución a este doble problema metodológico sobre cómo definir
el vínculo entre moral, ética y conducta, entendiéndolo como
parte de un ejercicio voluntario, me vino de la lectura del segundo
y tercer volumen de la _Historia de la sexualidad_ de Michel
Foucault. En el primero, al hacer las consideraciones en torno
al replanteamiento de su trabajo, Foucault escribe:

> El proyecto era por lo tanto el de una historia de la sexualidad
> como experiencia ---si entendemos por experiencia la correlación,
> dentro de una cultura, entre campos de saber, tipos de normatividad
> y formas de subjetividad.

> Hablar así de sexualidad implicaba liberarse de un esquema
> de pensamiento que entonces era muy común: hacer de la sexualidad
> una invariable y suponer que, si toma en sus manifestaciones
> formas históricas singulares, lo hace gracias a mecanismos
> diversos de represión, a los que se encuentra expuesta sea
> cual fuere la sociedad; lo cual corresponde a sacar del campo
> histórico al deseo y al sujeto de deseo.@note

Foucault apuesta por una historia del concepto de deseo y de
las formas en que el hombre ha llegado a entenderse como sujeto
de deseo. Para ello, sin embargo, ha de abordar la cuestión de
las relaciones entre sexualidad y moral, para poder dar cuenta
de la forma en que el sujeto se reconoce precisamente como sujeto
frente a su deseo.

Al hacerlo, Foucault encuentra que hablar de moral es problemárico.
La moral es un término ambiguo que define, por igual, al conjunto
de los valores y las reglas de acción, que al comportamiento
de los individuos en relación con esas reglas. Pero además, y
esto es lo que más llama la atención, también define las diferentes
maneras de conducirse, los distintos modos como los hombres asumen
en la práctica y materializan un grupo de normas determinadas.@note
Es decir, da cuenta de que existe una amplia gama de formas como
se llevan a cabo las prescripciones, por las que el hombre no
sólo es un agente que traduce mecánicamente normas en actos,
sino que tal traducción tiene un sentido.

Foucault ejemplifica lo anterior recurriendo a la forma en que
se entiende la fidelidad matrimonial. Ser fiel puede ser, para
algunos, una decisión única y definitiva, por la cual una vez
que se ha celebrado el compromiso, la persona renuncia a cualquier
otra relación sexual. Para otros, en cambio, la fidelidad puede
ser una meta a alcanzar, en pos de la cual, las infidelidades
constituyen parte del camino. La existencia de estas diferencias
en el modo de conducirse lleva a Foucault a interrogarse en qué
consisten.@note

Uno sería lo que llama la _determinación de la sustancia ética_;
es decir, «la manera con que el individuo debe dar forma a tal
o cual parte de sí mismo como materia prima de su conducta moral».@note
Se trata del problema y la forma en que se formula, que domina
la preocupación ética de la persona y a la que se propone, con
la conducta, dar respuesta.

Otro puede basarse también en el _modo de sujeción_: «la forma
en que el individuo establece su relación con esa regla y se
reconoce como vinculado con la obligación de ponerla en obra».@note
Pese a hacer hincapié en la «obligación», Foucault toma los modos
de sujeción como estrategias que buscan poner en «práctica» aquello
que resuelve la inquietud y que conforman, al mismo tiempo, los
valores desde los cuales se juzga. El tener que acatar un mandato,
el guiarse por la necesidad, etcétera, son modos de sujeción.

Uno más es la _elaboración del trabajo ético_ «que realizamos
en nosotfos mismos y no sólo para que nuestro comportamiento
sea conforme a una regla dada sino para intentar transformarnos
nosotros mismos en sujeto moral de nuestra conducta». Es el caso
del aprendizaje, la memorización, la asimilación de conceptos
de autocontrol para regular la conducta.

Por último, otra fuente de diferencias: la _teleología_ del sujeto
moral. Es decir, los fines que articulan la conducta: felicidad,
salvación, bienestar.

De esta forma, basándose en el estudio sistemático de las diferencias
que surgen de estos cuatro elementos, Foucault se propone hacer
una historia «de la manera en que los individuos son llamados
a constituirse como sujetos de conducta moral […] Tal es lo que
podríamos llamar ---señala--- una historia de la “ética” y de
la “ascética”, entendida como historia de las formas de subjetivación
moral y de las prácticas de sí que están destinadas a asegurarla».@note

Para la elaboración de una historia de la práctica ética, la
metodología usada por Michel Foucault es un instrumento esencial,
pero también insuficiente.

En principio, Foucault se basa en textos prescriptivos para establecer
los distintos elementos que componen la subjetividad griega,
entendiendo por ésta, la forma específica con que los individuos
llegan a ser conscientes de su deseo. Ello tiene el efecto de
hacer que la subjetividad que se describe en la _Historia…_ sea
la griega, y no la particular de tal o cual individuo griego.
Dicho de otra forma, el uso que Foucault le da a la metodología
es únicamente para establecer los elementos generales de la subjetividad
compartidos por el mundo antiguo. Pero ello no implica mostrar
la dinámica de la experiencia en un sujeto particular. No alcanza,
pues, a asir la práctica de esa subjetividad, con sus errores,
complicaciones y deformaciones, que aparecen cuando esas prescripciones
toman un cuerpo específico.

En algunos pasajes de la _Historia de la sexualidad_, justo cuando
abandona el marco general para concentrarse en anécdotas particulares
como la interpretación del regaño de Isócrates a su esposa en
la _Económica_ de Jenofonte, o la explicación de los beneficios
de la masturbación en Diógenes, aparece el horizonte de la experiencia
ética singular.

Y es a ese horizonte al que quiero dirigirme. Mi intención aquí
es utilizar las nociones foucaultianas para dar cuerpo a subjerividades
particulares y, en algunos casos, muy específicas, con el fin
de verlas tal y como aparecen a la luz de la experiencia. El
modo con que intento asir la relación entre la elaboración subjetiva
---el vínculo entre cuestiones y fines éticos, y convicciones
morales--- y la conducta, es apelando a textos literarios, no
tanto en sustitución sino como alternativa a las obras prescriptivas,
en el supuesto de que en ellos puedan encontrarse estas dos aristas
y el nudo que las une. Lo que equivale a afirmar que ciertas
obras literarias, si no es que su totalidad, comunican las convicciones
de los individuos y cómo se experimentan en su uso cotidiano.

El texto literario es en principio una elaboración subjetiva
y, por ello mismo, una de las expresiones de la subjetividad.
Esta condición lo convierte, de hecho, en una de las fuentes
básicas para el estudio que me propongo. Su importancia radica
en que es producto y refleja una subjetividad singular en operación
dentro del vértigo de la existencia. Esto es lo que lo distingue
de los textos prescriptivos que, justamente, abstraen la experiencia
vital y es ésta la que, precisamente, buscamos analizar aquí.

Aunque todo texto literario es un producto de la subjetividad,
hay un grupo de textos que se proponen la transmisión fidedigna
de una experiencia real, aunque se trate sólo de una experiencia
posible. Estos textos, que van desde la poesía erótica romana
a las memorias y los diarios personales del siglo +++XIX+++,
en ocasiones aluden a la experiencia real del poeta o el narrador,
pero las más de las veces, incluso en las obras con pretensiones
más objetivas, la fuente de la narración o el verso es la ficción.
Porque el poeta no necesita haber vivido el adulterio para hablar
de una experiencia adúltera. Al contrario, para lo que busco
aquí, la riqueza de la literatura no consiste en decir la verdad
sino en mostrar los rasgos generales de la práctica de la subjetividad
de una época. De este modo, si bien las obras literarias no nos
permiten conocer qué sentía Ovidio al seducir a las mujeres,
sí nos permiten saber dos cosas: sobre qué bases elaboraban él
y muchos de sus contemporáneos la percepción de su experiencia,
y qué prácticas eran las que les caracterizaban.

Por supuesto, nunca hay que confiarse cuando se trata de poetas.
Por ello es necesario corroborar ambas cosas por otros medios
para alcanzar cierto grado de certidumbre. Pero no tenemos por
qué temer aquí tanto a la ficción y a las «distorsiones» de la
realidad, cuando lo crucial es la forma en que los hechos se
entrelazan con la subjetividad, y dentro de ella, también con
la fantasía. Algunos de los textos que analizo aquí no remiten
necesariamente a realidades o formas tangibles del comportamiento;
es decir, no son una reflexión sobre una experiencia posible
o verosímil, sino fantástica. En otros, la intención del autor
es la de referirse a hechos supuestamente experimentados por
él mismo. La diferencia de unos y otros es parte de la diferencia
en la subjetividad que se transmite al estilo, pero que no resta
valor sino que eventualmente lo incrementa. Stevenson pensaba
que sus obras fantásticas constituían la mejor forma de plantear
los dilemas reales de su tiempo, el autor anónimo de _Mi vida
secreta_ creía, en cambio hacer una narración casi científica.

Los escritos de los que voy a ocuparme los elegí a partir de
dos criterios. El primero obedece al tema mismo de la investigación:
debían representar un modo de vida construida a partir de una
relación positiva con el placer. El segundo, a la rura que seguiré:
debían transparentar los elementos centrales de una subjetividad.
El número de las obras a tratar hubiera sido enorme pero en la
selección final pesó, por un lado, la importancia histórica de
los textos, como representativos de una época y, por ende, de
una subjetividad compartida en un momento dado. Del otro lado
de la balanza y como en toda selección de textos lirerarios,
está presente la subjerividad de quien la hace. Las obras analizadas
en esta investigación, también me gustan.

## 4. _¿Qué placeres?_

++Hablar del placer++ en la forma que hasta aquí ha sido expuesto,
no puede desarrollarse en abstracto. Al contrario, al tratarse
de prácticas tiene que hacerse referencia a placeres concretos.
La idea inicial fue partir de los tres placeres físicos esenciales:
los de la mesa, el vino y la alcoba, que a diferencia de los
placeres no físicos, han sido centro de una controversia permanente.
Haber tratado de llevar a cabo esto, hubiera supuesto un trabajo
inmenso, que rebasaría por completo las dimensiones de esta investigación.
Por ello, decidí restringirme al placer de la alcoba, aun a riesgo
de sólo glosar el trabajo de Foucault.

La razón de esta restricción, sin embargo, es justamente la que
marca las distancias entre la _Historia de la sexualidad_ y _El
libro del placer_. El eje de la propia investigación me fue llevando
hacia la idea de que los conceptos utilizados por Foucault no
describen uno de los lados de la práctica ética que es el de
la traducción de los criterios de las convicciones morales en
percepciones sensibles. Me explico.

A partir de una intención que buscaba cuestionar las ideas de
_autenticidad_ e _intensidad_ relativas a la sexualidad en el
siglo +++XX+++, me encontré con que el problema fundamental,
más allá de estas ideas, es que tienen su fundamento en un modo
de percibir el placer sexual o, para ser más exactos, de vivir
y practicar el goce. Sobre esta base, descubrí que mi investigación
rastreaba los vínculos entre sensibilidad, subjetividad y conducta,
a través del hilo conductor del tránsito que llevaba de las ideas
sobre el placer en Epicuro hasta las tesis sobre la autenticidad
en Freud.

Al final, la investigación terminó por ser, pese a sus intenciones
originarias, un análisis histórico del vínculo entre la reflexión
ética y producción de sensibilidades, y su enlace en formas de
comportamiento, como manifestaciones estéticas de las convicciones
morales y los rasgos de la percepción, en la construcción de
las ideas de autenticidad e intensidad.

Con esa dirección, me he propuesto una historia de las prácticas
éticas en torno al placer, que ha de ser necesariamente fragmentaria.
No pretendo agotar aquí todos los tiempos y momentos de la historia,
ni capturar todos los modos de relacionarse positivamente con
el placer, sino seguir algunas de las constantes, en la cultura
occidental, que acabarán por dar cuerpo a los esquemas de intensidad
y autenticidad, que en última instancia, sirven para dejar ver
la forma en que la ética se articula con la sensibilidad y la
conducta.

He dividido mi trabajo en dos partes, tituladas «El alma» y «El
cuerpo», para referir en ellas las prácticas éticas que tienen
como sustento, de manera correspondiente, la percepción del placer
como pasión del alma, y el placer como sensación del cuerpo.

A su vez, cada una de estas partes tiene una subdivisión. La
primera inicia con la «Subjetividad epicúrea», punto de arranque
de esta historia, en la que se examina el epicureísmo no como
doctrina, sino como construcción de una subjetividad, a partir
de la cual, y a través de dar funciones nuevas a sus elementos,
permanecerá siempre como trasfondo a nuevas formas de relacionarse
consigo mismo y con el placer.

El segundo capítulo, «Amores y artes», aborda la forma en que
dentro de las obras de Ovidio ---_Amores, Remedios del amor_
y _Arte de amar_---, la subjetividad epicúrea se vincula con
una práctica específica de relación con el placer sexual, que
viene a ser el reverso de las prácticas de la autenticidad y
la intensidad.

El rercero y úlrimo capítulo de esta parte, «La rosa», es un
examen de la doctrina del amor natural expuesta por Jean de Meun
en el _Roman de la rose_ en el siglo +++XIII+++, como constitutiva
de una nueva subjetividad. Nueva, claro, con relación a Ovidio
y Epicuro, y en la que se preludian una forma distinta de conciliación
consigo mismo a través de la armonización de la conducta humana
con «natura».

«El cuerpo» se subdivide en dos capítulos. En el primero, «El
peligro de las emociones», se rastrea la «invención» de la sensibilidad
corporal en el siglo +++XVII+++ y los problemas que este hecho
plantea para la aparición de nuevas subjetividades, primero a
través de Diderot, en el _Coloquio entre Diderot y D'Alembert_,
y después con _Las amistades peligrosas_ de Choderlos de Laclos
y las _Memorias_ de Giacomo Casanova. Aquí importa, sobre todo,
la aparición de la noción de intensidad como atributo del placer
corporal, como la conservación de la idea de mandato vinculada
a ésta.

En el segundo, «¿Qué hace usted Mr. Hyde?» se vincula un tópico
literario, la doble personalidad presente en los relatos de _El
extraño caso del doctor Jekyll y Mr. Hyde_, de R.L. Stevenson
y _El retrato de Dorian Gray_ de Oscar Wilde, con el relato que
un par de personajes decimonónicos hacen de su propia vida: Walther,
protagonista del texto anónimo _Mi vida secreta_, un libro de
memorias eróticas razonablemente auténtico y la epístola de Oscar
Wilde, _In carceres et vinculis_. La intención aquí es rastrear
cómo la doble personalidad no sólo es una fantasía literaria
sino también una estrategia subjetiva dominante en la época victoriana
y, por ende, enlazada con una práctica concreta del disfrute
sexual. Al mismo tiempo, el capítulo se orienta hacia la crisis
de esta subjetividad y la aparición de la autenticidad como alternativa
subjetiva.

# Primera parte. \
  El alma

# I. La subjetividad Epicúrea

## 1. _Sentir placer_

++¿Qué ha sentido++ el hombre singular, a lo largo de la historia,
cuando ha sentido placer? La pregunta puede parecer retórica,
pero no lo es. El placer no es algo que se percibe y nada más.
No puede reducirse, en el hombre, a la calidad de estímulo. La
pregunta interroga por la experiencia del placer; es decir, por
los «movimientos del alma» que suceden al goce. Se refiere, pues,
a las emociones; también al sentido que se busca o se encuentra
en esas-emociones; pero, sobre todo, lo hace sobre las preguntas,
las cuestiones que sobre sí mismo se hace el hombre frente al
placer.

Aristóteles, en la _Ética nicomaquea_ define el placer como una
pasión del alma. Nada más lejos de la vacua idea de estímulo.
¿Por qué pasión del alma y no afectación del cuerpo? En la elección
de uno, en lugar del otro está ya parte de la respuesta.

El placer es un asunto sobre todo del alma, a pesar de ser un
hecho del cuerpo, porque acepta los extremos ---intemperancia
y abstinencia--- que ponen en duda el equilibrio del alma misma.
Es decir, el placer y no cualquier placer, sino aquellos que
tienen por origen el gusto y el tacto (Aristóteles excluye los
placeres del olfato, la vista y el oído, así como los placeres
intelectuales, de todo posible exceso)@note son percibidos como
problemáticos en la medida en que permiten conductas diferenciadas,
relaciones distintas de los hombres con el hecho del placer.
Es decir que pone en juego, además de la salud del cuerpo, la
conducta, la manera en que el alma le da forma al cuerpo; la
forma, en suma, en que se es.

La experiencia del placer es, así, problemática y se plantea,
al menos en Aristóteles, aunque es extensiva a la comunidad del
pensamiento helénico, en términos de gasto. Lo que preocupa es
el exceso tanto como la falta, que constituyen, en relación con
el término medio aristotélico, los extremos indeseables del placer
en el orden de alcanzar una vida feliz.

Epicuro comparte con Aristóteles la idea de que el placer es
una pasión del alma, la experiencia problemática del placer y
la aspiración, última, a la felicidad. Sin embargo, introduce
una diferencia capital: el problema del placer no se plantea
en términos de exceso o falta. La idea central en Epicuro es
que el placer no es susceptible de acumulación. «El placer no
crece en la carne cuando de una vez se aleja el dolor causado
por alguna deficiencia, sólo varía de color» (Epicuro, «Máximas
capitales», p. 18, en _Máximas para una vida feliz_, p. 22).

La tesis epicúrea de que todo placer es un bien se sostiene en
una forma específica de comprender el placer, según la cual el
placer no puede crecer ---ni en duración ni en intensidad---
una vez que se percibe. Hay matices, coloración, pero es estrictamente
el mismo. Indistinguible incluso, porque: «Si, uno a uno, cada
placer se condensase y lo hiciere tanto en su duración como en
su localización en la totalidad de los átomos o en las partes
dominantes de nuestra naturaleza, entonces los placeres no podrían
distinguirse unos de otros» (Epicuro, «Máximas capitales», p.
9, en _Máximas para una vida feliz_, p. 19).

No hay diferencia, no hay posibilidad de valoración diferenciada,
no hay posibilidad de fundar una moral o una reflexión ética
de los placeres. Epicuro concibe el placer como el efecto único
de la supresión del dolor, producto de una necesidad no satisfecha.
En sentido estricto: «El límite de la magnitud de los placeres
es la anulación de todo dolor, físico o moral. Allí donde hay
placer, en tanto que dure, no existe uno u otro dolor o la mezcla
de ambos» (Epicuro, «Máximas capitales», p. 3, en _Máximas para
una vida feliz_, p. 18).

El gusto por el placer no puede ser problematizado ni en rérminos
de gasto ni éticos. Poco importa con qué satisfacemos el hambre
ni qué tanto, una vez satisfecha, podemos seguir comiendo. Logrado
el placer, éste no se modifica ni por lo uno ni por lo otro.
El sabio sabe que nada variará la sensación de placer y que por
ello resulta superfluo e inútil tanto el volumen como el tipo,
lo mismo si hablamos de satisfacer la sed, el hambre o el deseo
sexual. Por ello, escribe Epicuro, «de los alimentos escoge [el
sabio] el más agradable y no el más copioso» (Epicuro, «Carta
a Meneceo», p.18, en _Máximas para una vida feliz_, p. 10).

El problema para Epicuro se plantea en otro orden, en el de la
satisfacción y del deseo. Es ahí, y no en el hecho mismo de la
percepción del placer, donde se pone en juego la tranquilidad
del alma. El problema de quienes se entregan al frenesí de los
placeres disolutos no son los placeres en sí mismos, y no lo
son realmente porque no satisfacen aquella necesidad que se pretende
que satisfagan. Por ello escribe Epicuro a Meneceo:

> Si las cosas que pueden producir placeres a los disolutos
> cubriesen los miedos de sus mentes respecto a los fenómenos
> celestes, la muerte y los sufrimientos y, aún más, les enseñasen
> _el límite de los deseos_, no tendríamos qué censurarles,
> porque estarían plenos de todo rtipo de placeres y no poseerían
> ninguna clase de dolor, es decir, no poseerían lo que es el
> mal [Epicuro, «Máximas capitales», p. 10, en _Máximas para
> una vida feliz_, p. 20].

El límite no ha de buscarse en los placeres, ni en lo que se
refiere a la cantidad, la forma o la intensidad (que nada agregan
al placer), ni en lo que se refiere a su frecuencia, sino a los
deseos. Es entre éstos, y no entre los placeres, donde hay diferencia:
«Unos son físicos, otros injustificados o vanos. De los físicos,
unos son indispensables, el resto sólo físicos». Todavía dentro
de los indispensables hay «algunos necesarios para la felicidad,
otros para el bienestar del cuerpo, y unos terceros para la vida
misma» (Epicuro, «Carta a Meneceo», en _Máximas para una vida
feliz_, p. 11).

No está de más detenerse en esta caracterización que hace Epicuro
de los deseos, pues de éstos son sólo los deseos físicos aquellos
que pueden satisfacerse y entre los cuales encontramos que es
posible establecer una jerarquía que los distingue. La «bondad»
del deseo, su legitimidad, está atada a la percepción física
del placer y a que esta percepción está enlazada a la consecución
de un fin: felicidad, bienestar del cuerpo, mantenimiento de
la vida.

No hay en Epicuro ninguna distinción que ubique la obtención
de los fines en un ámbito distinto al del cuerpo físico. El equilibrio
del alma, su tranquilidad, la felicidad, depende de la percepción
del placer no sólo a modo de satisfacción física y mantenimiento
del orden del cuerpo y de la vida, sino como constancia de una
vida autónoma, libre de temores y de miedos.

El problema, por el contrario, es la persecución de deseos vanos
o de perder el tiempo en deseos como los de los libertinos, que
son, en realidad, fruto del miedo. La ética epicúrea se basa
en la distinción entre deseos en función de su efectiva satisfacción,
como en su ordenamiento a un fin. Lo que el epicureísmo propone
es una ética de los deseos, una reflexión sobre la naturaleza
de aquello que deseamos, lo cual propicia unos criterios de selección
que distingue, jerarquiza y valora los deseos. La meta es el
«equilibrio» del alma que no depende del gasto o ahorro físico,
sino de la cuota de dolor o de placer que se obriene de cada
ocurrencia vital. El lugar central, el mayor bien para Epicuro,
no es el placer sino la prudencia, que es lo que permite «la
sobria mesura que tanto investiga las causas de cada elección
y rechaza cuanto destierra de nosotros las ilusiones que generan
una enorme confusión para el alma» (Epicuro, «Carta a Meneceo»,
en _Máximas para una vida feliz_, p. 14).

![_Ménade acariciada por un sátiro_. Villa de L. Caecillus, Pompeya, siglo +++I+++ d.C.](../img/img1.jpg)

La prudencia es el bien mayor porque es sobre ella que recae
la elección de los deseos y el combate a las ilusiones. Es la
guía que, basándose en las sensaciones, determina la prudencia
de cada elección, tanto como garante de la tranquilidad del alma
como, en palabras de Emilio Lledó, contrapeso y continuidad a
la fugacidad de la sensación y el placer.@note

> La prudencia es el principio de todas estas cosas y el mayor
> bien. De ahí que sea más valiosa aun que la filosofía ya que
> de ella se generan todas las demás virtudes, porque nos enseña
> que no existe una vida feliz sin juicio, sin bondad y sin
> justicia, ni una vida juiciosa, buena y justa sin felicidad.
> Pues las virrudes nacen de una vida dichosa y el vivir con
> dicha es inseparable de ellas [Epicuro, «Carta a Meneceo»,
> p. 18, en _Máximas para una vida feliz_, pp. l4-15).

Hay un principio de realismo en Epicuro ---heredado de Aristóteles---
por el que es la evidencia de la sensación, la percepción efectiva
de placer y dolor, la que sirve de sustento para la prudencia.
Lo que inquieta al alma no son las sensaciones sino el confundirlas
con lo que imaginamos, haciendo aparecer como deseos, sólo fantasmas.
De ahí la necesidad de mantener en la sensación, en el placer
y el dolor, el principio para guiar la existencia. De ahí la
necesidad de un juicio firme y sostenido:

> Si rechazas complertamente cualquier sensación y no distingues
> lo ilusorio de lo que permanece, lo que ya es presente en
> la sensación y los sentimientos y en cada acto imaginativo
> de la mente, desordenarás también las demás sensaciones con
> tu vacua opinión, hasta el extremo de llegar a repudiar toda
> capacidad de juicio. Pero si estableces como seguro todo cuanto
> permanece en tus ilusorios pensamientos y lo que no puede
> hallar testimonio, no evitarás el error. De manera que en
> cualquier interpretación conservarás una duda total entre
> lo que es verdadero y lo que no lo es [Epicuro, «Máximas capitales»,
> p. 24, en _Máximas para una vida feliz_, p. 24].

La obtención de una vida feliz depende, pues, de la prudencia
que distingue entre deseos vanos y verdaderos, cuyo resultado
es la satisfacción efectiva, y su constatación, la sensación
de placer. Una vida feliz, en consecuencia, no es la que modera
los placeres, sino la que reconoce deseos y desecha vanidades
y temores ilusorios; la que se atiene a lo que se percibe y juzga
a partir de la sensación; la que certifica en el placer la certeza
del bien, que se revela, por lo demás, como tranquilidad del
alma, sosiego y deseo de vivir.

Hasta aquí la singularidad de la doctrina epicúrea nos aporta
una forma distinta de abordar el asunto de los placeres sensibles
frente al sistema aristotélico, aun cuando, en una visión de
conjunto, parezca no alejarse tanto de ésta.

![Diferencias y similitudes entre Aristóteles y Epicuro](../img/cuadro1.jpg)

Sin embargo, la doctrina epicúrea puede ser formulada siguiendo
las directrices señaladas por Michel Foucault en la _Historia
de la sexualidad_, como una estrategia de construcción del sujeto;
es decir, una subjetividad donde es todavía más palpable que
su distancia frente al aristotelismo que depende de la conformación
de una sensibilidad y de unas prácticas distintas, aunque iguales
en su base y en sus fines.

## 2. _La construcción de la subjetividad epicúrea_

++Para Michel Foucault++ las _aphrodisia_, los actos de Afrodita,
son actos, gestos, contactos que buscan cierta forma de placer
y en las que se manifiesta el vínculo entre acto, deseo y placer,
cuya relación constiruirá el eje de la experiencia ética griega
alrededor de la sexualidad. Los términos en que, según Foucault,
esto se formula no es: «¿qué deseos, qué actos, qué placeres?,
sino: ¿con qué fuerza nos dejamos llevar “por los placeres y
deseos”?» y agrega: «La ontología a la que se refiere esta ética
del comportamiento sexual, por lo menos en su forma general,
no es una ontología de la carencia y del deseo; no es la de una
naturaleza que fija la norma de los actos, es la de una fuerza
que asocia entre sí actos, placeres y deseos».@note

El epicureísmo, para Foucault, responderá también a este mismo
esquema de problematización ética, en la medida en que pertenece
a la cultura helénica.

Sin embargo, la experiencia de las _aphrodisia_, según se define
en la _Historia de la sexualidad_, consiste en problematizar
la posibilidad de mesurar el impulso, la fuerza, con que se busca
el placer, a través de los actos, para satisfacer los deseos.

Pero en Epicuro no encontramos una respuesta a la pregunta ¿con
qué fuerza? y no remite a una ontología de esa fuerza. En primer
lugar, porque Epicuro no considera ---al contrario de la tradición
clásica helénica--- que los deseos sean infinitos: «No es insaciable
el estómago, como la mayoría opina, sino la falsa opinión acerca
de su ilimitada voracidad» (Epicuro, «Exhortaciones», p. 26,
en _Máximas para una vida feliz_, p. 35).

Y, en segundo lugar, porque en Epicuro lo que encontramos es
una respuesta a la pregunta ¿qué deseos y con qué fin? Es decir,
la ética de Epicuro se basa en una ontología del deseo, en la
que éstos son realidad, es decir, sustancia, en la medida en
que son físicos y, por tanto, objeto de una satisfacción también
física. De esta manera, la percepción del placer como eliminación
del dolor, sólo prueba que se satisface un deseo real.

Esta diferencia es crucial para entender hasta qué punto Epicuro
enuncia una estrategia substancialmente distinta de la que caracteriza
el pensamiento ático clásico. De hecho, la primera gran distancia
entre la subjetividad que describe Foucault para el mundo griego
y la subjetividad epicúrea, radica en la diferencia en cuanto
a la «materia de la sustancia ética». No son las aphrodisia sino
el dolor, aquello que requiere nuestra atención. En realidad,
los actos placenteros, las aphrodisia mismas, ya no son materia
de preocupación ética, lo son los deseos vanos o irreales, es
decir aquellos que no son físicos.@note Así, la pregunta que
se hace Epicuro no es «¿con qué fuerza me dejo llevar por el
desco?», sino «¿por cuáles deseos me dejo llevar?».

Sin embargo, un punto en común entre el epicureísmo y la subjetividad
ática es la estrategia de la necesidad. De esta última, dice
Foucault: «permite un equilibrio en la dinámica del placer y
del deseo; le impide “desbocarse” y caer en el exceso al fijarle
como límite interno la satisfacción de una necesidad, y evita
que esta fuerza natural se resuelva y usurpe un lugar que no
es el suyo: pues no concede más que lo que, necesario al cuerpo,
la naturaleza quiere, y nada más».@note

Pero si este principio rector tiende a moderar, al «conjurar
la intemperancia» dentro del pensamiento ático, en Epicuro se
refuncionaliza. No es ya un mecanismo para impedir la desmesura
en términos de gasto, sino un instrumento para distinguir aquellos
deseos reales de aquellos ficticios.

La necesidad es la estrategia para vincular deseos y satisfacción;
pues necesarios serán aquellos deseos cuya satisfacción sea físicamente
perceptible como goce. En este sentido, la necesidad establece
un primer límite a los deseos, haciendo que no cualquiera, sino
sólo aquellos que sean reales, capaces de engendrar un bien,
sean atendidos.

Á esta estrategia de la necesidad ha de añadirse otra: la que
interroga acerca de la jerarquía del deseo. En efecto, no basta
con que el deseo sea real, sino que adicionalmente ha de responder
a cuestiones tales como: ¿qué tan necesario es para vivir, para
mi salud, para mi felicidad? De la respuesta que se dé a estas
preguntas dependerá la ponderación de los deseos.

El sentido que Foucault le da al _kairos_ o momento oportuno,
en el pensamiento griego, como regulador de la conveniencia de
los placeres, en Epicuro lo ocupa esta estrategia de la jerarquía
del deseo. En este sentido, no es la ocasión sino el objeto de
la necesidad ---para la vida, la salud, la felicidad--- lo que
determina la importancia de los placeres. No hay, pues, un «de
qué modo» o un «a qué hora» sino un «para qué».

Epicuro establece la forma del trabajo ético rambién en términos
distintos de como se da en el pensamiento griego clásico. Para
este último, la relación del hombre con su deseo es de carácter
agonístico, el hombre lucha contra sí mismo cada vez que se propone
sujetar la «fuerza» con que se entrega al deseo. Para Epicuro,
en cambio, el eje del trabajo ético es la supresión del temor
a la muerte. Es éste el que, por una parte, induce a los libertinos
a derrochar inútilmente su vida en placeres que no resuelven
ese temor, y que, por otra, impide al hombre darse cuenta de
la plenitud que, por sí misma, encierra la existencia.

Las «Máximas capitales» de Epicuro, la propia «Carta a Meneceo»,
no tienen otra orientación que la de establecer una certeza:
la muerte carece de significado en la vida:

> Es necio quien afirma que teme a la muerte, no porque su llegada
> produzca dolor, sino porque el tiempo que tarde en aparecer
> lo origina, pues, si la presencia de una cosa no nos causa
> perturbación, en absoluto su espera nos provoca tristeza [Epicuro,
> «Carta a Meneceo», en _Máximas para una vida_ feliz, p. 9].

La preparación para el disfrute de los placeres pasa, así, por
la eliminación del temor a la muerte. No hay modo de evaluar
la realidad del deseo y de establecer una jerarquía de los placeres
mientras persista ese temor, ante todo porque deseos y placeres
sólo tienen sentido en tanto remiten a la vida y son juzgados
a partir de ella. Se satisfacen y son percibidos como placenteros
en tanto que están presentes y no en la medida en que remiten
al ocaso. De este modo, la eliminación del temor a la muerte
se constituye en el verdadero trabajo sobre uno mismo, que debe
realizar el epicúreo.

![_Amantes en la cama_. Villa de Vetii, Pompeya, siglo +++I+++ d.C.](../img/img2.jpg)

Por último, en el orden de las finalidades, aparecen la autonomía,
la prudencia y el conocimiento de sí. Condiciones todas ellas,
pero particularmente la prudencia, de ser feliz. Las estrategias
de la necesidad y de la jerarquía del deseo, como el trabajo
de eliminar el miedo a la muerte, han de resultar en la autonomía;
es decir, en no depender de los deseos ficticios o de temores
infundados. Autonomía que es, en última instancia, producto del
conocimiento de sí. El que resulta de conocer la naturaleza de
los deseos propios y de distinguir aquellos que son convenientes
para la vida, la salud o la felicidad. Tal es el contenido de
una conducta dominada por la prudencia epicúrea.

Pero hasta qué punto el epicureísmo establece las bases para
la construcción de una subjetividad distinta de la que domina
el pensamiento clásico griego, pues los elementos que definen
la valoración de los actos tienen un sentido de todo punto distinto.
En efecto, no supone una conducta en extremo diferente a la aristorélica
o la estoica, por citar la influencia predominante y la doctrina
heredera. Pero aun cuando tuvieran hábitos idénticos, tendrían
una significación distinta y serían juzgados a partir de premisas
enteramente diferentes por cada una. De esta forma, aunque similares
en términos prácticos, la expresión de su sentido, las «maneras»
con que se realizan serían absoluramente distintas.

Y es precisamente la cuestión del sentido y las maneras lo que
nos interesa plantear ahora aquí. Pero antes de avanzar por ese
camino, es necesario dar un rodeo previo.

De las obras que disponemos de Epicuro, en ninguna se muestra
una preocupación diferenciada entre los placeres de la alcoba,
y aquellos de la mesa y la copa. Hay que suponer, siguiendo aquí
a Foucault nuevamente, que Epicuro comparte con el resto del
mundo griego la igualdad entre estos placeres. A la luz de esto,
la reflexión que sobre el placer sexual lleva a cabo Lucrecio,
heredero latino de Epicuro, en _Sobre la naturaleza de las cosas_,
nos lleva al camino que buscamos.

## 3. _El matiz lucreciano_

++En el libro IV++ de _Sobre la naturaleza de las cosas_ Lucrecio
establece que la diferencia entre los deseos de comer y beber,
y el deseo de fornicar, radica en que los alimentos y los líquidos
se asimilan al cuerpo y fijan un límite a la voracidad del estómago
y de la garganta. Fornicar, en cambio, antes de ser un proceso
que asimila, es uno que vierte y vacía. En la realización del
coito, el cuerpo nunca absorbe al del otro; es, en realidad,
uno mismo el que fluye y ahí no hay otro límite que la muerte
(cf. Lucrecio, _Sobre la naturaleza de las cosas_, vv. 10201090).

La razón de la diferencia es formal y parte de un principio físico
masculino que revela que la moral de Lucrecio es una moral de
varones, pues en tanto que los alimentos se ingieren y asimilan
dentro del cuerpo, durante el coito lo que ocurre es que se expulsa
semen. Pero de manera mucho más relevante, la diferencia tiene
fundamento en la ontología del deseo.

Lucrecio comparte con Epicuro que el placer es un bien cuando
es producto de la satisfacción de un deseo físico e indispensable
bien para la vida, el bienestar del cuerpo o la felicidad. Así,
el placer es un bien no cuando resulta de satisfacer el hambre
o la sed y, en esa medida, la calidad como la cantidad de los
alimentos es irrelevante. Al contrario, un error frecuente consiste
en creer que tal o cual alimento o bebida es el que satisface,
porque ahí se confunde la naturaleza del deseo real con la de
un deseo ficticio, tomando la imagen del objeto por sus cualidades.

Pero si la experiencia cotidiana muestra la verdad de esta afirmación
al ser múltiples y variados los alimentos y bebidas que satisfacen
al cuerpo, no parece ocurrir lo mismo en materia de amor. Ahí
la imagen, supone Lucrecio, se asimila de tal modo a las cualidades
del objeto que se confunden, haciendo imposible la satisfacción
del deseo.

Para Lucrecio, todas las cosas, pero particularmente los seres
animados, emiten efluvios que flotan en el aire y que son captados
por las membranas del cuerpo humano ---nariz, boca, oídos, ojos---
que al ser menos compactas en sus estructuras, permiten que penetren
en el cuerpo produciendo las sensaciones (cf. Lucrecio, _Sobre
la naturaleza de las cosas_ III, vv. 3046). Pero esos efluvios,
a los que Lucrecio llama simulacros (_simulacra_), tienen la
particularidad de contener algo que permite evocar la imagen
del objeto del cual provienen:

> Digo, pues, que de las cosas las efigies y tenues figuras
> / de las cosas, de lo sumo del cuerpo de las cosas, se envían,
> / las cuales deben como membranas o corteza nombrarse, / porque
> la imagen de ellas lleva símil la traza y la forma / del cuerpo
> de cualquier cosa de que se diga vaga escapada [Lucrecio,
> _Sobre la naturaleza de las cosas_ IV, vv. 46-50].

Son voces, olores, imágenes que flotan en el aire y que comparecen
ante los sentidos de manera ordenada y cotidiana, pero también
en forma arbitraria. Por ello, según Lucrecio, no sólo percibimos
nuestro entorno tal como es, sino que eventualmente escuchamos
voces, vemos espejismos en mitad del desierto o, cuando chocan
en conjunto, formamos las imágenes de los centauros u otras fantasías
y monstruos.

La voluntad responde, y aquí Lucrecio sigue la herencia helénica,
si no se ha previsto lo que quiere, si no hay una imagen de lo
que desea. Pero ese querer, en _De rerum natura_, siempre va
precedido por una imagen, el contenido de un simulacro (cf. Lucrecio,
_Sobre la naturaleza de las cosas_ IV, vv. 883-884). Por ello:

> […] en quien, primero, el fervor de la edad se insinúa / cuando
> el mismo día, maduro, creó en los miembros el semen / encuentran
> simulacros de cualquier cuerpo, por fuera, / nuncios de preclaro
> rostro y de un bello color, / que agita, irritante, los sitios
> de mucho semen / así a menudo, como concluidas todas las cosas,
> turgentes, / viertan ingentes olas de líquido y manche su
> veste [Lucrecio, _Sobre la naturaleza de las cosas_ IV, vv.
> 1029-1030].

El primer oleaje de semen tiene como origen una imagen a la que
quedará unido y será ésa la que despertará siempre el deseo.@note
En la base de la experiencia erótica, mucho antes de que tenga
lugar el coito, el deseo sexual aparece unido a un simulacro
y tiene los rasgos de ficticio. «La esperanza ---dice Lucrecio---
está en eso: que del mismo cuerpo de donde / es del ardor origen,
también pueda extinguirse la flama» (Lucrecio, _Sobre la naturaleza
de las cosas_ IV, vv. 1086-1087). Pero la esperanza es vana.
«Cuando la codicia reunida en los nervios se expulsa / del ardor
violento se hace una parva pausa un instante; / de allí regrese
la misma rabia y el furor aquel vuelve» (Lucrecio, _Sobre la
naturaleza de las cosas_ IV, vv. 11151117).

No es del cuerpo, que despierta el ardor, de donde se obtiene
satisfacción, aunque así lo anuncie el deseo. En la confusión,
al esperar llenarse con el cuerpo del otro, y que en realidad
sólo admiramos como imagen, se acaban las fuerzas, languidece
el deber, y se enferma, al no recibir nada a cambio que satisfaga
el deseo.

Tal es y no otro, el problema del deseo sexual para Lucrecio:
la confusión, en la raíz misma del deseo, entre lo que éste desea
y la imagen que lo despierta. La propia formulación del dilema
sigue, ya, el modo de pensar epicúreo, al plantearse en rérminos
de la confusión entre un deseo necesario ---el deseo sexual mismo---
y otro ficricio ---el deseo de tal o cual cuerpo específico.
La forma de enfrentarlo, sin embargo, es singular.

En primer lugar, Lucrecio propone distinguir entre aquello que
satisface un deseo físico e indispensable para el equilibrio
armónico de la vida, completar el coito con regularidad y excitado,
y lo añadido a esto: la imagen o conjunción de imágenes que provocan
el ardor pero no son objeto de satisfacción cuando uno consuma
el acto sexual.

La forma en que, desde la subjetividad epicúrea esto puede tener
lugar es el reconocimiento de la satisfacción a la que conduce
todo acto sexual. Por ello Lucrecio recomienda: «en cualesquier
cuerpos, arrojar el humor recogido, / y no retenerlo una vez
vuelto del amor de uno solo, / y conservar para sí la cuita y
el cierto dolor» (Lucrecio, _Sobre la naturaleza de las cosas_
IV, vv. 1052-1167).

La práctica que se sugiere aquí es la del abandono de la imagen
(amor a uno solo), con su dosis implícita de dolor, para poder
atender el deseo real y necesario, que no distingue cuerpos,
pero que es el único que puede ser satisfecho físicamente. Lo
que está en juego aquí, de nueva cuenta, es la ontología del
deseo. Sin embargo, en el terreno de la conducta, esto se traduce
como «indiferencia» del individuo frente a las formas que despiertan
el deseo, pero no frente a éste. Una imagen que recuerda la idea
del «antieros» platónico, tal y como aparece en _Fedro_.@note
ese amor que ha superado ya la etapa del ardor inicial y que
ahora expresa el control del sujeto sobre sí mismo.

No es tarea fácil. Lucrecio recalca que para alcanzar tal indiferencia
es necesario no perder de vista, nunca, los defectos del ser
amado. Tener presente, en suma, su condición de ser real y no
dejarse engañar por la imagen.

De este modo, Lucrecio está definiendo una conducta que busca
llevar al hombre hacia la satisfacción de sus deseos reales y
escapar de aquello que induce a la aparición de deseos vanos.
En otras palabras, el hombre se construye como sujeto, para Lucrecio,
en la medida en que da forma a su vida al huir de las imágenes
y perseguir los deseos reales. Con ello está sujetando las bases
de un modo de percepción del hombre respecto a sí mismo que se
traducirá en un estilo; es decir, en un conjunto de prácticas
que responden a esa subjetividad y se dirigen a construirla día
con día.

Sin embargo, las alternativas de conducta que se ofrecen para
ello en realidad parecen orientarse hacia su contrario: un juego
de artificios en que el vehículo para lograr satisfacer al deseo
sexual, serán precisamente las imágenes. Porque el problema de
alcanzar una satisfacción sexual constante pasa por lograr que
el otro lo consienta y es ahí donde aquello de lo que se huye
servirá de red para conquistar al otro. Ésta es la clave del
_Arte de amar_.

# II. Amores y artes

## 1. _Los amores_

++En++ _Amores_, Ovidio describe los amores del poeta con Corinna.
Son amores adúlteros. Ella es casada. Pero rtambién son amores
infieles. Ella le es regularmente infiel, no sólo al marido sino
también al poeta, y éste detalla cómo es que en cualquier mujer,
por encima de los defectos, encuentra siempre una virtud que
lo apasiona y cómo, al fin, tiene amores con la sirvienta de
su amada. Él sufre por las infidelidades de ella, y ella, a su
vez, por las de él.

El asunto central de _Amores_ no son tanto las cuitas de la pareja,
sino los movimientos en el alma del poeta que cuestionan sus
propias penurias. De una promesa absurda, amor fiel a una mujer
casada, a la reflexión sobre la propia incontinencia con las
mujeres, el lector aprecia cómo el poeta vive la intranquilidad
de su alma, el desconocimiento de su deseo. La cita es extensa
pero manifiesta en plenitud las oscilaciones del alma del poeta:

> Por una parte el amor y el odio por otra luchan entre sí,
> y orientan mi débil corazón en direcciones contrarias; pero
> creo que vence el amor. Si me es posible, odiaré; si no, amaré
> contra mi voluntad: tampoco el toro gusta del yugo, y a pesar
> de odiarlo, lo lleva. Huyo de tu frivolidad, pero tu hermosura
> me reclama cuando huyo; recrimino tu falta de moral, pero
> amo tu cuerpo. De manera que no puedo vivir ni sin ti ni contigo,
> y me parece no tener claro _cuál es mi deseo_ […]

> Quisiera que fueras menos hermosa o menos frívola: una hermosura
> tan maravillosa no concuerda con tu carácter depravado. Tus
> acciones se merecen odio, pero tu cara reclama amor; ay de
> mí, desgraciado, que ésta tiene más fuerza que tus vicios
> […]

> Yo desplegaría de buena gana mis velas y me dejaría llevar
> por los vientos: lo preferiría a verme obligado a amar en
> contra de mi voluntad [Ovidio, _Amores_ II, 11b, 1-20].

El poeta está atado a la imagen de la amada, a su hermosura,
que puede más que lo que ella es, depravada y frívola. ¿Qué es
lo que en realidad desea el poeta? La cuestión no es vana ni
es, sólo, un recurso retórico.

Lo que el poeta de _Amores_ desconoce es su deseo, confundido
por la hermosura de la amada ---ante todo sólo una imagen---
y su conducta, digamos, discordante con su belleza. El costo
en dolor que tiene este amor es demasiado alto; lo lleva a amar
en contra de su voluntad, a aceptar una esclavitud que reditúa
muy poco ¿cuántas veces no buscó, en vano, el poeta a su amada?

El problema es el desequilibrio entre sentimientos encontrados
que hace predominar el dolor. Poco le importa al poeta el hecho
mismo de que sus amores sean adúlteros o incluso que ella le
sea infiel. No son los hechos, en sí mismos, lo que considera
condenables, sino el desconocer qué es realmente lo que desea.
Un imposible: que fuera menos hermosa o menos frívola, o el acaso
simple y brutal deseo de fornicar. El poeta no se define, no
se conoce, atado como está a un cuerpo, un rostro, una mirada.

Ovidio no habla por sí mismo sino por todos. El relato de _Los
amores_ no es una crónica de la vida de Plubio Ovidio Nasón,
sino un conglomerado de experiencias conocidas y probables.@note
Lo que importa aquí, sin embargo, no es la realidad de la experiencia
del poeta, sino la elaboración subjetiva que hace de los hechos
posibles. Es decir, es la forma en que elabora la cuestión del
amor, el modo en que la piensa y la evalúa, porque en ello no
hay ficción que valga. Vivido o no un amor adúltero, Ovidio lo
mira, lo presenta y lo problematiza en los mismos términos que
lo hace Lucrecio: el amor y la pasión están atados a la imagen
de la mujer amada; pero esta imagen jamás podrá ser la que, por
sí misma, satisfaga las necesidades del poeta. Perseguirla es
perseguir el dolor, el sufrimiento y la desdicha. Es, simplemente,
desear lo que no es real, sólo una ficción.

Pero Ovidio no se limita a mostrar el problema del amor al modo
epicúreo, también formula una solución. En los _Remedios de amor_
Ovidio ofrece tres formas de enfrentar todo amor, con el fin
de no padecer, como el poeta de los _Amores_, los vaivenes de
la insatisfacción:

1) Contar con más de una mujer. Cuanto mayor es el número de
amores, menos doloroso será la pérdida de uno. {.espacio-arriba1}

> Os exhorto también a que tengáis, a la vez, dos amigas, /
> es alguno más fuerte, si puede más tener / cuando partida
> la mente por mitad hacia ambos discurre, / las fuerzas del
> uno, el otro amor sustrae. [Ovidio, _Remedios de amor_ I, vv. 441-444]

2) Acostarse con la amada hasta saciarse de ella, tomando más
de lo que se necesita. Por ese camino el amante acabará por fastidiarse.

> Por ti debe ser saciada esa sed en que ardes perdido; / cedemos;
> es ya lícito que a medio arroyo bebas. / Mas bebe más aun
> / que lo que tus entrañas demandan; / brote el agua tomada
> de tu garganta plena… / Busca tedios, hacen también fin a
> los males los tedios. [Ovidio, _Remedios de amor_, vv. 532-554]

3) Fijar la mente en los defectos de la amada e, incluso, exagerarlos.

> Si alguna niña es sin voz, tú exige que cante; / haz que dance,
> si alguna mover la mano ignora. / Bárbara es en la plática;
> haz que hable muchas cosas contigo. / No ha aprendido la lira
> a tocar; la lira pide. / Duramente avanza, haz que camine.
> Las tetas su pecho / todo tienen. El vicio ninguna faja cubra.
> / Si es mal dentada, lo que haga reír nárrale… / Y servirá,
> cuando para nadie se ha arreglado, de súbito / hacia tu dueña,
> al alba, llevar lo pasos céleres.

Y más adelante:

> Allí en tú ánimo fija cualquier defecto que haya en su cuerpo
> / y en los vicios de éste, siempre manten los ojos. [Ovidio,
> _Remedios de amor_, vv. 333-343]

Los términos no sólo son similares a los de Lucrecio. Acostarse
con muchas, resaltar los defectos, saciarse son en manos del
poeta, las directrices de un estilo. Las alternativas propuestas
en los _Remedios_ plantean para Ovidio la cuestión de cómo convertirlos
en formas permanentes de la conducta. Aquí el término «formas»
no se refiere sólo a esquemas de acción, es decir, los modos
moralmente deseables de la conducta. Significa también, en un
sentido estético, los elementos de una composición. Es decir,
que no valen sólo en función de que son moralmente deseables,
sino y quizá sobre todo, en tanto que en conjunto y en atención
a las circunstancias, lo son en la medida en que componen una
vida ---como las notas y los ritmos componen una pieza musical---
y que define un estilo.

¿Cómo ha de vivir quien ha erradicado el temor a la muerte, ha
reconocido el placer como un bien, juzga los deseos por su necesidad
y su ordenamiento a un fin?

## 2. _El arte_

++En el++ _Arte de amar_, Ovidio desarrolla alguna de las claves,
si su texto se mira desde la perspectiva de la inquietud que
provoca el amor y, particularmente, la forma en que esta inquierud
es expresada por el epicureísmo. De hecho, Ovidio da un paso
más al cuestionar ya no sólo el reconocimiento de uno mismo como
sujeto de deseo, sino que para establecer la «forma» de la conducta
es necesario reconocer al otro también como sujeto de deseo,
y las condiciones en que es posible la comunión entre el deseo
de los dos sujetos.

> Primera, a tu mente venga la confianza: pueden ser todas /
> tomadas; tomarás, tú sólo tiende redes. / Callen en primavera
> las aves, en estío las cigarras, / las espadas el can Menalio
> dé a la liebre, / antes que la mujer, blandamente tentada,
> al joven rechace. / Incluso ésta que puedas creer que no quiere,
> quiere. / Y como al hombre la Venus furtiva, así es grata
> a la niña; / mal disimula el hombre mas ella ansía en secreto
> [Ovidio, _Arte de amar_ I, vv. 270 ss.].

No hay duda en Ovidio respecto al deseo femenino. Pero las mujeres
tienden a disimular, esconder, disfrazar su deseo, mientras el
hombre, hambriento, lo exhibe. Pero el poeta nos recuerda que
en el prado es la hembra la que muge al toro o relincha al caballo.
Por ello escribe: «Más parca en nosotros la pasión y no tan furiosa;
/ un legítimo fin, viril la flama tiene» (Ovidio, _Arte de amar_
I, v. 28). El verso puede entenderse como la prohibición que
veda a las mujeres entregarse al hermano, al padre 0, como Pasífae,
ser adúltera con un toro. Pese a ello, la pasión femenina «es
más cruel que la nuestra, y más furia tiene» (Ovidio, _Arte de
amar_ I, v. 340).

La moderación del deseo femenino, en cuya base encontramos la
prohibición ---masculina, claro--- al incesto, hay que entenderla
tentativamente como el autorreconocimiento femenino de la mujer
como sujeto de deseo. Si en el hombre es la esclavitud a la imagen
lo que desata el cuestionamiento que lo lleva a reconocerse como
alguien que desea, en la mujer es el límite del incesto lo que
la lleva a tomar conciencia de su deseo.

No nos detendremos aquí a examinar con detalle la elaboración
que Ovidio hace, desde su posición evidentemente masculina, de
cómo la mujer se ve a sí misma como sujeto deseante. Indiquemos,
sólo, de qué manera en el _Arte de amar_ la conducta práctica
de mujeres y hombres es presentada en términos inversos a como
se dan en la naturaleza. En éste es la hembra la que busca y
llama al macho, sin embargo entre los humanos es a la inversa:
es el macho quien convoca a la hembra y ésta la que se resiste.

El poeta ofrece así, como lo hace en muchos otros lugares,@note
un tributo al arte, como aquello que distingue a los hombres
de los animales, e incluso, las culturas anteriores al esplendor
de su tiempo. Arte que el poeta entiende, en los primeros versos
del _Arte de amar_, como dominio y gobierno del hombre sobre
las cosas ---las velas, los remos, la brida--- (Ovidio, _Arte
de amar_ I, v. 500) pero que sólo es posible en tanto que gobierno
y dominio de uno sobre sí mismo.

La inversión de las conductas de hombres y mujeres, por la que
éste busca y ella resiste, es obra del arte. Es decir, se trata
del producto de una técnica que problemartiza la legitimidad
del deseo, trazando límites y estrategias, y define un estilo:
lo explícito del deseo en el hombre, y el recato y el pudor en
las mujeres. El arte es, así, al mismo tiempo que habilidad en
el dominio de sí, artificio en el modo de conducirse.

Será a partir de esta «forma» de la conducta que hombres y mujeres
aprenderán el reconocimiento mutuo como sujetos de deseo, y serán
capaces de traducir tal subjetividad en «maneras» que los conduzcan
a una relación donde el deseo aparezca como legítimo y el placer
efectivo. Por ello: «Quien quiera que amará sabiamente, / vencerá,
y tendrá aquello que de nuestro arte pida» (Ovidio, _Arte de
amar_ I, vv. 511-512).

Tal es la meta última del Arte de amar, lo mismo para los hombres,
a los que Ovidio dedica los dos primeros libros, como para las
mujeres, a quienes dedica el tercero.

Pero lograr la comunión de deseos, lo mismo para el hombre que
para la mujer, exige conocer cómo se relaciona el otro con su
propio deseo. Es decir, reclama saber de qué forma lo que hacen
---cómo asienten o resisten, cómo visten, callan o hablan, etcérera---
se vincula con la manera en que entienden su deseo como deseo
legítimo.

La mujer, recatada, pudorosa, fiel, lo oculta con artilugios.
No es que carezca de él, no es que lo haya suprimido. Sólo lo
domina, cuidando de sí, para evitar la lujuria equívoca y el
dolor. Pero ¿cómo cuida cada mujer de sí misma? ¿Cuándo esconde
y cuándo revela su pasión? ¿Cuándo aceptará los abrazos? ¿Cuándo
los rechazará?

Ovidio no se propone, porque es una empresa imposible, descubrir
cuáles son los secretos, porque cada mujer guarda los suyos y
sólo ella conoce en principio, en qué momento y a cuáles llamados
abrirá las puertas de su lujuria. El _Arte de amar_ no es un
recetario: «con esta mujer obra así, con ésta asá», Al contrario,
el poeta ofrece formas de conducirse que el buen juicio y el
ejercitarse a través de la experiencia, han de lograr lo que
se pretende:

> Iba a terminar; pero tiene las niñas diversos / pechos; ánimos
> mil toma de mil manera. / Ni la tierra misma pare todo; aquella
> a vide conviene; / ésta, a olivos; aquí los farros bien verdecen.
> / Hay en los pechos tantos usos, e innumerables usos, / y,
> como Proteo, ahora se atenuará en leves ondas, / ora león,
> ora árbol, será, ora, puerco hirsuto. / Aquí, con red los
> peces; allá son con anzuelos cogidos, / aquí, tendido el cable,
> arrastran huecas nasas. / Y no te convenga un solo modo para
> todos los años; / de más lejos, la cierva vieja verá los lazos;
> / si pareces docto a la ingara o a la modesta atrevido, /
> para sí, mísera, desconfiará ella al punto. / De allí sucede
> que la que temió entregarse al honesto, / vil, del inferior
> a los abrazos vaya [Ovidio, _Arte de amar_ I, vv. 768-773].

Las formas propuestas por Ovidio pueden agruparse, como se hace
en el _Arte de amar_, en dos grandes rubros: aquellas que conducen
a la conquista y aquellas destinadas a retener y conservar. Las
primeras constituyen un catálogo variado que abarca, por igual,
a dónde ir para elegir a la dama, qué ofrecer y de quién cuidarse,
y que puede sistematizarse, sin embargo, en tres grandes categorías
definidas por el objeto de la estrategia: la persona amada, uno
mismo, los otros.

Las estrategias que tienen por objeto a la persona amada comprenden
el grupo más amplio y variado de técnicas cuyo ejercicio depende,
en exclusiva, del carácter de la amada. Así, el tiempo propicio
para la conquista, los regalos, las promesas, la oportunidad
de insistir, los encuentros, el uso de la violencia. Todas éstas
definen una conducta a seguir, pero dejan en suspenso las consideraciones
de cuándo, dónde, cómo, que serán determinadas por la amada.
A modo de ejemplo puede citarse la técnica de hacer promesas:
«Ni tímido promete; a las niñas las promesas arrastran; / testigo
a cualquier dios añade a lo ofrecido» (Ovidio, _Arte de amar_
I, v. 304).

Ovidio no señala qué prometer ni cuándo ha de prometerse, eso
es cuestión de cada cual y su dama. Sin embargo, Ovidio sí establece
un perfil de una conducta para quien busca hacerse amar: la de
quien promete no para cumplir las promesas, sino para hacerse
amar.

> Si dieras, algo, con razón podrás ser dejado: / tomará lo
> pasado y nada habrá perdido; / mas lo que no dieres, siempre
> que has de darlo parezcas. / Así engañó el estéril campo,
> a menudo al dueño, / así, por no perder, no cesa el jugador
> de perder. […] / Esta, obra; ésta es labor: unirse sin un
> regalo primero; / por no dar lo que dio, gratis, dará sin
> tregua [Ovidio, _Arte de amar_ I, v. 450].

Nos encontramos aquí, nítida y claramente, ante una forma de
conducta que ofrece los dos sentidos antes señalados. No es sólo
un esquema de acción, sino que se presenta como un elemento de
composición que induce ciertas maneras, enrendidas éstas como
«forma» estética de la conducta.

En realidad, tras la imagen del hombre que sólo desea los favores
de la amada, rehuyendo todo compromiso, jurando en falso y haciendo
promesas vanas (y que ha servido para caracterizar al hombre
en una larga tradición que llega incluso hasta nosotros); tras
esta imagen, se revelan, en realidad, las maneras del hombre
solícito y complaciente. El que seduce a la mujer al asumir lo
que ella misma desea.

De hecho, las promesas definen aquí una relación entre el hombre
y la mujer similar a la del jugador y el juego, por la que el
hombre se presenta como capaz de satisfacer las expectativas,
deseos y anhelos de la mujer, en tanto que ésta acepta la posibilidad
como garantía de que el hombre hará y será como ella espera.
Se trata de un engaño, sí, pero un engaño en que está en juego
la realidad del deseo.

Al reconocer a la mujer como sujeto de deseo, hacer promesas
se convierte en un mecanismo para conocer cuándo ella considera
legítimo su deseo. Las promesas que tienen efecto, aquellas que
logran capturar la atención de la mujer, sirven para conocer
qué es lo que ella espera del hombre. El engaño es la llave,
finalmente, para abrir el candado de su recato.

Son distintos los consejos dados por Ovidio que tienen por objeto
a uno mismo. Éstos, que son dos en el primer libro, tienen que
ver con el ejercicio del dominio de sí. Las recomendaciones contemplan
por igual cuidados de salud, corrección al vestir, el evitar
el afeminamiento y la templanza con el vino.

Ovidio se detiene particularmente en la moderación con el vino,
porque «la ebriedad, como verdadera daña, así ayuda fingida»
(Ovidio, _Arte de amar_ I, v. 595).

La posibilidad de aprovechar la ebriedad para justificar un mayor
atrevimiento, o ciertas actitudes procaces destinadas a incitar
a la mujer, es también un fingimiento que abre la puerta a otras
maneras de conducirse con la dama, en el espacio específico del
banquete, pero que no deben comprometer el cuidado de uno mismo,
porque aun aquí, deben ser objeto de una composición.

En cuanto a la relación con los otros es poco lo que hay que
decir. Conquista a la criada (con la que después de lograr los
favores de su señora es posible recrearse) y huye de los amigos
que eventualmente pueden traicionarte. Consejos prácticos de
un arte en el que cualquiera puede participar.

El hombre que busca amada ha de tener maneras solícitas destinadas
a formar ante ella la imagen de aquel a quien le resulta lícito
entregar sus favores. Pero las formas para conservar a la amada
son distintas. Ya no se trata de descubrir y reflejar su deseo.
La meta ahora es plegarse a tal deseo, aparecer como la promesa
continuada de su satisfacción.

Las maneras que Ovidio propone para este caso aceptan la misma
clasificación que las anteriores. Las que tienen por objeto a
la amada, a uno mismo y a los otros (de esto último sólo reitera
lo ya anotado en el libro I). Las primeras, que abarcan rópicos
como ser amable con ella, asistirla en la enfermedad, hacerle
regalos, profesar admiración por sus encantos, etcétera, pueden
resumirse en una sola:

> Cede a la que se te opone; cediendo, saldrás victorioso; /
> tan sólo haz los papeles que ella te ordena que hagas. / Ella
> acusa, acusarás; todo apruebas cuanto apruebe; / di lo que
> dirá; lo que ella niegue, niega; / si riere, junto ríe; acuérdate
> de llorar, si llorare [Ovidio, _Arte de amar_ II, vv. 198201).

Hay cierta lógica en la recomendación de Ovidio. Si al conquistar
ha de empeñarse en parecer lo que ella desea, para conservar
es necesario dar algunas pruebas. No se trata, por supuesto,
de ser lo que ella quiere, sino de jugar ese papel. La clave
está, de nuevo, en la ilusión, la apariencia. Es decir, la distancia
que media entre acceder por conveniencia, reservando uno mismo
siempre el derecho de decir esto no, esto sí, y hacerlo por locura,
por frenesí, que impide negarse a todo lo que ella pida. De hecho:

> Mas lo que habrás de hacer por ti mismo y útil estimes, /
> harás que te lo ruegue siempre la amiga tuya. / A alguien
> de los tuyos, la libertad prometida haya sido; / con todo,
> haz que ésta pida él de la dueña tuya [Ovidio, _Arte de amar_
> II, vv. 286-290).

El poeta no pide doblegarse, pide sólo aceptar el papel asignado
por ella, en función de un fin propio y más alto: el placer.
Porque en el «parecen» que siempre se está dispuesto radica la
posibilidad de mantener el dominio de sí, la autonomía y la independencia.

¿Qué hacer, sin embargo, cuando siendo consecuente con el cuidado
de uno mismo se sostienen otros amores? ¿Qué, cuando ella hace
lo propio y es infiel? Las estrategias referidas a uno mismo
se concentran en estas dos eventualidades. En relación con las
propias aventuras, la recomendación queda abierta. Es preferible,
como norma general, ser totalmente discretos. Los galanteos con
otras amigas es asunto que concierne sólo a uno mismo y en nada
deben afectar el amor que se ha conservado. Sin embargo, existe
la posibilidad de que revelar infidelidades pueda fortalecer
un amor, en este caso la lógica es provocar inseguridad, temor
en la amada. Con todo, lo relevante es no caer en el error de
abandonarse a una sola mujer, sino aprender a mostrarse, a cada
una, como lo que ella quiere que se sea, sin perder por ello
el legítimo derecho de uno mismo.

¿Y cuando lo que ocurre es a la inversa? Entonces:

> Mas fue mejor no saber; consciente que los hurtos se cobran,
> / porque no huya el mostrado pudor, del falso rostro. / Tanto
> más, oh jóvenes, evitad sorprender a las vuestras; / pequen,
> pecadoras, que han engañado piensen. / Crece el amor en los
> sorprendidos; cuando es par la fortuna / de los dos, en la
> causa de su daño ambos duran [Ovidio, _Arte de amar_ II, vv.
> 550-560).

La resignación ante el dolor es también una estrategia. Lo es
en función de conservar a la mujer amada, de mantener su conciencia
culpable, de hacerla aparecer ante sí misma como en falta ante
nosotros y evitar con ello la persistencia de las infidelidades.
En la relación entre dos que se han reconocido como sujetos de
deseo, el dolor no se ausenta, pero en la cuenta de haberes y
deudas, ha de calcularse qué predomina. El dolor de saberse traicionado
se incrementa con nuevas traiciones, si se persiste en exigir
fidelidad. Otra cosa es la ignorancia, que hace deudora a la
mujer y que permite, a un tiempo, tomar distancia respecto a
la amiga. El propio Ovidio, recordando un pasaje de _Amores_
recuerda que él incluso está por debajo de su precepto (Ovidio,
_Arte de amar_ II, vv. 498-500).

En la paradoja de recomendar algo que uno mismo no es capaz de
cumplir está contenido el conjunto del sentido del arte de amar:
es un artificio de formas de acción, dispuestas en su conjunto
no sólo a normar la conducta, sino a componerla en torno a fines
bien precisos: auronomía, libertad, felicidad. Predominio de
placeres frente a dolores. La manera de ignorar las infidelidades
no es un don natural, es obra del ejercicio, de «pintar» las
propias conductas de acuerdo con un plan preconcebido. De este
modo, ha de entenderse que en el plano de las prácticas, la reflexión
ética se expresa en términos estéticos, de manera que, en última
instancia, son las que reditúan en un bien.

De ahí, por supuesto, que estén presentes, también y de manera
señalada en el acto final:

> Por su propio impulso, sin ti, hablarán muy sabias palabras;
> / no yacerá en el lecho la mano izquierda, inerte; / encontrarán
> los dedos qué hagan en las partes aquellas / en que moja sus
> flechas Amor ocultamente […]

> Créeme, el placer de Venus no apresurado ser debe, / mas traído
> por tarda demora, poco a poco, / cuando hallares lo sitios
> que goza la mujer que se toquen, / no te impida el pudor para
> que no los toques; / contemplarás de trémulo fulgor centelleantes
> los ojos, / como el sol a menudo refulge en el agua límpida;
> / lleguen las quejas, llegue el amable murmullo, los dulces
> / gemidos; y, adaptadas al juego, las palabras. / Mas ni a
> tu dueña tú, velas mayores usando, / abandones, ni ella tus
> cursos anteceda; / a la meta apresuraos a una. El placer,
> allí, pleno, / cuando varón y hembra, al par vencidos, yacen.
> / Debes tú observar esta norma cuando son dados los ocios
> / libres, y la fortuita obra el temor no urge; / cuando la
> demora no es salva, inclinarse en todos los remos / útil,
> y al lanzado caballo dar espuela [Ovidio, _Arte de amar_ I,
> vv. 703-732].

El acto sexual es, por supuesto, la meta última del arte de amar.
La compleja construcción de formas de conducta tiene sentido
cuando se acaba en la cama. Pero aún en ella, hay directrices
que definen también una conducta. La ausencia de pudor, el imperativo
de excitar a la pareja, los beneficios de no apresurar el placer
y la norma de concluir ambos a un tiempo, no sólo persiguen la
plenitud del placer, sino maneras que suscitan la plenitud en
ambos.

Aquí lo que se transparenta son los rasgos estilísticos. No hay,
en realidad, necesidad alguna de que el coito tenga lugar de
esta forma y, sin embargo, son estas maneras las que definen
su sentido, su valor positivo dentro de la subjetividad epicúrea.
El varón que no espera cuanto tarde su pareja en concluir, quien
apura el placer, en fin, quien simplemente fornica, se distingue
en la «forma» del epicúreo. El acto no tendrá, para él, el sentido
que cobra al ser juzgado desde la necesidad y la jerarquía, desde
el imperativo de huir de las imágenes. No verá en el acto el
bien del placer. Al contrario, al estilizarlo de esta forma,
el epicúreo lo convierte en un elemento de su propia composición
vital. En sentido estricto, el coito es el lugar donde la promesa
y la complacencia han de tornarse únicamente placer efectivo.
Ello garantiza, por un lado, la satisfacción del varón y le garantiza,
además, nuevos encuentros con la mujer.

## 3. _Las mujeres_

++Toda esta prédica++ dirigida al varón interesado en sí, tiene
también su contraparte femenina. El libro III del _Arte de amar_
está dirigido a ellas. Muchas de las recomendaciones que el poeta
ofrece a las mujeres guardan una unidad curiosa. La totalidad
---si exceptuamos el cuidarse de sirvientes y amigos, y la utilización
de negativas e infidelidades--- son estrategias de relación respecto
a sí misma. Ésta es una característica significativa que subraya
hasta qué punto Ovidio entiende el deseo masculino como explícito.
No hay necesidad de indagarlo, descubrirlo, medirlo. Por el contrario,
la estrategia es atraer ese deseo. En consecuencia, la mujer
ha de fijarse en la imagen, por encima de la legitimidad. Ha
de poner su acento en el arreglo, el ocultamiento de defectos,
la exaltación de sus atributos, el cultivo del ingenio, de los
juegos, etcétera. A los banquetes, por ejemplo, ha de llegar
tarde, cuando hayan encendido los candiles, pues, «aun cuando
fueres fea, hermosa parecerás a los ebrios / y a tus vicios,
la tenebra dará la noche misma» (Ovidio, _Arte de amar_ III,
vv. 751-752). Ha de evitarse ensuciarse al comer y evitar la
satisfacción plena, pues ver comer en exceso a una mujer produce
desprecio. Por supuesto, ha de evitar embriagarse, pues se expone
a acostarse con cualquiera, lo que según Ovidio es un justo castigo
a la ebriedad femenina.

El juego es también de apariencias, pero destinadas a conseguir
que el hombre encuentre en esa perfección de la apariencia, aquello
que anima su deseo. Ovidio no escatima espacio para describir
el uso del maquillaje, las pelucas. Incluso a este tema dedica
una pequeña obra, _La cosmética del rostro femenino_. El asunto
es ocultar defectos, particularmente defectos físicos y, en menor
medida, los de carácter, para propiciar el furor masculino. Recordemos,
por ejemplo, el lamento del poeta en Amores cuando ante el antagonismo
entre la belleza y el carácter de su amada, acepta que se rendirá
por la belleza, dado que pesa más, sujeta más, que lo que repugnan
su frivolidad y sus infidelidades.@note

Esta idea llegará a su cenit cuando Ovidio establezca cuál debe
ser la conducta observada por las mujeres durante el coito.

> Cada cual conocida sea; ciertos modos del cuerpo / tomad;
> no a todas sienta una figura sola. / La que insigne sea por
> su faz, yacerá boca arriba, / sean miradas de espalda las
> que su espalda gozan. / Tú también, a quien Lucina marcó con
> arrugas el vientre, / usarás, como el célere Parto, caballos
> vueltos. / Milano en los hombros, las piernas de Atalanta
> llevaba; / si buenas son, miradas ser de ese modo deben. /
> La parva se lleve a caballo; porque era larguísima, nunca
> / la tebana esposa subió al caballo Hectéreo. / Oprima el
> lecho con las rodillas, vuelta un poco la nuca, / la mujer
> que del flanco largo ser vista desee [Ovidio, _Arte de amar_
> III, vv. 771-780).

Asumir tal o cual posición durante el coito no obedece aquí a
la búsqueda de una intensificación del goce por cierto efecto
físico. Lo que en realidad se persigue es sostener la imagen,
disimular los defectos. La mujer monta o permite ser montada
de tal o cual manera, en función de aquello que ella desea exhibir
u ocultar al hombre. La estilización del acto sexual no persigue
un aparente incremento del placer ---imposible, en sentido estricto,
para el epicureísmo---@note sino la posibilidad misma de que
el hombre, en este caso, obtenga placer.

Hay otros elementos de estilización del acto amoroso que aparecen
en Ovidio y que se consideran práctica común en la Roma imperial:
el que tenga lugar a oscuras ---que para el poeta sirve para
disimular defectos en las mujeres---; el que la mujer nunca se
desnude por completo ---con el mismo propósito del anterior---
y que el hombre ocupe sólo la mano izquierda, a lo que no se
le atribuye un sentido preciso.@note

## 4. _Arte: conocimiento de sí y belleza_

++Es probable++ que al lector le resulte un tanto innecesario
este largo recuento de estrategias de seducción que seguramente
conoce y tal vez practique. Pero hay una cuestión, todavía, en
la que es necesario detenerse.

En la base de todo el desarrollo del _Arte de amar_ encontramos
la preocupación epicúrea por la realidad del deseo, distinguible
por su necesidad y su ubicación jerárquica. Está presente, también,
el matiz lucreciano a propósito de la fuerza de las imágenes,
como constructoras de deseos vanos, a la que debe hacerse frente
apelando a los instrumentos que permiten establecer qué deseo
es real. Pero de esta doble inquietud se construye un conjunto
de conductas encaminadas al artificio y el engaño, para lograr
vincular ese deseo, en este caso sexual, con su satisfacción
auténtica.

Los modos ovidianos, sin embargo, incluso en el caso concreto
de las técnicas amatorias para el coito, no son formas destinadas
a incrementar el goce, sino sólo de procurar satisfacción a un
deseo. La diferencia es crucial, porque no pretenden aumentar
el disfrute placentero a través de su prolongación en el tiempo,
una mayor cantidad o una variación en la cualidad ---alternativa
inconcebible en el pensamiento epicúreo---, sino algo completamente
diferente: formar una sensibilidad.

La clave está, y llega a Ovidio, desde el momento en que Lucrecio
establece el dilema de la sexualidad. La tendencia de los hombres
es perseguir imágenes. Aprender a huir de ellas y placerse sólo
en aquellos deseos que son físicos, y cuya satisfacción se consigue
físicamente, implica la elaboración de una sensibilidad diferente,
un gusto distinto, construido y cultivado por medio del arte.
Ante todo porque la relación entre el hombre y la mujer, tal
como la ve Ovidio, es conflictiva; en un caso porque el hombre
puede perder el dominio de sí, y porque la mujer puede verse
ultrajada y humillada por quien ha perdido sus cabales, en el
otro.

Es de esta forma como Ovidio convierte los problemas que el mundo
griego planteaba para la relación de «amistad» entre los varones
de edad distinta, al ámbito de las relaciones entre los dos sexos.
Y es sobre esta base que nace la sensibilidad de la necesidad
de conquistar al ser amado.

En efecto, la sensibilidad se construye a partir de que el hombre
reconoce que la mujer es también sujeto de deseo y engendra una
estética de la conducta, que es la que rige el arte de amar.
En ese sentido, el erotismo ovidiano constituye la forma de la
conquista y los modos son las técnicas con que se lleva a cabo
ésta. Y lo mismo puede decirse de la mujer: la sensibilidad femenina
se construye a partir de que es capaz de comprender que los hombres
persiguen imágenes y que engendra una estética que es el trato
que da al hombre.

Pero esta sensibilidad, esta estética de los sentidos,@note es
también una forma de conocimiento de sí. O, más exactamente,
la base de un conocimiento práctico de uno mismo:

> Cuando esto yo cantara, manifiesto Apolo de súbito / movió
> del áurea lira, con el pulgar, las cuerdas / […] Él a mí:
> «perceptor del amor lascivo ---me dijo---, / ea a tus discípulos
> hacia mis templos guía, / donde hay, celebrada por la fama
> en todo el orbe, / una letra que ordena que cada uno sea conocido.
> / Sólo quien conocido se fuere, amará súbitamente / y todo
> trabajo comparara a sus fuerzas. / A quien dio natura faz,
> sea, por ella mirado; / con un hombro patente, quien color
> tiene, se acueste; / quien place por la plática, cuide taciturnos
> silencios; / quien canta bien, que cante, quien bebe bien,
> que beba. / Mas ni declaren a media conversación los disertos
> / ni, no sano, el poeta escritos suyos lea» [Ovidio, _Arte
> de amar_ I, vv. 493-508).

No se trata de un conocimiento de sí en sentido epistemológico
u ontológico. Es un conocimiento subjetivo, incontrastable, que
resulta del proceso de construirse como sujeto de deseo de donde
emana una estética de los sentidos, una sensibilidad, desde la
cual es posible conocerse. Se trata, así, de un saber que define
las carencias y las virtrudes que puede percibir uno mismo respecto
a sí en el contexto de construir las imágenes del artificio para
seducir al otro. En última instancia, la conducta será resultado
y expresión de ese conocimiento de uno mismo y, al mismo tiempo,
el vehículo del conocimiento.

Así, los modos ovidianos están destinados a construir una estética
de los sentidos, una forma artística de la sensibilidad que se
expresa en una conducta estética, producto del trabajo voluntario
sobre uno mismo que es, por igual, el trato que se dispensa al
otro como el que se da a uno mismo.

# III. La rosa

## 1. _Iniciación al amor_

> Solamente en vos puse mi esperanza…@note

++Así finaliza++ el _Roman de la rose_ de Guillaume de Lorris
que ha llegado hasta nosotros. La falta de conclusión, sin embargo,
no impide ver en ella una obra terminada. La razón ha de buscarse
en que el final abrupto no necesariamente obliga a echar de menos
una continuación. Es más, puede pensarse que la falta de un final
en forma tiene un buen efecto cuando se trara de cantar un amor
desgraciado.

El _Roman_ de Guillaume de Lorris es una ingeniosa alegoría construida
mediante la personificación de las pasiones humanas, masculinas
y femeninas, que describe cómo el hombre (Amador) no sólo voluntaria,
sino impelido por una fuerza mayor ---Amor, que lo hiere para
hacerlo su vasallo (Guillaume de Lorris, _Roman de la rose_,
vv. 1681 ss.)--- busca alcanzar a la Rosa que es la imagen virginal
del sexo femenino.@note Sin embargo, la intención del hombre
es vana. La dama, en tanto que Rosa, le está vedada: si la alcanza,
la degrada y destruye; si no la alcanza, el amor es sólo insatisfacción.

Lorris es heredero cultural de una tradición, la del amor cortés,
que críticos e historiadores coinciden en señalar como «inventora»
del sentimiento de amor.@note Y lo hace en un momento peculiar,
justo cuando se ha iniciado una amplia y vigorosa movilización
social orientada a restituir y fortalecer las costumbres morales
y religiosas. El movimiento cátaro, al que se ha ligado siempre
el amor cortés@note y poderoso entonces en el sur de Francia,
impulsa una metafísica y, en consecuencia, una ética dualista,
en la que se condena todo acto sexual, incluyendo el destinado
a la procreación.@note Paralelamente, la iglesia católica, que
ha emprendido una campaña para moralizar el matrimonio, discute
a su vez en qué condiciones puede permitirse el acto sexual y
no faltan, por supuesto, aquellos que se inclinen por censurar
como pecado mortal incluso el realizado bajo el sacramento del
matrimonio.@note

En uno y otro caso, la disputa acerca del carácter pecaminoso
del acto sexual se centra, no tanto en el acto mismo sino en
su naturaleza placentera. Cátaros y católicos consideran que
el placer es connatural al coito, en la medida en que se trata
de un acto de la carne y, por ello, bajo el dominio del mal.
La iglesia católica, sin embargo, llegará a la conclusión que
la condena al coito no debe ser tan radical, puesto que sería
equivalente a condenar en definitiva el único medio conocido
de reproducción del hombre. Decidirá, en consecuencia, que el
placer experimentado en la cópula puede no ser pecado mortal,
sino venial y por tanto exculpable, cuando se realice siempre
con la misma mujer, con la que no se tenga parentesco, dentro
del matrimonio y siempre y cuando esté orientado hacia la procreación
y no hacia el placer.@note

En el contexto de esta amplia discusión sobre las relaciones
entre el hombre y la mujer, centrada alrededor del placer, se
desarrolla de modo paralelo el amor cortés. En un sentido, el
amor cortés o _fin'amor_ es una codificación de la conducta de
hombres y mujeres, que persigue el desarrollo de un sentimiento
mutuo concebido como trascendente al sólo goce erótico. Sin excluir
el placer, el _fin'amor_ se construye sobre cuatro características
básicas: humildad, cortesía, adulterio y feudalismo de amor.@note

El centro del amor cortés es la concepción del amante como _siervo_
de su dama, cuyas virtudes son la obediencia y la aceptación.
Se transmitirán, entonces, los rituales del pacto vasallático
a la relación entre hombres y mujeres: el homenaje, la fidelidad
y el beso como confirmación de las obligaciones son símbolos
de la rendición del hombre frente a la mujer. Pero a pesar de
que en este nuevo juego para las relaciones, la mujer aparece
como idealizada, puede ser alcanzada. Se ha subrayado mucho que
en los grados de amor el _drut_ (amigo, amante) corresponde al
momento en que la dama acepta a su amigo en el lecho.

En otro sentido, el _fin'amor_ es también un proceso de educación
que distinguirá al _cortesano_ del _villano_, al miembro de la
corte del habitante de la villa. A través de él, los jóvenes
caballeros, y hay que subrayar su carácter juvenil, como lo hace
el _Roman_ de Lorris, aprendían a conducir su deseo hasta que
éste se expresaba dentro de los cauces de la cortesía. A través
de esta última el deseo cobraba legitimidad y el joven se preparaba
para el matrimonio.

En efecto, el amor cortés, aunque no es posible en el matrimonio
porque no existe libertad,@note no se opone a éste; es decir,
no lo excluye. Se trata en realidad de un proceso a través del
cual se va dando forma al deseo para llegar al matrimonio. Es
cierto que, más adelante, con el arribo del petrarquismo la imposibilidad
de amor en el matrimonio se convertirá en una verdadera oposición.
Pero en el inicio el amor es sólo un paso previo al matrimonio,
una iniciación en el ámbito del deseo. Como puede verse en la
obra de Lorris, el amor es propio de los jóvenes y es a través
de él que se preparan para las rigideces de la vida conyugal.

La alegoría del _Roman_, que narra la lucha entre Celos, que
mantiene cautiva a Rosa, y el vasallo de amor que es Amador,
puede leerse como un proceso de iniciación por el cual el joven
que ha penetrado en el jardín donde está la corte de amor, y
ha osado besar a la rosa, aprende a dirigir su deseo de posesión
y ansias de placer, a través de la búsqueda de la comunión espiriual
con Rosa ---adúltera o libre, qué más da--- donde recibirá los
consuelos de amor que se explican por sí mismos: Esperanza, Dulce
Pensar, Dulce Palabra, Dulce Mirar (Guillaume de Lorris, _Roman
de la rose_, vv. 2618 ss.). Se trata de sentimientos y acciones
encaminados a construir sobre el deseo erótico un sentimiento
de comunión, que prepara para la madurez sexual que se expresa
en el matrimonio.

![_Historia completa de Adán y Eva en el paraíso_. Colonia, 1499.](../img/img3.jpg)

## 2. _De amor y erótica_

> Pero si he perdido ya toda esperanza, / Yo me desespero por
> poquita cosa. / ¿Yo desesperarme? ¡Cómo! ¡No lo haré, / Y
> nunca jamás desesperaré! [Jean de Meun, _Roman de la rose_,
> vv. 402-403)

++Así inicia++ Jean de Meun, cerca de medio siglo después, su
continuación del _Roman de la rose_. La suya es una reelaboración
del amor cortés de Lorris, a partir de dos influencias clave:
los modos de la erótica ovidiana y la espiritualidad de san Francisco
de Asís, a partir de los cuales enunciará una noción de amor,
como amor natural, donde el placer juega un papel fundamental.

Hubo un tiempo, dirá Amigo a Amante en el _Roman_ de Meun, en
que «los amores eran bellos y leales, / sin codicia alguna y
sin interés, / la vida así era placentera» (Jean de Meun, _Roman
de la rose_, vv. 8355 ss.). Un tiempo en que todo era común,
«y sobre un manto […] / sin otro _interés que el puro placer_,
/ venían a unirse y entrelazarse / aquellos a quienes urgía el
amor» (Jean de Meun, _Roman de la rose_, vv. 8431 ss.).

Así, evocando una fabulosa edad de oro, Jean de Meun introduce,
una idea que comparte rasgos franciscanos y corteses: el amor
natural.

La edad de oro es ahistórica. No está ubicada en el tiempo ni
en el espacio. Recuerda, en abstracto, los amores de Dafnis y
Cloe, pero también se confunde con el Paraíso. Nada impide que
De Meun haya proyectado en ambos sentidos su imagen. La función
de esta edad de oro es sólo una referencia, sirve en el Roman
para asir y comprender la experiencia amorosa como una experiencia
en sí misma placentera.

Al igual que Lorris, para De Meun el amor es un sentimiento que
trasciende el erotismo. Sin embargo, es un sentimiento por el
que también se trasciende el placer como mero goce sensual. A
diferencia de Lorris, De Meun ha sido receptor de la herencia
franciscana que «proclamaba que el mundo no es tan malo […] que
el creador ha situado a Adán en el Paraíso para que disfrute
y para que trabaje con el fin de hacerlo más bello, que la naturaleza
es hija de dios, y que, por tanto, merece ser mirada, observada
y comprendida».@note

El placer del amor, que aparece al fundirse los dos amantes,
tendrá en De Meun el eco de esta visión espiritual benigna de
la naturaleza, y la sensualidad se elevará con un rango distinto,
tendrá un carácter divino.

Natura es la gran protagonista del _Roman_. A ella se recurre
como última instancia para que Rosa se convenza de entregarse
a Amador. En una primera aproximación, Natura aparece como quien
se ocupa de producir las piezas individuales de las especies
para que Muerte no acabe con ellas. Su función es esencialmente
procreadora y proveedora. Por ello Dios la hizo «cual si fuera
manantial continuo / que siempre está lleno y siempre fluyente,
/ cuyo fondo y bordes son inabordables, / de donde derivan todas
las bellezas» (Jean de Meun, _Roman de la rose_, vv. 16233-16376).

Natura no otorga el ser por sí misma. Ello es prerrogativa de
Dios. La individuación de la que se hace cargo tiene un significado
cercano a la acción del Demiurgo en algunas metafísicas neoplatónicas.
Es el último ser dentro de la escala divina y su función es la
de reunir en un individuo las características que requiere para
existir.

En el sermón de Genio, enviado por Natura para explicar su posición
frente a la controversia entre Amador y Rosa, Natura queda asociada
a los órganos genitales (Jean de Meun, _Roman de la rose_, vv.
19505 ss.). Éstas son las herramientas de las que se vale para
llevar a cabo la obra divina y su propia misión.

> Tendrían que ser enterrados vivos / quienes no hacen uso de
> las herramientas / que Dios conformó con sus propias manos
> / y proporcionó a mi ama Natura / para que pudiera proseguir
> su obra. / Puesto que empleados convenientemente / tales utensilios,
> los cuerpos mortales / podrán gozar de perpetuidad. / [Jean
> de Meun, _Roman de la rose_, vv. 19575- 19582)

El uso de los órganos genitales, en el hombre y la mujer, se
revela a través del discurso de Genio, como lo propio natural,
el ejercicio mismo de la naturaleza. Pero a la vez, la naturaleza
es la ejecución de la obra divina. Así, como realización de la
obra de Dios, el acto sexual recibe el beneficio del placer (Jean
de Meun, _Roman de la rose_, vv. 19345-19356).

Y en realidad, lo que define las obras de Dios y de Natura en
el _Roman_ es el placer, aun por encima de su utilidad, racionalidad
o bondad. De este modo, el placer no sólo es positivo, sino un
rasgo divino de la operación natural. En el supuesto de una edad
de oro, no habría posibilidades de una conducta ética a partir
del placer, en la medida en que no habría ocasión de definir
estructuras diferenciadas y de establecer los términos de una
valoración. La inquietud, aquello que podemos llamar la definición
de la sustancia ética, surge dentro del _Roman_ en De Meun como
resultado de la existencia de tendencias contrarias dentro de
ese compuesto que es el hombre.

De Meun no explica cuáles son y qué características tienen tales
tendencias. En realidad recurre a tres imágenes que intentan
mostrar que el hombre es capaz de actuar contra natura amparado
en la fe, el miedo, pero sobre todo, con la intención de sustituir
el orden divino por uno humano.

Por el primero, De Meun intenta explicar que el origen supranatural
y divino del entendimiento humano no es equiparable a la inmaculada
concepción de Jesús. Éste es un hecho extraordinario que no puede
constituir modelo de conducta para los hombres que deben reconocer
en la procreación carnal la única manera natural y divina de
perpetuarse, pues sólo de ese modo es (Jean de Meun, _Roman de
la rose_, v. 17716).

De la misma forma, el alegato recogido de Lucrecio contra la
creencia en el significado de castigo divino a las acciones de
los hombres a través de los fenómenos extraordinarios, como terremotos
y tormentas, es dirigida a mostrar que no es Dios sino «sólo
el hombre el que consiente su daño» al atacar la propia naturaleza
(Jean de Meun, _Roman de la rose_, vv. 1915319162).

Finalmente, la castración de Saturno por Júpiter, que establece
el paso de la edad de oro a la de hierro, ejemplifica la sustitución
del orden natural y divino, paradisíaco, por uno humano que privatiza
la propiedad de la tierra y sus frutos, y se revela, finalmente,
como contrario a Natura (Jean de Meun, _Roman de la Rose_, vv.
20031-20071).

Para definir, en síntesis, en qué consiste la fuente de la inquietud
del hombre, ésta es: la pertenencia del ser humano a dos reinos
ontológicos y lógicamente distintos, por lo que no sólo son de
naturaleza contradictoria (divina y humana), sino intraducibles
(el orden divino no se expresa en el orden humano).

Hay dos maneras de ser hombre: una por la cual se responde al
orden divino y convierte la historia individual y colectiva en
una teofanía, y otra que se guía por el orden humano, es decir,
los intereses particulares, inmediatos y mezquinos, de los hombres.

Esta diferencia no es en realidad nueva, está ya en san Agustín
y forma parte de pensadores más próximos en el tiempo a De Meun
como san Buenaventura. La condición humana, aunque unitaria y
por ende, poseedora de rasgos divinos, puede responder a fines
distintos de los de la divinidad y en esa elección entra en juego
su condición ontológica: el hombre que se entrega al mal, en
realidad se entrega al no ser, pierde sustancia, según la idea
de san Agustín, en la medida en que se aleja de Dios.

De Meun no es extraño a esta tradición y, en esa medida, trazará
la diferencia entre el hombre que actúa para realizar la obra
divina, y el que lo hace sólo en pos de sus obras humanas.

La inquietud entonces es cómo descubrir qué actos, qué conductas,
son las que realizan la obra divina. Y la respuesta será: aquellos
que responden a los deseos naturales. En cuanto a la conducta
sexual, esta inquietud se formula como: de qué forma el deseo
sexual se transforma en nexo del hombre con la divinidad y puede
transformarlo en artífice de la obra divina.

Cuatro son las prácticas que De Meun identifica como contrarias
a la naturaleza: la castración, la sodomía, la abstinencia y
el matrimonio.@note Cada uno de los cuales es un ataque contra
uno de los principios con que opera Natura.

La castración es presentada en el _Roman_ como el peor ataque
a la naturaleza.@note La interrupción misma de su curso, el símbolo
de la caída ---De Meun asimila la castración de Saturno con la
expulsión del Paraíso. En sentido estricto, la castración es
un atentado contra la fecundidad desbordante de la naturaleza;
uno de los principios por lo que es capaz de trascender la muerte.

Condena similar recibe la sodomía. La razón es la misma que la
anterior, significa una limitación a la fecundidad, aunque por
supuesto, de ninguna manera tan grave y drástica.

Trato equivalente recibe la abstinencia, tanto como práctica
laica como religiosa. En cualquier caso, es una forma de ir en
contra de uno mismo y de la fecundidad natural.

Finalmente, el matrimonio es la práctica que más ocupa a De Meun.
En él se manifiesta ya no el ataque contra la fecundidad natural,
sino contra los otros dos principios que conforman a la naturaleza:
la libertad y la gratuidad o generosidad.

De Meun sigue aquí el principio de la Corte de Amor que contrapone
amor y matrimonio porque, mientras que en el primero los amantes
se reúnen libremente, en el segundo es el débito matrimonial
el que los enlaza, y el débito implica, entre otras cosas satisfacer
el deseo sexual de la pareja.

Según De Meun, y aquí sigue a Ovidio, el deseo femenino es prácticamente
insaciable. Así, el problema es que el matrimonio obliga a ese
natural y ardiente deseo de la mujer a satisfacerse a través
de un solo hombre. Así, lo primero que aparece es el temor del
marido a no cumplir plenamente el débito con su pareja y que,
en consecuencia, la mujer acuda a un amante para conseguir la
satisfacción no proporcionada.

La otra razón que aparece en el _Roman_ y que De Meun toma de
la correspondencia apócrifa entre Abelardo y Eloísa, es la concepción
del matrimonio como un acto de compraventa, pues la mujer que
se casa en realidad se prostituye: «La mujer que prefiere casarse
con un hombre rico que con uno pobre, y desea a su marido más
por sus posesiones que por él mismo, ella se está poniendo en
venta».@note

![_Rosarium philosophorum_. Frankfurt, 1556.](../img/img4.jpg)

Esta visión del matrimonio corre paralela a la concepción de
las dos naturalezas del hombre, una por la cual concurre hacia
una teofanía y otra por la que sólo vela por sus intereses individuales,
contradiciendo a natura. La mujer que prefiere los bienes al
hombre, como el hombre que quiere para sí auna mujer están, para
De Meun, privatizando lo que es un bien colectivo. El matrimonio
es visto así, no sólo como un acto distinto al amor ---o encontrado
con él--- sino como un acto de privatización de un bien que es
para toda la humanidad, sean éstos los bienes económicos o bien
el amor de la mujer o el hombre. De ahí que en De Meun, a diferencia
de Lorris, el amor no sólo se contraponga al matrimonio, sino
que aparezcan como excluyentes.

En el texto de Lorris el amor es presentado como una forma legítima
de educación del deseo y desaparece una vez que esa educación
ha cumplido su cometido y el joven cortés persigue la perpetuación
fecunda a través del matrimonio.

Casi medio siglo después, en De Meun, las razones que oponen
una y otra práctica las vuelve excluyentes: el matrimonio para
él arenta contra el orden de la naturaleza en lo que éste tiene
de libre y generoso, y que por ello no puede encerrarse en el
estrecho cinturón de los deberes mutuos del matrimonio.

En contrapartida, el placer que es propio del amor es resultado
de la realización efectiva de un acto fecundo, producido libremente
y sin un fin determinado.

Aunque Amigo diga a Amador en el _Roman_, al describir el amor
en la edad de oro, que «sin otro interés que el puro placer /
venían a unirse y a entrelazarse / aquellos a quienes urgía el
amor» (Jean de Meun, _Roman de la rose_, vv. 8433-8434), el puro
placer no implica, como podría entenderlo un lector del siglo
+++XX+++, la realización del acto sexual privándolo de toda intención
engendradora. Al contrario, se invita a la unión de los amantes
sin importar sus eventuales consecuencias procreadoras, porque
ha de ser una entrega libre, generosa y hasta tal punto desinteresada,
que la eventual fecundidad del amor no es obstáculo, sino fruto
consecuente.

La procreación, en consecuencia, es asimilada en el placer y
asumida dentro del amor que, con esto, adquiere un valor superlativo
y puede, ahora sí, ser antagonista del matrimonio. De esta forma,
De Meun se aleja de la concepción del amor en el _Roman_ de la
de Lorris. En éste, la procreación es todavía, función privativa
del matrimonio y la comunión espiritual no tiene por qué ir más
allá del enamoramiento que legitima el goce. En De Meun, en cambio,
no es el enamoramiento el que legitima la acción placentera,
sino el hecho de que es producto de un acto de naturaleza que
tiene implicaciones espirituales.

A esta concepción del placer, a la que subyace una idea del deseo
como principio natural para el cumplimiento de la obra divina,
no pueden corresponder los criterios de humildad y vasallaje
de amor que caracterizan el primer amor cortés, porque ya no
necesita de subterfugios que lo legitimen. El amor logra trascender
el placer porque le otorga, en De Meun, una función dentro del
plan divino y por ello reivindica la conquista y el engaño ---el
principio de los modos ovidianos--- como formas legítimas para
lograr la meta última de volver a vincular al hombre con dios,
a través de un acto que es propio de natura.

La figura alegórica que en el _Roman_ sintetiza la asimilación
de las conductas descritas por Ovidio es Falso Semblante. He
aquí su origen:

> Engaño engendró a Falso Semblante, que se apodera de los corazones,
> / al cual concibió con Hipocresía / que es dama pérfida y
> muy traicionera. / Toda la crianza de Falso Semblante / vino
> de su madre, esa puerca hipócrita, / que en todo lugar vive
> del engaño / valiéndose siempre de la religión. / [Jean de
> Meun, _Roman de la rose_, vv. 10468-10475]

Falso Semblante es cierramente el engaño, la hipocresía, un personaje
nefasto que el Ejército de Amor rechaza, conformado como está
por valores positivos tomados del _Roman_ de Lorris. Y, sin embargo,
será aceptado como compañero de Forzosa Abstinencia. Es el extremo
al que ha de recurrir Amador para librarse de ésta, que es un
estado contra la naturaleza. De este modo, hipocresía y engaño
son dos formas para cumplir con el misterio de amor y restituir
el orden divino, verdadero y natural de las cosas.

Falso Semblante tiene su correspondiente femenino en la vieja
encargada de vigilar a Buen Recibimiento (el rasgo femenino que
se abre a los hombres). Uno y otro harán una exposición, extensa,
de las recomendaciones que Ovidio indica en el _Arte de amar_
para la conquista de hombres y mujeres. La diferencia, sin embargo,
entre De Meun y Ovidio es el amor que transforma la erótica ovidiana
en una forma de cortesía. Las maneras corteses ya no serán muestra
de humildad y vasallaje, sino de astucia y engaño, porque el
principio ya no es la servidumbre de amor, sino la conquista
de la persona amada. En esta medida, De Meun vuelve a plantear
las relaciones del hombre y la mujer en términos de una relativa
igualdad, donde el amor no es el resultado del reconocimiento
del otro como superior a uno mismo sino como parecido y cuya
aceptación debe ser obrenida voluntariamente y no por violencia
de una u otra parte.

Respecto a Ovidio, en la medida en que el conflicto, el problema,
la determinación de la sustancia ética ya no es la elección entre
deseos vanos y físicos, sino entre un deseo natural, y por ende
divino, y otro contra natura y humano, la estructura de la conducta
se modifica. En el _Roman_ ya no se persigue una estética de
los sentidos dirigida y emanada del conocimiento de sí, como
conocimiento de las virtudes y defectos para hacer efectiva la
realización de los deseos físicos, que reditúa en la obtención
del placer. Una conducta que es, ante todo, erótica.

En el _Roman_, conocimiento de sí significa otra cosa: conciencia
de libertad. A través de ella, la conciencia de la misión teúrgica
del hombre como reconciliación del hombre con la naturaleza.
Me explico:

> A los animales, y en esto no hay duda, / que están desprovistos
> de razón, / les está negado conocerse. / Si les fuera dado
> poder hablar / y usar la razón convenientemente / para así
> poder tener buen sentido, / saldrían los hombres muy perjudicados.
> / Jamás el caballo, el de buena estampa, / permitir podría
> el que lo domasen / ni lo cabalgase ningún caballero… / La
> persona, en cambio, de razón dotada / tanto si es humano como
> si es divina… / si no se conoce, como tales bestias, / no
> debe pensar que este grave fallo / que lo vuelve loco no le
> es imputable, ya / que está provisto de razonamiento; / ni
> debe buscar nada en que excusarse, / pues puede emplear su
> libre albedrío. / [Jean de Meun, _Roman de la rose_, vv. 17793-17862]

En estos versos del _Roman_ la violencia antinatural hacia los
animales, su utilización en labores que no son de natura, aparecen
como resultado de que no están provistos de razón y, en consecuencia,
carecen de conciencia de su propia libertad. El hombre, en cambio,
goza de ese privilegio y ello le permite elegir entre esclavitud
y libertad, que junto con fecundidad y gratuidad, es una de las
características de la operación de natura. Es decir, es la conciencia
que hace posible la elección de la condición puramente humana
o su naturaleza como pieza de la obra divina. En otras palabras,
el conocimiento de sí establece la diferencia entre un hombre
que actúa contra natura y otro que se reconcilia con ella a través
de él mismo.

Las mismas formas ovidianas son en De Meun un acto voluntario
de generar una estética de los sentidos, en cuyo caso, sin embargo,
no es el autodominio, sino la reconciliación con natura y dios,
una educación de la sensibilidad para la percepción del espíritu
en la carne.

# Segunda parte. \
  El cuerpo

# I. El peligro de las emociones

## 1. _La medida del placer_

++En su++ _Tratado de las pasiones del alma_ Descartes establece
una clara distinción entre tres categorías de percepciones: las
que «referimos a los objetos situados fuera de nosotros» (Art.
23), las «que referimos a nuestro cuerpo» (Art. 24) y las «que
referimos a nuestra alma» (Art. 25). Las sensaciones corporales
son múltiples:

> Las percepciones que referimos a nuestro cuerpo o a alguna
> de sus partes son las del hambre, la sed y las de los demás
> apetitos naturales, a los que se puede añadir el dolor, el
> calor y otras afecciones que parece que sentimos en nuestros
> miembros y no en los objetos situados fuera de nosotros [René
> Descartes, _Tratado de las pasiones del alma_, p. 43).

En una suerte de paradoja, por tratarse de las «pasiones del
alma», Descartes circunscribe al cuerpo una serie de sensaciones
que hasta antes de él habían sido consideradas como propias del
alma por el hecho de ser capaces de poner en tela de juicio el
equilibrio del alma misma y, a fines de la Edad Media, su salvación
e, incluso, la conservación de la vida.

En la «provisionalidad» ética del pensamiento cartesiano, el
examen de los asuntos morales ocupa el último lugar entre los
temas de reflexión y a ellos, cierramente, Descartes no llega.
Eso impide conocer qué consecuencias problemáticas, en cuanto
al modo en que se plantean las relaciones del hombre con el placer,
tiene esta distinción entre ámbitos de la percepción.

Sin embargo, es evidente que a partir de Descartes, la forma
de problematizar placer y dolor ya no podrá ser la misma que
acompañó las reflexiones desde el helenismo hasta el renacimiento.
Ante todo porque en Descartes el cuerpo ha dejado de ser uno
de los elementos constitutivos del alma. Adicionalmente, el orden
natural y la satisfacción de los deseos físicos han perdido las
connotaciones que apenas unos siglos y años antes tenían en De
Meun y Boccaccio. El examen cartesiano del hombre y del mundo
no problematiza el hecho de percibir placer o dolor. Sólo muestra
cómo siente y cuáles son los mecanismos que producen esas percepciones.

> Mlle. de l'Espinasse: ---Doctor, mediráis.

> Bordeu: ---Es cierto.

> Mlle. de l'Espinasse: ---¿En qué meditáis?

> Bordeu: ---A propósito de Voltaire.

> Mlle. de l'Espinasse: ---¿Y bien?

> Bordeu: ---Pienso en la manera en que se hacen los grandes
> hombres.

> Mlle. de l'Espinasse: ---¿Y cómo se hacen?

> Bordeu: ---Como la sensibilidad…

> Mlle. de l'Espinasse: ---¿La sensibilidad?

> Bordeu: ---O la extrema movilidad de ciertas fibras de la
> red es la cualidad dominante de los seres mediocres.

> Mlle. de l'Espinasse: ---¡Ah, que blasfemia, doctor!

> Bordeu: ---Me lo esperaba. Pero ¿qué es un ser sensible? Un
> ser abandonado a la discreción de su diafragma. Que una palabra
> chocante afecte su vído, que un fenómeno singular afecte su
> ojo, y ya tenemos un rumulto interior que se levanta, todas
> las briznas del haz que se agitan, el estremecimiento que
> se expande, el horror que sobrecoge, las lágrimas que corren,
> los suspiros que sofocan, la voz que se interrumpe, el origen
> del haz que no sabe qué sucede; se acabó la sangre fría, se
> acabó la razón, se acabó el Juicio, el instinto, los recursos…
> [Denis Diderot, _Escritos filosóficos_, pp. 89-90].

En 1769 Dideror pone el diálogo precedente en boca de los personajes
del _Sueño de D'Alembert_. En él hay dos ideas que se han rornado
dominantes: un sentido de la «coexistencia» de nuestro cuerpo@note
que llega a tener una «conciencia» aurtónoma (que afecta al ojo,
al oído, no al alma o al sujeto en su conjunto), y una clara
dependencia de la conducta humana a partir de la disposición
del cuerpo.

La tesis desarrollada por Dideror en el _Sueño_ y posteriormente
en el _Diálogo entre Diderot y D'Alembert_, postula que cada
parte del organismo es un centro de percepción sensible que «renía
su yo antes de la aplicación» (de unirse a otras partes corporales).
Es decir que tienen la capacidad de sentir y de normar su conducta
por aquello que siente, una suerte de auronomía que no pierde
al integrarse. La idea predominante es precisamente esa independencia
de la percepción que tiende a organizarse en un centro receptor,
cuando las fracciones orgánicas se conglomeran, pero sin perder
por ello su autonomía.

El símil que utiliza Dideror es el de la araña en su tela. La
araña viene a constituir el centro receptor y su telaraña un
fino haz que lleva, desde ese centro, a cada una de las partes
del organismo de manera parecida a como hoy entendemos el sistema
nervioso. Así, la sensibilidad se encuentra anudada en una parte
específica del cuerpo, que transmite al centro un mensaje para
que éste lo organice junto con los mensajes restantes (cf. Denis
Diderot, «Coloquio entre Diderot y D'Alembert», en _Escritos
filosóficos_, pp. 58 ss.). Lo crucial, sin embargo, es que no
es el centro, sino los componentes individuales que se han organizado,
aquello que propiamente siente.

> Mlle. de l'Espinasse: ---¿Pensáis seriamente que el pie, la
> mano, el corazón, tienen sensaciones particulares?

> Bordeu: ---Lo pienso…

> […]

> Bordeu: ---¿Razón?, pues que discernimos en gran parte. Si
> esta infinita diversidad de tacto no existiese, sabríamos
> que experimentamos placer o dolor, pero no sabríamos a dónde
> referirlos. Se haría preciso el recurso de la vista. Ya no
> sería cosa de sensación sino de experiencia y de observación
> [Denis Diderot, «Coloquio entre Dideror y D'Alembert», en
> _Escritos filosóficos_, p. 55].

Son el brazo, la pierna, los genitales, el pecho, los que experimentan
placer y dolor. Ya no es el alma, ni siquiera el centro del haz,
que es sólo receptor de información. El que los experimenta es
el cuerpo.

El paso dado desde Descartes hasta Diderot ha sido decisivo para
toda reflexión sobre la experiencia del placer. En principio
ya no es más un asunto del alma sino del cuerpo y, por ende,
la relación del hombre con el placer ya no es la relación voluntaria
consigo mismo, sino con uno de sus costados: el cuerpo.

La conciencia de esta «autonomía» del cuerpo, en cuanto éste
es el que experimenta placer ---y que tiene el buen gusto de
comunicarlo al centro del haz--- presupone un cierto grado de
independencia de cada parte frente a la totalidad, que obliga
a ésta a responder de una u otra manera, según lo exija lo percibido.
Para Diderot, el centro del haz es sólo un organizador de prioridades,
a partir de la intensidad de los mensajes que los distintos puntos
del haz envían y de su propia fortaleza para hacer que se sigan
esas prioridades.

En efecto, «si no hay más que una conciencia en el animal, hay
una infinidad de voluntades; cada órgano tiene la suya» (Denis
Dideror, «Coloquio entre Diderot y D'Alembert», en _Escritos
filosóficos_, p. 80). El estómago puede querer alimentos que
el paladar no quiere y viceversa; es el caso, por supuesto, de
quien padece úlcera péptica y está obligado a hacer dieta. La
función de haz es la de administrar esos deseos distintos, esa
necesidad de sal del paladar y la imposibilidad de tomarla porque
se tiene la presión alta.

El animal está bajo el despotismo cuando «el origen del haz manda,
y todo el resto obedece». Vivirá la anarquía cuando es «la imagen
de una administración débil, en la que cada uno inclina hacia
sí la autoridad del amo» (Denis Diderot, «Coloquio entre Diderot
y D'Alembert», en _Escritos filosóficos_, pp. 82-83). Las reacciones
del hombre, sus formas de conducta, están determinadas, pues,
sobre todo, por la naturaleza de su cuerpo, como por la fortaleza
del haz, que deriva, precisamente de su constirución corporal.
Así, en sueños aún, D'Alembert expresa: «¿La disolución de las
diversas partes no origina hombres de diversos caracteres? El
cerebro, el corazón, el pecho, los pies, las manos, los testículos…
¡Oh, cómo simplifica esto la moral!» (Denis Diderot, «Coloquio
entre Dideror y D'Alembert», en _Escritos filosóficos_, p. 49).

Son tres las consecuencias prácticas que origina este cambio
en el eje de las reflexiones sobre el placer. La primera es que
pasará a ser entendido como una suerte de proeza física, lo mismo
en la obrención que en la abstención de placeres. En segundo
lugar, hará su aparición un mayor número de variedades cromáticas
de placer que dependen de la posibilidad de incrementar el estímulo
sensorial. Y, tercero, las conductas humanas quedan asociadas
a un tipo, en la medida en que están unidas a un cuerpo específico.

En el propio _Cologuio entre Diderot y D'Alembert_, los personajes
discuten los beneficios que podrían procurar a la humanidad el
cruce del hombre y la cabra, con el fin de obtener una raza de
siervos mejor dotados físicamente para realizar los trabajos
pesados. A esa idea Mlle. de I'Espinasse opone a Bordeu, que
defiende el intento de cruce, el inmoderado apetito sexual que
tendrían tales hombre-cabras. Bordeu replica que, ciertamente
no serían muy morales (Denis Diderot, «Coloquio entre Diderot
y D'Alembert», en _Escritos filosóficos_, pp. 109 ss.).

La discusión gira en torno a un par de temas bien definidos:
la construcción de un «cuerpo» de siervo y en los rasgos que
tendría su conducta. El cruce produciría el cuerpo de un siervo
no sólo dotado para los trabajos que le son propios, sino incluso
un ser humano que únicamente podría ser un siervo. El énfasis,
sin embargo, está puesto en el hecho de que estos seres serían
inmorales no por rebasar los límites de su propio autocontrol,
sino por estar dotados de un apetito sexual que rebasaría, por
mucho, los límites de cualquier ser humano.

El exceso es concebido, entonces, como el hecho de rebasar la
capacidad física corporal de placer o dolor. En conformidad con
esta idea, es claro que el placer ha pasado a ser mesurado como
cantidad que, a diferencia del nulo gasto que implica el que
se defina como ausencia de dolor, está puesta en relación con
la naturaleza del cuerpo que lo busca o lo recibe. De esta forma,
no sólo hay posibilidades de incrementar el placer ---la fama
del amante de Casanova radicará en su fortaleza para repetir
el coito---,@note sino que habría cuerpos para los que el exceso
aniquila, como en el caso de la presidenta Tourvel de _Las amistades
peligrosas_.

La simplificación de la moral de la que habla entre sueños D'Alembert
en el pasaje que hemos citado antes, consiste en el hecho de
comprender el vínculo entre cualidades del cuerpo y las del carácter.
Todo el _Coloquio de Diderot…_ va tornándose un alegato en contra
de la moralización de los hechos del cuerpo, bajo el supuesto
de que la moral instituye arbitrariamente límites a la conducta
de los hombres que no corresponden a sus necesidades físicas.
Algo que, cierramente, recuerda la lucha del amor natural contra
las costumbres que limitan su transcurrir.

![Dibujo de Borel y grabado de Elluin para _Teresa filósofa_, atribuida a Diderot, 1785.](../img/img5.jpg)

El diálogo final entre Bordeu y Mlle. de l'Espinasse, en que
se discuren los beneficios del coito y la masturbación, transcurre
entre la alarma de la dama ---por los temas impropios de la conversación---
y los esfuerzos del médico por mostrar cómo los prejuicios morales
atentan contra la naturaleza y, por ende, contra la salud y la
vida.

> Todo lo que es no puede ser ni contra natura ni fuera de la
> naturaleza, y no exceptúa ni siquiera la castidad y la continencia
> voluntarias, que serían los primeros crímenes contra natura,
> si se pudiera pecar contra la naturaleza, y los primeros crímenes
> contra leyes sociales de un país en el que se pesen la acciones
> con otra balanza que la del fanatismo y el prejuicio [Denis
> Diderot, «Coloquio entre Diderot y D'Alembert», en _Escritos
> filosóficos_, p. 108].

En realidad, Diderot no es ajeno, como no lo son los hombres
del siglo +++XVII+++, a la tesis del mandato de natura. La sensibilidad
en el texto del _Coloquío_ tiene los mismos componentes que constituyen
la tesis del mandato. No hay en la exposición teórica de Dideror,
el reconocimiento de una necesidad de conciliación, pero se reconoce
en el cuerpo una serie de voluntades independientes a la voluntad
que se forma a partir de la totalidad de las partes sensibles,
lo que vuelve la relación entre unas y otra, en un proceso de
administración de intereses. Pero al mismo tiempo ---y más allá
de que Diderot utilice la palabra naturaleza--- esas voluntades
del cuerpo establecen un orden que debe ser atendido por la voluntad.

No está ya en juego aquí ni el ser ni la libertad del sujeto,
y la naturaleza ha sido ampliada hacia el sentido de «todo lo
que tiene lugar en el mundo». No están presentes, pues, los valores
troncales de la Edad Media. Pero ello no implica que la concepción
del mandato como modo de sujeción, no esté presente. En realidad,
adquieren una nueva función. Ya no se cumple con el orden divino
cuando se acata la naturaleza, sino con el cuerpo. Es decir,
ya no se ponen en riesgo ser y libertad, sino la salud y la vida.

Si con la castidad, la sodomía, la castración o el matrimonio
se perdía, según De Meun, la posibilidad del paraíso, un castigo
que afectaba al sujeto en su conjunto (alma / cuerpo) lo mismo
en la tierra que tras la muerte, estos mismos hechos (si exceptuamos
el matrimonio al que no alude Diderot) aparecen como causantes
de enfermedad y, señaladamente, de enfermedad física.

Bordeu, en el mismo diálogo, pone el ejemplo de la hija que,
al despertar a la pubertad, muestra los síntomas de la «abundancia
seminal» y de su flujo. La joven está enferma, se marea, se debilita,
tal vez enloquezca. El médico se pronuncia: es conveniente que
se masturbe. Entonces, al darle salida a la vida, la salud se
restablece.

Casanova narra un caso similar en sus Memorías. Durante su estancia
en Venecia conoce a una mujer joven, virgen aún, a la que se
le practican sangrías regularmente para evitar asfixias y desmayos.
Casanova cae en la cuenta de que la regularidad coincide con
la regla y que las sangrías, según sostiene, vienen a compensar
en el cuerpo de la joven un exceso de sangre, la misma que debería
expulsar reglando. Generoso, Casanova acude a su socorro y, tras
acostarse con ella, le restituye la salud.

El siglo +++XVIII+++ reconocerá como causa de enfermedad la detención
oel desvío del libre transcurrir de la naturaleza, contrariando
los mandatos del cuerpo. Ello implica la cuestión, trascendental
para la medicina moderna, de que la enfermedad se resuelve en
el ámbito del cuerpo. Paralelamente, llegará a la conclusión
que el carácter de la persona, e incluso sus hábitos, dependen
de la constitución de su cuerpo y que, incidiendo en éste, se
incide en el carácter y la conducta.

Pero sobre todo, y retornando al asunto del placer, esta concepción
refleja un cambio en aquello que inquieta del placer, una transformación
de la pregunta sobre la ontología del deseo. De hecho, se abandonará
la preocupación por aquello que vincula deseo y placer con su
realidad, o con el orden divino de la naturaleza, para fijar
la mirada en la fuerza que lo convierte en la base de las emociones,
y con la que puede configurar el carácter, los hábitos, la salud
y la enfermedad; el exceso o la mesura.

El problema de construcción de la subjetividad en el siglo +++XVII+++
se plantea, entonces, como de qué forma dar al cuerpo lo que
pide, sin que ello signifique estar a su merced, que altere la
conducta. Esto es, ser dueño de un gobierno fuerte que, al mismo
riempo, atienda todas las necesidades del cuerpo. Un amo que
sea, también, un esclavo.

La respuesta será necesariamente doble, sin que ello implique
un desdoblamiento del sujeto. Al contrario, lo que veremos nacer
es una doble intencionalidad, y una doble estructura dentro de
la subjetividad del siglo +++XVIII+++, que parte, primero, del
control de las emociones, pero sólo para crear el ambiente propicio
para atender al deseo.

## 2. _El valor del exceso_

++El libro++ de Choderlos de Laclos, _Las amistades peligrosas_,
puede leerse como la historia de un enfrentamiento entre el modo
de subjetividad heredado y el nacimiento de nuevas estrategias
del yo, que responden a esta nueva visión del ser humano.

En la obra, el vizconde de Valmont y la marquesa de Metruil,
protagonistas de _Las amistades_, encarnan una y otra forma de
subjetividad. Y lo interesante es observar cómo, desde la perspectiva
de Laclos, la inclinación por el amor natural de Valmont es ineficaz
frente a la depuración del placer por el exceso, que define a
la marquesa.

Pero comencemos con el vizconde. En la novela aparece, a primera
vista, como un hombre que pública y abiertamente, reconoce ser
un cortesano libertino. En otros términos, su fama pública corresponde
a su vida personal. Es a partir de ese carácter que se lanza
a conquistar a la presidenta Tourvel, a la sazón modelo de la
mujer virtuosa. No se trata de un sinsentido, Valmont busca con
ello una intención precisa: encontrar para su observación…

> […] a una mujer delicada y sensible, que sólo se entregara
> por su amor, y que, incluso en el amor, no viese más que a
> su amante, cuya emoción, contrariando el camino ordinario,
> partiese siempre del corazón, para llegar a los sentidos;
> a la que he visto, por ejemplo, salir del placer desconsolada
> para un momento después recuperar la voluptuosidad con una
> palabra que respondía a su alma. Finalmente, había de reunir
> también el candor natural, insuperable ya por el hábito de
> entregarse a él, y que no le permite disimular ninguno de
> los sentimientos de su corazón [Choderlos de Laclos, _Las
> amistades peligrosas_, p. 404].

Lo que Valmont se ha propuesto es encontrar una mujer, y considera
que la presidenta reúne tal condición, para la que, al modo de
Ovidio, la entrega a un hombre sólo sea posible en tanto que
éste aparezca como legítimo para satisfacer su deseo. El atractivo
que el vizconde supone en la virtud de la Tourvel, a años luz
de la que tiene la virtud de Justine para Sade, y en franco contraste
con el libertinaje de la marquesa o la inocencia de Cecile de
Volanges,@note es que cree que su recato y disimulo se funda
en la conciencia de su deseo, el temor natural a la violación
y al incesto.

La diferencia crucial es que, siempre según Valmont, para la
presidenta gustar a un hombre es un placer por sí mismo, que
requiere de ciertos parámetros de legitimidad; en tanto que las
demás ven en el placer físico un medio para alcanzar otros fines.

Será con ella y no con otras, con las que podrá llevar a cabo
su idea de amor, según la cual, el amor es el arte de ayudar
a la naruraleza.@note Relata así Valmont su primera relación
sexual con la presidenta:

> Fue con ese candor inocente o sublime como me entregó su persona
> y sus encantos, y aumentó mi felicidad compartiéndola. La
> exaltación fue total y recíproca; y por primera vez, la mía
> sobrevivió al placer. No salí de sus brazos sino para caer
> a sus pies y jurarle amor eterno; y, he de confesarlo todo,
> pensaba lo que decía. En fin, que incluso tras habernos separado,
> no se apartaba de mí su imagen, hube de esforzarme para distraerme
> [Choderlos de Laclos, _Las amistades peligrosas_, p. 358].

No vivido, no experimentado antes, que incluso provoca inesperadas
confesiones de amor en un libertino, los amores con la presidenta
son insospechadamente únicos. De esta forma, el vizconde reconoce
en ella algo que no encuentra en el resto de sus amantes: una
acto sexual que no es distinguible del acto de amor.

Sin embargo, el significado profundo de los amoríos entre Valmont
y la presidenta es que ya no está Dios como la fuente de reconciliación
de uno mismo, ni existe en ella, particularmente, una noción
de deseo legítimo.

La empresa de Valmont está condenada al fracaso por una razón
que él mismo es incapaz de contemplar: el recato de la presidenta
no significa, de ninguna manera, un producto del conocimiento
de la naturaleza de su propio deseo. Al contrario, es manifestación
de su ignorancia, un rasgo de temor antes que de sabiduría.

La presidenta Tourvel no juega con las mismas armas que el vizconde.
Su absoluto desconocimiento del ardor sexual la hace ser más
inocente que la aprendiz de Cecile de Volanges, y será incapaz
de controlar, cuando llegue la ocasión, la naturaleza de sus
impulsos. De ahí que cuando es despreciada por el vizconde, sin
medios ni estrategias para retenerlo, simplemente se hunda en
una melancolía que acabará por destruir su vida y arrastrará
con ella la de su propio amante.

El modo de subjetividad de Valmont y el de la propia presidenta,
son ineficaces. No coinciden ni encuentran en la realidad los
elementos que la validen. Si él, a través de la presidenta alcanza
a entrever el amor como expresión de la propia naturaleza, no
puede asirlo porque ya no es vehículo de transformación de él
mismo. En suma, no sirve más.

Pero si Choderlos de Laclos ha querido que viéramos en la subjetividad
de Valmont una especie de tanto para nada, dilarándose de manera
curiosa en los detalles de las prácticas de seducción, y casi
nada en la conformación de su subjetividad; en el caso de la
marquesa de Metruil se tomará su tiempo en describir lo que con
toda justicia podemos llamar su estrategia del yo.

Hay una carta que la marquesa envía al vizconde, marcada con
el número 81, en la que hace una descripción cabal de su estrategia.
El relato es largo, pero no tiene desperdicio. Siendo aún joven,
dice ella misma, se da cuenta de la utilidad de disimular las
emociones y aquellos gestos donde éstas se traslucen a los demás.
Y será en un primer plano la manifestación de emociones el objeto
de la inquietud.

> Desde entonces ---escribe--- conseguí adoptar en el momento
> deseado la mirada distraída […] Animada por mi primer éxito,
> intenté dominar igualmente los distintos gestos de mi semblante.
> ¿Qué tenía algún disgusto? Aplicábame en adoptar un aire de
> seguridad, incluso de alegría; llegó mi celo hasta el punto
> de causarme dolores voluntarios, para hacer gala mientras
> tanto de una expresión de placer […] [Choderlos de Laclos,
> _Las amistades peligrosas_, p. 255].

El párrafo transparenta, aunque no detalla, de qué modo la marquesa
trabaja para sujetarse y alcanzar el dominio de las emociones.
Lo que busca es desterrar los sentidos de la cabeza y ello es
claro cuando reclama al vizconde que guarde sus consejos para:

> […] esas mujeres delirantes y que se dicen «sentimentales»
> cuya exaltada imaginación haría pensar que la naturaleza les
> ha colocado los sentidos en la cabeza; las cuales, por no
> haber reflexionado jamás, confunden sin cesar el amor con
> el amante; en su loca ilusión, creen que sólo aquel con el
> que han buscado el placer es su depositario; y verdaderas
> supersticiosas, tienen por el sacerdote, el respeto y la fe
> que sólo le son debidos a la divinidad [Choderlos de Laclos,
> _Las amistades peligrosas_, p. 254].

Pero aún más, reprocha a esas mujeres sentimentales, que

«engendren cartas ardientes, tan dulces y tan peligrosas de escribir,
sin miedo a confiar esas pruebas de su debilidad al objeto que
las provoca» (Choderlos de Laclos, _Las amistades peligrosas_,
p. 254). Las condena, en suma, simplemente porque no son capaces
de somerer la expresión de sus emociones al dominio de su voluntad.

No está demás destacar hasta qué punto la marquesa llega a extremos
notables, como el de causarse dolor, con tal de lograr controlar
sus emociones. El trabajo ético, en ella, no constituye una forma
de aprendizaje ni un proceso de memorización, sino de ejercitamiento.
Quien quiera que deseara, como ella, ocultar sus emociones, primero
tendría que hacer un cierto número de ejercicios ante el espejo
o en ceremonias públicas, hasta volverlo un hábito.

Lo interesante, sin embargo, es que la marquesa afirma que, tras
mucho ejercicio «mi modo de pensar me perteneció sólo a mí, y
no mostré sino aquello que me resultase útil dejar traslucir»
(Choderlos de Laclos, _Las amistades peligrosas_, p. 254). Casada
y luego viuda, la técnica hasta entonces desarrollada, enfrenta
un nuevo reto:

> Sentía una necesidad de coquetear que me reconcilió con el
> amor; en verdad que no para sentirlo, sino para inspirarlo
> y fingirlo. En vano me habían dicho y había leído yo que no
> podía fingirse dicho sentimiento; veía yo, sin embargo, que
> para conseguirlo bastaba con sumar al ingenio del escritor,
> el talento del comediante. Me ejercité en ambos géneros y
> quizá con cierto éxito; mas en lugar de perseguir los vanos
> aplausos del teatro, resolví emplear para mi felicidad, lo
> que tantos sacrifican a la vanidad [Choderlos de Laclos, _Las
> amistades peligrosas_, p. 258].

¿Pero qué busca en realidad la marquesa con todo este ejercicio
sobre sí misma?

Es fácil darse cuenta de que Choderlos de Laclos ha retratado
a la marquesa persiguiendo, precisamente, ese primer éxito de
su ejercicio antes de contraer matrimonio: hacer que sus pensamientos
sólo pertenezcan a ella. Y el mismo sentido tiene el que, alcanzada
la maestría en su arte, sus sentimientos, especialmente el sentimiento
de amor, sólo le pertenezcan a ella. Esto equivale a decir que
el fin perseguido no es otro que la construcción de la vida privada
de la marquesa, hecha por la intimidad que supone la existencia
de sentimientos no compartidos con nadie más.

Está implícita aquí la aparición de la vida privada de la marquesa,
entendida como el espacio formado voluntariamente, en el interior
del sujeto, a partir del desplazamiento de la fuente de la emoción
del ámbito de lo público. En este sentido, lo privado aparece
como producto del aurodominio y lugar de la irrestricta autarquía
y libertad, como contraste con la legalidad impuesta por uno
mismo a la vida pública.

Pero la vida privada no está formada por las emociones sino por
su fuente: el cuerpo. En realidad, el ejercicio al que se somete
la marquesa tiende a crear el parapeto para que las emociones
manifiestas sean espurias, mientras se cultiva su fuente en otro
terreno compleramente distinto. Éste es el principio de un rompimiento
que se tornará fundamental. No olvidemos que ella se encuentra
ante el dilema de dominar al cuerpo que es, simultáneamente amo
y esclavo. Y no aparece en el horizonte nada mejor que controlar
los conductos por los que domina, y abrirle, paralelamente, un
espacio para su solo dominio.

En este punto, abandonamos nuestro interés por las maneras que
conforman el parapeto, para concentrarnos únicamente en la forma
como en este nuevo terreno «interior» va construyéndose un nuevo
sujeto.

Laclos es sutil cuando permite que de Valmont sepamos detalles
de sus constantes encuentros, y en cambio, desconozcamos los
de ella. Hay, cuando mucho, algunos datos: posee una casa, distinta
a su domicilio, en la que recibe a sus amantes. Hay un encuentro
con un conocido libertino, del que se mofa poniéndole en ridículo
cuando éste llega hasta su alcoba. Finalmente, viaja al campo
con otro amante, con el que piensa romper. Es todo, en el misterio
queda la frase que define su relación con el placer: «éste se
destila por su exceso».

Será Valmont, sin embargo, aquel que nos dé una pista sobre ese
ámbito menos público. Al clasificar a las mujeres de su tiempo
señala que:

> En primer lugar, para muchas mujeres el placer es siempre
> placer, y nada más; y con éstas, cualquiera que sea el título
> con el que nos adornen, no somos nunca, sino factores, simples
> comisionados, cuyo mérito reside en la actividad, y entre
> los cuales, _el que más hace es siempre el mejor_ [Choderlos
> de Laclos, _Las amistades peligrosas_, p. 404].

No puede exigirse una idea más exacta. En el ámbito de lo privado,
en esa esfera donde rige el cuerpo, «el que más hace es el mejor».
Estamos ante la fórmula que reconoce un incremento cuantitativo
del placer. Porque ya no es el que hace, sino el que más hace.@note

Pero ¿qué tanto es esto cierto? ¿Qué tanto existe y rige la vida
privada esa lógica de la acumulación? ¿Qué prácticas son consecuentes
con esta estrategia?

## 3. _Casanova_

++La respuesta++ tenemos que buscarla en un texto de naturaleza
bien distinta a _Las amistades peligrosas_. Se trata de las _Memorias_
de Giacomo Casanova y, en particular, por su inusitada precisión,
en el pasaje conocido como «Mi aventura veneciana».

El rexto de Casanova es excepcional en varios sentidos. Se trata,
en primer lugar, de uno de los primeros ejemplos de memorias
privadas, cuya redacción supone, en principio, el develamiento
al público de aquellas sensaciones cuyas emociones correspondientes
no han sido mostradas antes, por ser precisamente privadas, y
en las que ese «develamiento» constituye el único centro de interés.

Esta particularidad, que es la que la hace original respecto
a sus antecesores inmediatos en el género@note parte del reconocimiento
de la existencia de una subjetividad desdoblada en dos planos
distintos. Casanova no repara en los acontecimientos públicos
de los que él fuera partícipe ---lo hace sólo a modo de referencia
histórica--- y se concentra únicamente en sus avatares privados.
Pero no sólo los hechos sino, que pone de relieve, el conjunto
de las emociones que los rodean y dan sentido.

Casanova enmarca su aventura veneciana en torno a una reflexión
inicial sobre el placer. La naturaleza animal, afirma en principio,
posee tres instintos básicos: el de alimentarse, el de preservar
la propia especie mediante la procreación, y la tercera, «una
tendencia insuperable a destruir a su enemigo». Y afirma:

> Las tres sensaciones de hambre, apetencia del coito y odio
> que tiende a destruir al enemigo, son en los animales satisfacciones
> habituales, que nos guardaremos de llamar placeres; no pueden
> serlo sino dentro de su propia medida, pues su raciocinio
> no va más allá [Giacomo Casanova, _Mi aventura veneciana_,
> p. 24].

En contraste, sólo el hombre, dotado de razón, es capaz de sentir
placer. Lo que ocurre, explica, es que en el momento en que participa
de la inteligencia, esas tres sensaciones mudan de condición
y se convierten en placer:

> El hombre voluptuoso que razona desdeña la gula, la lujuria
> y la venganza brutal, fruro de un primer movimiento de cólera;
> es goloso y se enamora, pero no quiere gozar del objeto de
> su amor si no está seguro de ser amado; y al verse insultado,
> no sabe vengarse hasta haber combinado a sangre fría los medios
> de venganza más adecuados para hacérsela saborear. Se sabe
> cruel, pero se consuela sabiéndose por lo menos razonable.
> Estas tres operaciones son obra del alma, que para proporcionarse
> placer a sí misma se convierte en ministro de las pasiones
> _Quoe nisi parent imperant_ (que si no obedecen, mandan).
> Sufrimos el hambre para mejor deleitarnos con los guisos;
> posponemos el disfrute del amor para hacerlo más vivo; y suspendemos
> una venganza para hacerla más asesina. Es cierto que con frecuencia
> morimos de indigestión, que nos engañamos, o nos dejamos engañar
> por sofismas en lides de amor, y que el objeto que queremos
> exterminar escapa a nuestra venganza; pero corremos de buen
> grado con ese riesgo [Giacomo Casanova, _Mi aventura veneciana_,
> p. 25].

La cita es algo extensa pero resulta especialmente rica. ¿Qué
afirma realmente Casanova? Comencemos por no pasar por alto la
coincidencia plena con la concepción de Diderort, antes expuesta,
acerca del papel de la razón como «ministro de las pasiones»
y la advertencia, que encontramos también en Diderot, a propósito
de la doble naturaleza de las pasiones: que si no obedecen, mandan.
¿No es en estos términos como ha sido planteado el problema troncal
con uno mismo en el siglo +++XVIII+++?

Pero no dejemos de advertir, rambién, la vinculación de las pasiones
con los instintos animales, que conforman el sustrato real de
las pasiones humanas, y lo que caracteriza a estas últimas: la
posibilidad de ser desplazadas. ¿No es esto mismo lo que encontramos
como mareria de todo el ejercicio de sí de la marquesa de Metruil?
Por supuesto, Casanova afirma aquí como hecho antropológico lo
que en realidad es producto de una estrategia. Pero ello carece
de relevancia. En efecto, al enmarcar en este contexto su aventura
en Venecia, lo que está es sujetando sus emociones y prácticas
a la coherencia con el ejercicio de sí que propone esta definición
de las pasiones: el control y su desplazamiento.

Lo crucial, sin embargo, se encuentra en el hecho de que el fruto
del desplazamiento de las pasiones es el placer. Su incremento
definitivo en relación con la sola sensación animal. Pero el
desplazamiento de la satisfacción es, al mismo tiempo, un medio
para ocasionar la acumulación. La superación de la inmediatez
se resuelve, al final, en un incremento cuantitativo del placer
físico real. Por un lado lo hace humano, por otro, lo incrementa.

Encontramos aquí lo que ya intuíamos en _Las amistades peligrosas_.
Al desdoblarse en dos ámbitos, la estrategia del control de las
emociones se traduce en una estrategia de su liberación, bajo
el régimen de la acumulación, en la vida privada. ¿Cuáles, son
entonces las prácticas que corresponden a esta estrategia?

Hay una práctica que ya refiere la marquesa y que encontramos
también en Casanova: el desplazamiento espacial de la vida privada.
En el caso de Metruil posee una casa, distinta de la que habita,
donde recibe a sus amantes. Casanova tiene lo propio: un casino
en Murano accesible tanto a su amante como a él, donde llevan
a cabo sus encuentros amorosos.

No es fortuito que así sea. La habitación no es, aún, un espacio
propiamente privado, en la medida en que la casa donde se habita
---el domicilio--- es un lugar público en tanto que es referencia
de quien vive ahí.

Por ello, la necesidad de desplazarse hacia un espacio que no
pueda ser asociado a tal o cual persona en particular, porque
sólo ahí es libre de actuar en intimidad. Ese espacio, equivale
aquí al fingimiento, al ocultamiento de las emociones; el restarlas
al escrutinio público.

¿Pero qué es lo que se oculta a la mirada de los hombres?

Lo que sigue no es, ni mucho menos, un recuento exhaustivo de
las prácticas privadas propias del siglo +++XVIII+++, pero se
trata de seis prácticas altamente significativas para Casanova,
en lo que se refiere a los actos de la vida privada, y que constituyen
un núcleo coherente con la estrategia del control de las emociones.
Éstas son: la anticoncepción, la pornografía, la regularidad
y la forma de realizar el coito, la relación con la amante y
la exhibición de los atributos sexuales.

Aunque existen prácticas anticonceprtivas desde la antigiiedad
clásica, éstas se valían ante todo de medios naturales ---posiciones
durante el coito, ritmo de la menstruación, preparados y brebajes,
el célebre «salto del tigre»--- cuya eficacia no siempre es óptima.
El siglo +++XVIII+++, en cambio, descubre el condón y de hecho,
aunque no muy aficionado a él, Casanova se inicia en su práctica.

El asunto es sustantivo porque se trata de un medio artificial
cuya eficacia es notablemente mayor que las anteriores. Pero,
ante todo, su significado tiene que ver no tanto con sus cualidades,
como con la preocupación por evitar el embarazo. Una preocupación,
además, fundamentalmente femenina en la época.

No es, pues, extraña, la escena en que Casanova descubre los
condones en el secreter de su amante, los hurte, dejando a cambio
un poema, y que sea ella, precisamente, la que exija su devolución.
Es bajo la responsabilidad de ella, y no la de él, que su uso
está regulado. Los versos con los que ella los reclama apuntan
muy bien sus razones:

> Cuando un ángel me…, quiero estar bien segura, / de que mi
> esposo es de la natura. / Pero para dejar su raza libre de
> especulaciones, / mi amor debe ahora mismo devolverme mis
> condones. / Así siempre sometida a su sana voluntad, / a mi
> amigo animo a F…me con tranquilidad [Giacomo Casanova, _Mi
> aventura veneciana_, p. 46).

La ucilización del condón permite la acumulación de placeres
sin que se hagan manifiestas sus consecuencias. Aquí tiene el
valor del oculramiento, en primer lugar, pero también el de la
multiplicación de los actos placenteros, precisamente por reducir
riesgos. Es aquí donde podemos notar que el cuerpo y su mandato
no tienen las mismas características de la «natura» medieval.
Al contrario, son su reverso, porque supone la satisfacción sin
seguir el curso de la naturaleza. Lo que implica el uso del condón
y la posibilidad de su aparición, depende del interés por ocultar,
interrumpir lo que lleva del cuerpo al carácter, pero resolviendo
a la vez lo que éste necesita.

De este modo, la anticoncepción es coherente con una subjetividad
que se ha propuesto dominar y ser esclava, dar al curso normal
de los acontecimientos una salida, sin que trastoque al sujeto.

La exhibición del cuerpo y la narración de escenas sexuales como
formas de estimulación son otras de las novedades del siglo +++XVIII+++,
no sólo en cuanto a la aparición de la pornografía como objeto
de consumo, sino como medio de incitación sexual.

A decir del historiador Jean Marie Goulemor: «el libro erótico
incita a una lectura que supone una violación de lo íntimo».@note
El mismo sentido tiene, a su vez, la exhibición de los atributos
sexuales, reiterativos en las _Memorias_ de Casanova, como una
práctica dirigida a producir una estimulación. La visión de la
vagina, el pene o los senos, produce el mismo efecto que la lectura
o la visión de dibujos eróticos. Produce una estimulación física
y ella una emoción, excita, y esto conforma una transformación
del momento en un instante único.@note

Es curioso observar que en el siglo +++XVIII+++ la desnudez aparece,
por primera vez, como objeto de excitación directa, cultivada
con esa intención y en función de un goce solitario. Y es necesario
recalcar que ello ocurre cuando el placer ha adquirido una naturaleza
enteramente física. Y habría que evaluar hasta qué punto, ello
es lo que posibilita esta nueva práctica erótica.

Comparado con los textos de Boccaccio y otras novelas eróticas
de los siglos +++XV+++ y +++XVI+++, en el que el peso cae sobre
el ingenio y la burla, aquí lo hace sobre la crudeza de la desnudez,
la narración reiterativa del acto y las características físicas
de los participantes. Y ello es válido sólo si atribuimos a los
cuerpos y su acción física, el que pueda generarse placer y adivinarlo
en solitario. Las imágenes ---dirá Casanova--- «parecían vivas»,
las historias, reales.

Es en esta medida en que mirar puede convertirse en un acto continuo
y excitante, mediante el puro estímulo de la visión. Por supuesto,
el _voyeur_ no está ausente de la práctica sexual de Casanova
quien, desde una habitación construida _ad hoc_, mira el apasionamiento
sexual de los amantes.

Aquí aparece no sólo la violación de la intimidad, sino la percepción
directa del hecho del placer, más allá de que no sea uno mismo
quien lo experimente. No excluyo, por supuesto, otras explicaciones
acerca del _voyeurismo_ y la pornografía, pero deseo subrayar
su aparición a partir de una concepción física del placer y su
reclusión en un ámbito distinto del público. Dicho de otro modo,
sólo puede excitarse quien adivina en la pornografía o en la
visión directa del acto, las sensaciones posibles; o quien adivina
las emociones desatadas por esa sensación, aun en el caso de
que no pueda sino sólo imaginarlas.

El acto sexual es, sin embargo, el asunto propio de la vida privada,
no por el acto mismo, sino por la polifonía de las sensaciones.
Es curioso el relaro que Casanova hace de su primer encuentro
sexual con M.M.:

> Me lancé en sus brazos ardientes inflamado de amor, del cual
> estuve dando las más vivas pruebas durante siete horas seguidas,
> no interrumpida más que por otros cuartos de hora animados
> por las pláticas más conmovedoras. _No me enseñó nada nuevo
> en lo más material de la hazaña, pero sí novedades infinitas
> en forma de suspiros, éxtasis, arrebatos, y otros sentimientos
> naturales que no se manifiestan sino en esos momentos_ [Giacomo
> Casanova, _Mi aventura veneciana_, p. 36].

En el fondo, las variantes del acto sexual son mínimas y todavía
irrelevantes desde el punto de vista del incremento del goce.
La acumulación de placeres recae en la repetición, por encima
de los modos específicos con que cada vez se lleve a cabo. No
es el _cunnilingus_ o la _fellatio_, por ejemplo, los que determinan
el mayor o menor goce. Es la manifestación de las sensaciones,
por una parte, y la repetición constante, por otra, lo que define
lo placentero.

Lo curioso es admirar como Casanova no está tanto inquieto por
la frecuencia de sus relaciones sexuales con M.M. ---que religiosamente
tienen lugar una vez por semana--- sino de cómo, una vez puestos
en circunstancia, repetir una y otra vez el acto. Las siete horas
a las que alude, más otras tantas en que seis o cuatro horas
son disfrutadas en los brazos de su amada, son una prueba de
cómo la repetición del acto constituye una factor esencial de
la acumulación del placer.

Ello es, precisamente, lo que define el portento: la capacidad
física del hombre, tanto como de la mujer, para mantener despierto
el deseo sexual durante largos periodos de tiempo, y sostener
relaciones sexuales continuas en ese lapso. Lo que vale aquí,
la estrategia de la privacidad, es el incremento del placer por
la reiteración de los actos.

Así es como debe entenderse la afirmación de Casanova, ya en
el terreno práctico, acerca de que el placer nace a partir del
desplazamiento del deseo. Es la posposición al ámbito de lo privado
del origen de las emociones, lo que establece la posibilidad
de este goce reiterado, extenuante y prolongado, que define al
placer, como placer propiamente humano, aunque al margen de la
legalidad social.

Es fundamental subrayar esta última afirmación, porque es la
que delata la doble intencionalidad, definida a partir de fines
distintos, y la doble estructura de la subjetividad de los hombres
del siglo +++XVIII+++.

Se dijo antes que no hay, propiamente, una necesidad de conciliación
en el individuo equivalente a la que plantea el origen «sobrenatural»
del entendimiento en la Edad Media. Y, en efecto, así ocurre.
El reconocimiento de la existencia de «voluntades» cuya aglomeración
constituye el cuerpo y que se caracterizan por ser «demandantes»
de satisfacción, exigen la aparición de una «voluntad» administradora,
pero no plantea ni sugiere una «ruptura» de la unidad del hombre.
Lo que sí hace es definir dos niveles de «atención» o dos esferas
de necesidades, en el que las inquietudes son distintas. Por
una parte, el ámbito de esas «voluntades» y, por otra, el de
la «voluntad general» ---para hacer explícito el vínculo de esta
concepción del individuo con la organización social. Mientras
una se rige por la «exigencia», la otra se gobierna por la moderación.

![Grabado de Chauvet para la publicación póstuma de las _Memorias de Casanova_ (1725-1798)](../img/img6.jpg)

Ahora bien, si esto no es una escisión, porque no se parte de
reconocer la existencia de dos seres distintos, sí pone de manifiesto
que existe una contradicción en cuanto a lo que es legítimo hacer
en estas dos esferas de la vida humana: la pública y la privada.
En sentido estricto, el cuerpo y, específicamente, las sensaciones
de placer y dolor, no son objeto de regulación, como sí lo son
las emociones; es decir, las sensaciones que han sido traducidas
por la voluntad general como pasiones del alma ---utilizando
el término cartesiano. De este modo, lo que es válido para las
pasiones, no lo es para las sensaciones. Se puede moderar la
lujuria, convirtiéndola en galantería, pero no la excitación.

Esto es clave para comprender, al fin, que a la visión del problema
que implica la presencia de esta contradicción, conduce a la
elaboración de una bipartición dentro de la subjetividad, por
la cual, la moderación de las pasiones, bajo un régimen de convivencia
social, permite la aparición de otro orden, el de la vida privada,
donde el objeto de dominio de la primera queda liberado. Aquí
ya no rige la mesura, sino el mandato, la exigencia cuantitativa
de satisfacción según la «naturaleza», la conformación del cuerpo.
Es en ese lugar donde «las voluntades» establecen los ritmos,
los alcances y las metas de la satisfacción.

Hay que ser cuidadosos con la forma en que adquiere una nueva
función el concepto de «naturaleza». Valmont todavía conserva
los ecos nítidos del amor natural. Pero se ha perdido el parámetro
central de referencia. El que define la función de «artífice»
del hombre: Dios. Ello no implica, claro, que se haya perdido
la religiosidad o una «muerte de dios» al modo de Nietzsche,
simplemente, ha dejado de ser el vértice de la estructura. Sin
él, lo natural se seculariza. Nartural ya no es lo que obedece
al mandato divino, sino todo lo que tiene lugar en la naturaleza,
lo que acontece dentro del mundo físico.

Sin embargo, conserva dos ideas centrales: la vinculación entre
naturaleza y cuerpo, en el hombre, específicamente la asociación
de la naturaleza con las necesidades de los órganos sexuales:
el deseo de procreación del que habla Casanova, y la idea de
que natura formula o manifiesta un «mandato», por el cual el
hombre ha de someterse. Este «mandato», sin embargo, ya no será
entendido como «orden»; es decir, una estructura de funciones
que permite la armonía y la sucesión de los individuos de las
especies, sino como una exigencia cuantitativa, que determina
el horizonte entre salud y enfermedad. La pregunta es, entonces,
cuánto goce, cuánto dolor necesita cada cuerpo.

Es fácil comprender ahora, de qué modo la sexualidad pasó a ser
el objeto central de la vida privada, y cómo comenzó a evaluarse
la cantidad de goce. Es entonces cuando se vuelve muy importante
la duración, la reiteración, la fortaleza, como valores predominantes
de la actividad sexual, que van de la mano con la aparición de
nuevas prácticas, éstas sí, muy próximas a nosotros, como la
pornografía, el _voyeurismo_, la anticoncepción. Pero sobre todo,
la prolongación de la relación sexual, su repetición tras un
buen descanso.

En estos valores se esboza, por primera vez, el contenido de
la idea de intensidad, justo en el momento en que la conciliación
del sujeto consigo mismo es desplazada de las prioridades. Pero
la intensidad no ha pasado a ser, todavía, el eje definitorio
de lo verdadero, aquello que uno debe necesariamente cumoplir
para que la vida valga la pena.

En realidad, en el siglo +++XVIII+++ la acción pública y la vida
privada guardarán una relación muy próxima. Es decir, no encontramos
la escisión que acabará por convertirlas en ámbitos compleramente
separados. Por ello es que, Casanova y la marquesa Merruil harán
uso de su sexualidad para escalar posiciones sociales. Hay contradicción
entre las esferas, pero no rompimiento. De ahí que al final,
ambos extremos tengan el mismo valor, el mismo peso en la construcción
de la vida.

Sin embargo, ya encontramos su valoración en otro orden, el de
la medición cuantitativa de las sensaciones. Y el que hace más,
pasará a ser considerado mejor que el que hace menos. Hay que
preguntarse, entonces, si ello no es posible, precisamente porque
no hay ruptura ni necesidad de conciliación, ni jerarquía entre
las esferas pública y privada de la vida. Esto es crucial porque
sugiere que la intensidad sólo vale o, mejor dicho, es estrategia
útil en el momento en que no está haciendo falta un reencuentro
consigo mismo, y sólo en la medida en que se reconozca en el
placer una gradación mesurable en términos de cantidad.

Pero intentemos resolver estas cuestiones preguntando ¿qué hace
usted Mr. Hyde?

# II. ¿Qué hace usted, Mr. Hyde?

## 1. _El otro que disfruta_

++La última parte++ del relato de _El extraño caso del doctor
Jekyll y Mr. Hyde_ de Robert Louis Stevenson, es una confesión
del propio doctor Jekyll a propósito de sí mismo:

> De modo que fue más bien la naturaleza exigente de mis aspiraciones
> y no ninguna clase de degradación particular en mis faltas,
> lo que me llevó a ser cuanto fui, lo que con un surco más
> hondo del que ordinariamente existe para la mayor parte de
> los hombres, dividió en dos, en mi ser, aquellas potencias
> del bien y del mal, que dividen y forman el dualismo de la
> naturaleza humana. En tal estado de ánimo, me vi inclinado
> a reflexionar profundamente y sin descanso esa dura ley de
> la existencia que se encuentra en las bases de toda religión
> y que es una de las causas de la desgracia de nuestra raza.
> A pesar de ser en modo tan absoluto un hombre de doble rostro,
> no era hipócrita en el sentido que se le da a esta palabra;
> las dos partes de mi yo eran ambas verdaderamente reales.
> No era más yo en realidad, cuando arrojando todo freno obraba
> vergonzosamente, que cuando a la luz del día trabajaba para
> aumentar mis conocimientos, o cuando procuraba aliviar a los
> desgraciados y a los enfermos. La casualidad quiso que la
> orientación de mis estudios científicos, que me guiaban absolutamente
> hacia lo místico y trascendental, actuase y me hiciese comprender,
> iluminándolo con mayor claridad, ese estado de perpetua lucha
> entre las distintas partes de mi ser. Cada día, y desde el
> doble punto de vista de la moral y de la inteligencia, llegaba
> con mayor seguridad al conocimiento de aquella verdad, cuyo
> descubrimiento parcial me arrastró a este espantoso naufragio:
> a saber que el hombre no es realmente una entidad, sino que
> existen dos entidades en él [R.L. Stevenson, _El extraño caso
> del doctor Jekyll y Mr. Hyde_, pp. 79-80].@note

No es necesario subrayar la conciencia y la intención deliberada
con que Jekyll crea la fórmula que lo desdoblará en ese _alter
ego_ que es Mr. Hyde.

Es bastante claro que el deseo de poder transformarse a voluntad
en una persona distinta, y cuyos actos son ajenos a la persona
que le dio origen, es una salida sumamente conveniente para quien,
como el doctor, quiere también satisfacer esa región del mal
sin perder un ápice de su honor como médico reputado.

Claro que es una ficción. Pero ello no implica que retrate con
realismo la subjetividad de los hombres del siglo +++XIX+++ y
que dentro de esa narración fantástica estén presentes las inquierudes,
las estrategias, el esfuerzo ético, que la forma.

No se trata sólo aquí de dar constancia de la aspiración voluntaria
a la escisión de la persona, sino de preguntarse cuáles son las
condiciones que lo hacen deseable. Hyde es diferente de Jekyll,
en cuanto a la moralidad y las costumbres pero, sobre todo, físicamente:
Hyde es más joven, más fuerte y dinámico; es, figurativamente
hablando, el cuerpo juvenil escondido dentro del maduro cuerpo
adusto de Jekyll.@note Inclusive, los gestos de su rostro, que
Stevenson resalta, tienen una característica central: son los
de la lujuria y la crueldad, entendida esta última como la crudeza
del que no modera sus pasiones; cualidad que se espera desterrada
de un _gentleman_.

Pero ¿es esto lo que caracteriza a Hyde? La polaridad negativa
que recae sobre él no tiene tanto que ver con la condición misma
de sus correrías nocturnas, de las que en realidad nada nos dice
el autor, ni con su moralidad. Recae sobre la naturaleza de su
cuerpo, que es donde las emociones se manifiestan sin mediación.

Hay, pues, una asociación explícita entre juventud, pasión e
inmoderación; y la convicción de que se encuentran latentes dentro
del cuerpo del hombre maduro. Pero ¿qué significa esta tríada
y su latencia dentro del cuerpo maduro? ¿Qué nos dice a propósito
del problema de la subjetividad del siglo +++XIX+++?

Comencemos por invertir el orden: dentro del cuerpo de un hombre
maduro, posicionado y reconocido socialmente, hay una «fuerza»,
llamémosla así tentativamente, que une juventud, crueldad e inmoderación
como base de su naturaleza, y que por sus características, es
de todo punto opuesta a la tendencia propia de la madurez. Ninguna
de las dos, sin embargo, es «deshonesta», ninguna es una impostura
sobre la realidad de la otra; ambas son igualmente reales. El
hombre no es uno, sino dos.

Lo que hasta el siglo +++XVIII+++ era una contradicción entre
las prioridades del cuerpo en su multiplicidad ---estómago, paladar,
brazos, piernas--- y en su unidad ---la jerarquía que establece
la voluntad general---, se nos presenta aquí como la constirución
de dos naturalezas distintas, una habitando dentro de la otra,
y buscando salir. Ya no se trata del mismo cuerpo enfrentado
en sus intereses, sino de dos cuerpos. El hombre ha pasado a
verse a sí mismo como habitado.

## 2. _Una vida secreta_

_Mi vida secreta_ es un texto anónimo. De Walther, su protagonista,
no sabemos otra cosa que la serie infinita de sus relaciones
sexuales. La crítica especializada, y a pesar de las numerosas
hipótesis, no ha podido establecer quién puede haber sido el
autor de la obra, no obstante la autenticidad histórica de la
narración.@note Dicho de otro modo, las aventuras sexuales de Walther
han sido consideradas como auténticas. Por supuesto, no es posible
tener la certeza absoluta de la realidad de cada una de las escenas
narradas en la obra, y ni siquiera es posible asegurar que todas
ellas correspondan a una sola persona que se concibe a sí misma
como Walther. Sin embargo, independientemente de que lo narrado
tenga sustento parcial o total en una vida, su sola existencia
nos ofrece material de sobra para nuestro análisis y un alto
grado de certidumbre sobre cómo abordaban en el siglo +++XIX+++
la cuestión del deseo y, particularmente, del deseo sexual.

Hay muchas aristas de la narración de Walther que pueden ser
objeto de reflexión. En primer lugar, la narración misma, su
forma, los elementos que utiliza; en segundo lugar, la forma
vital de Walther, que Antonio Escohotado ha descrito como epicúrea.
Pero también es objeto de interés lo que la novela no cuenta.
Los silencios y las omisiones. Comencemos por esto último.

Las ediciones que circulan de _Mi vida secreta_ son una selección
del original, compuesto por un total de once volúmenes de 380
páginas cada uno. A lo largo de este titánico trabajo, el autor
omite cualquier referencia a su «otra» vida. Es decir, la que
corresponde a su nombre propio, sus relaciones sociales y laborales.
Nada hay que permita saber qué hacía, en vida, cuando no estaba
en la cama. Aun cuando por exigencias de la narración ha de señalar
lugares específicos ---la casa de los padres, parientes, amigos
0, incluso la suya propia--- éstos se convierten de inmediato
en un escenario sin otra referencia que la disposición del lugar
donde los personajes se conducirán y actuarán en función del
único objeto del relato: la experiencia sexual.

En contraste con las _Memorias_ de Casanova, nada hay aquí de
las aventuras fuera del lecho, ni de las consecuencias que las
numerosas seducciones pudieran tener en otro orden de la vida.
Se trata, en términos de narración, de una omisión absolutamente
intencional del autor. Quien quiera que haya sido Walther, él
mismo optó por ver en sí mismo dos personas distintas y mutuamente
excluyentes, y nos ha contado la vida de una de ellas: la que
«fornica».

De hecho, no sólo los lugares son indiferentes a la acción que
ocurre en ellos; también es indiferente el tiempo: día de la
semana, horario. En la mayoría de los casos, se trata sólo de
una eventualidad que no enlaza con ese otro orden temporal de
las cosas. No hay actividades que determinen el momento del encuentro.
Al revés, es el acto el que define el tiempo. Esto debe ponernos
alerta en cuanto al absoluto aislamiento de su vida sexual en
relación con el resto de su existencia. Un horizonte temporal
y espacial distinto, pese a ser el mismo, del que domina la «otra
vida». Esto no supone que Walther fuera una persona ontológicamente
distinta de la que trabajaba, leía, contraía matrimonio, pero
que le era posible, como lo demuestra su relato, pensarse como
absolurtamente independiente de esa «otra» persona.

Tomando como evidencia que alguien es capaz de narrar su vida
sexual con tal discreción de su vida pública, como si fuera un
personaje dentro de otro, tenemos ya una idea del modo de conducirse,
pero no de los resortes que desembocan en él. Descubrirlos, sin
embargo, enfrenta un problema. ¿A qué responde su narración?
¿Es fruto de la represión que ordena hablar del sexo, o es la
historia de alguien que se siente cómodo en ese desdoblamiento?

En las escasas páginas que en el primer tomo de la _Historia
de la sexualidad_ Michel Foucault dedica a _Mi vida secreta_,
sobresale su interés por que sólo se narre la vida sexual y sobre
este punto giran sus comentarios:

> El anónimo autor de _My secret life_ se sometió también a
> la misma prescripción (la de la pastoral cristiana que señala
> «decirlo todo y repetirlo»); sin duda fue, al menos en apariencia,
> una especie de libertino tradicional; pero a esa vida que
> había consagrado casi por entero a la actividad sexual, tuvo
> la idea de acompañarla con el más meticuloso relato de cada
> episodio […] En lugar de ver en este hombre singular al evadido
> valiente de un «victorianismo» que lo constreñía al silencio,
> me inclinaría a pensar que, en una época donde dominaban consignas
> muy prolijas de discreción y pudor, fue el representante más
> directo y en cierro modo más ingenioso de una plurisecular
> conminación a hablar de sexo.@note

Foucault presenta _Mi vida secreta_ como parte de una larga tradición,
que nace con la pastoral cristiana, que conmina a hablar del
sexo. Pero a hacerlo de una forma que no consiste en declarar
las falras a las leyes sexuales ---adulterio, sodomía, etcétera---
sino la tarea casi infinita de decirlo todo. En esa medida, el
autor anónimo de _Mi vida secreta_ aparece ante los ojos del
filósofo francés como el más característico de los _victorianos_.

Pero Foucault no percibe el otro sentido que rtiene en el auror
de Míi vida hablar del sexo y que reafirma su carácter victoriano:
describir con toda honestidad ---porque es tan verdadero como
Jekyll lo es respecto a Hyde--- a ese otro hombre que ve dentro
sí mismo. La pregunta, sin embargo, no es tanto descubrir la
trama que hace que un dispositivo de trabajo ético como la confesión
se encuentre en la base de las memorias eróticas, sino cómo éste
opera en el marco de una sensibilidad y una inquietud distinta
a la que le dieron origen en los remotos tiempos de Gregorio
Magno.

La narración que hace Walther de su vida no es la que haría alguien
que sabe de sexo. Al contrario, para alguien mínimamente informado
del siglo +++XX+++, se trataría de un ingenuo. En la relación
que hace de sus conocimientos sobre la materia (Anónimo, _Mi
vida secreta_, vol. 1, pp. 9 ss.), se limita a describir con
extremada minuciosidad los órganos genitales, las características
de la excitación de la mujer y el hombre. Al respecto, reconoce
que «adolece probablemente de muchos errores y omisiones, porque
no soy médico; pero era todo cuanto sabía acerca del tema cuando
lo escribí. No se pretende ninguna definición académica. Es lo
que podría considerarse una descripción esencialmente popular,
adecuada a las capacidades mínimas y pensado para ambos sexos
---o si les parece--- como texto instructivo para los jóvenes»
(Anónimo, _Mi vida secreta_, vol. II, p. 10).

Walther habla desde su propia experiencia y desde su propia inquietud
por ese otro que es su sexualidad. Contrario a lo que considera
Foucault, la narración de _Mi vida_ no hace tanto hincapié en
la necesidad de decirlo todo, como en la de transmitir una constante:
la preocupación por los resortes físicos del placer.

Desde joven, a Walther le interesa sobre todo, _lo que hace fisicamente
al placer sexual_. Su interés está fijo en la vagina de sus múlriples
amantes, a las que a veces obliga y otras incita, a exhibir ante
sus ojos la plenitud del monte Venus, no como medio para excitarse,
sino como expresión de su inquietud. ¿Qué del cuerpo hace al
placer?

En su relato hay una relación manifiesta entre estímulo físico
y excitación placentera, algo que es común a la literatura pornográfica
de la época. Con gran amplitud, Walther enlaza una acción física
específica, con la correspondiente obtención de placer. De ahí
que el protagonista se preocupe personal y narrativamente en
perseguir innovaciones físicas de la sexualidad y que van, por
supuesto, en un derrotero completamente distinto al de Sade.

![_Tan bueno como una venida_. Anónimo, siglo +++XIX+++.](../img/img7.jpg)

Persigue la excitación física por medio de posiciones durante
el coito, modos de acariciamiento, _cunnilingus_ y _fellatio_;
la presencia o el acoplamiento con dos o más mujeres. Eventuales
aunque infructuosos encuentros homosexuales. En fin, la exploración
se da en el registro de los estímulos físicos que producen las
sensaciones, y la narración insiste en ellos, mucho más que en
la duración, la reiteración, etcétera, que dependerán, en realidad,
de los primeros. Lo importante no es tanto la acumulación por
cantidad, como en Casanova, sino la ampliación de la excitación
que exige mayor satisfacción. En esa medida diferir el encuentro
sexual no determina la «humanidad» del goce, sino a la inversa,
el placer estriba en satisfacer el deseo sexual con la extensión
e intensidad que éste exija. Incluso los trucos para lograr la
resistencia de las mujeres que se oponen al encuentro sexual
son palabras soeces, las declaraciones del tipo «mira que cachondo
estoy» pero, sobre todo, persecuciones, forzamientos. En realidad,
Walther confía en que todo obstáculo cae por sí solo si se logra,
por medios físicos, la excitación.@note Antonio Escohotado tiene
razón cuando afirma que:

> [El libertino victoriano] es quien es porque se sabe y quiere
> ser juguete de sus ánimos. Su voluntad es sólo complacerlos,
> rehuir el dolor de la represión, nunca legislar sus caminos.
> Por eso, no insiste jamás en una relación donde sus reacciones
> no sean lascivamente inducidas; no se empeña en fornicar cuando
> falta sinceridad genital al deseo y, desde luego, no repite
> la visita tras un encuentro insulso.@note

Estamos ahora en posición de entender qué es lo que está detrás
de la preocupación de Walther por los resortes físicos del placer,
qué guían esas prácticas específicas. La clave está en lo que
se esconde tras ese quererse juguete de sus ánimos. Porque Walther,
en la soledad de la habitación y en compañía de una mujer, se
deja llevar sólo por las órdenes de su deseo. Sin otra legalidad
que la de su excitación, producto del estímulo físico.

Por supuesto, aquí permanece la convicción de que placer y dolor
son sensaciones corporales, pero ya no son mesurables por su
cantidad ---a partir de un parámetro fijado por el cuerpo, en
el que el exceso significa demasiada falta o la demasiada acumulación---,
sino por su cualidad. Todo el interés por la estimulación física
se dirige hacia una evaluación de la excitación. Y, a través
de ella un incremento del placer. En última instancia, amarse
durante seis horas o dos, puede valer lo mismo, medido desde
la excitación.

Mientras permanece «ajeno», mientras no despierta el deseo, no
hay necesidad de someterse a él. Cuando aparece, gracias a la
estimulación física, estamos ante una lógica distinta. El hombre
ha de darle cuerpo y dejarse guiar por su demanda. Es claro que
el hombre se reconoce como sujeto de deseo cuando se sujeta a
ese mandato. Un mandato que ya no es ni divino ni originado en
voluntades que exigen organización, sino el que reclama al cuerpo
como instrumento. Y eso es lo que inquieta: que ya no es suficiente
entregarle un espacio dentro de uno mismo ---el privado---, sino
que es necesario entregarle el cuerpo.

Algo ha cambiado, y mucho desde el siglo +++XVIII+++. El deseo
continúa siendo entendido como un asunto del cuerpo que establece
una relación con el sujeto de amo y esclavo. Pero el siglo +++XIX+++
parece hacer hincapié en la condición de esclavo para que el
hombre sea, efectivamente, sujeto. Eso es lo que determina el
que sea visto como «ajeno» y excluido; pero imbuida en esta visión,
está su contrario, cuando el deseo emerge, acaba con el sujeto,
al imponerle una legalidad distinta. Lo hace, literalmente ser
otro: haciendo suyo el cuerpo y transformándolo. Así, se revela
un segundo hombre, ése más joven, pasional e inmoderado que es
el cuerpo del deseo.

Walther es, no sólo figurativamente hablando, ese «otro» de alguien
que quiere ser juguete de sus ánimos, y que para dejarlo salir
y que no interfiera con la condición del hombre al que habita,
se le ha otorgado su propio tiempo y lugar. La narración de Walther
es la del otro, el hombre privado, que se manifiesta ya no sólo
en la «interioridad» del sujeto, sino que se proyecta: la vida
privada pasará a ser la habitación, la noche, los hoteles, los
días de recreo. La otra cara del sujeto, gentilhombre, que cultiva
las relaciones sociales y trabaja.

Pero lo interesante es ver cómo esta nueva interioridad, este
dejarse llevar por el deseo, este ser otro, pasa a ser reconocido
como intensidad y más tarde como lo que hace intensa la vida.
Como un nuevo modo de sujeción para construir desde la división
a un nuevo sujeto.

## 3. _El satisfecho señor Dorian Gray_

++La génesis++ de la única novela de Oscar Wilde es un encargo
de su editor. Wilde no se complica la vida. A partir del relato
de Stevenson, _Dr. Jekyll y Mr. Hyde_, y el célebre cuento de
Edgar Allan Poe, _El retrato oval_, construye una obra única:
_El retrato de Dorian Gray_.

La anécdota es simple: Gray, un joven y excepcionalmente atractivo
muchacho es retratado por un amigo pintor. El retrato es de tal
calidad que no tardará en ser él quien acuse los efectos de la
vida y la existencia del modelo. Así, mientras Dorian conservará
por años su cuerpo juvenil, lozano e ingenuo, el retrato padecerá
los estigmas de su conducta.

El motivo vuelve a ser la doble personalidad. En este caso, la
bipartición está exhibida en el retrato. Es éste el que envejece,
éste el que toma un gesto adusto, cruel. En tanto Gray puede
vivir siendo joven toda su existencia.

De nuevo se repiten los elementos: el cuerpo juvenil dentro del
cuerpo del adulto. De nuevo las maneras de la crueldad expresadas
en el rostro.

Pero el valor de la obra de Wilde no es la reiteración de los
tópicos de Stevenson. Dorian Gray es ante todo belleza. La fuerza
de sus formas, la ingenuidad de su rostro, el candor y la alegría
de la mirada, son presentadas como la esencia misma de la espiritualidad
artística. Es esa belleza la que provoca en el pintor la necesidad
de retratarlo, y es esa misma belleza, la que hará que el cuadro
pueda robarle la vida, convirtiendo a Gray en un retrato desalmado,
es decir, sin el alma de él mismo.

Pero esa belleza, que supone al mismo tiempo una situación desahogada
en lo económico y maneras finas en el trato, es objeto también
de conflicto. Las cualidades de Gray lo colocan en la disyuntiva
de optar por el arte ---es decir una vida espiritual inclinada
a la bondad y la generosidad--- representada por Basil Hallward,
el pintor autor del retrato; o por los placeres, es decir, la
vida del _dandy_ que explota su condición social y su belleza
en favor de los placeres legítimos e ilegítimos, representados
por lord Henry Worton.

El conflicto se revela, propiamente, en el momento en que Gray
se enamora de una actriz de un vulgar teatro citadino. Al verla
actuar como Julieta en _Romeo y Julieta_, Gray cae prendido a
sus pies y ella le corresponde. Sin embargo, cuando éste vuelve
a verla actuar, ya sin el carisma de la primera ocasión, puesto
que enamorada como está ya no sueña con ser actriz sino sólo
la esposa de su amado, Dorian Gray la rechaza.

Una sola es la razón del rechazo: el descubrimiento de la mujer
vulgar, con sueños de maternidad y hogar detrás de la actriz.

> Sí ---gritó---, has matado mi amor. Antes, excitabas mi imaginación,
> y ahora, ni siquiera consigues despertar mi curiosidad. Me
> dejas completamente frío. Yo te quería porque eras maravillosa,
> porque había en ti genio y entendimiento; porque hacías realidad
> los sueños de los grandes poetas, y dabas forma y sustancia
> a las sombras del arte. Tú misma te has despojado de todo.
> Eres superficial y tonta. ¡Santo Dios, qué loco fui en quererte!
> ¡Qué necio! En este momento, ya no eres nada para mí. No quiero
> volver a verte… Tú has destruido la novela de mi vida. ¡Qué
> poco sabes del amor, si piensas que perjudica a tu arte! Sin
> tu arte no eres nada [Oscar Wilde, _El retrato de Dorian Gray_,
> p. 264].

El airado reclamo de Gray manifiesta el punto medular del conflicto.
A él no le interesa esa mujer en particular, sino en tanto que
puede dar vida, no tanto a otras mujeres, sino a emociones distintas,
a diversos tonos de intensidad. Pero sólo alcanza a ver, en realidad,
los sueños de la actriz de carpa. Será eso lo que le revele,
una vez que la abandona y después de que ella se ha quitado la
vida, la posibilidad de atender sus pasiones sin buscar en ellas
la «espiritualidad», el «arte». En última instancia, descubre
que la crueldad se marca en el rostro del retrato.

Pero el verdadero fondo es el desengaño. Gray se dará cuenta
de las ventajas que ofrece el retrato para mantener su juvenrud
intacta de las marcas de la depravación, sólo cuando sabe que
la vida artística es una fantasía irrealizable. Aquí Wilde ilustra
el conflicto romántico. Introduce en su personaje una instancia
que simboliza la unidad, la aspiración humana última, que se
revela como inalcanzable. Él no está hecho para la vida artística,
para la encarnación de la intensidad. La actriz no es un conjunto
de personajes encarnados, de promesas de vidas llenas de fuerza
y de pasión. Es sólo una actriz.

De modo que es una decepción, surgida de introducir una valoración
diferenciada entre la vida artística y la vida, que le lleva
a sustituir la primera, por los goces ilícitos para darle profundidad
a la segunda. Su desdoblamiento es fruto de una derrota, al extremo
de que Gray se ve impelido a matar al autor del retrato, el culpable
que robó a la vida el arte, para plasmarlo en el arte. Culpable
de que, gracias a que no ha cambiado físicamente desde sus amoríos
con la actriz, es asesinado por el hermano de ésta, que lo reconoce
años después.

La diferencia del desenlace entre las novelas de Stevenson y
Wilde, permiten una reflexión. Jekyll se suicida al perder el
control sobre sí mismo. Pero Gray parece conforme y en realidad
está añorando recomenzar su vida en otra parte, cuando finalmente
es asesinado. Aquí, es la sociedad la que cobra venganza, la
que ha de asumirse como representación de la imposibilidad de
éxito total de una vida escindida; si es uno mismo el que ha
logrado desarrollar una relación dual perfecta consigo mismo,
son los otros quienes se oponen a que ésta tenga lugar. Si algo
descubre el siglo +++XIX+++ es el temor a esa esfera de la intimidad.
A ese registro donde no hay legalidad ni vigilancia, salvo del
deseo. El punto de partida de todo escándalo.

Aunque escrita un año después de conocer a lord Alfred Douglas,
con el que sostendrá una relación que terminará por llevar a
Wilde a la cárcel tras perder un proceso por homosexualidad a
instancias del padre de Douglas, _El retrato de Dorian Gray_
parece describir el modo en que Wilde asumió la conducción de
su vida e incluso su destino.

Más allá de paralelismos y coincidencias entre los acontecimientos
de la vida de Wilde y la de Dorian Gray, el autor desdobla sus
preocupaciones en los personajes.

Contamos con un documento inapreciable para corroborar y examinar
esto: la epístola _In carceres et vinculis_, conocida como _De
profundis_, que Oscar Wilde escribió y envió desde la cárcel
a su querido Boise, el propio Alfred Douglas.

Ahí, y por encima de los reproches y reclamos que hace a su antiguo
compañero, encontramos el cuestionamiento profundo que Wilde
se hace a sí mismo, a partir de su encarcelamiento.

Sin embargo, es en el siguiente reproche donde encontramos esbozado
por primera vez el problema que define la conducta de Wilde.
«Me reprocho por haber permitido que dominara enteramente mi
vida una amistad no intelectual cuyo primer objetivo no era la
creación y la contemplación de las cosas bellas» (Oscar Wilde,
_In carceres et vinculis_, p. 26).

Porque, en efecto, Oscar Wilde se observa a sí mismo como un
artista; pero uno en quien «la obra depende de la intensificación
de la personalidad» (Oscar Wilde, _In carceres et vinculis_,
p- 27), por la vía de cultivarla artísticamente.

A esa vida, cuyo plan ciertamente no conocemos aún, Wilde encuentra
que se le opone «la vida» como tal, es decir, aquella que atiende
a goces y diversiones, por ello Wilde reprocha a Boise.

> Mientras estuviste conmigo significaste la absoluta ruina
> de mi trabajo artístico, y al permitir que te interpusieras
> forrosamente entre el arte y yo me atraje los mayores oprobios
> […] Tus únicos intereses eran las comidas y tus caprichos;
> tus deseos se limitaban a las diversiones, a los placeres
> ordinarios y no ordinarios [Oscar Wilde, _In carceres et vinculis_,
> p. 29].

Y, sin embargo, no es tanto el hecho del placer, sino la revelación
que esa búsqueda de placeres está ligada al odio y la crueldad,
lo que realmente se opone al temperamento del artista.

Wilde reprocha a su amante la falta de gentileza, la ausencia
de control en sus emociones, la crueldad que lo guía en busca
de los goces. De algún modo, le echa en cara la falta absoluta
de control sobre sus deseos.

La razón que Wilde se da a sí mismo para tolerar y complacer
un número cada vez más grande de caprichos de Boise es el temor
a las escenas que lo violentan. Cedió siempre por «una repulsión
artística a las vulgares escenas y las malas palabras». Pero
también, por su incapacidad para guardar resentimientos y un
enorme afecto y piedad hacia su amado (Oscar Wilde, _In carceres
et vinculis_, pp. 33-39). Y aunque creyente en que nada en lo
que cedía podía afectarlo, en la cárcel, a la hora de redactar
su epístola, reconoce que esas nimiedades le revelaron la ineficacia
de su voluntad. «Nunca reparé ---escribe--- que en la vida no
hay verdaderamente cosas grandes ni pequeñas; todas las cosas
tienen el mismo valor y la misma altura» (Oscar Wilde, _In carceres
et vinculis_, p. 35). Reconocimiento cruel que significa caer
en la cuenta de que las concesiones a los placeres, a esa vida
que se oculta y no es tan «alta» o «valiosa» como el arte, también
exige del sujeto, dominio y conducción.

Pero dejemos que sea él, aunque de manera extensa, quien establezca
los términos de la conducción de su vida y la forma en que ésta
resulta, al final, ineficaz para llevar a las metas que se había
propuesto.

> Los dioses me concedieron casi todo. Tuve genio, un nombre
> distinguido, alta posición social, brillantez, audacia intelectual;
> hice del arte una filosofía y de la filosofía un arte; cambié
> las ideas de los hombres y los colores de las cosas. Ninguno
> de mis actos ni de mis palabras dejó de asombrar a la gente
> […] Drama, novela, poema en prosa, poema rimado, diálogo sutil
> o fantástico; todo lo que toqué se volvió bello con una nueva
> forma de belleza. A la verdad misma le di lo falso no menos
> que lo verdadero como su legítima esfera, y mostré que lo
> falso y lo verdadero son nada más que formas de existencia
> intelectual. Traté el arte como la suprema realidad y la vida
> como una forma de ficción […] Junto con estas cosas tuve otras
> diferentes. Porque me deje extraviar y caí en largos encantamientos
> de insensatez y sensualidad. Me divertía ser un _flâneur_,
> un dandy, un hombre a la moda. Me rodeé de las naturalezas
> más pequeñas y de las mentes más mezquinas. Me convertí en
> el derrochador de mi propio genio y encontré un goce extraño
> en malgastar mi eterna juventud. Cansado de la sima baje _deliberadamente_
> al abismo, en busca de nuevas sensaciones. En la esfera de
> la pasión la perversidad fue para mí lo que la paradoja en
> la esfera del pensamiento. A la postre, el deseo fue una enfermedad,
> una locura o ambas cosas. Llegué a despreocuparme de las vidas
> ajenas. Tomé el placer de donde quise y seguí mi camino. _Olvidé
> que hasta la mínima acción diaria construye o destruye el
> carácter: lo que hicimos en la cámara secreta un día lo lloraremos
> a gritos en los tejados_. Dejé de ser el dueño de mí mismo.
> Ya no guiaba mi alma y lo ignoraba. Permití que me dominaras
> y que tu padre me aterrorizara, terminé en la horrible desgracia.
> Ahora sí me resta la absolura humildad [Oscar Wilde, _In carceres
> et vinculis_, pp. 104-105].

Los dos extremos de la vida de Wilde son vistos, por él mismo,
como condiciones enteramente distintas de sí. Una escisión deliberada
de su propio ser, un desgajamiento entre el artista y _Flâneur_,
entre el _Dandy_ homosexual y el feliz artista que admira a su
mujer y adora a sus dos hijos.

Pero en él, y a diferencia de Walther, sí hay hipocresía. Desprecia
las «mínimas acciones diarias», a las que olvidó estimar en su
justo valor y por las que más tarde «llorará a gritos en los
tejados». La frase revela una diferencia valorativa en Wilde
respecto a aquello que es «importante» para la existencia. Los
placeres, pues se trasluce siempre que de ello habla, carecen
de esa relevancia.

Esto establece una jerarquía entre los extremos de la vida y
que vuelve muy dolorosa la experiencia de la cárcel, a la que
llega por esas «nimiedades».

Algo que hace realmente extraordinaria la epístola de Wilde es
que muestre cómo éste abandona su pose para transitar hacia una
nueva forma de elaboración subjetiva en que la bipartición de
la personalidad aparece como problema y exige conciliación.

> Un gran amigo que me ha brindado su amistad durante diez años,
> vino a verme hace tiempo y me dijo que no creía una sola palabra
> de las acusaciones en mi contra y aseguró que me consideraba
> inocente y víctima de una siniestra conspiración tramada por
> tu padre. No pude contener mis lágrimas ante sus palabras
> y le respondí que, si bien muchos de los cargos que me hizo
> tu padre son falsos y me los echó encima sólo por indignante
> alevosía, mi vida, no obstante, estuvo llena de placeres perversos
> y pasiones extrañas. Y a menos que él aceptara y comprendiera
> enteramente este hecho no podría seguir siendo su amigo ni
> siquiera volver a estar en su compañía. Le causé una impresión
> terrible, pero somos amigos y no he obtenido su amistad _con
> base en apariencias fingidas_ [Oscar Wilde, _In carceres et
> vinculis_, pp. 175-170].

Lo que Wilde descubre en la cárcel, y cuya historia seguimos
a lo largo del texto, es lo que llamaremos aquí, el modo de la
autenticidad. Tras la derrota que en la corte le inflige el padre
de Boise, la estrategia de la doble personalidad se viene abajo,
se torna insostenible. Obligado a escuchar en público la verdad
sobre su conducta privada ---aún a pesar de las distorsiones
que el rencor o la maledicencia implican---, no tiene caso ya
empeñarse en mantenerlo en secreto. Aquellas minucias, aquello
que nadie veía, es ahora del dominio de todos pero, fundamentalmente,
le ha sido mostrado a él, que no forman parte de dos sujetos
distintos. La alternativa es entonces única: decir la verdad,
ser honesto consigo mismo y con los demás. Wilde ya no podrá
como Jekyll, ser honesto en el tugurio como en la cúspide. Por
ello, a partir del juicio que lo condena, pero que también ha
revelado públicamente su «otra vida», descubre que ya no puede
mentir, que necesita hacerse de una única vida, que además sea
intensa.

«El sufrimiento ---escribe Wilde--- a diferencia del placer,
no lleva máscara.» Y continúa: «La Verdad en el arte _es la unidad
de una cosa consigo misma: lo externo se hace interno_, el alma
hecha carne, el cuerpo animado por el espíritu. Por esta razón
no hay verdad comparable con el dolor. A veces creo que _el dolor
es la única verdad_». Y concluye: «En el dolor hay una inmensa,
extraordinaria, realidad» (Oscar Wilde, _In carceres et vinculis_,
p. 119).

En el fondo, Wilde ha descubierto, para sí mismo, una nueva práctica
de sí. Una nueva forma de constituirse en sujeto a través de
la realidad y verdad del dolor.

Una experiencia extraordinaria como la prisión ha puesto en jaque
el modelo de vida que había moderado la conducta de Wilde. Se
le muestra que, quien antes eran dos, ahora es uno solo: «El
alma hecha carne, el cuerpo animado por el espíritu». Pero no
creamos que ello significa una renuncia a sí mismo, a lo que
ha sido (cf. Oscar Wilde, _In carceres et vinculis_, p. 123).

La necesidad de conciliación lo lleva a buscar la autenticidad
como un modo de ampliar sus perspectivas sin renunciar a su pasado:
una forma nueva de construirse ex post como sujeto, a partir
de la delación de aquello que ocultaba. Una reconciliación y
un perdón.

Hacia el final de la epístola n carceres et vinculis, Wilde elabora
un proyecto de lo que llamará «la vida artística y sus relaciones
con la conducta» ---un título que cierramente echaremos de menos
entre sus obras. La idea de la vida artística tiene dos referentes
fundamentales: Verlaine, a quien reconoce como el único poeta
cristiano posterior a Dante, y Kropotkin, «un hombre con el alma
de ese bello Cristo níveo que parece a punto de surgir de Rusia»
(Oscar Wilde, _In carceres et vinculis_, p- 143). Personajes
cierramente encontrados pero unidos por dos ideas esenciales:
su estancia en la cárcel, a la que Wilde otorgará una especie
de naturaleza catártica del dolor, y su asociación con Cristo,
que no debe extrañarnos.

El día anterior a su muerte, el 30 de noviembre de 1900, Oscar
Wilde se convirtió al cristianismo. La suya no es, sin embargo,
una conversión sin matices. Al contrario, Wilde verá en Cristo
la encarnación de la «vida romántica», de ese afán de unidad.
En _De profundis_, lo que realza de la vida de Cristo es el perdón
a los pecadores: «su moral ---escribirá--- es toda simpatía,
justamente lo que debe será la moral. Sus únicas palabras habrían
sido: “Sus pecados le son perdonados porque amó mucho” (Lucas
VII, 47, 48) valdría la pena haber muerto después de pronunciarlas»
(Oscar Wilde, _In carceres et vinculis_, p. 143).

Un Cristo encarnado en Verlaine y en Kropotkin. En un poeta pederasta
y un anarquista. Extremos extraños para pensar un cristianismo
a ultranza. Dos ideas conducen por ese sendero. La primera es
que para Wilde «Cristo considera el pecado y el sufrimiento como
maneras de perfección hermosas y sagradas», y, la segunda, porque
con el arrepentimiento el hombre es capaz de «alterar el pasado».

El pecado como vía de salvación y el arrepentimiento como camino
de unidad del sujeto. Ése es el cristianismo de Wilde, que en
realidad conforman los nuevos modos de sujetarse y de entender
el placer.

Detengámonos un momento a considerar esto. Qué significa el pecado
aquí propiamente. No es la homosexualidad ni la pederastia. No
parece referirse a un tipo de acción concreta, sino a lo ocurrido
en el pasado, todo lo que antecede al momento de la conciliación.
El pecado aparece como lo que induce a ruptura 0, más simplemente,
como la condición de no conciliado. Lo que ha ocurrido al margen
del sujeto, lo que no es fruto de la conducción. El pecado es
el momento en que se reconoce la totalidad de las acciones como
fundadoras del único sujeto que se ve a sí mismo y se reconoce
en aquello que ha hecho. La autenticidad no es una condición
original, sino un estado surgido del dolor. No es lo natural,
sino un descubrimiento: el de la verdad.

La amistad cobrará valor cuando nada se sustraiga a esa amistad,
cuando ya no haya secretos. Cuando la máxima es el perdón y la
aceptación del otro en su unidad.

De la misma forma, el placer vendrá a ser valorado cuando resulte
de la unidad y no de la escisión. Cuando sea auténtico, es decir,
cuando no requiera ni de subterfugios ni de máscaras. Se aceptan
las emociones y se paga su precio.

A través de la necesidad de otorgarle al deseo un lugar, un espacio
y una personalidad distinta, se llega a entender que es precisamente
esa necesidad del deseo lo que se ha vuelto inquietante.

Ya no preocupa cómo ahoga, como nos hace juguete de los ánimos,
sino que produzca división y ocultamiento. La forma de sujetarlo,
en consecuencia, será procurando reconciliarse con aquellos deseos
que no reconocemos como propios, que no pueden emerger su rubor.
Porque con esa reconciliación el deseo, además de auténtico,
es garante de intensidad.

La nueva misión del arte será, para Wilde, «no la amplitud, sino
la intensidad […] no necesito decir que no puedo expresar mis
sentimientos en la forma que tuvieron. El arte comienza cuando
termina la imitación» (Oscar Wilde, _In carceres et vinculis_,
p. 132). Cuando simplemente es auténtico, es vida, constituye
una experiencia por sí mismo. Cuando no prerende ni ocultar,
ni esconder. Cuando no simula.

Ésa es la intensidad de la vida auténtica. La que el precio de
la reconciliación paga por sus emociones. El reproche final que
Oscar Wilde hace a lord Alfred Douglas, puede ser, en esencia,
el reproche que el hombre del siglo +++XX+++ se hace constantemente
a sí mismo.

> El hecho es que eras un típico sentimental: alguien que pretende
> darse el lujo de una emoción sin tener que pagar por ella
> […] Es falso; hasta las mejores y más abnegadas emociones
> tienen un precio. Por extraño que parezca es lo que las hace
> valiosas. La vida emotiva e intelectual de la gente común
> es algo despreciable. Así como piden prestadas sus ideas a
> una especie de biblioreca circulante del pensamiento ---el
> Zeitgeist de una época sin alma--- y las devuelven estropeadas
> a fin de semana, también tratan de obtener a crédito sus emociones
> y cuando las reciben se niegan a pagar la factura [Oscar Wilde,
> _In carceres et vinculis_, p. 174].

## 4. _El crédito de Freud_

++¿Qué podría decir++ Freud de esta afirmación? Muy probablemente,
Freud daría la razón a este Wilde sorprendido de su propio dolor,
porque a eso «sin importancia», que un día asalta la belleza
de lo sublime, ha dado un nombre nuevo: inconsciente.

De alguna forma, y valga sólo como metáfora, el desinterés de
Wilde por su conducta licenciosa, su nula valoración, equivale
a inconsciencia: simplemente, no era capaz de valorar en su justa
dimensión la necesidad de esas fuerzas a las que atendía con
constancia pero sin sentido de su importancia. De hecho, la concepción
de varias personas habitando un sujeto aparece, precisamente,
como consecuencia de vincular necesidad y deseo. Pero ese vínculo
parte siempre de concebir el deseo como algo que accede siempre
a la conciencia y por ello, es actual e inmediato y, por tanto,
objeto de manipulación. Pero esa misma necesidad del deseo poco
a poco va apareciendo como algo que escinde y destruye, que pone
al hombre social en contradicción con su vida privada e íntima,
y que convierte esas técnicas de escisión en un mecanismo fuera
de control. Es el caso de Wilde, pero también el de Jekyll y
Hyde que vendría a representar, sólo como metáfora, el proceso
de cómo una técnica de la subjetividad termina por convertirse
en una forma de regulación autónoma, independiente del sujeto,
que lo hunde en una dinámica que puede terminar por ser dolorosa
y que impone la necesidad de una conciliación entre las partes.

Freud es capaz de apreciar este fenómeno de primera mano, porque
su trabajo como médico le pone en contacto con el producto de
esta «pérdida» de control sobre el régimen que cada cual ha intentado
dar a su vida. De hecho, la idea de lo inconsciente es un diagnóstico
de este proceso en sus dos extremos. Por un lado, la necesidad
última de conciliación ---la cura--- obliga a una concepción
no escindida del sujeto. Por otro, la existencia de una dinámica
autónoma supone la realidad de un mecanismo que construye al
sujeto pero de manera imperceptible para la conciencia y que
llega a entrar en conflicto con la vida social y moral de los
hombres. El inconsciente es la solución a estas necesidades teóricas:
se trata de una entidad autónoma del sujeto, que sin embargo
lo constituye. Aún más, el inconsciente cumple el mismo papel
que el «otro» en el esquema de la doble personalidad: está sometido
a una relación con lo social en que ciertas cosas pueden alcanzar
la luz y otras no. La diferencia es que mientras ese mecanismo
de selección es lo que Walther construye de sí mismo, para Freud
es un proceso psíquico autónomo.

Y ésta es, precisamente, una de las claves en el cambio de la
percepción de las relaciones entre placer y deseo: el que constituyan,
ahora un mecanismo autónomo:

> Lo que llamamos nuestro inconsciente ---los estratos más profundos
> de nuestra alma, constituidos por impulsos instintivos---
> no conoce, en general, nada negativo, ninguna negación ---los
> contrarios se funden en él [Sigmund Freud, _Consideraciones
> de actualidad sobre la guerra y la muerte_, vol. I, pp. 1,014].

> En este sistema (el del inconsciente) no hay negación ni duda
> alguna, ni tampoco grado ninguno de seguridad […] Los procesos
> del sistema Inc. se hallan fuera del tiempo; esto es, no aparecen
> ordenados cronológicamente, no sufren modificación alguna
> por el transcurso del tiempo y carecen de toda relación con
> él […] Los procesos del sistema Inc., carecen también de toda
> relación con la realidad. Se hallan sometidos al principio
> del placer […] Resumiendo, caracteres que esperamos encontrar
> en los procesos pertenecientes al sistema Inc. son la falta
> de contradicción, la independencia del tiempo, y la sustitución
> de la realidad exterior por la psíquica […] De por sí son
> incognoscibles e incapaces de existencia [Sigmund Freud, _Lo
> inconsciente_, en _Obras completas_, vol. 1, pp. 1,052-1,053].
> {.espacio-arriba1 .sin-sangria}

Pero la autonomía del inconsciente implica en Freud la ruptura
entre su funcionamiento y los principios que rigen la vida consciente.
De ahí que lo inconsciente no acepte categorías tales como la
negación, la duda o la seguridad, y que, en consecuencia, no
tenga relación con la realidad y, por ende, con el tiempo. Su
régimen, a la vez, está conformado por el principio del placer.
Y el placer aquí es entendido como una fuerza fundamentalmente
narcisista; es decir, volcada hacia el sujeto y en contraste
con el altruismo de la vida moral, que tiende hacia una forma
de ataraxia e inmovilismo:

> Nuestra actividad psíquica tiene por objeto procurarnos placer
> y evitarnos displacer, hallándose automáticamente regida por
> el principio del placer […] el placer se halla en relación
> con la disminución, atenuación o extinción de las magnitudes
> de excitación acumuladas en el aparato psíquico, mientras
> que el dolor va paralelo al aumento o exacerbación de dichas
> excitaciones [Sigmund Freud, _Introducción al psicoanálisis_,
> vol. I, p. 243].

Freud está fundiendo aquí dos ideas. Una económica, que sabemos
propia de la mentalidad posterior al siglo +++XVII+++, que convierte
al cuerpo en un puro mecanismo de fuerzas, y una concepción del
placer como eliminación de dolor. Aunque es necesario tener cuidado.
El placer es concebido como disminución de la excitación; es
decir, la tendencia dentro del proceso económico dentro de un
mecanismo de fuerzas, hacia el reposo.

La percepción del placer que tiene el padre del psicoanálisis
no remite ya, de manera inmediata, al orden de la sensibilidad.
Placer ya no es sensación, aunque guarde alguna relación con
ella. Es sólo el régimen de operación propio de lo inconsciente
y su principio de autonomía.

No voy a entrar aquí en la discusión sobre los cambios en la
noción de placer que Freud lleva a cabo a lo largo de su vida.
Me interesa detenerme en esta fórmula primera porque, en última
instancia, es desde la que se opera este cambio en la percepción
de la noción de placer que influirá, de manera decisiva, en la
estética y la conducta de las vanguardias de principios de siglo
que son determinantes en el momento de interrogarnos sobre nuestra
propia conducta e, implíciramente, nuestra noción de placer y
la conformación de nuestra sensibilidad.

El placer en Freud tiene una formulación negativa. No sólo es
insensible, en el sentido de que no es algo propio de la sensibilidad,
sino que su propia esencia es la tendencia a la insensibilidad
del sujeto.

> Si el placer tiene el carácter negativo que Freud propende
> a adjudicarle, la pulsión libidinal hacia el placer es en
> ella misma una pulsión «thanática»: el movimiento que tiende
> a la inmovilidad; la energía que busca su autoeliminación
> en lo inerte. El deseo universal de placer es el deseo pulsional
> inconsciente (intemporal y amoral) que impele al psiquismo
> ---como fuerza de gravedad--- hacia su trasfondo primordial,
> que es trasfondo inorgánico.@note

De este modo, toda la autonomía del mecanismo inconsciente depende
de su tendencia, más que hacia la muerte, porque ésta no existe
para él, hacia lo último originario, hacia el principio mismo
de las cosas, hacia una fuerza primordial desprovista de toda
sensibilidad, de toda capacidad de percepción. Una tendencia
que debe ser negada, en principio, en función de la vida.

Por supuesto, uno de los elementos esenciales en la tesis de
Freud es que aquello que es constitutivo de lo inconsciente necesita
ser negado. Es decir, que es necesaria la existencia de fuerzas
que limiten el ámbito del mecanismo autónomo de lo inconsciente,
para que el hombre «supere» la condición insensible, prácticamente
inorgánica a la que tiende el principio del placer.

Esta negación equivale al ocultamiento de la personalidad doble
porque, en efecto, la idea de lo inconsciente en Freud parte
de excluir del ámbito de sujeción las fuerzas primarias que rigen
la vida y, señaladamente, aquellas que conforman la sexualidad.
Dicho de otra manera, Freud invierte el principio por el cual
el hombre se sujeta a sí mismo a través de plantearse el problema
de su propia sensibilidad: dentro de su tesis, el hombre es,
por un lado, sujeto de las formas de represión que anulan las
fuerzas inconscientes y, por otro, de estas mismas fuerzas en
su acomodo a partir de su represión.

Es decir, el sujeto se construye de una manera pasiva y no activa,
como producto de dos campos encontrados, el de la represión y
el de lo reprimido, cuyo choque es autónomo respecto al ámbito
de la conciencia. El desafío de Freud y, en general, del psicoanálisis,
va a estribar en cómo devolverle al sujeto una capacidad de decisión
respecto a sí mismo. Es decir, cómo reivindicar la libertad sobre
un trasfondo que construye al sujeto con autonomía respecto a
éste.

La terapia psicoanalítica, que acabará engendrando modos y formas
de conducirse como las que ha caricaturizado Woody Allen, consiste
precisamente en constituirse como un vehículo para restituir
la capacidad de decisión del sujeto respecto a lo que él mismo
es.

Dentro del discurso freudiano, la clave de ello reside, no en
modos de conducirse que constituyan formas de conocimiento de
sí, sino a la inversa, en un conocimiento de sí ---éste con rasgos
epistemológicos y ontológicos--- que por sí mismo modifica la
dirección en que se construye el sujeto: de ese choque entre
fuerzas encontradas, a una conciencia que reconoce ese proceso
y le da sentido. De ese cambio de dirección se derivan formas
de comportamiento, que hay que entender cabalmente como una conciliación
que redunda en una actitud auténtica del sujeto respecto a sí.

Hay, pues, una verdad del ser, piedra de toque de toda autenticidad,
sobre la que hay que volver para conciliarse y conducirse; si
bien no de acuerdo con ella, sí al menos en consonancia con ella.
Nada más lejos y más contradicrorio con la idea de artificio
que encabeza Ovidio.

Freud y, en realidad, todo el siglo +++XIX+++, llega a la conclusión
que la conducta no es una cuestión que dependa únicamente de
la conciencia del sujeto, por lo que propone una doctrina del
reencuentro y la reconciliación con esa fuente última y auréntica
de la conducta.

Sobre la base de ella está conformada buena parte de nuestra
sensibilidad que busca, ya no en la responsabilidad del individuo,
sino en los elementos dentro de su historia personal, sus antecedentes
familiares, sus grandes traumas, las razones de su comportamiento.
Que busca en sus olvidos, en lo no dicho, en lo ignorado, los
resortes de aquello que lo conduce siempre en contra de sí mismo.

Para nosotros, sin embargo, Freud da cuenta de algo que resulta
sumamente relevante: que los procesos de conducción de sí mismo
pueden llegar a operar de manera autónoma al sujerto. Que lo
que ayer era un artificio voluntario y útil, puede tornarse extraño
y hacer extraño, al mismo tiempo, al sujeto respecto a sí. Que
el artificio, cuando no conduce a un conocimiento de sí, cuando
no enriquece, cuando no nutre, se vuelve contra el sujero.

# Conclusión

## 1. _Dos bombres toman café_

++Dos hombres++ toman un café en la terraza de una misma cafetería.
Para cada uno, sin embargo, la sensación placentera del café
en sus labios es radicalmente distinta. Se podría decir que no
están tomando lo mismo aunque, en realidad, lo que ocurre es
que su conducta no es idéntica. Son, simplemente, dos sujetos
cuya percepción del sorbo de café es diferente.

Hay tres cuestiones que se entrelazan en esta afirmación de perogrullo:
la sensibilidad, la conducta y el sujeto. ¿Cuáles son las relaciones
que existen entre estos tres elementos de la persona que establecen
la diferencia? A lo largo de esta investigación he tratado de
destacar la trama que existe entre sensibilidad, conducta y sujeto,
a partir de una pregunta inicial: ¿qué ha sentido el hombre singular
cuando ha sentido placer?

A esta pregunta no hay sino respuestas específicas a partir de
la época y los textos expuestos. Sin embargo, el conjunto de
la exposición de esas formas de percepción del placer, de las
conductas relacionadas con ellas y de los elementos de la subjetividad
que la conforman, permiten establecer al menos tres niveles de
análisis:

* Las relaciones entre sensibilidad y conducta.
* Las relaciones entre el sujeto y la sensibilidad.
* Las relaciones entre el sujeto y la conducta.

Lo que sigue son las conclusiones generales a las que he llegado
al final de este recuento sobre la práctica ética de los placeres,
y algunas reflexiones finales del horizonte que esto nos ofrece
hoy, para la reflexión sobre las prácticas contemporáneas de
la ética.

## 2. _Sensibilidad y conducta_

++Por sensibilidad++ he entendido los movimientos del alma que
siguen a la percepción del placer y, por tanto, las distintas
maneras en que una misma sensación puede ser percibida. Esto
significa que la sensibilidad tiene un carácter dinámico gracias
al cual se modifica lo mismo durante la vida del sujeto que a
lo largo de la historia. La conducta, es decir, el conjunto de
reglas de comportamiento cuya base es estética porque se fundan
en criterios de belleza y gusto, aparece como traducción de esa
sensibilidad en el ámbito del comportamiento.

En efecto, una y otra vez, a lo largo de los capítulos, se ha
hecho hincapié en cómo, de acuerdo con una manera de entender
el placer, surgen formas de comportamiento específicas que están
ligadas a esa sensibilidad. Es el caso de Ovidio, en el que encontramos
un arte de amar dirigido a producir la obtención legítima de
placeres tanto del hombre como de la mujer, a partir de una noción
de placer como bien, cuando es la satisfacción efectiva de un
deseo físico. Pero es también la conducta amorosa que recomienda
el _Roman de la rose_ sobre otros criterios. Y lo mismo puede
decirse de la conducta de Casanova o de Walther que parten de
otra concepción del origen del placer.

¿Pero de qué forma es esta liga que une sensibilidad y conducta?
He hecho énfasis en el carácter «artístico», como resultado de
una técnica, de estas formas de comportamiento, buscando destacar
su carácter de artificio o; utilizando un término más bien teórico,
su naturaleza de construcción. Con ello he querido subrayar que
el comportamiento no está enlazado con el ser, en un sentido
fuerte. Es decir, con el substrato último de la naturaleza y
que, en principio, no es efecto necesario de un modo de ser definido.
Más acorde con la idea renacentista del ser humano que aparece,
por ejemplo, en el Discurso sobre la _dignidad del hombre_ de
Giovanni Pico, la conducta es la «forma» que se da el hombre
a sí mismo, en la medida en que la naturaleza de su ser es, precisamente,
la indeterminación.

De este modo, si no es producto de una previa determinación del
ser, la hipótesis es que el comportamiento es resultado de cómo
el hombre es capaz de aprehender lo que siente; de cómo se abre
paso a través de sus experiencias para ir construyendo una imagen
de sí mismo. Así, la forma en que se ligan, en un primer momento,
sensibilidad y conducta, es el modo en que la persona se apercibe
de ciertas cosas y los términos en que esa percepción se plantea
como problema, define un rango de conducta, establece un principio
de comportamiento. Lo que ocurre con el placer es, por ello,
un ejemplo transparente: Ovidio era incapaz de percibir intensidad
o amor en la ejecución del acto sexual, como lo harían después
los personajes del Roman u Oscar Wilde. Por ello mismo, no prestaba
atención a la necesidad de repetir el coito hasta extenuarse
como Casanova o en exaltar la fidelidad como Amigo: satisfecho
ese deseo físico no había necesidad de ir más allá.

Pero la conducta es, a su vez, un mecanismo de exploración de
la sensibilidad por el cual ésta se modifica, ya sea cambiando
de dirección, ensanchando su capacidad de percepción de una sensación
determinada o volviendo a plantear la forma en que se aprehende
y conforma.

Lo que une sensibilidad y conducta no es un camino unidireccional
sino bidireccional, porque la conducta es, también, un instrumento
del sujeto para explorarse a sí mismo.

Uno de los elementos que aquí he destacado es el hecho de que
la conducta debe resultar eficaz en el terreno de los hechos,
para legitimar la propia sensibilidad. Un ejemplo burdo de esto
sería aquel que intenta caminar a través de un vidrio para comprobar
que tal vidrio ---en exceso transparente--- efectivamente está
ahí. A eso me refiero. Walther explora la naturaleza física de
las vaginas en busca de la fuente física del placer y forzará
posiciones y eventos inusuales con tal de intensificar su placer.
El poeta del _Arte de amar_, que quiere huir de las imágenes,
se preocupa porque se encela cuando sabe que su amada se divierte
con otro. Sólo en la medida en que el cuerpo sienta esa intensidad
o disminuya su deseo por una imagen, su conducta es útil y su
percepción tiene sentido.

¿Pero qué ocurre cuando esto no es así? Es el caso de Oscar Wilde.
Cuando es llevado a la cárcel por sus prácticas homosexuales,
la conducta que hasta entonces había tenido pierde eficacia y
es puesta en duda. La estrategia de la doble personalidad antes
firme y segura, va demoliéndose por sí misma, en la medida en
que Wilde reconsidera su propia sensibilidad y aprehende y da
importancia a nuevos matices, antes inobservables, como la necesidad
de conciliar los dos horizontes de su propia existencia. Es de
este modo como la conducta altera la sensibilidad, pero no todas
sus conductas. En particular, Wilde seguirá manteniendo prácticas
homosexuales hasta el último de sus días.

Lo que ocurre es que la práctica en que se prueba la eficacia
de la conducta pone a prueba, también, la validez de la sensibilidad.
Reconsiderar la conducta equivale, entonces, a reconsiderar lo
que se percibe. Dicho de otro modo, el tiempo y la práctica provee
de elementos para valorar, a un tiempo, sensibilidad y conducta,
induciendo ya sea convicciones más firmes o matices, diferencias
o cambios que afectan lo mismo a un ámbito que al otro.

Sensibilidad y conducta se ligan siguiendo dos direcciones. Se
derermina, a partir del modo en que la persona se apercibe de
ciertas cosas, un principio de comportamiento. Por la otra, este
comportamiento pone a prueba la sensibilidad misma, induciendo
rectificaciones y cambios. En el tránsito de una a otra es donde
hay que ubicar al sujero.

## 3. _Sensibilidad y sujeto_

++El nudo++ que ata a la sensibilidad con la conducta, ya sea
en una dirección o en otra es el sujeto. En el caso concreto
del placer, el sujeto se construye a partir de que se reconoce
como «alguien» que siente placer; ello implica, en primer lugar,
que es capaz de distinguir su percepción, pero, sobre todo, que
se pregunta por ella para construir el ámbito propio de la sensibilidad.

La naturaleza del sujeto es, así, la condición problemática.
La capacidad de plantear problemas, de examinar las propias sensaciones
no como estímulos simples, objetos indiferentes de la percepción,
sino como estímulos que desatan un conflicto que pone en jaque
a la persona.

La larga historia del placer que he expuesto aquí, y las otras
posibles que recorren muchos y variados caminos, sólo muestra
hasta qué punto sentir placer es un hecho problemático para la
persona. Pero también muestra cómo este hecho es el que, en principio,
construye al sujeto, puesto que a través de la forma en que formula
lo problemático del placer construye su propia sensibilidad.

Y aquí es, justamente, donde mayor énfasis he puesto: en la diferencia
radical que puede haber entre una y otra forma de sentir el placer,
de percibir el goce. La distancia histórica, en este caso, entre
sensibilidades, a partir del modo como se formula el problema
del placer; de cómo se reconoce lo que tiene de problemático
el goce. El tema que sufre variaciones una y otra vez de edad
en edad, componiendo una pieza infinita.

No hay espacio aquí, ni fue la intención de este trabajo, explorar
los elementos que intervienen en la formación de la sensibilidad,
más allá de los tópicos más generales que establecen las coordenadas
de la sensibilidad en una época dada. Porque si bien, por hipótesis,
existe la posibilidad de examinar las diferencias mínimas en
la sensibilidad entre des personas, involucrando los infinitos
detalles que las conducen a una forma muy particular de plantear
por el problema del placer, mi interés aquí era explorar lo que
guardan en común, aquello que al final, no las distancia tanto
y las vuelven traducibles ---a través de la conducta--- unas
con otras.

## 4. _Las relaciones entre el sujeto y la conducta_

++El papel++ del sujeto no termina con la construcción de su
sensibilidad. No finaliza, porque de hecho nunca acaba, al formular
los términos en que se plantea el problema del placer. Y no puede
concluir ahí porque lo que el placer hace es, en sentido estricto,
establecer un desafío. Como hemos examinado aquí, el conflicto
que los placeres ficticios presentan a Lucrecio y Ovidio, el
reto que representa la exigencia de cumplir con la cuota de placer
en Casanova y Walther, conlleva a concebir una forma de conducta
dirigida a salvar al sujeto del conflicto, a darle una solución.

En este otro extremo el sujeto se define como capacidad de sujeción.
Es decir, como la traducción de los términos de esa sensibilidad
en una conducción de sí mismo: en una conducta.

Junto con la construcción de la sensibilidad el sujeto construye
también una manera de vivir de acuerdo con ella. Es decir, la
estrategia que el individuo se propone para hacer frente a ese
reto: los medios de los que se vale: las reglas de conducta y
las razones por las que esas reglas han de ser seguidas; es también
el trabajo que ha de llevarse a cabo para cumplir con ella y
son, por supuesto, los fines que se persiguen.

La investigación se ha concentrado en las distintas formas que
esa estrategia toma en cada época y los rasgos que la constiruyen.
Es el arte de amar, pero también es el amor natural de De Meun.
Es la escisión de la personalidad en las _Amistades peligrosas_
y Casanova, es la doble personalidad en Walther y Wilde. Es la
autenticidad en Freud.

Pero lo interesante de este conjunto son tres rasgos que se destacan
al hablar de las estrategias: _a_) al responder al desafío de
la sensibilidad son un mecanismo de transformación del sujeto;
_b_) dicha transformación sólo opera en la medida en que la estrategia
prueba ser eficaz, y _c_) que al final la estrategia se articula,
basándose en una imagen del sujeto a partir de criterios de belleza
y gusto.

Tomemos este principio: la conducta es un acomodo de orden estérico
---no puede serlo de otro. modo para estar en consonancia con
la sensibilidad de la que surge--- en que la articulación de
los actos busca transformar al hombre de acuerdo con la percepción
que ha hecho de sí mismo, de lo que puede apreciar con su sensibilidad.

Comprendida así, cada actitud no constituye un hecho separado,
sino parte de un conjunto. Esto es particularmente claro según
se expuso a lo largo del trabajo. Las recomendaciones de Ovidio
en cuanto a las formas que han de tomarse durante el acto sexual,
las formas en que Walther practica el coito, la generosidad con
que debe practicarse, según el _Roman_, etcétera, no tiene valor
separadas del conjunto porque no operan de manera aislada sino
como parte de una estrategia total que busca hacer al hombre
de acuerdo con una imagen de libertad (Ovidio), divinidad (_Roman_)
o salud (Casanova).

Así, cada acto es un instrumento que lleva o no hacia esa meta.
Hay un resultado de los actos, una enseñanza, un saber que parte
de su eficacia. El _Arte de amar_ de Ovidio está siempre amenazado
si el hombre no logra superar los celos que siente cuando sabe
que su amada también tiene otros amigos, porque sólo superando
esos celos es como el hombre puede verse a sí mismo liberado
de toda dependencia de las imágenes. El ejercicio, la práctica
de la conducta, revelarán hasta qué punto es realmente factible
llegar a ser verdaderamente libre. El caso contrario es el de
Wilde. En el momento en que éste dejar de percibir como igualmente
valiosos los dos costados que ha señalado en su vida, la doble
personalidad pierde su eficacia. O se trata a uno y otro sujeto
como iguales o simplemente, deja de funcionar, genera angustia,
contradicciones y problemas en el sujeto que percibe una pérdida
de control sobre lo que él mismo es y quiere ser. Su ejercicio,
pues, muestra que no es útcil, que no es eficaz con relación
a la consecución de la imagen que el hombre ha hecho de sí.

En el caso de Wilde, la imagen del artista. Al cuestionarse esa
eficacia, al iniciar el tránsito hacia nuevas formas de conducta,
también Wilde modifica su percepción. Así, comienza a apreciar
la conciliación y la autenticidad como dos formas de fundar una
nueva conducta, y son las que dirigirán los nuevos modos de comportamiento.

Detengámonos, finalmente, en la cuestión de la imagen que el
hombre construye de sí mismo. Uso la palabra imagen para caracterizar
el fin que el sujeto pretende alcanzar al transformarse a través
de la conducta para darle una connotación sensible. No es en
vano. La imagen, es decir, el conjunto articulado de los actos
y las actitudes, es una traducción de las formas de la sensibilidad.
El amor natural corresponde a una percepción del placer que lo
asocia a la obra divina plasmada en la naturaleza. El casanova
lo es de una percepción del placer como «necesidad» y exigencia.
En ella no se aprecia tanto lo que de manera particular lleva
a ella ---el amor libre, la reiteración del coito--- como lo
que une estas prácticas con, por ejemplo, la aversión a la sodomía
o a la masturbación, en uno y otro caso: es decir, la percepción
misma del placer.

![_Profundo_. Aristide Maillol, 1939.](../img/img8.jpg)

De este modo, la imagen, la figura, el «tipo», viene a ser la
traducción de la sensibilidad en la percepción que el hombre
tiene de sí mismo y que no sólo refleja lo que es en un momento
específico de su existencia, sino lo que pretende ser.

## 5. _Ética y estética_

++Partí de la++ metodología que utiliza Foucault en la _Historia
de la sexualidad_ para conformar el cuerpo teórico de esta investigación,
pero la investigación misma concluye en un lado distinto. La
tesis de Foucault buscaba sobre todo responder al problema de
cómo el hombre llega a reconocerse como sujeto de una moral.
Para ello se propone indagar, a partir de cuatro elementos clave:
la determinación de la sustancia ética, el modo de sujeción,
la elaboración del trabajo ético, la teleología del sujeto moral;
las formas, modos en que el sujeto se reconoce a sí mismo como
tal.

Aquí, sin embargo, poco a poco hemos ido entresacando, a partir
de estos principios de método, un orden distinto donde también
operan y que revela de qué manera la trama que tejen ética y
estética es mucho más estrecha y fuerte. Quizá el punto principal
respecto a esto mismo ha sido asumir que la dererminación de
la sustancia ética no sólo es el principio constitutivo del sujeto
sino, al mismo tiempo, de su sensibilidad. La sustancia ética
así, opera también en el orden de la sensibilidad. Los otros
términos asimismo fueron mostrando cómo también se cruzan con
los principios de la elaboración de una sensibilidad que se aparece
como dinámica y por ende histórica.

Así, los elementos que conforman el segundo sentido de sujeto,
el que resulta de su sometimiento a una conducta a partir de
una estrategia de sí, fueron definiéndose por su papel en esa
transformación. Los modos de sujeción se acentúan por su función
transformadora, mientras la elaboración del trabajo ético constituye
los mecanismos a través de los cuales se prueba no sólo la eficacia
de una conducta, sino también de la sensibilidad en que se sostiene.
Finalmente, la teleología no sólo marca el fin que se propone
alcanzar el sujeto con el conjunto de sus prácticas, sino que
tiene también un carácter plástico: es una imagen.

Sin embargo, esto no significa un abandono de la preocupación
ética, al contrario. Justamente lo que ha aparecido a lo largo
de la investigación es la intuición de que la práctica ética
del sujeto es producto de un proceso complejo que vincula dos
niveles y dos direcciones: la condición reflexiva de la sensibilidad
y la expresión estética de la reflexión.

En el siguiente cuadro sintetizo los tres órdenes de la reflexión
llevada a cabo en esta investigación: el orden ético, el de la
sensibilidad y el de la estética.

![](../img/cuadro2.jpg)

En el sentido de lo que he llamado la práctica ética, el sujeto
es siempre sujeto ético. Pero como tal, conforma una sensibilidad
y expresa su reflexión a través de formas estéticas. Y éste es,
precisamente el tema central al que he querido arribar: la forma
en que la reflexión ética acaba por manifestarse en formas estéticas.
La clave aquí, es la sensibilidad, una sensibilidad que entiendo,
con las características que desde Aristóteles se le da a la facultad
imaginativa. Es decir, servir de enlace entre las facultades
superiores e inferiores, entre entendimiento y cuerpo. Pero ante
todo, el hecho de proveer la materia del pensamiento y que ésta
sea las imágenes plásticas, los fantasmas, los simulacros de
las cosas. Sobre todo porque ello permite observar, quizá de
mejor manera como la conducta y el comportamiento están referidas
no a principios, sino a imágenes que son las traducciones de
esos principios y que permiten, durante el medievo y el renacimiento
construir imágenes de las virtudes y los vicios o, en el caso
de Giordano Bruno, hacer del orden del cielo expresión del orden
de la vida, porque es ahí donde de manera más clara se ve precisamente
esa liga: la que ata símbolos con la forma de poner en práctica
normas éticas.

A partir de esto hay un horizonte inmenso que esta investigación
apenas se atreve a atisbar. Está pendiente, por supuesto, un
desarrollo propiamente teórico de este vínculo, un principio
de sistematización. Está pendiente también una indagación detallada
en cuestiones tales como aquello que conforma la sensibilidad,
los elementos a incluir como propios de esa construcción. Queda
pendiente, rtambién el vínculo entre esta intuición y el papel
que juega la represión en la conformación de las formas de conducta.
En fin, quedan pendientes.

# Bibliografía

Anónimo. _Mi vida secreta_, trad. Antonio Escohotado, Tusquets,
Barcelona, 1987. {.frances}

++Aristóteles++. _Ética nicomaquea_, versión de Antonio Gómez
Robledo, México, +++UNAM+++, 1983. {.frances}

++Boccaccio++, Giovanni. _Decamerón_, trad. Pilar Gómez Bedarte,
Siruela, Madrid, 1990. {.frances}

_Cartas de Abelardo y Eloísa_, trad. Mateo Sempere, Alianza,
Madrid, 1995. {.frances}

++Casanova++, Giacomo. _Mi aventura veneciana_, trad. Marta Pérez,
Fontamara, Barcelona, 1983. {.frances}

++Deleuze++, Gilles. _Presentación de Sacher-Masoch_, versión
de Ángel María García Martínez, Taurus, Madrid, 1974. {.frances}

++Descartes++, René. _Tratado de las pasiones del alma_, trad.
Laura Benítex, +++SEP+++, México, 1992. {.frances}

++Diderot++, Denis. _Escritos filosóficos_, trad. Fernando Savater,
Editora Nacional, Madrid, 1981. {.frances}

++Duby++, Georges. _El caballero, la mujer y el cura_, trad.
Mauro Armiño, Taurus, Madrid, 1992. {.frances}

---, _El amor en la Edad Media y otros ensayos_, 2.ª ed., Alianza,
Madrid, 1992. {.frances}

++Escohotado++, Antonio. _Historia de las drogas_, Alianza, Madrid,
1989. {.frances}

++Epicuro++. _Máximas para una vida feliz_, trad. y edición de
Carmen Fernández-Daza. {.frances}

---, _Obras_, trad. Monserrat Jufresa, Tecnos, Madrid, 1991.
{.frances}

++Foucault++, Michel. _Historia de la sexualidad_, tomo 1: _La
voluntad de saber_, trad. Ulises Guiñazú, Siglo XXI, México,
1979. {.frances}

---, _Historia de la sexualidad_, tomo 2: _El uso de los placeres_,
trad. Martí Soler, Siglo XXI, México, 1988. {.frances}

---, _Historia de la sexualidad_, tomo 3: _La inquietud de sí_,
trad. Tomás Segovia, Siglo XXI, México, 1989. {.frances}

---, _Tecnologías del yo_, trad. Mercedes Allende Salazar, Paidós,
Madrid, 1991. {.frances}

++González++, Juliana. _Ethos, destino del hombre_, +++UNAM /
FCE+++, México, 1996. {.frances}

++Goulemot++, Jean Marie. «Las prácticas literarias o la publicidad
de lo privado», en Philippe Arles, Georges Duby (dirs.), _Historia
de la vida privada_, trad. Francisco Pérez Gutiérrez, Taurus,
Buenos Aires, 1990, tomo VII. {.frances}

++Huizinga++, Johan. _El otoño de la Edad Media_, trad. José
Gaos, Alianza, Madrid, 1982. {.frances}

++Junger++, Ernst. _La emboscadura_, trad. Andrés Sánchez Pascual,
Tusquets, Madrid, 1994. {.frances}

++Kappler++, Claude. _Monstruos, demonios y maravillas_, trad.
Julio Rodríguez Púertolas, Akal, Madrid, 1986. {.frances}

++Laclos++, Chederlos de. _Las amistades peligrosas_, trad. Almudena
Montojo, Rei, México, 1991. {.frances}

++Lipovetsky++, Gilles. _El crepúsculo del deber_, 4.ª ed., Anagrama, Barcelona,
1998. {.frances}

++Lledó++, Emilio. _El epicureísmo_, Taurus, Madrid, 1985. {.frances}

++Lluli++, Ramon. _Blanquerna_, trad. Ramón Xirau, Porrúa, México,
1990. {.frances}

++Lorris++, Gillaume de y Jean de ++Meun++. _Roman de la rose_,
versión de Juan Victorio, Cátedra, Madrid, 1987. {.frances}

++Lucrecio++. _Acerca de la naturaleza de las cosas_, versión
de Rubén Bonifaz Nuño, +++UNAM+++, México, 1984. {.frances}

++María de Francia++. _Lais_, trad. Carlos Alvar, Alianza, Madrid,
1991. {.frances}

++Matiarena++, Oscar. _Michel Foucault, historiador de la subjervidad_,
El equilibrista, México, 1995. {.frances}

++Ovidio++. _Amores, Arte de amar, Sobre la cosmética del rostro femenino,
Remedios de amor_, trad. Vicente Cristóbal López, Gredos, Madrid,
1989. {.frances}

---, _Arte de amar_ y _Remedios de amor_, versión de Rubén Bonifaz
Nuño, +++UNAM+++, México, 1986. {.frances}

++Paz++, Octavio. _La llama doble_, Seix Barral, Barcelona, 1997.
{.frances}

++Platón++. _Diálogos III_ (_Fedón_, _El Banquete_, _Fedro_),
trads. Carlos García Gual, M. Martínez Hernández y Emilio Lledó,
Gredos, Madrid, 1988. {.frances}

++Rougemont++, Denis de. _El amor y Occidente_, trad. Antoni
Vicens, Kairós, Barcelona, 1987. {.frances}

++Stevenson++, Robert Louis. _The Strange case of Dr. Jekyll
and Mr. Hyde_, Penguin, Londres, 1994. {.frances}

++Schwartz++, Hillel. «El problema de los tres cuerpos y el fin
del mundo», en Michel Feher (ed.), _Fragmentos para una historia
del cuerpo humano_, trad. Carlos Laguna, Taurus, Madrid, 1992,
tomo II. {.frances}

++Starobinski++, Jean. «Breve historia de la conciencia del cuerpo»,
en Michel Feher (ed.), _Fragmentos para una historia del cuerpo
humano_, trad. Carlos Laguna, Taurus, Madrid, 1992, tomo II.
{.frances}

++Veyne++, Paul. _La elegía erótica romana_, trad. Juan José
Utrilla, +++FCE+++, México, 1991. {.frances}

---, «El imperio romano», en Philippe Arles y Georges Duby (dirs.),
_Historia de la vida privada_, trad. Francisco Pérez Gutiérrez,
Taurus, Buenos Aires, 1990. {.frances}

++Wilde++, Oscar. _El retrato de Dorian Gray_, trad. Rodrigo
Fernández, Ed. Austral, Barcelona, 1983. {.frances}

---, _In carceres et vinculis_, trad. José Emilio Pacheco, Munchnik,
Barcelona, 1975. {.frances}
