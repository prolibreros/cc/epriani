++Juliana González++, _Ethos, destino del hombre_, +++UNAM /
FCE+++, México, 1996, p. 33.

_Ibid._, p. 24.

++Ennst Jünger++, _La emboscadura_, trad. Andrés Sánchez Pascual,
Tusquets, Madrid, 1994, p. 101.

Cf. ++Gilles Lipovetsky++, _El crepúsculo del deber_, 4.ª ed.,
Anagrama, Barcelona, 1998, pp. 46 ss.

++Michel Foucault++, _Historia de la sexualidad_, tomo 2: _El
uso de los placeres_, trad. Martí Soler, Siglo XXI, México, 1988,
p. 8.

_Ibid._, p. 17.

Hay un problema fundamental en la metodología propuesta por Foucault:
las definiciones que se dan en la «Introducción» al tomo 2 de
la _Historia de la sexualidad_, sufren severas variaciones en
el momento de aplicarlas. Sin querer entrar en demasiados detalles
aquí las exponemos retomando su definición, pero explicándolas
por su uso. siguiendo para ello la interpretación que hace ++Oscar
Martiarena++ en _Michel Foucault, historiador de la subjetividad_,
El equilibrista, México, 1995, p. 76.

++Michel Foucault++, _op. cit._, tomo 2, p. 27.

_Ibid._, pp. 27-28.

_Ibid._, p. 30. Asimismo, Foucaulr establece que estos elementos
permiten discernir «lo que estructura la moral de los placeres
sexuales ---su ontología, su deontología, su ascética y su teleología»,
cf. _ibíd._, p. 37.

++Aristóteles++, _Ética nicomaquea_, versión de Antonio Gómez
Robledo, +++UNAM+++, México, 1983, III, 10, 1.118 a-b.

++Emilio Lledó++, _El epicureísmo_, Taurus, Madrid, 1985, p.
110.

++Michel Foucault++, _Historia de la sexualidad_, ed. cit., tomo
2, p. 42.

Cf. _supra_, p. 28.

++Michel Foucault++, _op. cit._, p. 55.

La idea es similar en parte a la tesis freudiana del fetichismo.
Cf. ++Deleuze++, _Presentación de Sacher-Masoch_, versión de
Ángel María García, Taurus, Madrid, 1974, pp. 35-36.

++Platón++, _Fedro_, en _Diálogos III_, trads. Carlos García
Gual, M. Martínez Hernández y Emilio Lledó, Gredos, Madrid, 1988.
255e.

Cf. ++Paul Veyne++, _La elegía erótica romana_, trad. Juan José
Utrilla, +++FCE+++, México, 1991, p. 76.

Cf. ++Ovidio++, _Arte de amar_, versión de Rubén Bonifaz Nuño,
+++UNAM+++, México, 1986, II, vv. 467-480; I, vv. 101 ss.; 1,
vv. 106-113; III vv. 112-120.

Cf. _supra_, pp. 45-46.

Cf. _supra_, p. 26.

++Paul Veyne++, _Historia de la vida privada_ I, trad. Francisco
Pérez Gutiérrez, Taurus, Buenos Aires, 1990, p. 200.

El término «Estética de los sentidos» lo utiliza Octavio Paz
a propósito del amor cortés. Sin embargo, es completamente aplicable
a Ovidio. Cf. ++Octavio Paz++, _La llama doble_, Seix Barral,
Barcelona, 1997, p. 94.

++Guillaume de Lorris++, _Roman de la rose_, versión de Juan
Victorio, Cátedra, Madrid, 1987, vv. 4055-4058.

Cf. ++Johan Huizinga++, _El otoño de la Edad Media_, trad. José
Gaos, Alianza, Madrid, 1982, pp. 161 ss.

Por ejemplo Octavio Paz en _La llama doble_, ed. cit.; ++Georges
Duby++ en _El amor en la Edad Media y otros ensayos_, 2.ª ed.,
Alianza, Madrid, 1992, y por supuesto, ++Denis de Rougemont++
en _El amor y Occidente_, trad. Antoni Vicens, Kairós, Barcelona,
1987.

Cf. ++Denis de Rougemont++, _op. cit._, p. 77.

_Ibid._, pp. 81-82.

++Georges Duby++, _El caballero, la mujer y el cura_, trad. Mauro
Armiño, Taurus, Madrid, 1992, pp. 20-27.

_Ibíd._, pp. 29-30. Es decir, que el acto no sea realizado por
lujuria y que el contenido placentero en el mismo sea el mínimo.

++Aurelio González++, _De amor y matrimonio en la Europa medieval_,
p. 37.

Cf. ++Georges Duby++, _El amor en la Edad Media y orros ensayos_,
pp. 81 y 82.

_Ibíd._, p. 83.

Natura da un listado mayor de prácticas que le atacan ---el suicidio,
la embriaguez contumaz, el ayuno, etcétera, pero que no merecen
la misma atención que estas cuatro. Para mayor referencia cf.
++Jean de Meun++, _Roman de la rose_, ed. cit.. vv. 499-500.

Cf. ++Joann Hoepner Moran++, _The roman de la rose and the thirteen-century
prohibitions of homosexualiry_, Georgetown University Cultural
Srudies. Conferencia en Internet: [www.georgetown.edu](http://www.georgetown.edu).

_Cartas de Abelardo y Eloísa_, trad. Mateo Sempere, Alianza,
Madrid, 1995, p. 111.

Cf. ++Jean Starobinski++, «Breve historia de la conciencia del
cuerpo», en Michel Feher (ed.). _Fragmentos para una historia
del cuerpo humano_, trad. Carlos Laguna, Taurus, Madrid, 1992,
romo II, p. 355.

Cf. véase _infra_, cap. 11: «¿Qué hace usted Mr. Hyde?».

Cecile de Volanges es un personaje peculiar en la novela de De
Laclos. Enamorada del caballero Danceny y amiga de la marquesa,
su educación sentimental será encargada por ésta a Valmont.

El enunciado permite ver hasta qué punto para Valmont el hombre
todavía es un «artífice de la naturaleza» y por ello se sujeta
al amor natural. Cf. ++Choderlos de Laclos++, _Las amistades
peligrosas_, trad. Almudena Montojo, Rei, México, 1991, p. 94.

Valmont añade el interés de las mujeres por el estatus social
de su amante, cuando no es el placer el que rige la relación.
Ahí, el más placentero se convierte en un más social. Habría
que cuestionarse en que ambas actitudes se corresponden y son,
incluso, traducibles: si el pago no es el placer, entonces es
posición social. Cf. Choderlos de Laclos, _Las amistades peligrosas_,
ed. cit., p. 404.

Hasta antes de la aparición de las Confesiones de Rousseau y
de las Memorias de Casanova, entre otras, el género sólo tenía
sentido cuando quien los redactaba había participado o sido agente
de un gran acontecimiento. Cf. ++Jean Marie Gollemot++, «Las
prácticas literarias o la publicidad de lo privado», en Philippe
Arles, Georges Duby (dirs.), _Historia de la vida privada_, trad.
Francisco Pérez Gurtiérrez, Taurus, Buenos Aires, 1990, tomo
VII, p. 371.

_Ibid._, p. 402.

«[…] se encontraba allí todo lo que había escrito contra la religión
los filósofos más sabios, y todo lo que habían escrito las plumas
más voluptuosas sobre la materia único objeto del amor. Libros
seductores, cuvo estilo incendiario fuerza al lector a ir a buscar
la realidad, única capaz de apagar el fuego que se siente circular
por las venas. Además de los libros, había infolios que no contenían
más que estampas lascivas. Su mayor mérito consistía en la belleza
del dibujo, mucho más que en la lubricidad de las posturas. Vi
las estampas de _portir des Chartreux_, hechas en Inglaterra,
así como las de Meursius o Luisa Sigea de Toledo, que eran lo
más hermoso que había visto en mi vida. Por otra parte, los cuadros
pequeños que adornaban el gabinete estaban tan bien pintados,
que las figuras parecían vivas. La aparición de M.M. vestida
de monja me hizo gritar. Le dije, saltándole al cuello, que no
podría haber llegado más oportunamente para evitar una masturbación
de colegial, a la que sin duda me habría forzado todo lo que
llevaba viendo desde hacía una hora» (++Giacomo Casanova++, _Mi
aventura veneciana_, trad. Marta Pérez, Fontamara, Barcelona,
1983, p. 39).

«It was thus rather the exacting nature of my aspirations, than
any particular degradation in my faults, that made me what I
was and, with even a deeper trench than in che majority of men,
severed in me those provinces of good and ill which divide and
compound mans dual nature. In this case, I was driven to reflect
deeply and inveterately on that hard law of life which lies at
the root of religion, and is one of the most plentiful springs
of distress. Though so profound a doubledealer, I was in no sense
a hypocrite; both sides of me were in dead earnest; I was no
more myself when I laid aside restraint and plunged in shame,
than when I laboured, in the eye of day, ar the furtherance of
knowledge or the reliet of sorrow and suffering. And it chanced
thar the direction of my scientific studies, which led wholly
rowards the mystic and the transcendental, reacted and shed a
strong light on this consciousness of perennial war among my
members. With every day, and from both sides of my intelligence,
the moral and the intellectual, I chus drew steadily nearer to
thar truth by whose partial discovery I have been doomed to such
a dreadful shipwreck: that man is not truly one, but truly rwo»
(++R.L. Stevenson++, _The strange case of Dr. Jekyll and Mr.
Hyde_, pp. 96-97).

La idea de un cuerpo delgado dentro de otro más grueso es una
idea común a lo largo del siglo +++XIX+++. Al respecto, véase
el ensayo de ++Hillel Schwarts++, «El problema de los tres cuerpos
y el fin del mundo», en Michel Feher, _Fragmentos para una historia
del cuerpo humano_, trad. Carlos Laguna, Taurus, Madrid, 1992,
tomo II, p. 400.

Cf. prefacio del editor norteamericano en _Mi vida secreta_,
pp. 17 ss.

++Michel Foucault++, _Historia de la sexualidad_, tomo I: _La
voluntad de saber_, trad. Ulises Guiñazú, Siglo XXI, México,
1979, pp. 30, 31.

Su derrora en las experiencias homosexuales se debe a que no
le producen ninguna estimulación física, y ello le impide vencer
sus resistencias.

++Antonio Escohotado++, «Introducción» en _Mi vida secreta_,
trad. Antonio Escohotado, Tusquets, Barcelona, 1987, p. 19.

++Juliana González++, _Malestar en la moral_, p. 211.
